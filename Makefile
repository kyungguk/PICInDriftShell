#export CXX=xcrun --sdk macosx clang++ -stdlib=libc++ #mpicxx -cxx="xcrun --sdk macosx clang++"
#export LIBTOOL=xcrun --sdk macosx libtool  -static -o
export CXX=mpicxx -cxx=g++
export LIBTOOL=ar rcs

#export CFLAGS=-O3 -std=c++11 -F/Library/com.kyungguk.Frameworks.localized -I.. -I../../UtilityKit -I../../HDF5Kit -I../../MPIKit -I../../GeometryKit -I../../PICKit #-I../../HybridKit
#export LDFLAGS=-O3 -flto -F/Library/com.kyungguk.Frameworks.localized -framework hdf5 -framework mpich2 -framework json
export CFLAGS=-O3 -std=c++11 -fpermissive -I$(HOME)/local/include -I.. -I../../UtilityKit -I../../HDF5Kit -I../../MPIKit -I../../GeometryKit -I../../PICKit #-I../../HybridKit
export LDFLAGS=-O3 -flto -lz -ldl -lpthread
export JSONCPPLIB=$(HOME)/local/lib/libjsoncpp.a
export HDF5LIB=$(HOME)/local/lib/libhdf5.a $(HOME)/local/lib/libsz.a

all:
	+make -eC ./mpic-dipole_field/mpic-dipole_field all

clean:
	@make -eC ./mpic-dipole_field/mpic-dipole_field clean
