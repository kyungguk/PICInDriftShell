//
//  PICKitTests-Domain__1_1D.mm
//  PICKitTests
//
//  Created by KYUNGGUK MIN on 2/10/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include "PICKitTests-Domain__1_1D.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <chrono>
#include <cmath>

@interface PICKitTests_Domain__1_1D : XCTestCase

@end

@implementation PICKitTests_Domain__1_1D

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testSingleParticleTracing {
    std::ostringstream os;
    os.precision(20);

    constexpr double dt = 0.01, O0ref = 2*M_PI, r0ref = 30, dip_type = 0;
    constexpr bool use_metric_table = true;
    constexpr unsigned number_of_worker_threads = 0;
    constexpr unsigned shape_order = 1, n_smooth = 1;
    TestDelegate<ND>::boundary_type const bc = {TestDelegate<ND>::periodic};
    ParameterSet<ND>::size_vector_type const N = {11};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {.9};
    double const c = D.x/dt;

    constexpr bool enable = false;

    TracingDelegate delegate{bc};
    std::unique_ptr<DelegateWrapper> wrapper;
    try {
        CoordSystem const cs{r0ref, D, dip_type};
        ParameterSet<ND>::OptionSet opts; {
            opts.use_metric_table = use_metric_table;
            opts.number_of_worker_threads = number_of_worker_threads;
        }
        ParameterSet<ND> const params{cs, q_min, N, c, O0ref, opts};
        wrapper.reset(new DelegateWrapper{params, &delegate, dt, {}});

        // add species
        //
        for (Domain<ND> &dom : wrapper->domains()) {
            Species<ND> &sp = dom.add_species();
            sp.set_properties(O0ref, c, 1, ShapeOrder{shape_order}, NSmooth{n_smooth}); {
                Species<ND>::position_type pos;
                Species<ND>::velocity_type vel;

                vel.x = 1;
                vel.y = .9;
                vel.z = 0;
                pos.x = 0.5*(params.ptl_q_max().x - params.ptl_q_min().x) + params.ptl_q_min().x;
                sp.bucket().push_back({vel, pos});
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
        return;
    }

    try {
        for (Domain<ND> &dom : wrapper->domains()) {
            XCTAssert(dom.params().use_metric_table() == use_metric_table);
            XCTAssert(dom.params().number_of_worker_threads() == number_of_worker_threads);
            for (auto const &sp : dom.species()) {
                XCTAssert(sp.shape_order() == shape_order);
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
            for (auto const &sp : dom.cold_fluids()) {
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( enable ) {
        Domain<ND> &dom = wrapper->domain();

        println(os, "r0ref = ", dom.params().coord_system().r0ref());
        println(os, "gridTypeID = ", dom.params().coord_system().n());
        println(os, "\\[CapitalDelta] = ", dom.params().coord_system().D());
        println(os, "qmin = ", dom.params().grid_q_min());
        println(os, "qmax = ", dom.params().grid_q_max()) << '\n';

        constexpr long n = 2000;
        try {
            print(os, "ptl = {\n\t") << dom.species().at(0).bucket();
            //
            wrapper->run([&os](Domain<ND> &dom, const bool is_master) {
                for (int i = 1; i < n; ++i) {
                    dom.advance_by(1);

                    if (is_master) {
                        if (i % 1 == 0) {
                            print(os, ",\n\t") << dom.species().at(0).bucket();
                        }
                    }
                }
            });
            //
            print(os, "\n}");
        } catch (std::exception &e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
            contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            NSError *error;
            XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"particle.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    }
}

- (void)testMultipleParticleTracing {
    std::ostringstream os;
    os.precision(20);

    constexpr double dt = 0.005, O0ref = 2*M_PI, r0ref = 30, dip_type = -1;
    constexpr bool use_metric_table = true;
    constexpr unsigned number_of_worker_threads = 0;
    constexpr unsigned shape_order = 1, n_smooth = 1;
    TestDelegate<ND>::boundary_type const bc = {TestDelegate<ND>::periodic};
    ParameterSet<ND>::size_vector_type const N = {11};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {.9};
    constexpr long number_of_particles = 100;
    double const c = D.x/dt;
    static_assert(number_of_particles % (number_of_worker_threads + 1) == 0, "number_of_particles is not divisable by N threads");

    constexpr bool enable = false;

    TracingDelegate delegate{bc};
    std::unique_ptr<DelegateWrapper> wrapper;
    try {
        CoordSystem const cs{r0ref, D, dip_type};
        ParameterSet<ND>::OptionSet opts; {
            opts.use_metric_table = use_metric_table;
            opts.number_of_worker_threads = number_of_worker_threads;
        }
        ParameterSet<ND> const params{cs, q_min, N, c, O0ref, opts};
        wrapper.reset(new DelegateWrapper{params, &delegate, dt, {}});

        // add species
        //
        for (Domain<ND> &dom : wrapper->domains()) {
            Species<ND> &sp = dom.add_species();
            constexpr long Nc = number_of_particles / (number_of_worker_threads + 1);
            sp.set_properties(O0ref, c, Nc, ShapeOrder{shape_order}, NSmooth{n_smooth}); {
                RandomRealPool &rng = RandomRealPool::rng();
                Species<ND>::position_type pos;
                Species<ND>::velocity_type vel;
                // vx-dist:
                UTL::UniformDistribution const dist_vx{-1, 1};
                // vy-dist:
                UTL::UniformDistribution const dist_vy{-1, 1};
                // vz-dist:
                UTL::UniformDistribution const dist_vz{-1, 1};
                // x-dist:
                UTL::UniformDistribution const dist_x{params.ptl_q_min().x, params.ptl_q_max().x};
                for (long i = 0; i < Nc; ++i) {
                    vel.x = dist_vx.icdf(rng.v1());
                    vel.y = dist_vy.icdf(rng.v2());
                    vel.z = dist_vz.icdf(rng.alpha());
                    pos.x = dist_x.icdf(rng.q1());
                    sp.bucket().push_back({vel, pos});
                }
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
        return;
    }

    try {
        for (Domain<ND> &dom : wrapper->domains()) {
            XCTAssert(dom.params().use_metric_table() == use_metric_table);
            XCTAssert(dom.params().number_of_worker_threads() == number_of_worker_threads);
            for (auto const &sp : dom.species()) {
                XCTAssert(sp.shape_order() == shape_order);
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
            for (auto const &sp : dom.cold_fluids()) {
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( enable ) {
        Domain<ND> &dom = wrapper->domain();

        println(os, "r0ref = ", dom.params().coord_system().r0ref());
        println(os, "gridTypeID = ", dom.params().coord_system().n());
        println(os, "\\[CapitalDelta] = ", dom.params().coord_system().D());
        println(os, "qmin = ", dom.params().grid_q_min());
        println(os, "qmax = ", dom.params().grid_q_max()) << '\n';

        constexpr long outer_steps = 1000, inner_steps = 2;
        try {
            print(os, "ptl = {\n\t") << dom.species().at(0).bucket();
            for (long i = 1; i <= number_of_worker_threads; ++i) {
                print(os, " ~ Join ~ ") << wrapper->domains().at(i).species().at(0).bucket();
            }
            //
            std::pair<std::atomic<long>, std::atomic<long>> barrier{0, 0};
            wrapper->run([&os, &barrier, &wrapper](Domain<ND> &dom, const bool is_master) {
                for (int i_step = 1; enable && i_step < outer_steps; ++i_step) {
                    dom.advance_by(inner_steps);

                    if (is_master) {
                        println(std::cout, "i_step = ", i_step);

                        // print out master's
                        //
                        print(os, ",\n\t") << dom.species().at(0).bucket();

                        // print out workers'
                        //
                        for (long i = 1; i <= number_of_worker_threads; ++i)
                        {
                            // wait for workers' arrival
                            //
                            do {} while (!barrier.first.load());
                            --barrier.first;

                            // print worker's
                            //
                            print(os, " ~ Join ~ ") << wrapper->domains().at(i).species().at(0).bucket();
                        }

                        // notify master's departure
                        //
                        barrier.second.store(number_of_worker_threads);
                    } else {
                        ++barrier.first; // notify arrival of worker
                        do {} while (!barrier.second.load()); // wait for master
                        --barrier.second;
                    }
                }
            });
            //
            print(os, "\n}");
        } catch (std::exception &e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
            contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

            NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
            NSError *error;
            XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"particle.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    }
}

- (void)testMomentUniformity_BiMaxwellian {
    std::ostringstream os;
    os.precision(20);

    constexpr double dt = 0.01, O0ref = 2*M_PI, r0ref = 30, dip_type = -1;
    constexpr bool use_metric_table = true;
    constexpr unsigned number_of_worker_threads = 0;
    constexpr unsigned shape_order = 1, n_smooth = 1;
    TestDelegate<ND>::boundary_type const bc = {TestDelegate<ND>::periodic};
    ParameterSet<ND>::size_vector_type const N = {90};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {.9};
    double const c = D.x/dt;
    constexpr unsigned Nc = 500;
    double const Oc = O0ref, op = c*Oc, beta1_0 = .1, T2OT1_0 = 10;

    constexpr bool enable = false;

    TracingDelegate delegate{bc};
    std::unique_ptr<DelegateWrapper> wrapper;
    try {
        CoordSystem const cs{r0ref, D, dip_type};
        ParameterSet<ND>::OptionSet opts; {
            opts.use_metric_table = use_metric_table;
            opts.number_of_worker_threads = number_of_worker_threads;
        }
        ParameterSet<ND> const params{cs, q_min, N, c, O0ref, opts};
        wrapper.reset(new DelegateWrapper{params, &delegate, dt, {}});

        // add species
        //
        BiMaxwellianLoader<ND>::param_type bimax; {
            bimax.vdf = BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1_0)*c * std::abs(Oc)/op, T2OT1_0 - 1};
        }
        BiMaxwellianLoader<ND> const loader{cs, params.grid_q_min(), params.N(), std::move(bimax)};
        for (Domain<ND> &dom : wrapper->domains()) {
            dom.add_species().load(Oc, op, Nc, ShapeOrder{shape_order}, NSmooth{n_smooth}, loader);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        for (Domain<ND> &dom : wrapper->domains()) {
            XCTAssert(dom.params().use_metric_table() == use_metric_table);
            XCTAssert(dom.params().number_of_worker_threads() == number_of_worker_threads);
            for (auto const &sp : dom.species()) {
                XCTAssert(sp.shape_order() == shape_order);
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
            for (auto const &sp : dom.cold_fluids()) {
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // debug particle distributions
    //
    if ( (0) ) {
        os.str("");
        try {
            Domain<ND> &dom = wrapper->domain();

            println(os, "r0ref = ", dom.params().coord_system().r0ref());
            println(os, "gridTypeID = ", dom.params().coord_system().n());
            println(os, "\\[CapitalDelta] = ", dom.params().coord_system().D());
            println(os, "qmin = ", dom.params().grid_q_min());
            println(os, "qmax = ", dom.params().grid_q_max()) << '\n';
            println(os, "Nc = ", Nc);
            println(os, "beta1 = ", beta1_0);
            println(os, "T2OT1 = ", T2OT1_0) << '\n';

            print(os, "species[0] = ") << dom.species().at(0).bucket();
            for (long i = 1; i <= number_of_worker_threads; ++i) {
                print(os, " ~ Join ~ ") << wrapper->domains().at(i).species().at(0).bucket();
            }
            print(os, "\n\n");
        } catch (std::exception& e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *content = @(os.str().c_str());
            content = [content stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            content = [content stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
            NSURL *url = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"debugSpecies.m"]];
            NSError *error;
            XCTAssert([content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
        return;
    }

    // advance
    //
    if ( enable ) {
        os.str("");
        try {
            Domain<ND> &dom = wrapper->domain();

            println(os, "r0ref = ", dom.params().coord_system().r0ref());
            println(os, "gridTypeID = ", dom.params().coord_system().n());
            println(os, "\\[CapitalDelta] = ", dom.params().coord_system().D());
            println(os, "qmin = ", dom.params().grid_q_min());
            println(os, "qmax = ", dom.params().grid_q_max());
            println(os, "beta1 = ", beta1_0);
            println(os, "A0 = ", T2OT1_0 - 1) << '\n';

            println(os, "$n = List[]");
            println(os, "$V = List[]");
            println(os, "$vv = List[]") << '\n';

            constexpr long outer_steps = 100, inner_steps = 100;
            wrapper->run([&os](Domain<ND> &dom, const bool is_master) {
                for (long i_step = 1; i_step <= outer_steps; ++i_step) {
                    dom.advance_by(inner_steps);

                    if (is_master) {
                        println(std::cout, "i_step = ", i_step);

                        Species<ND>::size_vector_type const begs{0};
                        Species<ND> &sp = dom.species().at(0);
                        auto &n = sp.moment<0>();
                        println(os, "$n = $n ~ Append ~ ", UTL::make_slice<0>(n, begs, n.dims()));
                        auto &V = sp.moment<1>();
                        println(os, "$V = $V ~ Append ~ ", UTL::make_slice<0>(V, begs, V.dims()));
                        auto &vv = sp.moment<2>();
                        println(os, "$vv = $vv ~ Append ~ ", UTL::make_slice<0>(vv, begs, vv.dims())) << '\n';
                    }
                }
            });
        } catch (std::exception &e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *content = @(os.str().c_str());
            content = [content stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            content = [content stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
            NSURL *url = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"moment_uniform-bi_maxwellian.m"]];
            NSError *error;
            XCTAssert([content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    }
}

- (void)testMomentUniformity_PartialShell {
    std::ostringstream os;
    os.precision(20);

    constexpr double dt = 0.01, O0ref = 2*M_PI, r0ref = 30, dip_type = -1;
    constexpr bool use_metric_table = true;
    constexpr unsigned number_of_worker_threads = 0;
    constexpr unsigned shape_order = 1, n_smooth = 1;
    TestDelegate<ND>::boundary_type const bc = {TestDelegate<ND>::periodic};
    ParameterSet<ND>::size_vector_type const N = {90};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {.9};
    double const c = D.x/dt;
    constexpr unsigned Nc = 500;
    double const Oc = O0ref, op = c*Oc, beta = .2, zeta = 5, vs = 2;

    constexpr bool enable = false;

    TracingDelegate delegate{bc};
    std::unique_ptr<DelegateWrapper> wrapper;
    try {
        CoordSystem const cs{r0ref, D, dip_type};
        ParameterSet<ND>::OptionSet opts; {
            opts.use_metric_table = use_metric_table;
            opts.number_of_worker_threads = number_of_worker_threads;
        }
        ParameterSet<ND> const params{cs, q_min, N, c, O0ref, opts};
        wrapper.reset(new DelegateWrapper{params, &delegate, dt, {}});

        // add species
        //
        PartialShellLoader<ND>::param_type shell; {
            shell.vdf = PartialShellLoader<ND>::vdf_type{std::sqrt(beta)*c * std::abs(Oc)/op, vs, zeta};
        }
        PartialShellLoader<ND> const loader{cs, params.grid_q_min(), params.N(), std::move(shell)};
        for (Domain<ND> &dom : wrapper->domains()) {
            dom.add_species().load(Oc, op, Nc, ShapeOrder{shape_order}, NSmooth{n_smooth}, loader);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        for (Domain<ND> &dom : wrapper->domains()) {
            XCTAssert(dom.params().use_metric_table() == use_metric_table);
            XCTAssert(dom.params().number_of_worker_threads() == number_of_worker_threads);
            for (auto const &sp : dom.species()) {
                XCTAssert(sp.shape_order() == shape_order);
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
            for (auto const &sp : dom.cold_fluids()) {
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // debug particle distributions
    //
    if ( (0) ) {
        os.str("");
        try {
            Domain<ND> &dom = wrapper->domain();

            println(os, "r0ref = ", dom.params().coord_system().r0ref());
            println(os, "gridTypeID = ", dom.params().coord_system().n());
            println(os, "\\[CapitalDelta] = ", dom.params().coord_system().D());
            println(os, "qmin = ", dom.params().grid_q_min());
            println(os, "qmax = ", dom.params().grid_q_max()) << '\n';
            println(os, "Nc = ", Nc);
            println(os, "beta = ", beta);
            println(os, "vs = ", vs);
            println(os, "sinIdx = ", zeta) << '\n';

            print(os, "species[0] = ") << dom.species().at(0).bucket();
            for (long i = 1; i <= number_of_worker_threads; ++i) {
                print(os, " ~ Join ~ ") << wrapper->domains().at(i).species().at(0).bucket();
            }
            print(os, "\n\n");
        } catch (std::exception& e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *content = @(os.str().c_str());
            content = [content stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            content = [content stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
            NSURL *url = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"debugSpecies.m"]];
            NSError *error;
            XCTAssert([content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
        return;
    }

    // advance
    //
    if ( enable ) {
        os.str("");
        try {
            Domain<ND> &dom = wrapper->domain();

            println(os, "r0ref = ", dom.params().coord_system().r0ref());
            println(os, "gridTypeID = ", dom.params().coord_system().n());
            println(os, "\\[CapitalDelta] = ", dom.params().coord_system().D());
            println(os, "qmin = ", dom.params().grid_q_min());
            println(os, "qmax = ", dom.params().grid_q_max());
            println(os, "beta = ", beta);
            println(os, "vs = ", vs);
            println(os, "zeta = ", zeta) << '\n';

            println(os, "$n = List[]");
            println(os, "$V = List[]");
            println(os, "$vv = List[]") << '\n';

            constexpr long outer_steps = 100, inner_steps = 100;
            wrapper->run([&os](Domain<ND> &dom, const bool is_master) {
                for (long i_step = 1; i_step <= outer_steps; ++i_step) {
                    dom.advance_by(inner_steps);

                    if (is_master) {
                        println(std::cout, "i_step = ", i_step);

                        Species<ND>::size_vector_type const begs{0};
                        Species<ND> &sp = dom.species().at(0);
                        auto &n = sp.moment<0>();
                        println(os, "$n = $n ~ Append ~ ", UTL::make_slice<0>(n, begs, n.dims()));
                        auto &V = sp.moment<1>();
                        println(os, "$V = $V ~ Append ~ ", UTL::make_slice<0>(V, begs, V.dims()));
                        auto &vv = sp.moment<2>();
                        println(os, "$vv = $vv ~ Append ~ ", UTL::make_slice<0>(vv, begs, vv.dims())) << '\n';
                    }
                }
            });
        } catch (std::exception &e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *content = @(os.str().c_str());
            content = [content stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            content = [content stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
            NSURL *url = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"moment_uniform-partial_shell.m"]];
            NSError *error;
            XCTAssert([content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    }
}

- (void)testDrivenLightWave {
    std::ostringstream os;
    os.precision(12);

    constexpr double c = 1, A_J = 1, o_J = 1, O0ref = 1, dt = .01, r0ref = 100*1, grid_type = 0;
    constexpr bool use_metric_table = true;
    constexpr unsigned number_of_worker_threads = 0;
    constexpr unsigned shape_order = 1, n_smooth = 1;
    TestDelegate<ND>::boundary_type const bc = {TestDelegate<ND>::periodic};
    ParameterSet<ND>::size_vector_type const N = {500*2 + 1};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {.02};
    ParameterSet<ND>::size_vector_type const q_damp = {50};
    ParameterSet<ND>::position_type const r_r = {1}, r_d = {.2};

    constexpr bool enable = false;

    SourceDelegate delegate{bc};
    std::unique_ptr<Source<ND>> src;
    std::unique_ptr<DelegateWrapper> wrapper;
    try {
        // init domain
        //
        CoordSystem const cs{r0ref, D, grid_type};
        ParameterSet<ND>::OptionSet opts; {
            opts.use_metric_table = use_metric_table;
            opts.number_of_worker_threads = number_of_worker_threads;
        }
        ParameterSet<ND> const params{cs, q_min, N, c, O0ref, opts};
        WaveDamper<ND> damper{q_damp, r_d, r_r};
        damper.set_boundary_mask(~damper.none);
        src.reset(new Source<ND>{params});
        delegate.src_ptr = src.get();
        wrapper.reset(new DelegateWrapper{params, &delegate, dt, damper});

        // source
        //
        Source<ND>::Descriptor desc; {
            desc.location = {0.};
            desc.J0carts.z = Source<ND>::complex_type{0, 1}*A_J;
            desc.omega = o_J;
            desc.start = 0;
            desc.duration = 2*M_PI/o_J;
            desc.ease_in = 0;
        }
        desc.J0carts /= 2.;
        src->add(desc);
        src->add(desc);
        XCTAssert(src->descriptors().size() == 2);
        if (src->descriptors().size() != 2) {
            return;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
        return;
    }

    try {
        for (Domain<ND> &dom : wrapper->domains()) {
            XCTAssert(dom.params().use_metric_table() == use_metric_table);
            XCTAssert(dom.params().number_of_worker_threads() == number_of_worker_threads);
            for (auto const &sp : dom.species()) {
                XCTAssert(sp.shape_order() == shape_order);
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
            for (auto const &sp : dom.cold_fluids()) {
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // advance
    //
    if ( enable ) {
        constexpr long outer_steps = 100, inner_steps = 20;
        try {
            println(os, "$B = List[]");
            println(os, "$E = List[]") << '\n';

            wrapper->run([&os](Domain<ND> &dom, const bool is_master) {
                auto &B = dom.bfield()->carts;
                auto &E = dom.efield()->carts;
                for (long i_step = 1; i_step <= outer_steps; ++i_step) {
                    dom.advance_by(inner_steps);

                    if (is_master) {
                        printo(std::cout, "i_step = ", i_step, "\n");

                        Species<ND>::size_vector_type const begs{0};
                        println(os, "$B = $B ~ Append ~ ", UTL::make_slice<0>(B, begs, B.dims()));
                        println(os, "$E = $E ~ Append ~ ", UTL::make_slice<0>(E, begs, E.dims())) << '\n';
                    }
                }
            });
        } catch (std::exception &e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *content = @(os.str().c_str());
            content = [content stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            content = [content stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
            NSURL *url = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"driven_wave.m"]];
            NSError *error;
            XCTAssert([content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    }
}

- (void)testDrivenXmode {
    std::ostringstream os;
    os.precision(12);

    constexpr double c = 1, A_J = 1, o_J = 1, O0ref = 1, dt = .01, r0ref = 40*1, grid_type = 0;
    constexpr bool is_cold = false;
    constexpr unsigned number_of_worker_threads = 0;
    constexpr unsigned shape_order = 1, n_smooth = 1;
    TestDelegate<ND>::boundary_type const bc = {TestDelegate<ND>::periodic};
    ParameterSet<ND>::size_vector_type const N = {500*2 + 1};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {.02};
    ParameterSet<ND>::size_vector_type const q_damp = {0}; // 50
    ParameterSet<ND>::position_type const r_r = {1}, r_d = {.2};

    constexpr unsigned Nc = 100;
    double const Oc = -8*o_J, op = 2*o_J, beta1 = .0001, T2OT1 = 1;

    constexpr bool enable = false;

    SourceDelegate delegate{bc};
    std::unique_ptr<Source<ND>> src;
    std::unique_ptr<DelegateWrapper> wrapper;
    try {
        // init domain
        //
        CoordSystem const cs{r0ref, D, grid_type};
        ParameterSet<ND>::OptionSet opts; {
            opts.use_metric_table = true;
            opts.enforce_force_balance = true;
            opts.number_of_worker_threads = number_of_worker_threads;
        }
        ParameterSet<ND> const params{cs, q_min, N, c, O0ref, opts};
        WaveDamper<ND> damper{q_damp, r_d, r_r};
        damper.set_boundary_mask(~damper.none);
        src.reset(new Source<ND>{params});
        delegate.src_ptr = src.get();
        wrapper.reset(new DelegateWrapper{params, &delegate, dt, damper});

        // source
        //
        Source<ND>::Descriptor desc; {
            desc.location = {0.};
            desc.J0carts.z = Source<ND>::complex_type{0, 1}*A_J;
            desc.omega = o_J;
            desc.start = 0;
            desc.duration = 2*M_PI/o_J;
            desc.ease_in = 0;
        }
        desc.J0carts /= 2.;
        src->add(desc);
        src->add(desc);
        XCTAssert(src->descriptors().size() == 2);
        if (src->descriptors().size() != 2) {
            return;
        }

        // init species
        //
        if (is_cold) {
            for (Domain<ND> &dom : wrapper->domains()) {
                dom.add_cold_fluid().load(Oc, op, NSmooth{n_smooth});
            }
        } else {
            BiMaxwellianLoader<ND>::param_type bimax; {
                bimax.vdf = BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1)*c * std::abs(Oc)/op, T2OT1 - 1};
            }
            BiMaxwellianLoader<ND> const loader{cs, params.grid_q_min(), params.N(), std::move(bimax)};
            for (Domain<ND> &dom : wrapper->domains()) {
                dom.add_species().load(Oc, op, Nc, ShapeOrder{shape_order}, NSmooth{n_smooth}, loader);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
        return;
    }

    try {
        for (Domain<ND> &dom : wrapper->domains()) {
            XCTAssert(dom.params().use_metric_table() == true);
            XCTAssert(dom.params().enforce_force_balance() == true);
            XCTAssert(dom.params().number_of_worker_threads() == number_of_worker_threads);
            for (auto const &sp : dom.species()) {
                XCTAssert(sp.shape_order() == shape_order);
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
            for (auto const &sp : dom.cold_fluids()) {
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // advance
    //
    if ( enable ) {
        constexpr long outer_steps = 100, inner_steps = 30;
        try {
            println(os, "$B = List[]");
            println(os, "$E = List[]") << '\n';

            wrapper->run([&os](Domain<ND> &dom, const bool is_master) {
                auto &B = dom.bfield()->carts;
                auto &E = dom.efield()->carts;
                for (long i_step = 1; i_step <= outer_steps; ++i_step) {
                    dom.advance_by(inner_steps);

                    if (is_master) {
                        printo(std::cout, "i_step = ", i_step, "\n");

                        Species<ND>::size_vector_type const begs{0};
                        println(os, "$B = $B ~ Append ~ ", UTL::make_slice<0>(B, begs, B.dims()));
                        println(os, "$E = $E ~ Append ~ ", UTL::make_slice<0>(E, begs, E.dims())) << '\n';
                    }
                }
            });
        } catch (std::exception &e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *content = @(os.str().c_str());
            content = [content stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            content = [content stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
            NSURL *url = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"driven_wave.m"]];
            NSError *error;
            XCTAssert([content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    }
}

- (void)testMagnetosonicSource {
    std::ostringstream os;
    os.precision(12);

    constexpr double c = 30, dt = 0.001, O0ref = 1, mpOme = 100, r0ref = 40*1, grid_type = 0;
    constexpr bool is_cold = false;
    constexpr unsigned number_of_worker_threads = 0;
    constexpr unsigned shape_order = 1, n_smooth = 1;
    TestDelegate<ND>::boundary_type const bc = {TestDelegate<ND>::periodic};
    ParameterSet<ND>::size_vector_type const N = {528};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {0.05};
    ParameterSet<ND>::size_vector_type const q_damp = {0}; // 20
    ParameterSet<ND>::position_type const r_r = {1}, r_d = {.2};
    constexpr unsigned Nc = 100;

    constexpr bool enable = false;

    SourceDelegate delegate{bc};
    std::unique_ptr<Source<ND>> src;
    std::unique_ptr<DelegateWrapper> wrapper;
    try {
        // init domain
        //
        CoordSystem const cs{r0ref, D, grid_type};
        ParameterSet<ND>::OptionSet opts; {
            opts.use_metric_table = true;
            opts.enforce_force_balance = true;
            opts.number_of_worker_threads = number_of_worker_threads;
        }
        ParameterSet<ND> const params{cs, q_min, N, c, O0ref, opts};
        WaveDamper<ND> damper{q_damp, r_d, r_r};
        damper.set_boundary_mask(~damper.none);
        src.reset(new Source<ND>{params});
        delegate.src_ptr = src.get();
        wrapper.reset(new DelegateWrapper{params, &delegate, dt, damper});

        // source
        //
        Source<ND>::Descriptor desc; {
            desc.location = {-50.};
            desc.J0carts.x = Source<ND>::complex_type{0, 0.0951974};
            desc.J0carts.z = Source<ND>::complex_type{15.3787, 0};
            desc.omega = 4.5*O0ref;
            desc.start = 2;
            desc.duration = 45;
            desc.ease_in = 3;
        }
        desc.J0carts /= 2.;
        src->add(desc);
        src->add(desc);
        XCTAssert(src->descriptors().size() == 2);
        if (src->descriptors().size() != 2) {
            return;
        }

        // init species
        //
        if (is_cold) {
            { // protons
                double const Oc = O0ref, op = c*O0ref;
                for (Domain<ND> &dom : wrapper->domains()) {
                    dom.add_cold_fluid().load(Oc, op, NSmooth{n_smooth});
                }
            }
            { // electrons
                double const Oc = -mpOme*O0ref, op = c*O0ref*std::sqrt(mpOme);
                for (Domain<ND> &dom : wrapper->domains()) {
                    dom.add_cold_fluid().load(Oc, op, NSmooth{n_smooth});
                }
            }
        } else {
            { // protons
                double const Oc = O0ref, op = c*O0ref, beta1 = 0.001, T2OT1 = 1;
                BiMaxwellianLoader<ND>::param_type bimax; {
                    bimax.vdf = BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1)*c * std::abs(Oc)/op, T2OT1 - 1};
                }
                BiMaxwellianLoader<ND> const loader{cs, params.grid_q_min(), params.N(), std::move(bimax)};
                for (Domain<ND> &dom : wrapper->domains()) {
                    dom.add_species().load(Oc, op, Nc, ShapeOrder{shape_order}, NSmooth{n_smooth}, loader);
                }
            }
            { // electrons
                double const Oc = -mpOme*O0ref, op = c*O0ref*std::sqrt(mpOme), beta1 = 0.001, T2OT1 = 1;
                BiMaxwellianLoader<ND>::param_type bimax; {
                    bimax.vdf = BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1)*c * std::abs(Oc)/op, T2OT1 - 1};
                }
                BiMaxwellianLoader<ND> const loader{cs, params.grid_q_min(), params.N(), std::move(bimax)};
                for (Domain<ND> &dom : wrapper->domains()) {
                    dom.add_species().load(Oc, op, Nc, ShapeOrder{shape_order}, NSmooth{n_smooth}, loader);
                }
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
        return;
    }

    try {
        for (Domain<ND> &dom : wrapper->domains()) {
            XCTAssert(dom.params().use_metric_table() == true);
            XCTAssert(dom.params().enforce_force_balance() == true);
            XCTAssert(dom.params().number_of_worker_threads() == number_of_worker_threads);
            for (auto const &sp : dom.species()) {
                XCTAssert(sp.shape_order() == shape_order);
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
            for (auto const &sp : dom.cold_fluids()) {
                XCTAssert(sp.number_of_smoothing_passes() == n_smooth);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // advance
    //
    if ( enable ) {
        constexpr long outer_steps = 200, inner_steps = 150;
        try {
            println(os, "$B = List[]");
            println(os, "$E = List[]");
            println(os, "$n[0] = $n[1] = List[]");
            println(os, "$V[0] = $V[1] = List[]");
            println(os, "$vv[0] = $vv[1] = List[]") << '\n';

            wrapper->run([&os](Domain<ND> &dom, const bool is_master) {
                auto &B = dom.bfield()->carts;
                auto &E = dom.efield()->carts;
                for (long i_step = 1; i_step <= outer_steps; ++i_step) {
                    dom.advance_by(inner_steps);

                    if (is_master) {
                        printo(std::cout, "i_step = ", i_step, "\n");

                        Species<ND>::size_vector_type const begs{0};
                        println(os, "$B = $B ~ Append ~ ", UTL::make_slice<0>(B, begs, B.dims()));
                        println(os, "$E = $E ~ Append ~ ", UTL::make_slice<0>(E, begs, E.dims()));
                        if (is_cold) {
                            auto &n0 = dom.cold_fluids().at(0).moment<0>();
                            auto &n1 = dom.cold_fluids().at(1).moment<0>();
                            auto &V0 = dom.cold_fluids().at(0).moment<1>();
                            auto &V1 = dom.cold_fluids().at(1).moment<1>();
                            auto &vv0 = dom.cold_fluids().at(0).moment<2>();
                            auto &vv1 = dom.cold_fluids().at(1).moment<2>();
                            // sp[0]
                            println(os, "$n[0] = $n[0] ~ Append ~ ", UTL::make_slice<0>(n0, begs, n0.dims()));
                            println(os, "$V[0] = $V[0] ~ Append ~ ", UTL::make_slice<0>(V0, begs, V0.dims()));
                            println(os, "$vv[0] = $vv[0] ~ Append ~ ", UTL::make_slice<0>(vv0, begs, vv0.dims()));
                            // sp[1]
                            println(os, "$n[1] = $n[1] ~ Append ~ ", UTL::make_slice<0>(n1, begs, n1.dims()));
                            println(os, "$V[1] = $V[1] ~ Append ~ ", UTL::make_slice<0>(V1, begs, V1.dims()));
                            println(os, "$vv[1] = $vv[1] ~ Append ~ ", UTL::make_slice<0>(vv1, begs, vv1.dims()));
                        } else {
                            auto &n0 = dom.species().at(0).moment<0>();
                            auto &n1 = dom.species().at(1).moment<0>();
                            auto &V0 = dom.species().at(0).moment<1>();
                            auto &V1 = dom.species().at(1).moment<1>();
                            auto &vv0 = dom.species().at(0).moment<2>();
                            auto &vv1 = dom.species().at(1).moment<2>();
                            // sp[0]
                            println(os, "$n[0] = $n[0] ~ Append ~ ", UTL::make_slice<0>(n0, begs, n0.dims()));
                            println(os, "$V[0] = $V[0] ~ Append ~ ", UTL::make_slice<0>(V0, begs, V0.dims()));
                            println(os, "$vv[0] = $vv[0] ~ Append ~ ", UTL::make_slice<0>(vv0, begs, vv0.dims()));
                            // sp[1]
                            println(os, "$n[1] = $n[1] ~ Append ~ ", UTL::make_slice<0>(n1, begs, n1.dims()));
                            println(os, "$V[1] = $V[1] ~ Append ~ ", UTL::make_slice<0>(V1, begs, V1.dims()));
                            println(os, "$vv[1] = $vv[1] ~ Append ~ ", UTL::make_slice<0>(vv1, begs, vv1.dims()));
                        }
                    }
                }
            });
        } catch (std::exception &e) {
            XCTAssert(false, @"c++ exception: %s", e.what());
            return;
        }

        @autoreleasepool {
            NSString *content = @(os.str().c_str());
            content = [content stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
            content = [content stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
            NSURL *url = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"self_consistency.m"]];
            NSError *error;
            XCTAssert([content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    }
}

@end
