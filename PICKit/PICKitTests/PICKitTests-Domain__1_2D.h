//
//  PICKitTests-Domain__1_2D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 2/11/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#include <PICKit/PICKit.h>
#include "PICDelegate__2D.h"
//#include "PICMasterDelegate__2D.h"
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <functional>
#include <ostream>
#include <future>
#include <atomic>
#include <unordered_map>

constexpr long ND = 2;
using namespace PIC::__1_;
using GEO::__1_::RandomRealPool;
using GEO::__1_::BiMaxwellianLoader;
using GEO::__1_::PartialShellLoader;

namespace {
    class TracingDelegate : public TestDelegate<ND> {
    public:
        using TestDelegate::TestDelegate;

        void pass(domain_type const*, bfield_type &bfield) override {
            bfield->contr.fill(bfield_type::vector_type{0});
        }
        void pass(domain_type const*, efield_type &efield) override {
            efield->contr.fill(efield_type::vector_type{0});
        }
    };

    class SourceDelegate : public TestDelegate<ND> {
        using Delegate = TestDelegate<ND>;
    public:
        explicit SourceDelegate(boundary_type const bc) : Delegate{bc} {
            lock.clear();
        }

        bool will_do_next_cycle(domain_type const* domain, long const i_step) override {
            get_time(domain).second += domain->dt();
            return Delegate::will_do_next_cycle(domain, i_step);
        }
        void gather(domain_type const* domain, current_type &current) override {
            auto &t = get_time(domain);
            if (t.first != t.second) {
                src_ptr->update(t.first = t.second);
                current += *src_ptr;
            }
            Delegate::gather(domain, current);
        }

    private:
        std::atomic_flag lock;
        std::unordered_map<void const*, std::pair<double, double>> time_table;
        std::pair<double, double>& get_time(void const* key) {
            std::pair<double, double> *ptr;
            while (lock.test_and_set(std::memory_order_acquire)) continue;
            ptr = &time_table[key];
            lock.clear(std::memory_order_release);
            return *ptr;
        }
    public:
        Source<ND> *src_ptr;
    };

    class DelegateWrapper : public MasterDelegate<ND> {
        UTL::StaticArray<Domain<ND>, 64> _domains{};
    public:
        explicit DelegateWrapper(ParameterSet<ND> const &params, Delegate<ND> *const delegate, double const dt, WaveDamper<ND> const &damper)
        : MasterDelegate{params.number_of_worker_threads(), delegate} {
            _domains.emplace_back(params, dt, damper).set_delegate(this); // master domain
            for (auto &worker : this->workers()) {
                _domains.emplace_back(params, dt, damper).set_delegate(&worker); // worker domain
            }
        }
        //
        UTL::Array<Domain<ND>> const &domains() const noexcept { return _domains; }
        UTL::Array<Domain<ND>>       &domains()       noexcept { return _domains; }
        //
        Domain<ND> const &domain() const { return domains()[0]; }
        Domain<ND>       &domain()       { return domains()[0]; }
        //
        void run(std::function<void(Domain<ND>&, bool const)> loop) {
            UTL::DynamicArray<std::future<void>> futures;
            for (Domain<ND> &dom : domains()) {
                std::future<void> f =
                std::async(std::launch::async, loop, std::ref(dom), &dom == domains().begin());
                futures.push_back(std::move(f));
            }
            for (std::future<void> &f : futures) {
                f.wait();
            }
        }
    };
}

namespace {
    template <class CharT, class Traits>
    std::basic_ostream<CharT, Traits> &operator<<(std::basic_ostream<CharT, Traits> &os, Species<ND>::bucket_type const &ptls) {
        printo(os, "{");
        if (ptls.size()) {
            printo(os, "{", ptls.front().vel, ", ", ptls.front().pos, "}");
        }
        for (long i = 1, n = ptls.size(); i < n; ++i) {
            printo(os, ", {", ptls[i].vel, ", ", ptls[i].pos, "}");
        }
        return printo(os, "}");
    }
}
