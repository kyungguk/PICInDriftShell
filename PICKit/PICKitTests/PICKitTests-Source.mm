//
//  PICKitTests-Source.mm
//  PICKitTests
//
//  Created by KYUNGGUK MIN on 4/22/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#include <PICKit/PICSource.h>
#include <iostream>
#include <sstream>
#include <cmath>

@interface PICKitTests_Source : XCTestCase

@end

@implementation PICKitTests_Source

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- __1_
- (void)testSource__1_1D {
    using PIC::__1_::Source;
    using PIC::__1_::ParameterSet;
    using PIC::__1_::CoordSystem;
    constexpr long ND = 1;

    std::ostringstream os;
    os.precision(12);

    constexpr double dt = 0.01, O0ref = 2*M_PI, r0ref = 1000000, grid_type = 1;
    ParameterSet<ND>::size_vector_type const N = {5};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {1.1};
    double const c = D.x/dt;

    constexpr bool enable = false;

    try {
        CoordSystem const cs{r0ref, D, grid_type};
        ParameterSet<ND>::OptionSet opts; {
        }
        ParameterSet<ND> const params(cs, q_min, N, c, O0ref, opts);
        Source<ND> source{params};

        Source<ND>::Descriptor desc{}; {
            desc.location = params.grid_q_max() - .5;
            desc.J0carts.x = Source<ND>::complex_type{1.1, 0};
            desc.J0carts.y = Source<ND>::complex_type{0, 1.5};
            desc.J0carts.z = Source<ND>::complex_type{.5, .5};
            desc.omega = 2*M_PI;
            desc.start = 15;
            desc.duration = 5;
            desc.ease_in = 10;
        }
        source.add(desc);
        XCTAssert(source.descriptors().size() == 1); {
            constexpr long i = 0;
            XCTAssert(UTL::reduce_bit_and(source.descriptors()[i].location == desc.location));
            XCTAssert(source.descriptors()[i].J0carts.x.real() == desc.J0carts.x.real() && source.descriptors()[i].J0carts.x.imag() == desc.J0carts.x.imag());
            XCTAssert(source.descriptors()[i].J0carts.y.real() == desc.J0carts.y.real() && source.descriptors()[i].J0carts.y.imag() == desc.J0carts.y.imag());
            XCTAssert(source.descriptors()[i].J0carts.z.real() == desc.J0carts.z.real() && source.descriptors()[i].J0carts.z.imag() == desc.J0carts.z.imag());
            XCTAssert(source.descriptors()[i].omega == desc.omega);
            XCTAssert(source.descriptors()[i].start == desc.start && source.descriptors()[i].duration == desc.duration);
            XCTAssert(source.descriptors()[i].ease_in == desc.ease_in);
            XCTAssert(desc.ease_in == 0.0 || source.descriptors()[i].slope() == M_PI/desc.ease_in);
        }

        desc.location = params.grid_q_max() + 1.;
        source.add(desc);
        XCTAssert(source.descriptors().size() == 1);

        desc = source.descriptors().at(0);
        println(os, "assoc = Association[]");
        println(os, "assoc[\"J0\"] = {",
                "Complex[", desc.J0carts.x.real(), ", ", desc.J0carts.x.imag(), "], ",
                "Complex[", desc.J0carts.y.real(), ", ", desc.J0carts.y.imag(), "], ",
                "Complex[", desc.J0carts.z.real(), ", ", desc.J0carts.z.imag(), "]",
                "}");
        println(os, "assoc[\"\\[Omega]\"] = ", desc.omega);
        println(os, "assoc[\"start\"] = ", desc.start);
        println(os, "assoc[\"duration\"] = ", desc.duration);
        println(os, "assoc[\"easein\"] = ", desc.ease_in);
        println(os, "assoc[\"slope\"] = ", desc.slope());
        println(os, "assoc[\"location\"] = ", desc.location);
        println(os, "assoc[\"qmin\"] = ", params.grid_q_min());
        println(os, "assoc[\"pad\"] = ", source.moment<1>().pad_size()) << std::endl;

        print(os, "assoc[\"J\"] = {");
        UTL::DynamicArray<double> t;
        constexpr long outer_steps = 400*2, inner_steps = 5;
        for (long outer = 0; outer < outer_steps; ++outer) {
            t.push_back(outer*inner_steps + inner_steps);
            t.back() *= dt;
            source.update(t.back());
            if (outer) {
                printo(os, ", ");
            }
            printo(os, "\n", source.moment<1>());
        }
        println(os, "\n}");
        println(os, "assoc[\"t\"] = ", t) << std::endl;

    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (enable) ) {
        NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
        contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
        contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
        contents = [contents stringByReplacingOccurrencesOfString:@"|" withString:@", "];

        NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
        NSError *error;
        XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"source__1D.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

- (void)testSource__1_2D {
    using PIC::__1_::Source;
    using PIC::__1_::ParameterSet;
    using PIC::__1_::CoordSystem;
    constexpr long ND = 2;

    std::ostringstream os;
    os.precision(12);

    constexpr double dt = 0.01, O0ref = 2*M_PI, r0ref = 1000000, grid_type = 1;
    ParameterSet<ND>::size_vector_type const N = {5, 3};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {1.1, .9};
    double const c = D.x/dt;

    constexpr bool enable = false;

    try {
        CoordSystem const cs{r0ref, D, grid_type};
        ParameterSet<ND>::OptionSet opts; {
        }
        ParameterSet<ND> const params(cs, q_min, N, c, O0ref, opts);
        Source<ND> source{params};

        Source<ND>::Descriptor desc{}; {
            desc.location = params.grid_q_max() - .5;
            desc.J0carts.x = Source<ND>::complex_type{1.1, 0};
            desc.J0carts.y = Source<ND>::complex_type{0, 1.5};
            desc.J0carts.z = Source<ND>::complex_type{.5, .5};
            desc.omega = 2*M_PI;
            desc.start = 15;
            desc.duration = 5;
            desc.ease_in = 10;
        }
        source.add(desc);
        XCTAssert(source.descriptors().size() == 1); {
            constexpr long i = 0;
            XCTAssert(UTL::reduce_bit_and(source.descriptors()[i].location == desc.location));
            XCTAssert(source.descriptors()[i].J0carts.x.real() == desc.J0carts.x.real() && source.descriptors()[i].J0carts.x.imag() == desc.J0carts.x.imag());
            XCTAssert(source.descriptors()[i].J0carts.y.real() == desc.J0carts.y.real() && source.descriptors()[i].J0carts.y.imag() == desc.J0carts.y.imag());
            XCTAssert(source.descriptors()[i].J0carts.z.real() == desc.J0carts.z.real() && source.descriptors()[i].J0carts.z.imag() == desc.J0carts.z.imag());
            XCTAssert(source.descriptors()[i].omega == desc.omega);
            XCTAssert(source.descriptors()[i].start == desc.start && source.descriptors()[i].duration == desc.duration);
            XCTAssert(source.descriptors()[i].ease_in == desc.ease_in);
            XCTAssert(desc.ease_in == 0.0 || source.descriptors()[i].slope() == M_PI/desc.ease_in);
        }

        desc.location = params.grid_q_max() + 1.;
        source.add(desc);
        XCTAssert(source.descriptors().size() == 1);

        desc = source.descriptors().at(0);
        println(os, "assoc = Association[]");
        println(os, "assoc[\"J0\"] = {",
               "Complex[", desc.J0carts.x.real(), ", ", desc.J0carts.x.imag(), "], ",
               "Complex[", desc.J0carts.y.real(), ", ", desc.J0carts.y.imag(), "], ",
               "Complex[", desc.J0carts.z.real(), ", ", desc.J0carts.z.imag(), "]",
               "}");
        println(os, "assoc[\"\\[Omega]\"] = ", desc.omega);
        println(os, "assoc[\"start\"] = ", desc.start);
        println(os, "assoc[\"duration\"] = ", desc.duration);
        println(os, "assoc[\"easein\"] = ", desc.ease_in);
        println(os, "assoc[\"slope\"] = ", desc.slope());
        println(os, "assoc[\"location\"] = ", desc.location);
        println(os, "assoc[\"qmin\"] = ", params.grid_q_min());
        println(os, "assoc[\"pad\"] = ", source.moment<1>().pad_size()) << std::endl;

        print(os, "assoc[\"J\"] = {");
        UTL::DynamicArray<double> t;
        constexpr long outer_steps = 400*2, inner_steps = 5;
        for (long outer = 0; outer < outer_steps; ++outer) {
            t.push_back(outer*inner_steps + inner_steps);
            t.back() *= dt;
            source.update(t.back());
            if (outer) {
                printo(os, ", ");
            }
            printo(os, "\n", source.moment<1>());
        }
        println(os, "\n}");
        println(os, "assoc[\"t\"] = ", t) << std::endl;

    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (enable) ) {
        NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
        contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
        contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
        contents = [contents stringByReplacingOccurrencesOfString:@"|" withString:@", "];

        NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
        NSError *error;
        XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"source__2D.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

- (void)testSource__1_3D {
    using PIC::__1_::Source;
    using PIC::__1_::ParameterSet;
    using PIC::__1_::CoordSystem;
    constexpr long ND = 3;

    std::ostringstream os;
    os.precision(12);

    constexpr double dt = 0.01, O0ref = 2*M_PI, r0ref = 1000000, grid_type = 1;
    ParameterSet<ND>::size_vector_type const N = {5, 3, 3};
    ParameterSet<ND>::position_type const q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
    ParameterSet<ND>::position_type const D = {1.1, .9, 1};
    double const c = D.x/dt;

    constexpr bool enable = false;

    try {
        CoordSystem const cs{r0ref, D, grid_type};
        ParameterSet<ND>::OptionSet opts; {
        }
        ParameterSet<ND> const params(cs, q_min, N, c, O0ref, opts);
        Source<ND> source{params};

        Source<ND>::Descriptor desc{}; {
            desc.location = params.grid_q_max() - .5;
            desc.J0carts.x = Source<ND>::complex_type{1.1, 0};
            desc.J0carts.y = Source<ND>::complex_type{0, 1.5};
            desc.J0carts.z = Source<ND>::complex_type{.5, .5};
            desc.omega = 2*M_PI;
            desc.start = 15;
            desc.duration = 5;
            desc.ease_in = 10;
        }
        source.add(desc);
        XCTAssert(source.descriptors().size() == 1); {
            constexpr long i = 0;
            XCTAssert(UTL::reduce_bit_and(source.descriptors()[i].location == desc.location));
            XCTAssert(source.descriptors()[i].J0carts.x.real() == desc.J0carts.x.real() && source.descriptors()[i].J0carts.x.imag() == desc.J0carts.x.imag());
            XCTAssert(source.descriptors()[i].J0carts.y.real() == desc.J0carts.y.real() && source.descriptors()[i].J0carts.y.imag() == desc.J0carts.y.imag());
            XCTAssert(source.descriptors()[i].J0carts.z.real() == desc.J0carts.z.real() && source.descriptors()[i].J0carts.z.imag() == desc.J0carts.z.imag());
            XCTAssert(source.descriptors()[i].omega == desc.omega);
            XCTAssert(source.descriptors()[i].start == desc.start && source.descriptors()[i].duration == desc.duration);
            XCTAssert(source.descriptors()[i].ease_in == desc.ease_in);
            XCTAssert(desc.ease_in == 0.0 || source.descriptors()[i].slope() == M_PI/desc.ease_in);
        }

        desc.location = params.grid_q_max() + 1.;
        source.add(desc);
        XCTAssert(source.descriptors().size() == 1);

        desc = source.descriptors().at(0);
        println(os, "assoc = Association[]");
        println(os, "assoc[\"J0\"] = {",
                "Complex[", desc.J0carts.x.real(), ", ", desc.J0carts.x.imag(), "], ",
                "Complex[", desc.J0carts.y.real(), ", ", desc.J0carts.y.imag(), "], ",
                "Complex[", desc.J0carts.z.real(), ", ", desc.J0carts.z.imag(), "]",
                "}");
        println(os, "assoc[\"\\[Omega]\"] = ", desc.omega);
        println(os, "assoc[\"start\"] = ", desc.start);
        println(os, "assoc[\"duration\"] = ", desc.duration);
        println(os, "assoc[\"easein\"] = ", desc.ease_in);
        println(os, "assoc[\"slope\"] = ", desc.slope());
        println(os, "assoc[\"location\"] = ", desc.location);
        println(os, "assoc[\"qmin\"] = ", params.grid_q_min());
        println(os, "assoc[\"pad\"] = ", source.moment<1>().pad_size()) << std::endl;

        print(os, "assoc[\"J\"] = {");
        UTL::DynamicArray<double> t;
        constexpr long outer_steps = 400*2, inner_steps = 5;
        for (long outer = 0; outer < outer_steps; ++outer) {
            t.push_back(outer*inner_steps + inner_steps);
            t.back() *= dt;
            source.update(t.back());
            if (outer) {
                printo(os, ", ");
            }
            printo(os, "\n", source.moment<1>());
        }
        println(os, "\n}");
        println(os, "assoc[\"t\"] = ", t) << std::endl;

    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (enable) ) {
        NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
        contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
        contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];
        contents = [contents stringByReplacingOccurrencesOfString:@"|" withString:@", "];

        NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
        NSError *error;
        XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"source__3D.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

@end
