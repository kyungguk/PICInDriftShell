add_library(PICKit EXCLUDE_FROM_ALL)
#add_library(PICKit)
target_sources(PICKit PUBLIC
    PICKit/PICBField__1D.cc
    PICKit/PICBField__2D.cc
    PICKit/PICBField__3D.cc
    PICKit/PICColdFluid__1D.cc
    PICKit/PICColdFluid__2D.cc
    PICKit/PICColdFluid__3D.cc
    PICKit/PICCurrent__1D.cc
    PICKit/PICCurrent__2D.cc
    PICKit/PICCurrent__3D.cc
    PICKit/PICDelegate__1D.cc
    PICKit/PICDelegate__2D.cc
    PICKit/PICDelegate__3D.cc
    PICKit/PICDomain__1D.cc
    PICKit/PICDomain__2D.cc
    PICKit/PICDomain__3D.cc
    PICKit/PICEField__1D.cc
    PICKit/PICEField__2D.cc
    PICKit/PICEField__3D.cc
    PICKit/PICMasterDelegate__1D.cc
    PICKit/PICMasterDelegate__2D.cc
    PICKit/PICMasterDelegate__3D.cc
    PICKit/PICSource__1D.cc
    PICKit/PICSource__2D.cc
    PICKit/PICSource__3D.cc
    PICKit/PICSpecies__1D.cc
    PICKit/PICSpecies__2D.cc
    PICKit/PICSpecies__3D.cc
    PICKit/PICWorkerDelegate__1D.cc
    PICKit/PICWorkerDelegate__2D.cc
    PICKit/PICWorkerDelegate__3D.cc
    )

set_target_properties(PICKit PROPERTIES
    LINKER_LANGUAGE CXX
    OUTPUT_NAME PICKit
    )
target_include_directories(PICKit PUBLIC .)

set_project_warnings(PICKit)
enable_sanitizers(PICKit)
