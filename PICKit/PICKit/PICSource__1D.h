//
//  PICSource__1D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 4/21/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICSource__1D_h
#define PICSource__1D_h

#include <PICKit/PICSource__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::Source<ND>
    //
    template <>
    class Source<1L> : public _Source<1L> {
        Source(Source &&) = default;
        Source &operator=(Source &&) = default;

    public:
        ~Source();
        explicit Source() noexcept;
        explicit Source(ParameterSet<ND> const& params, WaveDamper<ND> const &damper = {});
        explicit Source(Source&& old, WaveDamper<ND> const &damper);

        WaveDamper<ND> const &damper() const noexcept { return _damper; }

        void add(Descriptor const &desc);
        void update(value_type const t);

    private:
        inline void _update(vector_moment_type &J, value_type const t) const;

        WaveDamper<ND> _damper;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICSource__1D_h */
