//
//  PICSpecies__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICSpecies__ND_h
#define PICSpecies__ND_h

#include <PICKit/PICKit-config.h>
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <memory>
#include <tuple>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class Species;
    template <long ND> class BField;
    template <long ND> class EField;

    template <long ND>
    struct Particle {
        static_assert(ND <= 3, "invalid ND");
        using value_type = double;
        using _Velocity = UTL::Vector<value_type, 3L>;
        using _Position = typename std::conditional<ND == 3, CurviCoord, UTL::Vector<value_type, ND>>::type;

        _Velocity vel;
        _Position pos;
    };

    // MARK:- PIC::__1_::_Species<ND>
    //
    template <long _ND>
    class _Species {
    public:
        static constexpr long ND = _ND;
        static constexpr long reducedND = ND == 3 ? ND - 1 : ND;
        static constexpr long pad_size = 3;
        // types
        //
        using size_type = long;
        using value_type = double;
        using velocity_type = UTL::Vector<value_type, 3L>;
        using position_type = UTL::Vector<value_type, reducedND>;
        using size_vector_type = UTL::Vector<size_type, reducedND>;
        using scalar_moment_type = UTL::ArrayND<UTL::Vector<value_type, 1>, reducedND, pad_size>;
        using vector_moment_type = UTL::ArrayND<UTL::Vector<value_type, 3>, reducedND, pad_size>; // cartesian components
        using tensor_moment_type = UTL::ArrayND<UTL::Vector<value_type, 6>, reducedND, 1>; // cartesian components
        using electric_field_type = vector_moment_type;
        using magnetic_field_type = vector_moment_type; // padding should be >= 2; see _full_B()
        using moment_triplet_type = std::tuple<scalar_moment_type, vector_moment_type, tensor_moment_type>;
        struct package_type {
            moment_triplet_type moments;
            vector_moment_type force_balancing_mom1; // force balancing current
            explicit package_type(size_vector_type const &N) : moments{std::make_tuple(scalar_moment_type{N}, vector_moment_type{N}, tensor_moment_type{N})}, force_balancing_mom1{N} {}
        };
        using particle_type = Particle<ND>;
        using bucket_type = UTL::DynamicArray<particle_type>;

        // accessors
        //
        ParameterSet<ND> const& params() const noexcept { return _params; }
        value_type const& Oc0ref() const noexcept { return _Oc0ref; } //!< Cyclotron frequency of this species at the reference point.
        value_type const& op0ref() const noexcept { return _op0ref; } //!< Plasma frequency of this species at the reference point.
        value_type const& Nc0ref() const noexcept { return _Nc0ref; } //!< Number of particles in the cell at the reference point.
        ShapeOrder const& shape_order() const noexcept { return _shape_order; }
        NSmooth const& number_of_smoothing_passes() const noexcept { return _n_smooth; }

        value_type charge_density_conversion_factor() const noexcept { return UTL::pow<2>(op0ref())*params().O0ref()/Oc0ref(); }
        value_type current_density_conversion_factor() const noexcept { return charge_density_conversion_factor()/params().c(); }
        value_type energy_density_conversion_factor() const noexcept { return UTL::pow<2>(params().O0ref()/Oc0ref()*op0ref()/params().c()); }

        template <long rank> typename std::tuple_element<rank, moment_triplet_type
        >::type const& moment() const noexcept { return std::get<rank>((*this)->moments); }
        template <long rank> typename std::tuple_element<rank, moment_triplet_type
        >::type      & moment()       noexcept { return std::get<rank>((*this)->moments); }

        moment_triplet_type const &moments() const noexcept { return (*this)->moments; }
        moment_triplet_type       &moments()       noexcept { return (*this)->moments; }

        bucket_type       &bucket()       noexcept { return *_bucket; }
        bucket_type const &bucket() const noexcept { return *_bucket; }

        vector_moment_type const &force_balancing_moment1() const noexcept { return (*this)->force_balancing_mom1; }
        vector_moment_type       &force_balancing_moment1()       noexcept { return (*this)->force_balancing_mom1; }

    protected:
        _Species(_Species &&) = default;
        _Species &operator=(_Species &&) = default;
        virtual ~_Species() {}
        explicit _Species() noexcept {}
        explicit _Species(ParameterSet<ND> const& params);

        package_type const* operator->() const noexcept { return _pkg.get(); }
        package_type      * operator->()       noexcept { return _pkg.get(); }

        void set_properties(value_type const Oc0ref, value_type const op0ref, value_type const Nc0ref, ShapeOrder const shape_order, NSmooth const n_smooth);

    private:
        ParameterSet<ND> _params;
        value_type _Oc0ref;
        value_type _op0ref;
        value_type _Nc0ref;
        ShapeOrder _shape_order{0};
        NSmooth _n_smooth{0};
        std::unique_ptr<bucket_type> _bucket;
        std::unique_ptr<package_type> _pkg;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICSpecies__ND_h */
