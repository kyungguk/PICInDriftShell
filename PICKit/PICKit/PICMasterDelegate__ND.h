//
//  PICMasterDelegate__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 7/1/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICMasterDelegate__ND_h
#define PICMasterDelegate__ND_h

#include <PICKit/PICInterThreadComm.h>
#include <PICKit/PICWorkerDelegate__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class MasterDelegate;
    template <long ND> class WorkerDelegate;

    // MARK:- PIC::__1_::_MasterDelegate<ND>
    //
    template <long ND>
    class _MasterDelegate : public Delegate<ND> {
    public:
        static constexpr long Cap = 256;
        using Ticket = typename InterThreadComm<MasterDelegate<ND>, WorkerDelegate<ND>, WorkerDelegate<ND>::n_comm_channels>::Ticket;
        using typename Delegate<ND>::Direction;

    protected:
        explicit _MasterDelegate(Delegate<ND> *const delegate);

        UTL::DynamicArray<Particle<ND>> L_bucket{}, R_bucket{};
        UTL::StaticArray<Ticket, Cap> tickets{};
        UTL::StaticArray<WorkerDelegate<ND>, Cap> _workers{};
    public:
        Delegate<ND> *const delegate; // e.g., serial version

        UTL::Array<WorkerDelegate<ND>> const &workers() const noexcept { return _workers; }
        UTL::Array<WorkerDelegate<ND>>       &workers()       noexcept { return _workers; }

    protected:
        void _pass(Domain<ND> const*, Species<ND> &sp, Direction const dir);

        template <long i, class T, long Rank, long Pad>
        void broadcast_to_workers(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> const &payload);
        template <long i, class T, long Rank, long Pad>
        void collect_from_workers(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &buffer);
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICMasterDelegate__ND_h */
