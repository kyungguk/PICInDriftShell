//
//  PICParticlePush.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 1/25/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICParticlePush_h
#define PICParticlePush_h

#include <UtilityKit/UtilityKit.h>
#include <cmath>

// MARK: Boris Push
// see Zenitani and Umeda (2018)
//
namespace {
    // common
    //
    class _BorisPush {
    public:
        using Real = double;
        using Vector = UTL::Vector<Real, 3>;

        constexpr explicit _BorisPush() noexcept = delete;
        explicit _BorisPush(Real const dt, Real const c, Real const O0, Real const Oc) noexcept {
            dt_2 = 0.5*dt;
            dtOc_2O0 = Oc*dt_2/O0;
            cDtOc_2O0 = c*dtOc_2O0;
        }

    protected:
        Real  dt_2{};
        Real  dtOc_2O0{};
        Real cDtOc_2O0{};
    };

    // Boris-B
    // the one previously used
    //
    class Boris_B : public _BorisPush {
    public:
        using _BorisPush::_BorisPush;

        void operator()(Vector &v, Vector &_B, Vector &_E) const noexcept {
            auto       &B = _B *=  dtOc_2O0; // B_DtOc_2O0
            auto const &E = _E *= cDtOc_2O0; // EcDtOc_2O0
            //
            // step 1
            //
            v += E;
            //
            // step 2 & 3
            //
            rotate(v, B);
            //
            // step 4
            //
            v += E;
        }
    private:
        void rotate(Vector &v, Vector &B) const noexcept {
            //
            // step 2
            //
            Vector v0{v};
            v0.x += v.y*B.z - v.z*B.y;
            v0.y += v.z*B.x - v.x*B.z;
            v0.z += v.x*B.y - v.y*B.x;
            //
            // step 3
            //
            B *= 2 / (1 + (B.x*B.x + B.y*B.y + B.z*B.z));
            v.x += v0.y*B.z - v0.z*B.y;
            v.y += v0.z*B.x - v0.x*B.z;
            v.z += v0.x*B.y - v0.y*B.x;
        }
    };

    // Boris-A
    //
    class Boris_A : public _BorisPush {
        constexpr static Real eps = 1e-5;

    public:
        using _BorisPush::_BorisPush;

        void operator()(Vector &v, Vector &_B, Vector &_E) const noexcept {
            auto       &B = _B *=  dtOc_2O0; // B_DtOc_2O0
            auto const &E = _E *= cDtOc_2O0; // EcDtOc_2O0
            //
            // phase angle
            //
            Vector::value_type const Blen = std::sqrt(UTL::reduce_plus(B * B));
            if (Blen > eps) { // non-zero B or sufficiently large rotation in one step
                B *= std::tan(Blen)/Blen;
            }
            //
            // step 1
            //
            v += E;
            //
            // step 2 & 3
            //
            rotate(v, B);
            //
            // step 4
            //
            v += E;
        }
    private:
        void rotate(Vector &v, Vector &B) const noexcept {
            //
            // step 2
            //
            Vector v0{v};
            v0.x += v.y*B.z - v.z*B.y;
            v0.y += v.z*B.x - v.x*B.z;
            v0.z += v.x*B.y - v.y*B.x;
            //
            // step 3
            //
            B *= 2 / (1 + (B.x*B.x + B.y*B.y + B.z*B.z));
            v.x += v0.y*B.z - v0.z*B.y;
            v.y += v0.z*B.x - v0.x*B.z;
            v.z += v0.x*B.y - v0.y*B.x;
        }
    };

    // Boris-C
    //
    class Boris_C : public _BorisPush {
        constexpr static Real eps = 1e-5;

    public:
        using _BorisPush::_BorisPush;

        void operator()(Vector &v, Vector &_B, Vector &_E) const noexcept {
            auto       &B = _B *=  dtOc_2O0; // B_DtOc_2O0
            auto const &E = _E *= cDtOc_2O0; // EcDtOc_2O0
            //
            // step 1
            //
            v += E;
            //
            // step 2: rotation
            //
            rotate(v, B);
            //
            // step 3
            //
            v += E;
        }
    private:
        void rotate(Vector &v, Vector &B) const noexcept {
            Vector::value_type const B2 = UTL::reduce_plus(B * B);
            if (B2 > UTL::pow<2>(eps)) { // non-zero B or sufficiently large rotation in one step
                Vector::value_type const th = 2*std::sqrt(B2); // phase angle
                B /= .5*th; // unit vector along B
                //
                Vector const vxb_sinth = Vector{
                    v.y*B.z - v.z*B.y,
                    v.z*B.x - v.x*B.z,
                    v.x*B.y - v.y*B.x
                } *= std::sin(th);
                Vector const v1 = B * UTL::reduce_plus(v * B); // v parallel
                //
                (v -= v1) *= std::cos(th); // v_perp * cos(th)
                v += vxb_sinth;
                v += v1;
            } else { // small angle expansion
                Vector tmp{
                    v.y*B.z - v.z*B.y,
                    v.z*B.x - v.x*B.z,
                    v.x*B.y - v.y*B.x
                };
                tmp += B * UTL::reduce_plus(v * B);
                tmp -= B2*v;
                tmp *= 2;
                v += tmp;
            }
        }
    };
}

#endif /* PICParticlePush_h */
