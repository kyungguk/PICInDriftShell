//
//  PICCurrent__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICCurrent__ND_h
#define PICCurrent__ND_h

#include <PICKit/PICKit-config.h>
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <memory>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class Current;
    template <long ND> class Species;
    template <long ND> class ColdFluid;
    template <long ND> class Source;

    // MARK:- PIC::__1_::_Current<ND>
    //
    template <long _ND>
    class _Current {
    public:
        static constexpr long ND = _ND;
        static constexpr long reducedND = ND == 3 ? ND - 1 : ND;
        static constexpr long pad_size = 3;

        // types
        //
        using size_type = long;
        using value_type = double;
        using vector_type = UTL::Vector<value_type, 3L>;
        using position_type = UTL::Vector<value_type, reducedND>;
        using size_vector_type = UTL::Vector<size_type, reducedND>;
        using current_density_type = UTL::ArrayND<vector_type, reducedND, pad_size>;
        using vector_moment_type = current_density_type;
        struct triplet_type { // at cell center
            current_density_type mfa, contr, tmp;
            explicit triplet_type(size_vector_type const &N) : mfa{N}, contr{N}, tmp{N} {}
        };

        // accessors
        //
        ParameterSet<ND> const& params() const noexcept { return _params; }

        triplet_type const* operator->() const noexcept { return _J.get(); }
        triplet_type      * operator->()       noexcept { return _J.get(); }

        // common operations
        //
        void reset() { (*this)->mfa.fill(vector_type{0}); }
        void smooth() { _smooth((*this)->mfa, (*this)->tmp); }

    protected:
        _Current(_Current &&) = default;
        _Current &operator=(_Current &&) = default;
        virtual ~_Current() {}
        explicit _Current() noexcept {}
        explicit _Current(ParameterSet<ND> const& params);

    private:
        ParameterSet<ND> _params;
        std::unique_ptr<triplet_type> _J;

        static void _smooth(current_density_type &J, current_density_type &tmp) {
            constexpr UTL::HammingFilter<current_density_type::rank()> filter{};
            filter(J.begin(), J.end(), tmp.begin());
            using std::swap; swap(J, tmp);
        }
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICCurrent__ND_h */
