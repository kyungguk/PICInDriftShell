//
//  PICEField__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICEField__ND_h
#define PICEField__ND_h

#include <PICKit/PICKit-config.h>
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <memory>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class EField;
    template <long ND> class BField;
    template <long ND> class Current;

    // MARK:- PIC::__1_::_EField<ND>
    //
    template <long _ND>
    class _EField {
    public:
        static constexpr long ND = _ND;
        static constexpr long reducedND = ND == 3 ? ND - 1 : ND;
        static constexpr long pad_size = 3;

        // types
        //
        using size_type = long;
        using value_type = double;
        using vector_type = UTL::Vector<value_type, 3L>;
        using position_type = UTL::Vector<value_type, reducedND>;
        using size_vector_type = UTL::Vector<size_type, reducedND>;
        using electric_field_type = UTL::ArrayND<vector_type, reducedND, pad_size>;
        using magnetic_field_type = UTL::ArrayND<vector_type, reducedND, pad_size>;
        using current_density_type = electric_field_type;
        struct triplet_type { // at cell center
            electric_field_type contr, covar, carts;
            explicit triplet_type(size_vector_type const &N) : contr{N}, covar{N}, carts{N} {}
        };

        // accessors
        //
        ParameterSet<ND> const& params() const noexcept { return _params; }

        triplet_type const* operator->() const noexcept { return _E.get(); }
        triplet_type      * operator->()       noexcept { return _E.get(); }

    protected:
        _EField(_EField &&) = default;
        _EField &operator=(_EField &&) = default;
        virtual ~_EField() {}
        explicit _EField() noexcept {}
        explicit _EField(ParameterSet<ND> const& params);

    private:
        ParameterSet<ND> _params;
        std::unique_ptr<triplet_type> _E;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICEField__ND_h */
