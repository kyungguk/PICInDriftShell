//
//  PICParticleDeposit.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 1/25/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICParticleDeposit_h
#define PICParticleDeposit_h

#include <UtilityKit/UtilityKit.h>

// MARK: Particle-to-Grid Deposit
//
namespace {
    template <long S>
    using _Shape = UTL::Shape<double, S>;

    // MARK: deposit::shape<1>
    //
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 1, Pad> &n, _Shape<1> const &sx, T const &w) {
        static_assert(Pad >= 1, "Not enough padding");
        n[sx.i<0>()] += w*sx.w<0>();
        n[sx.i<1>()] += w*sx.w<1>();
    }
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 2, Pad> &n, _Shape<1> const &sx, _Shape<1> const &sy, T const &w) {
        deposit(n[sx.i<0>()], sy, w*sx.w<0>());
        deposit(n[sx.i<1>()], sy, w*sx.w<1>());
    }
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 3, Pad> &n, _Shape<1> const &sx, _Shape<1> const &sy, _Shape<1> const &sz, T const &w) {
        deposit(n[sx.i<0>()], sy, sz, w*sx.w<0>());
        deposit(n[sx.i<1>()], sy, sz, w*sx.w<1>());
    }

    // MARK: deposit::shape<2>
    //
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 1, Pad> &n, _Shape<2> const &sx, T const &w) {
        static_assert(Pad >= 2, "Not enough padding");
        n[sx.i<0>()] += w*sx.w<0>();
        n[sx.i<1>()] += w*sx.w<1>();
        n[sx.i<2>()] += w*sx.w<2>();
    }
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 2, Pad> &n, _Shape<2> const &sx, _Shape<2> const &sy, T const &w) {
        deposit(n[sx.i<0>()], sy, w*sx.w<0>());
        deposit(n[sx.i<1>()], sy, w*sx.w<1>());
        deposit(n[sx.i<2>()], sy, w*sx.w<2>());
    }
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 3, Pad> &n, _Shape<2> const &sx, _Shape<2> const &sy, _Shape<2> const &sz, T const &w) {
        deposit(n[sx.i<0>()], sy, sz, w*sx.w<0>());
        deposit(n[sx.i<1>()], sy, sz, w*sx.w<1>());
        deposit(n[sx.i<2>()], sy, sz, w*sx.w<2>());
    }

    // MARK: deposit::shape<3>
    //
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 1, Pad> &n, _Shape<3> const &sx, T const &w) {
        static_assert(Pad >= 3, "Not enough padding");
        n[sx.i<0>()] += w*sx.w<0>();
        n[sx.i<1>()] += w*sx.w<1>();
        n[sx.i<2>()] += w*sx.w<2>();
        n[sx.i<3>()] += w*sx.w<3>();
    }
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 2, Pad> &n, _Shape<3> const &sx, _Shape<3> const &sy, T const &w) {
        deposit(n[sx.i<0>()], sy, w*sx.w<0>());
        deposit(n[sx.i<1>()], sy, w*sx.w<1>());
        deposit(n[sx.i<2>()], sy, w*sx.w<2>());
        deposit(n[sx.i<3>()], sy, w*sx.w<3>());
    }
    template <class T, long Pad>
    inline void deposit(UTL::ArrayND<T, 3, Pad> &n, _Shape<3> const &sx, _Shape<3> const &sy, _Shape<3> const &sz, T const &w) {
        deposit(n[sx.i<0>()], sy, sz, w*sx.w<0>());
        deposit(n[sx.i<1>()], sy, sz, w*sx.w<1>());
        deposit(n[sx.i<2>()], sy, sz, w*sx.w<2>());
        deposit(n[sx.i<3>()], sy, sz, w*sx.w<3>());
    }
}

#endif /* PICParticleDeposit_h */
