//
//  PICBField__1D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 3/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICBField__1D.h"
#include "PICBField__ND.hh"
#include "PICEField__1D.h"

constexpr long ND = 1;

// MARK:- PIC::__1_::BField<ND>
//
PIC::__1_::BField<ND>::~BField<ND>()
{
}
PIC::__1_::BField<ND>::BField() noexcept
: _BField{} {
}
PIC::__1_::BField<ND>::BField(ParameterSet<ND> const &params, WaveDamper<ND> const &damper)
: _BField{params}, _damper{damper} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params.N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
}
PIC::__1_::BField<ND>::BField(BField&& old, WaveDamper<ND> const &damper)
: _BField{std::move(old)}, _damper{damper} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params().N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
}

void PIC::__1_::BField<ND>::component_transform()
{
    _component_transform(*this);
}
void PIC::__1_::BField<ND>::_component_transform(_BField &B) const
{
    magnetic_field_type const& contr = B->contr;
    magnetic_field_type      & covar = B->covar;
    magnetic_field_type      & carts = B->carts;
    constexpr long pad = magnetic_field_type::pad_size();
    size_vector_type const N = contr.dims() + pad;
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_edge>(); // use cell edge metric if staggered; otherwise cell center
        size_vector_type ijk{0}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            ijk.x = i;
            covar[i] = metric.contr_to_covar(contr[i], ijk);
            carts[i] = metric.contr_to_carts(contr[i], ijk);
        }
    } else {
        position_type const q_min = params().grid_q_min() + .5*long{params().grid_strategy()}; // at cell edge if staggered; otherwise cell center
        CoordSystem const &cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            covar[i] = cs.contr_to_covar(contr[i], curvi);
            carts[i] = cs.contr_to_carts(contr[i], curvi);
        }
    }

    // 'primed' covariant component
    //
    for (long i = -pad; i < N.x; ++i) {
        covar[i].z = contr[i].z;
    }
}

void PIC::__1_::BField<ND>::update(EField<ND> const &efield, value_type const dt)
{
    value_type const cdtOsqrtg{ params().c()*dt/std::sqrt(params().coord_system().g()) };
    if (UTL::reduce_bit_or(damper().lo_boundary_mask() | damper().hi_boundary_mask())) { // at least one boundary section
        // phase retardation
        //
        (*this)->covar.fill(vector_type{0}); // temporarily holds -curl E
        _update((*this)->covar, efield->covar, cdtOsqrtg);
        damper()((*this)->covar, damper().phase_retardation());

        // amplitude damping
        //
        (*this)->contr += (*this)->covar; // note that covar is already -curl E
        damper()((*this)->contr, damper().amplitude_damping());
    } else { // middle section
        _update((*this)->contr, efield->covar, cdtOsqrtg);
    }
}
void PIC::__1_::BField<ND>::_update(magnetic_field_type &B, electric_field_type const &E, value_type cdtOsqrtg) const
{
    static_assert(electric_field_type::pad_size() >= 1, "invalid padding size");
    cdtOsqrtg *= 1;
    size_vector_type const N = B.dims();
    long const _1 = +long{params().grid_strategy() > 0}; // +1 when cell edge is leading; otherwise -0
    long const _0 = -long{params().grid_strategy() < 0}; // +0 when cell edge is leading; otherwise -1
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_edge>(); // use cell edge metric if staggered; otherwise cell center
        size_vector_type ijk{0}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            ijk.x = i;

            // 3rd row of contravariant metric tensor
            //
            vector_type const h3 = metric.contr_metric(ijk).z;
            value_type const cdtOsqrtgh33 = cdtOsqrtg/h3.z;

            // dt*c*curlE
            //
            vector_type const dB = {
                0,
                -(E[i+_1].z -E[i+_0].z) * cdtOsqrtgh33,
                +(E[i+_1].y -E[i+_0].y) * cdtOsqrtgh33
            };

            // update B
            //
            vector_type &Bi = B[i];
            Bi.x -= dB.x;
            Bi.y -= dB.y + dB.z*h3.y;
            Bi.z -= dB.z*h3.z;
        }
    } else {
        position_type const q_min = params().grid_q_min() + .5*long{params().grid_strategy()}; // at cell edge if staggered; otherwise cell center
        CoordSystem const &cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            curvi.x = i + q_min.x;

            // 3rd row of contravariant metric tensor
            //
            vector_type const h3 = cs.contr_metric(curvi).z;
            value_type const cdtOsqrtgh33 = cdtOsqrtg/h3.z;

            // dt*c*curlE
            //
            vector_type const dB = {
                0,
                -(E[i+_1].z -E[i+_0].z) * cdtOsqrtgh33,
                +(E[i+_1].y -E[i+_0].y) * cdtOsqrtgh33
            };

            // update B
            //
            vector_type &Bi = B[i];
            Bi.x -= dB.x;
            Bi.y -= dB.y + dB.z*h3.y;
            Bi.z -= dB.z*h3.z;
        }
    }
}
