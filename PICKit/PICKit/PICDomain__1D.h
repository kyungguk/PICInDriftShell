//
//  PICDomain__1D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 3/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICDomain__1D_h
#define PICDomain__1D_h

#include <PICKit/PICDomain__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::Domain<ND>
    //
    template <>
    class Domain<1L> : public _Domain<1L> {
        Domain(Domain &&) = default;
        Domain &operator=(Domain &&) = default;

    public:
        ~Domain();
        explicit Domain() noexcept;
        explicit Domain(ParameterSet<ND> const& params, value_type const dt, WaveDamper<ND> const& damper = {});

        // update
        //
        void advance_by(size_type const n_steps) override;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICDomain__1D_h */
