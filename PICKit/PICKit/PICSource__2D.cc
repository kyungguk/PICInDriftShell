//
//  PICSource__2D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 4/22/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICSource__2D.h"
#include "PICSource__ND.hh"

constexpr long ND = 2;

// MARK:- PIC::__1_::Source<ND>
//
PIC::__1_::Source<ND>::~Source<ND>()
{
}
PIC::__1_::Source<ND>::Source() noexcept
: _Source{} {
}
PIC::__1_::Source<ND>::Source(ParameterSet<ND> const &params, WaveDamper<ND> const &damper)
: _Source{params}, _damper{damper} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params.N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
}
PIC::__1_::Source<ND>::Source(Source&& old, WaveDamper<ND> const &damper)
: _Source{std::move(old)}, _damper{damper} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params().N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
}

void PIC::__1_::Source<ND>::add(Descriptor const &desc)
{
    _Source::_add(desc);
}

void PIC::__1_::Source<ND>::update(value_type const t)
{
    if (UTL::reduce_bit_or(damper().lo_boundary_mask() | damper().hi_boundary_mask())) { // at least one boundary section
        _update(moment<1>(), t);
        // phase retardation
        //
        damper()(moment<1>(), damper().phase_retardation());
        // amplitude damping
        //
        damper()(moment<1>(), damper().amplitude_damping());
    } else { // middle section
        _update(moment<1>(), t);
    }
}
void PIC::__1_::Source<ND>::_update(vector_moment_type &J, value_type const t) const
{
    J.fill(vector_type{0});
    for (Descriptor const &desc : descriptors()) {
        deposit(J, desc.shape<0>(), desc.shape<1>(), _Jcarts(desc, t));
    }
}
