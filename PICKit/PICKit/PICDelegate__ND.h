//
//  PICDelegate__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 11/7/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICDelegate__ND_h
#define PICDelegate__ND_h

#include <PICKit/PICKit-config.h>
#include <PICKit/PICSpecies__ND.h>
#include <UtilityKit/UtilityKit.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class BField;
    template <long ND> class EField;
    template <long ND> class Current;
    template <long ND> class Species;
    template <long ND> class ColdFluid;
    template <long ND> class Domain;
    template <long ND> class Delegate;

    // MARK:- PIC::__1_::Delegate<ND>
    //
    template <long _ND>
    class Delegate {
    protected:
        Delegate(Delegate&&) noexcept = default;
        Delegate &operator=(Delegate&&) noexcept = default;
        explicit Delegate() noexcept = default;

    public:
        static constexpr long ND = _ND;
        virtual ~Delegate() = default;

        // types
        //
        using domain_type = Domain<ND>;
        using bfield_type = BField<ND>;
        using efield_type = EField<ND>;
        using current_type = Current<ND>;
        using species_type = Species<ND>;
        using cold_fluid_type = ColdFluid<ND>;

        // notification
        //
        virtual void once(Domain<ND> *) {}
        virtual bool will_do_next_cycle(Domain<ND> const*, [[maybe_unused]] long const i_step) { return true; }

        // communication
        //
        virtual void pass(Domain<ND> const*, Species<ND> &sp) = 0;
        virtual void pass(Domain<ND> const*, BField<ND> &bfield) = 0;
        virtual void pass(Domain<ND> const*, EField<ND> &efield) = 0;
        virtual void pass(Domain<ND> const*, Current<ND> &current) = 0;
        virtual void gather(Domain<ND> const*, Current<ND> &current) = 0;
        virtual void gather(Domain<ND> const*, Species<ND> &sp) = 0;

        // for efficient work breakdown for multi-threaded version
        //
        enum Direction : long {
            q1 = 0, q2, q3
        };
        virtual void partition(Species<ND>&, Direction const, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket) = 0;
        virtual void pass(Domain<ND> const*, Direction const, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket) = 0;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICDelegate__ND_h */
