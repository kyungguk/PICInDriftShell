//
//  PICColdFluid__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 9/19/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICColdFluid__ND_h
#define PICColdFluid__ND_h

#include <PICKit/PICKit-config.h>
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <memory>
#include <tuple>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class ColdFluid;
    template <long ND> class EField;

    // MARK:- PIC::__1_::_ColdFluid<ND>
    //
    template <long _ND>
    class _ColdFluid {
    public:
        static constexpr long ND = _ND;
        static constexpr long reducedND = ND == 3 ? ND - 1 : ND;
        static constexpr long pad_size = 3;

        // types
        //
        using size_type = long;
        using value_type = double;
        using vector_type = UTL::Vector<value_type, 3L>;
        using position_type = UTL::Vector<value_type, reducedND>;
        using size_vector_type = UTL::Vector<size_type, reducedND>;
        using scalar_moment_type = UTL::ArrayND<UTL::Vector<value_type, 1>, reducedND, 0>; // equilibrium density normalized to a reference density; time-independent; at cell center
        using vector_moment_type = UTL::ArrayND<UTL::Vector<value_type, 3>, reducedND, pad_size>; // 1st velocity moment weighted by equilibrium density; at cell center
        using tensor_moment_type = UTL::ArrayND<UTL::Vector<value_type, 6>, reducedND, 0>; // 2nd velocity moment weighted by equilibrium density; at cell center
        using electric_field_type = vector_moment_type;
        using magnetic_field_type = vector_moment_type;
        using moment_triplet_type = std::tuple<scalar_moment_type, vector_moment_type, tensor_moment_type>;
        struct package_type {
            moment_triplet_type moments;
            explicit package_type(size_vector_type const &N) : moments{std::make_tuple(scalar_moment_type{N}, vector_moment_type{N}, tensor_moment_type{N})} {}
        };

        // accessors
        //
        ParameterSet<ND> const& params() const noexcept { return _params; }
        value_type const& Oc0ref() const noexcept { return _Oc0ref; } //!< Cyclotron frequency of this species at the reference point.
        value_type const& op0ref() const noexcept { return _op0ref; } //!< Plasma frequency of this species at the reference point.
        NSmooth const& number_of_smoothing_passes() const noexcept { return _n_smooth; }

        value_type charge_density_conversion_factor() const noexcept { return UTL::pow<2>(op0ref())*params().O0ref()/Oc0ref(); }
        value_type current_density_conversion_factor() const noexcept { return charge_density_conversion_factor()/params().c(); }
        value_type energy_density_conversion_factor() const noexcept { return UTL::pow<2>(params().O0ref()/Oc0ref()*op0ref()/params().c()); }

        template <long rank> typename std::tuple_element<rank, moment_triplet_type
        >::type const& moment() const noexcept { return std::get<rank>((*this)->moments); }
        template <long rank> typename std::tuple_element<rank, moment_triplet_type
        >::type      & moment()       noexcept { return std::get<rank>((*this)->moments); }

        moment_triplet_type const &moments() const noexcept { return (*this)->moments; }
        moment_triplet_type       &moments()       noexcept { return (*this)->moments; }

    protected:
        _ColdFluid(_ColdFluid &&) = default;
        _ColdFluid &operator=(_ColdFluid &&) = default;
        virtual ~_ColdFluid() {}
        explicit _ColdFluid() noexcept {}
        explicit _ColdFluid(ParameterSet<ND> const& params);

        package_type const* operator->() const noexcept { return _pkg.get(); }
        package_type      * operator->()       noexcept { return _pkg.get(); }

        void set_properties(value_type const Oc0ref, value_type const op0ref, NSmooth const n_smooth);

    private:
        ParameterSet<ND> _params;
        value_type _Oc0ref;
        value_type _op0ref;
        NSmooth _n_smooth{0};
        std::unique_ptr<package_type> _pkg;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICColdFluid__ND_h */
