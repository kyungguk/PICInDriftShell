//
//  PICCurrent__1D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 3/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICCurrent__1D_h
#define PICCurrent__1D_h

#include <PICKit/PICCurrent__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::Current<ND>
    //
    template <>
    class Current<1L> : public _Current<1L> {
        Current(Current &&) = default;
        Current &operator=(Current &&) = default;

    public:
        ~Current();
        explicit Current() noexcept;
        explicit Current(ParameterSet<ND> const& params);

        // update
        //
        virtual void component_transform();
        virtual Current& operator+=(Species<ND> const &sp);
        virtual Current& operator+=(ColdFluid<ND> const &cf);
        virtual Current& operator+=(Source<ND> const &src);
    private:
        inline void _component_transform(_Current &J) const;
        inline void _accumulate(current_density_type &J, vector_moment_type const &V, value_type const &weight) const;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICCurrent__1D_h */
