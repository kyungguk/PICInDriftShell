//
//  PICWorkerDelegate__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 7/1/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICWorkerDelegate__ND_h
#define PICWorkerDelegate__ND_h

#include <PICKit/PICInterThreadComm.h>
#include <PICKit/PICDelegate__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class MasterDelegate;
    template <long ND> class WorkerDelegate;

    // MARK:- PIC::__1_::_WorkerDelegate<ND>
    //
    template <long ND>
    class _WorkerDelegate : public Delegate<ND> {
    public:
        // inter-thread comm channels
        //
        static constexpr long n_comm_channels = 1;
        using typename Delegate<ND>::Direction;

    protected:
        explicit _WorkerDelegate(long const id, MasterDelegate<ND> *const master);

        long const id;
        MasterDelegate<ND> *const master;
        UTL::DynamicArray<Particle<ND>> L_bucket{}, R_bucket{};
        InterThreadComm<WorkerDelegate<ND>, WorkerDelegate<ND>, n_comm_channels> worker_to_worker{};
    public:
        InterThreadComm<MasterDelegate<ND>, WorkerDelegate<ND>, n_comm_channels> master_to_worker{};
        InterThreadComm<WorkerDelegate<ND>, MasterDelegate<ND>, n_comm_channels> worker_to_master{};

    protected:
        void _pass(Domain<ND> const*, Species<ND> &sp, Direction const dir);

        template <long i, class T, long Rank, long Pad>
        void recv_from_master(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &buffer);
        template <long i, class T, long Rank, long Pad>
        void reduce_to_master(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &payload);

        template <long i, class T, long Rank, long Pad>
        void reduce_divide_and_conquer(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &payload);
        template <long i, class T, long Rank, long Pad>
        void accumulate_by_worker(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> const &payload);
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICWorkerDelegate__ND_h */
