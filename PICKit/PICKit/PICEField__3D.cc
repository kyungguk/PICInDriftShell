//
//  PICEField__3D.cc
//  PICKit
//
//  Created by Kyungguk Min on 4/2/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICEField__3D.h"
#include "PICEField__ND.hh"
#include "PICBField__3D.h"
#include "PICCurrent__3D.h"

constexpr long ND = 3;

// MARK:- PIC::__1_::EField<ND>
//
PIC::__1_::EField<ND>::~EField<ND>()
{
}
PIC::__1_::EField<ND>::EField() noexcept
: _EField{} {
}
PIC::__1_::EField<ND>::EField(ParameterSet<ND> const &params, WaveDamper<ND> const &damper)
: _EField{params}, _damper{} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params.N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
    _damper = WaveDamper<ND - 1>{
        damper.masking_inset().take<2>(),
        damper.amplitude_damping().take<2>(),
        damper.phase_retardation().take<2>()
    };
    _damper.set_boundary_mask(UTL::reduce_bit_or(damper.lo_boundary_mask() | damper.hi_boundary_mask()));
}
PIC::__1_::EField<ND>::EField(EField&& old, WaveDamper<ND> const &damper)
: _EField{std::move(old)}, _damper{} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params().N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
    _damper = WaveDamper<ND - 1>{
        damper.masking_inset().take<2>(),
        damper.amplitude_damping().take<2>(),
        damper.phase_retardation().take<2>()
    };
    _damper.set_boundary_mask(UTL::reduce_bit_or(damper.lo_boundary_mask() | damper.hi_boundary_mask()));
}

void PIC::__1_::EField<ND>::component_transform()
{
    _component_transform(*this);
}
void PIC::__1_::EField<ND>::_component_transform(_EField &E) const
{
    electric_field_type const& contr = E->contr;
    electric_field_type      & covar = E->covar;
    electric_field_type      & carts = E->carts;
    constexpr long pad = electric_field_type::pad_size();
    size_vector_type const N = contr.dims() + pad;
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // use cell center metric
        MetricTable<ND>::size_vector_type ijk = {0, 0, std::get<2>(params().N())/2}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            ijk.x = i;
            for (long j = -pad; j < N.y; ++j) {
                ijk.y = j;
                covar[i][j] = metric.contr_to_covar(contr[i][j], ijk);
                carts[i][j] = metric.contr_to_carts(contr[i][j], ijk);
            }
        }
    } else {
        ParameterSet<ND>::position_type const& q_min = params().grid_q_min(); // at cell center
        CoordSystem const &cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            for (long j = -pad; j < N.y; ++j) {
                curvi.y = j + q_min.y;
                covar[i][j] = cs.contr_to_covar(contr[i][j], curvi);
                carts[i][j] = cs.contr_to_carts(contr[i][j], curvi);
            }
        }
    }

    // 'primed' covariant component
    //
    for (long i = -pad; i < N.x; ++i) {
        for (long j = -pad; j < N.y; ++j) {
            covar[i][j].z = contr[i][j].z;
        }
    }
}

void PIC::__1_::EField<ND>::update(BField<ND> const &bfield, Current<ND> const &current, value_type const dt)
{
    value_type const cdtOsqrtg{ params().c()*dt/std::sqrt(params().coord_system().g()) };
    if (UTL::reduce_bit_or(damper().lo_boundary_mask() | damper().hi_boundary_mask())) { // at least one boundary section
        // phase retardation
        //
        (*this)->covar.fill(vector_type{0}); // temporarily holds curl B - J
        _update((*this)->covar, bfield->covar, cdtOsqrtg, current->contr, dt);
        damper()((*this)->covar, damper().phase_retardation());

        // amplitude damping
        //
        (*this)->contr += (*this)->covar;
        damper()((*this)->contr, damper().amplitude_damping());
    } else { // middle section
        _update((*this)->contr, bfield->covar, cdtOsqrtg, current->contr, dt);
    }
}
void PIC::__1_::EField<ND>::_update(electric_field_type &E, magnetic_field_type const &B, value_type cdtO2sqrtg, current_density_type const &J, value_type const dt) const
{
    static_assert(magnetic_field_type::pad_size() >= 1, "invalid padding size");
    cdtO2sqrtg *= 0.5;
    size_vector_type const N = E.dims();
    long const _1 = +long{params().grid_strategy() > 0}; // +1 when cell edge is leading; otherwise -0
    long const _0 = -long{params().grid_strategy() < 0}; // +0 when cell edge is leading; otherwise -1
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // use cell center metric
        MetricTable<ND>::size_vector_type ijk = {0, 0, std::get<2>(params().N())/2}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            ijk.x = i;
            for (long j = 0; j < N.y; ++j) {
                ijk.y = j;

                // 3rd row of contravariant metric tensor
                //
                vector_type const h3 = metric.contr_metric(ijk).z;
                value_type const cdtO2sqrtgh33 = cdtO2sqrtg/h3.z;

                // dt*c*curlB
                //
                vector_type const dE = {
                    +((B[i-_0][j-_0].z + B[i-_1][j-_0].z) -(B[i-_0][j-_1].z + B[i-_1][j-_1].z)) * cdtO2sqrtgh33,
                    -((B[i-_0][j-_0].z + B[i-_0][j-_1].z) -(B[i-_1][j-_0].z + B[i-_1][j-_1].z)) * cdtO2sqrtgh33,
                    +((B[i-_0][j-_0].y + B[i-_0][j-_1].y) -(B[i-_1][j-_0].y + B[i-_1][j-_1].y)) * cdtO2sqrtgh33
                    -((B[i-_0][j-_0].x + B[i-_1][j-_0].x) -(B[i-_0][j-_1].x + B[i-_1][j-_1].x)) * cdtO2sqrtgh33
                };

                // update E
                //
                vector_type &Ei = E[i][j];
                Ei.x += dE.x;
                Ei.y += dE.y + dE.z*h3.y;
                Ei.z += dE.z*h3.z;
                //
                Ei -= J[i][j] * dt;
            }
        }
    } else {
        ParameterSet<ND>::position_type const& q_min = params().grid_q_min(); // at cell center
        CoordSystem const &cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            for (long j = 0; j < N.y; ++j) {
                curvi.y = j + q_min.y;

                // 3rd row of contravariant metric tensor
                //
                vector_type const h3 = cs.contr_metric(curvi).z;
                value_type const cdtO2sqrtgh33 = cdtO2sqrtg/h3.z;

                // dt*c*curlB
                //
                vector_type const dE = {
                    +((B[i-_0][j-_0].z + B[i-_1][j-_0].z) -(B[i-_0][j-_1].z + B[i-_1][j-_1].z)) * cdtO2sqrtgh33,
                    -((B[i-_0][j-_0].z + B[i-_0][j-_1].z) -(B[i-_1][j-_0].z + B[i-_1][j-_1].z)) * cdtO2sqrtgh33,
                    +((B[i-_0][j-_0].y + B[i-_0][j-_1].y) -(B[i-_1][j-_0].y + B[i-_1][j-_1].y)) * cdtO2sqrtgh33
                    -((B[i-_0][j-_0].x + B[i-_1][j-_0].x) -(B[i-_0][j-_1].x + B[i-_1][j-_1].x)) * cdtO2sqrtgh33
                };

                // update E
                //
                vector_type &Ei = E[i][j];
                Ei.x += dE.x;
                Ei.y += dE.y + dE.z*h3.y;
                Ei.z += dE.z*h3.z;
                //
                Ei -= J[i][j] * dt;
            }
        }
    }
}
