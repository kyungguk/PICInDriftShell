//
//  PICDelegate.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 11/7/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICDelegate_h
#define PICDelegate_h

#include <PICKit/PICDelegate__ND.h>
#include <PICKit/PICInterThreadComm.h>
#include <PICKit/PICWorkerDelegate__1D.h>
#include <PICKit/PICWorkerDelegate__2D.h>
#include <PICKit/PICWorkerDelegate__3D.h>
#include <PICKit/PICMasterDelegate__1D.h>
#include <PICKit/PICMasterDelegate__2D.h>
#include <PICKit/PICMasterDelegate__3D.h>

#endif /* PICDelegate_h */
