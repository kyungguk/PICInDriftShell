//
//  PICMasterDelegate__3D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 6/29/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICMasterDelegate__3D_h
#define PICMasterDelegate__3D_h

#include <PICKit/PICMasterDelegate__ND.h>
#include <PICKit/PICWorkerDelegate__3D.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::MasterDelegate<ND>
    //
    template <>
    class MasterDelegate<3L> : public _MasterDelegate<3L> {
    public:
        ~MasterDelegate();
        explicit MasterDelegate(unsigned const number_of_worker_threads, Delegate<ND> *const delegate);

    protected:
        // notification; relay calls to internal delegate
        //
        void once(Domain<ND> *domain) override;
        bool will_do_next_cycle(Domain<ND> const* domain, long const i_step) override;

    private:
        // communication
        //
        void pass(Domain<ND> const*, Species<ND> &sp) override;
        void pass(Domain<ND> const*, BField<ND> &bfield) override;
        void pass(Domain<ND> const*, EField<ND> &efield) override;
        void pass(Domain<ND> const*, Current<ND> &current) override;
        void gather(Domain<ND> const*, Current<ND> &current) override;
        void gather(Domain<ND> const*, Species<ND> &sp) override;
        void partition(Species<ND>&, Direction const, UTL::DynamicArray<Particle<ND>> &, UTL::DynamicArray<Particle<ND>> &) override {}
        void pass(Domain<ND> const*, Direction const, UTL::DynamicArray<Particle<ND>> &, UTL::DynamicArray<Particle<ND>> &) override {}
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICMasterDelegate__3D_h */
