//
//  PICColdFluid__1D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 9/19/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICColdFluid__1D_h
#define PICColdFluid__1D_h

#include <PICKit/PICColdFluid__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::ColdFluid<ND>
    //
    template <>
    class ColdFluid<1L> : public _ColdFluid<1L> {
        ColdFluid &operator=(ColdFluid &&) = default;
        ColdFluid(ColdFluid &&) = default;

    public:
        ~ColdFluid();
        explicit ColdFluid() noexcept;
        explicit ColdFluid(ParameterSet<ND> const& params);

        // loading
        //
        void set_properties(value_type const Oc0ref, value_type const op0ref, NSmooth const n_smooth);
        void load(value_type const Oc0ref, value_type const op0ref, NSmooth const n_smooth);

        // update
        //
        virtual void update(EField<ND> const &efield, value_type const dt); // update flow velocity by dt; nV^n-1/2 -> nV^n+1/2
        virtual void collect_all_moments(); // calculate moment<2>

    private:
        inline void _update_V(vector_moment_type &nVcarts, scalar_moment_type const &n0, electric_field_type const &Ecarts, value_type const dt) const;
        inline void _collect_vv(tensor_moment_type &nvvcarts, scalar_moment_type const &n0, vector_moment_type const &nVcarts) const;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICColdFluid__1D_h */
