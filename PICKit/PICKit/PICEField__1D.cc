//
//  PICEField__1D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 3/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICEField__1D.h"
#include "PICEField__ND.hh"
#include "PICBField__1D.h"
#include "PICCurrent__1D.h"

constexpr long ND = 1;

// MARK:- PIC::__1_::EField<ND>
//
PIC::__1_::EField<ND>::~EField<ND>()
{
}
PIC::__1_::EField<ND>::EField() noexcept
: _EField{} {
}
PIC::__1_::EField<ND>::EField(ParameterSet<ND> const &params, WaveDamper<ND> const &damper)
: _EField{params}, _damper{damper} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params.N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
}
PIC::__1_::EField<ND>::EField(EField&& old, WaveDamper<ND> const &damper)
: _EField{std::move(old)}, _damper{damper} {
    if (UTL::reduce_bit_or(damper.masking_inset() > params().N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large damping inset");
    }
}

void PIC::__1_::EField<ND>::component_transform()
{
    _component_transform(*this);
}
void PIC::__1_::EField<ND>::_component_transform(_EField &E) const
{
    electric_field_type const& contr = E->contr;
    electric_field_type      & covar = E->covar;
    electric_field_type      & carts = E->carts;
    constexpr long pad = electric_field_type::pad_size();
    size_vector_type const N = contr.dims() + pad;
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // use cell center metric
        size_vector_type ijk{0}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            ijk.x = i;
            covar[i] = metric.contr_to_covar(contr[i], ijk);
            carts[i] = metric.contr_to_carts(contr[i], ijk);
        }
    } else {
        position_type const& q_min = params().grid_q_min(); // at cell center
        CoordSystem const &cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            covar[i] = cs.contr_to_covar(contr[i], curvi);
            carts[i] = cs.contr_to_carts(contr[i], curvi);
        }
    }

    // 'primed' covariant component
    //
    for (long i = -pad; i < N.x; ++i) {
        covar[i].z = contr[i].z;
    }
}

void PIC::__1_::EField<ND>::update(BField<ND> const &bfield, Current<ND> const &current, value_type const dt)
{
    value_type const cdtOsqrtg{ params().c()*dt/std::sqrt(params().coord_system().g()) };
    if (UTL::reduce_bit_or(damper().lo_boundary_mask() | damper().hi_boundary_mask())) { // at least one boundary section
        // phase retardation
        //
        (*this)->covar.fill(vector_type{0}); // temporarily holds curl B - J
        _update((*this)->covar, bfield->covar, cdtOsqrtg, current->contr, dt);
        damper()((*this)->covar, damper().phase_retardation());

        // amplitude damping
        //
        (*this)->contr += (*this)->covar;
        damper()((*this)->contr, damper().amplitude_damping());
    } else { // middle section
        _update((*this)->contr, bfield->covar, cdtOsqrtg, current->contr, dt);
    }
}
void PIC::__1_::EField<ND>::_update(electric_field_type &E, magnetic_field_type const &B, value_type cdtOsqrtg, current_density_type const &J, value_type const dt) const
{
    static_assert(magnetic_field_type::pad_size() >= 1, "invalid padding size");
    cdtOsqrtg *= 1;
    size_vector_type const N = E.dims();
    long const _1 = +long{params().grid_strategy() > 0}; // +1 when cell edge is leading; otherwise -0
    long const _0 = -long{params().grid_strategy() < 0}; // +0 when cell edge is leading; otherwise -1
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // use cell center metric
        size_vector_type ijk{0}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            ijk.x = i;

            // 3rd row of contravariant metric tensor
            //
            vector_type const h3 = metric.contr_metric(ijk).z;
            value_type const cdtOsqrtgh33 = cdtOsqrtg/h3.z;

            // dt*c*curlB
            //
            vector_type const dE = {
                0,
                -(B[i-_0].z -B[i-_1].z) * cdtOsqrtgh33,
                +(B[i-_0].y -B[i-_1].y) * cdtOsqrtgh33
            };

            // update E
            //
            vector_type &Ei = E[i];
            Ei.x += dE.x;
            Ei.y += dE.y + dE.z*h3.y;
            Ei.z += dE.z*h3.z;
            //
            Ei -= J[i] * dt;
        }
    } else {
        position_type const& q_min = params().grid_q_min(); // at cell center
        CoordSystem const &cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            curvi.x = i + q_min.x;

            // 3rd row of contravariant metric tensor
            //
            vector_type const h3 = cs.contr_metric(curvi).z;
            value_type const cdtOsqrtgh33 = cdtOsqrtg/h3.z;

            // dt*c*curlB
            //
            vector_type const dE = {
                0,
                -(B[i-_0].z -B[i-_1].z) * cdtOsqrtgh33,
                +(B[i-_0].y -B[i-_1].y) * cdtOsqrtgh33
            };

            // update E
            //
            vector_type &Ei = E[i];
            Ei.x += dE.x;
            Ei.y += dE.y + dE.z*h3.y;
            Ei.z += dE.z*h3.z;
            //
            Ei -= J[i] * dt;
        }
    }
}
