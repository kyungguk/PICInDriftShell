//
//  PICSource__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 4/21/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICSource__ND_hh
#define PICSource__ND_hh

#include "PICSource__ND.h"
#include "PICParticleDeposit.h"
#include <stdexcept>
#include <string>
#include <cmath>

// MARK:- PIC::__1_::_Source<ND>
//
template <long ND>
PIC::__1_::_Source<ND>::_Source(ParameterSet<ND> const& params)
: _params{params}, _pkg{new package_type{params.N().template take<reducedND>()}} {
    static_assert(vector_moment_type::pad_size() <= ParameterSet<ND>::pad_size, "insufficient padding");
}

template <long ND>
void PIC::__1_::_Source<ND>::_add(Descriptor desc)
{
    if (desc.duration < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative duration");
    }
    if (desc.ease_in < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative ease_in");
    }
    //
    // find neighboring grid indices for location
    //
    static_assert(vector_moment_type::pad_size() >= shape_type::order(), "invalid padding size");
    if (!UTL::reduce_bit_and((desc.location >= params().ptl_q_min()) & (desc.location < params().ptl_q_max()))) {
        return; // not in this domain; skip
    }
    typename ParameterSet<ND>::position_type const &q_min = params().grid_q_min(); // at cell center
    for (long i = 0; i < ND; ++i) {
        desc._shape.at(i) = shape_type{desc.location.at(i) - q_min.at(i)};
    }
    //
    // find ramp slope
    //
    constexpr value_type eps = 1e-30;
    (desc._slope = M_PI) /= desc.ease_in > eps ? desc.ease_in : 1.0;
    //
    _descs.push_back(desc);
}

template <long ND>
auto PIC::__1_::_Source<ND>::_ramp(Descriptor const &desc, value_type const _t) noexcept
-> value_type {
    value_type t = _t - desc.start;
    if (t < -desc.ease_in) { // before ease-in
        return 0;
    } else if (t < 0) { // ease-in phase
        return .5*(1 + std::cos(desc.slope()*t));
    } else if (t < desc.duration) { // middle phase
        return 1;
    } else if ((t -= desc.duration) < desc.ease_in) { // ease-out phase
        return .5*(1 + std::cos(desc.slope()*t));
    } else { // after ease-out
        return 0;
    }
}

template <long ND>
auto PIC::__1_::_Source<ND>::_Jcarts(Descriptor const &desc, value_type const t) noexcept
-> vector_type {
    complex_type const phase{0, -desc.omega*(t - desc.start)};
    complex_type const exp = std::exp(phase);
    vector_type J{
        (desc.J0carts.x * exp).real(),
        (desc.J0carts.y * exp).real(),
        (desc.J0carts.z * exp).real()
    };
    return J *= _ramp(desc, t);
}

#endif /* PICSource__ND_hh */
