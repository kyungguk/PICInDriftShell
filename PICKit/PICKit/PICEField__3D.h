//
//  PICEField__3D.h
//  PICKit
//
//  Created by Kyungguk Min on 4/2/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICEField__3D_h
#define PICEField__3D_h

#include <PICKit/PICEField__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::EField<ND>
    //
    template <>
    class EField<3L> : public _EField<3L> {
        EField(EField &&) = default;
        EField &operator=(EField &&) = default;

    public:
        ~EField();
        explicit EField() noexcept;
        explicit EField(ParameterSet<ND> const& params, WaveDamper<ND> const &damper = {});
        explicit EField(EField&& old, WaveDamper<ND> const &damper);

        WaveDamper<ND - 1> const &damper() const noexcept { return _damper; }

        // update
        //
        virtual void update(BField<ND> const &bfield, Current<ND> const &current, value_type const dt);
        virtual void component_transform();
    private:
        inline void _component_transform(_EField &E) const;
        inline void _update(electric_field_type &E, magnetic_field_type const &B, value_type cdtOsqrtg, current_density_type const &J, value_type const dt) const;

        WaveDamper<ND - 1> _damper;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICEField__3D_h */
