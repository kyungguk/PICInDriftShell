//
//  PICCurrent__3D.cc
//  PICKit
//
//  Created by Kyungguk Min on 4/2/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICCurrent__3D.h"
#include "PICCurrent__ND.hh"
#include "PICSpecies__3D.h"
#include "PICColdFluid__3D.h"
#include "PICSource__3D.h"

constexpr long ND = 3;

// MARK:- PIC::__1_::Current<ND>
//
PIC::__1_::Current<ND>::~Current<ND>()
{
}
PIC::__1_::Current<ND>::Current() noexcept
: _Current{} {
}
PIC::__1_::Current<ND>::Current(ParameterSet<ND> const &params)
: _Current{params} {
}

void PIC::__1_::Current<ND>::component_transform()
{
    _component_transform(*this);
}
void PIC::__1_::Current<ND>::_component_transform(_Current &J) const
{
    current_density_type const& mfa   = J->mfa;
    current_density_type      & contr = J->contr;
    constexpr long pad = current_density_type::pad_size();
    size_vector_type const N = mfa.dims() + pad;
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // use cell center metric
        MetricTable<ND>::size_vector_type ijk = {0, 0, std::get<2>(params().N())/2}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            ijk.x = i;
            for (long j = -pad; j < N.y; ++j) {
                ijk.y = j;
                contr[i][j] = metric.carts_to_contr(metric.mfa_to_carts(mfa[i][j], ijk), ijk);
            }
        }
    } else {
        ParameterSet<ND>::position_type const& q_min = params().grid_q_min(); // at cell center
        CoordSystem const &cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            for (long j = -pad; j < N.y; ++j) {
                curvi.y = j + q_min.y;
                contr[i][j] = cs.carts_to_contr(cs.mfa_to_carts(mfa[i][j], curvi), curvi);
            }
        }
    }
}

auto PIC::__1_::Current<ND>::operator+=(Species<ND> const &sp)
-> Current& {
    _accumulate((*this)->mfa, sp.moment<1>(), sp.current_density_conversion_factor());
    return *this;
}
auto PIC::__1_::Current<ND>::operator+=(ColdFluid<ND> const &cf)
-> Current& {
    _accumulate((*this)->mfa, cf.moment<1>(), cf.current_density_conversion_factor());
    return *this;
}
auto PIC::__1_::Current<ND>::operator+=(Source<ND> const &src)
-> Current& {
    _accumulate((*this)->mfa, src.moment<1>(), src.current_density_conversion_factor());
    return *this;
}

void PIC::__1_::Current<ND>::_accumulate(current_density_type &Jmfa, vector_moment_type const &Vcarts, value_type const &weight) const
{
    static_assert(current_density_type::pad_size() <= vector_moment_type::pad_size(), "invalid padding size");
    constexpr long pad = current_density_type::pad_size();
    size_vector_type const N = Jmfa.dims() + pad;
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // use cell center metric
        MetricTable<ND>::size_vector_type ijk = {0, 0, std::get<2>(params().N())/2}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            ijk.x = i;
            for (long j = -pad; j < N.y; ++j) {
                ijk.y = j;
                Jmfa[i][j] += weight*metric.carts_to_mfa(Vcarts[i][j], ijk);
            }
        }
    } else {
        ParameterSet<ND>::position_type const& q_min = params().grid_q_min(); // at cell center
        CoordSystem const& cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = -pad; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            for (long j = -pad; j < N.y; ++j) {
                curvi.y = j + q_min.y;
                Jmfa[i][j] += weight*cs.carts_to_mfa(Vcarts[i][j], curvi);
            }
        }
    }
}
