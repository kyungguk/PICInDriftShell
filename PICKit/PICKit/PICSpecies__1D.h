//
//  PICSpecies__1D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 3/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICSpecies__1D_h
#define PICSpecies__1D_h

#include <PICKit/PICSpecies__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::Species<ND>
    //
    template <>
    class Species<1L> : public _Species<1L> {
        Species &operator=(Species &&) = default;
        Species(Species &&) = default;

    public:
        ~Species();
        explicit Species() noexcept;
        explicit Species(ParameterSet<ND> const& params);

        // loading
        //
        void set_properties(value_type const Oc0ref, value_type const op0ref, value_type const Nc0ref, ShapeOrder const shape_order, NSmooth const n_smooth);
        void load(value_type const Oc0ref, value_type const op0ref, size_type const Nc, ShapeOrder const shape_order, NSmooth const n_smooth, ParticleLoader<ND> const& loader);

        // update
        //
        virtual void update(BField<ND> const &bfield, EField<ND> const &efield, value_type const dt); // update velocity by full dt; v^n-1/2 -> v^n+1/2
        virtual void update(value_type const dt, value_type const fraction_of_cell_size_allowed_to_travel); // update x^n -> x^n+1
        virtual void collect_vector_moment(); // collected at full grid
        virtual void collect_all_moments(); // collected at full grid

    private:
        void (Species::*_update_velocity)(bucket_type&, magnetic_field_type const&, electric_field_type const&, value_type const) const;
        void (Species::*_collect_V)(vector_moment_type&, bucket_type const&) const;
        magnetic_field_type const& _full_dB(magnetic_field_type const& H);

        inline bool _update_position(bucket_type &bucket, value_type const dt, value_type const travel_scale_factor) const;
        template <long Order>
        void _update_velocity_(bucket_type &bucket, magnetic_field_type const &dB, electric_field_type const &dE, value_type const dt) const;

        template <long Order>
        void _collect_V_(vector_moment_type &nV, bucket_type const &bucket) const;
        inline void _collect_all(scalar_moment_type &n, vector_moment_type &nV, tensor_moment_type &nvv, bucket_type const &bucket) const;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICSpecies__1D_h */
