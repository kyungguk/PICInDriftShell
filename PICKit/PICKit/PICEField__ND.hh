//
//  PICEField__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICEField__ND_hh
#define PICEField__ND_hh

#include "PICEField__ND.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath>

// MARK: ArrayND Operator Overloadings
//
namespace {
    template <class T, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad>& operator+=(UTL::ArrayND<T, ND, Pad>& lhs, UTL::ArrayND<T, ND, Pad> const& rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        auto r_first = rhs.pad_begin();
        while (l_first != l_last) {
            *l_first++ += *r_first++;
        }
        return lhs;
    }
}


// MARK:- PIC::__1_::_EField<ND>
//
template <long ND>
PIC::__1_::_EField<ND>::_EField(ParameterSet<ND> const& params)
: _params{params}, _E{new triplet_type{params.N().template take<reducedND>()}} {
    static_assert(electric_field_type::pad_size() <= ParameterSet<ND>::pad_size, "insufficient padding");
}

#endif /* PICEField__ND_hh */
