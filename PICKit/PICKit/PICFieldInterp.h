//
//  PICFieldInterp.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 1/25/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICFieldInterp_h
#define PICFieldInterp_h

#include <UtilityKit/UtilityKit.h>

// MARK: Grid-to-Particle Interpolation
//
namespace {
    template <long S>
    using _Shape = UTL::Shape<double, S>;

    // MARK: interp::shape<1>
    //
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 1, Pad> const &f, _Shape<1> const &sx) {
        static_assert(Pad >= 1, "Not enough padding");
        return f[sx.i<0>()]*sx.w<0>()
        +      f[sx.i<1>()]*sx.w<1>();
    }
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 2, Pad> const &f, _Shape<1> const &sx, _Shape<1> const &sy) {
        return interp(f[sx.i<0>()], sy)*sx.w<0>()
        +      interp(f[sx.i<1>()], sy)*sx.w<1>();
    }
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 3, Pad> const &f, _Shape<1> const &sx, _Shape<1> const &sy, _Shape<1> const &sz) {
        return interp(f[sx.i<0>()], sy, sz)*sx.w<0>()
        +      interp(f[sx.i<1>()], sy, sz)*sx.w<1>();
    }

    // MARK: interp::shape<2>
    //
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 1, Pad> const &f, _Shape<2> const &sx) {
        static_assert(Pad >= 2, "Not enough padding");
        return f[sx.i<0>()]*sx.w<0>()
        +      f[sx.i<1>()]*sx.w<1>()
        +      f[sx.i<2>()]*sx.w<2>();
    }
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 2, Pad> const &f, _Shape<2> const &sx, _Shape<2> const &sy) {
        return interp(f[sx.i<0>()], sy)*sx.w<0>()
        +      interp(f[sx.i<1>()], sy)*sx.w<1>()
        +      interp(f[sx.i<2>()], sy)*sx.w<2>();
    }
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 3, Pad> const &f, _Shape<2> const &sx, _Shape<2> const &sy, _Shape<2> const &sz) {
        return interp(f[sx.i<0>()], sy, sz)*sx.w<0>()
        +      interp(f[sx.i<1>()], sy, sz)*sx.w<1>()
        +      interp(f[sx.i<2>()], sy, sz)*sx.w<2>();
    }

    // MARK: interp::shape<3>
    //
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 1, Pad> const &f, _Shape<3> const &sx) {
        static_assert(Pad >= 3, "Not enough padding");
        return f[sx.i<0>()]*sx.w<0>()
        +      f[sx.i<1>()]*sx.w<1>()
        +      f[sx.i<2>()]*sx.w<2>()
        +      f[sx.i<3>()]*sx.w<3>();
    }
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 2, Pad> const &f, _Shape<3> const &sx, _Shape<3> const &sy) {
        return interp(f[sx.i<0>()], sy)*sx.w<0>()
        +      interp(f[sx.i<1>()], sy)*sx.w<1>()
        +      interp(f[sx.i<2>()], sy)*sx.w<2>()
        +      interp(f[sx.i<3>()], sy)*sx.w<3>();
    }
    template <class T, long Pad>
    inline T interp(UTL::ArrayND<T, 3, Pad> const &f, _Shape<3> const &sx, _Shape<3> const &sy, _Shape<3> const &sz) {
        return interp(f[sx.i<0>()], sy, sz)*sx.w<0>()
        +      interp(f[sx.i<1>()], sy, sz)*sx.w<1>()
        +      interp(f[sx.i<2>()], sy, sz)*sx.w<2>()
        +      interp(f[sx.i<3>()], sy, sz)*sx.w<3>();
    }
}

#endif /* PICFieldInterp_h */
