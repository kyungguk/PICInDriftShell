//
//  PICKit-config.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/29/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICKit_config_h
#define PICKit_config_h


// root namespace
//
#ifndef PICKIT_NAMESPACE
#define PICKIT_NAMESPACE PIC
#define PICKIT_BEGIN_NAMESPACE namespace PICKIT_NAMESPACE {
#define PICKIT_END_NAMESPACE }
#endif


// GCC Deprecated attribute
//
#ifndef PICKIT_DEPRECATED_ATTRIBUTE
#define PICKIT_DEPRECATED_ATTRIBUTE __attribute__ ((deprecated))
#endif


// aggregate particle pass for multi-threaded version
//
#ifndef PICKIT_MULTI_THREAD_AGGREGATE_PARTICLE_PASS
#define PICKIT_MULTI_THREAD_AGGREGATE_PARTICLE_PASS 0
#endif


// GEOKit version selection
//
#include <GeometryKit/GeometryKit.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::MetricTable;
    using GEO::__1_::ParameterSet;
    using GEO::__1_::WaveDamper;
    using GEO::__1_::ParticleLoader;

    struct ShapeOrder {
        long v;
        constexpr operator long() const noexcept { return v; }
        constexpr explicit ShapeOrder(long v) noexcept : v(v) {}
    };
    struct NSmooth {
        unsigned v;
        constexpr operator unsigned() const noexcept { return v; }
        constexpr explicit NSmooth(unsigned v) noexcept : v(v) {}
    };
}
PICKIT_END_NAMESPACE


#endif /* PICKit_config_h */
