//
//  PICSource__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 4/21/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICSource__ND_h
#define PICSource__ND_h

#include <PICKit/PICKit-config.h>
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <memory>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class Source;

    // MARK:- PIC::__1_::_Source<ND>
    //
    template <long _ND>
    class _Source {
    public:
        static constexpr long ND = _ND;
        static constexpr long reducedND = ND == 3 ? ND - 1 : ND;
        static constexpr long pad_size = 3;

        // types
        //
        using size_type = long;
        using value_type = double;
        using vector_type = UTL::Vector<value_type, 3L>;
        using position_type = UTL::Vector<value_type, reducedND>;
        using size_vector_type = UTL::Vector<size_type, reducedND>;
        using complex_type = UTL::SIMDComplex<value_type>;
        using complex_vector_type = UTL::Vector<complex_type, 3>;
        using shape_type = UTL::Shape<value_type, 1>;
        struct Descriptor { // J0*exp(-i*omega*(t - start))
            UTL::Vector<value_type, ND> location{}; // source location
            complex_vector_type J0carts{}; // cartesian vector components of complex external current amplitude
            value_type omega{}; // angular frequency
            value_type start{}; // start time
            value_type duration{}; // duration; non-negative
            value_type ease_in{}; // ease-in/-out duration; non-negative
            explicit Descriptor() noexcept {}
            template <long i>
            shape_type const &shape() const noexcept { return std::get<i>(_shape); }
            value_type const &slope() const noexcept { return _slope; }
        private:
            UTL::Vector<shape_type, ND> _shape{}; // for interpolation
            value_type _slope{1}; // ease-in/-out ramp slope
            friend _Source;
        };
        using vector_moment_type = UTL::ArrayND<vector_type, reducedND, pad_size>; // cartesian components
        using moment_triplet_type = std::tuple<decltype(nullptr), vector_moment_type, decltype(nullptr)>;
        struct package_type {
            moment_triplet_type moments;
            explicit package_type(size_vector_type const &N) : moments{std::make_tuple(nullptr, vector_moment_type{N}, nullptr)} {}
        };

        // accessors
        //
        ParameterSet<ND> const& params() const noexcept { return _params; }
        UTL::DynamicArray<Descriptor> const &descriptors() const noexcept { return _descs; }

        static value_type current_density_conversion_factor() noexcept { return 1; }

        template <long rank> typename std::tuple_element<rank, moment_triplet_type
        >::type const& moment() const noexcept { return std::get<rank>((*this)->moments); }
        template <long rank> typename std::tuple_element<rank, moment_triplet_type
        >::type      & moment()       noexcept { return std::get<rank>((*this)->moments); }

    protected:
        _Source(_Source &&) = default;
        _Source &operator=(_Source &&) = default;
        virtual ~_Source() {}
        explicit _Source() noexcept {}
        explicit _Source(ParameterSet<ND> const& params);

        void _add(Descriptor desc);
        static value_type _ramp(Descriptor const &desc, value_type const t) noexcept;
        static vector_type _Jcarts(Descriptor const &desc, value_type const t) noexcept;

        package_type const* operator->() const noexcept { return _pkg.get(); }
        package_type      * operator->()       noexcept { return _pkg.get(); }

    private:
        ParameterSet<ND> _params;
        std::unique_ptr<package_type> _pkg;
        UTL::DynamicArray<Descriptor> _descs{};
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICSource__ND_h */
