//
//  PICSpecies__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICSpecies__ND_hh
#define PICSpecies__ND_hh

#include "PICSpecies__ND.h"
#include "PICFieldInterp.h"
#include "PICParticlePush.h"
#include "PICParticleDeposit.h"
#include <type_traits>
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath>

// MARK: ArrayND Operator Overloadings
//
namespace {
    template <class T, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad>& operator+=(UTL::ArrayND<T, ND, Pad>& lhs, UTL::ArrayND<T, ND, Pad> const& rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        auto r_first = rhs.pad_begin();
        while (l_first != l_last) {
            *l_first++ += *r_first++;
        }
        return lhs;
    }
    //
    template <class T, long ND, long Pad, class U>
    inline UTL::ArrayND<T, ND, Pad> &operator*=(UTL::ArrayND<T, ND, Pad> &lhs, U const &rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        while (l_first != l_last) {
            *l_first++ *= rhs;
        }
        return lhs;
    }
}

// MARK: Helpers
//
namespace {
    using BorisPush = Boris_B;
    constexpr double quiet_nan = std::numeric_limits<double>::quiet_NaN();
    //
    inline UTL::Vector<long, 1> d2i_vector(UTL::Vector<double, 1> const &d) noexcept {
        return {long(d.x)};
    }
    inline UTL::Vector<long, 2> d2i_vector(UTL::Vector<double, 2> const &d) noexcept {
        return {long(d.x), long(d.y)};
    }
    inline UTL::Vector<long, 3> d2i_vector(UTL::Vector<double, 3> const &d) noexcept {
        return {long(d.x), long(d.y), long(d.z)};
    }
    //
    template <class T>
    inline UTL::Vector<T, 3> cross(UTL::Vector<T, 3> const &A, UTL::Vector<T, 3> const &B) noexcept {
        return {-A.z*B.y + A.y*B.z, A.z*B.x - A.x*B.z, -A.y*B.x + A.x*B.y};
    }
}


// MARK:- PIC::__1_::_Species<ND>
//
template <long ND>
PIC::__1_::_Species<ND>::_Species(ParameterSet<ND> const& params)
: _params{params}, _Oc0ref{quiet_nan}, _op0ref{quiet_nan}, _Nc0ref{quiet_nan}, _bucket{new bucket_type}, _pkg{new package_type{params.N().template take<reducedND>()}} {
    static_assert(vector_moment_type::pad_size() <= ParameterSet<ND>::pad_size, "insufficient padding");
}
template <long ND>
void PIC::__1_::_Species<ND>::set_properties(value_type const Oc0ref, value_type const op0ref, value_type const Nc0ref, ShapeOrder const shape_order, NSmooth const n_smooth)
{
    _Oc0ref = Oc0ref;
    _op0ref = op0ref;
    _Nc0ref = Nc0ref;
    _shape_order = shape_order;
    _n_smooth = n_smooth;
    if (params().grid_strategy() > 0 && shape_order > 1) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - shape order cannot be greater than 1 for grid strategy other than collocate");
    }
}

#endif /* PICSpecies__ND_hh */
