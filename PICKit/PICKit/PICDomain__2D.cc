//
//  PICDomain__2D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "PICDomain__2D.h"
#include "PICDomain__ND.hh"
#include "PICBField__2D.h"
#include "PICEField__2D.h"
#include "PICCurrent__2D.h"
#include "PICSpecies__2D.h"
#include "PICColdFluid__2D.h"
#include "PICDelegate__2D.h"

constexpr long ND = 2;

// MARK:- PIC::__1_::Domain<ND>
//
PIC::__1_::Domain<ND>::~Domain<ND>()
{
}
PIC::__1_::Domain<ND>::Domain() noexcept
: _Domain{} {
}
PIC::__1_::Domain<ND>::Domain(ParameterSet<ND> const& params, value_type const dt, WaveDamper<ND> const& damper)
: _Domain{params, dt, damper} {
}

void PIC::__1_::Domain<ND>::advance_by(size_type const n_steps)
{
    _advance_by(n_steps);
}
