//
//  PICDomain__2D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICDomain__2D_h
#define PICDomain__2D_h

#include <PICKit/PICDomain__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::Domain<ND>
    //
    template <>
    class Domain<2L> : public _Domain<2L> {
        Domain(Domain &&) = default;
        Domain &operator=(Domain &&) = default;

    public:
        ~Domain();
        explicit Domain() noexcept;
        explicit Domain(ParameterSet<ND> const& params, value_type const dt, WaveDamper<ND> const& damper = {});

        // update
        //
        void advance_by(size_type const n_steps) override;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICDomain__2D_h */
