//
//  PICBField__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICBField__ND_hh
#define PICBField__ND_hh

#include "PICBField__ND.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath>

// MARK: ArrayND Operator Overloadings
//
namespace {
    template <class T, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad>& operator+=(UTL::ArrayND<T, ND, Pad>& lhs, UTL::ArrayND<T, ND, Pad> const& rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        auto r_first = rhs.pad_begin();
        while (l_first != l_last) {
            *l_first++ += *r_first++;
        }
        return lhs;
    }
}


// MARK:- PIC::__1_::_BField<ND>
//
template <long ND>
PIC::__1_::_BField<ND>::_BField(ParameterSet<ND> const& params)
: _params{params}, _B{new triplet_type{params.N().template take<reducedND>()}} {
    static_assert(magnetic_field_type::pad_size() <= ParameterSet<ND>::pad_size, "insufficient padding");
}

#endif /* PICBField__ND_hh */
