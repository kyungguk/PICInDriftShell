//
//  PICWorkerDelegate__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 7/1/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICWorkerDelegate__ND_hh
#define PICWorkerDelegate__ND_hh

#include "PICWorkerDelegate__ND.h"
#include "PICDelegate__ND.hh"
#include <stdexcept>
#include <algorithm>
#include <iterator>

// MARK:- PIC::__1_::_WorkerDelegate<ND>
//
template <long ND>
PIC::__1_::_WorkerDelegate<ND>::_WorkerDelegate(long const id, MasterDelegate<ND> *const master)
: id{id}, master{master} {
}

template <long ND>
void PIC::__1_::_WorkerDelegate<ND>::_pass(Domain<ND> const*, Species<ND> &sp, Direction const dir)
{
    WorkerDelegate<ND> *self = static_cast<WorkerDelegate<ND> *>(this);
    constexpr std::integral_constant<long, 0> tag{};
    using Bucket [[maybe_unused]] = typename std::remove_reference<decltype(sp.bucket())>::type;

#if defined(PICKIT_MULTI_THREAD_AGGREGATE_PARTICLE_PASS) && PICKIT_MULTI_THREAD_AGGREGATE_PARTICLE_PASS
    L_bucket.clear();
    R_bucket.clear();
    master->delegate->partition(sp, dir, L_bucket, R_bucket);

    // 1. send to master
    //
    worker_to_master.send(*self, tag, std::make_tuple(sp.bucket().size(), static_cast<Bucket const*>(&L_bucket), static_cast<Bucket const*>(&R_bucket))).wait();

    // 2. recv from master
    //
    {
        using Payload = std::tuple<long, long, Bucket const*, Bucket const*>;
        auto const pkt = master_to_worker.recv(*self, tag);
        Payload const payload = pkt.template payload<Payload>();
        Bucket const &L_bucket = *std::get<2>(payload);
        Bucket const &R_bucket = *std::get<3>(payload);
        long offset = std::get<0>(payload);
        long length = std::get<1>(payload);
        for (long const n = L_bucket.size(); offset < n && length > 0; ++offset, --length) {
            sp.bucket().push_back(L_bucket[offset]);
        }
        offset -= L_bucket.size();
        for (long const n = R_bucket.size(); offset < n && length > 0; ++offset, --length) {
            sp.bucket().push_back(R_bucket[offset]);
        }
    }
#else
    L_bucket.clear();
    R_bucket.clear();
    master->delegate->partition(sp, dir, L_bucket, R_bucket);
    worker_to_master.send(*self, tag, std::make_tuple(&L_bucket, &R_bucket)).wait();
    std::move(L_bucket.begin(), L_bucket.end(), std::back_inserter(sp.bucket()));
    std::move(R_bucket.begin(), R_bucket.end(), std::back_inserter(sp.bucket()));
#endif
}

template <long ND>
template <long i, class T, long Rank, long Pad>
void PIC::__1_::_WorkerDelegate<ND>::recv_from_master(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &buffer)
{
    WorkerDelegate<ND> *self = static_cast<WorkerDelegate<ND> *>(this);

    using ArrayND = UTL::ArrayND<T, Rank, Pad>;
    auto const pkt = master_to_worker.recv(*self, tag);
    buffer = *pkt.template payload<ArrayND const*>();
}
template <long ND>
template <long i, class T, long Rank, long Pad>
void PIC::__1_::_WorkerDelegate<ND>::reduce_to_master(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &payload)
{
    reduce_divide_and_conquer(tag, payload);
    accumulate_by_worker(tag, payload);
}

template <long ND>
template <long i, class T, long Rank, long Pad>
void PIC::__1_::_WorkerDelegate<ND>::reduce_divide_and_conquer(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &payload)
{
    WorkerDelegate<ND> *self = static_cast<WorkerDelegate<ND> *>(this);

    // divide and conquer
    // e.g., assume 9 worker threads (arrow indicating where data are accumulated)
    // stride = 1: [0 <- 1], [2 <- 3], [4 <- 5], [6 <- 7], 8
    // stride = 2: [0 <- 2], [4 <- 6], 8
    // stride = 4: [0 <- 4], 8
    // stride = 8: [0 <- 8]
    //
    long const n_workers = master->workers().size();
    long const id = self - master->workers().begin();
    for (long stride = 1; stride < n_workers; stride *= 2) {
        long const divisor = stride * 2;
        if (id % divisor == 0 && id + stride < n_workers) {
            (this + stride)->worker_to_worker.send(*self, tag, &payload).wait();
        }
    }
}
template <long ND>
template <long i, class T, long Rank, long Pad>
void PIC::__1_::_WorkerDelegate<ND>::accumulate_by_worker(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> const &payload)
{
    WorkerDelegate<ND> *self = static_cast<WorkerDelegate<ND> *>(this);

    using ArrayND = UTL::ArrayND<T, Rank, Pad>;
    if (this == master->workers().begin()) {
        auto const pkt = master_to_worker.recv(*self, tag);
        ArrayND *buffer = pkt.template payload<ArrayND*>();
        *buffer += payload;
    } else {
        auto const pkt = worker_to_worker.recv(*self, tag);
        ArrayND *buffer = pkt.template payload<ArrayND*>();
        *buffer += payload;
    }
}

#endif /* PICWorkerDelegate__ND_hh */
