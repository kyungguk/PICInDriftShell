//
//  PICDomain__3D.cc
//  PICKit
//
//  Created by Kyungguk Min on 4/2/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICDomain__3D.h"
#include "PICDomain__ND.hh"
#include "PICBField__3D.h"
#include "PICEField__3D.h"
#include "PICCurrent__3D.h"
#include "PICSpecies__3D.h"
#include "PICColdFluid__3D.h"
#include "PICDelegate__3D.h"

constexpr long ND = 3;

// MARK:- PIC::__1_::Domain<ND>
//
PIC::__1_::Domain<ND>::~Domain<ND>()
{
}
PIC::__1_::Domain<ND>::Domain() noexcept
: _Domain{} {
}
PIC::__1_::Domain<ND>::Domain(ParameterSet<ND> const& params, value_type const dt, WaveDamper<ND> const& damper)
: _Domain{params, dt, damper} {
}

void PIC::__1_::Domain<ND>::advance_by(size_type const n_steps)
{
    _advance_by(n_steps);
}
