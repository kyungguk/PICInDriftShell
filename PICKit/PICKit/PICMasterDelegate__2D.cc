//
//  PICMasterDelegate__2D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 6/28/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#include "PICMasterDelegate__2D.h"
#include "PICMasterDelegate__ND.hh"
#include "PICWorkerDelegate__2D.h"
#include "PICBField__2D.h"
#include "PICEField__2D.h"
#include "PICCurrent__2D.h"
#include "PICSpecies__2D.h"

constexpr long ND = 2;

// MARK:- PIC::__1_::MasterDelegate<2D>
//
PIC::__1_::MasterDelegate<ND>::~MasterDelegate<ND>()
{
}
PIC::__1_::MasterDelegate<ND>::MasterDelegate(unsigned const number_of_worker_threads, Delegate<ND> *const delegate)
: _MasterDelegate{delegate}
{
    if (!delegate) {
        throw std::invalid_argument{__PRETTY_FUNCTION__};
    }
    for (long i = 0; i < number_of_worker_threads; ++i) {
        _workers.emplace_back(i, this);
    }
}

void PIC::__1_::MasterDelegate<ND>::once(Domain<ND> *domain)
{
    delegate->once(domain);
}
bool PIC::__1_::MasterDelegate<ND>::will_do_next_cycle(Domain<ND> const* domain, long const i_step)
{
    return delegate->will_do_next_cycle(domain, i_step);
}

void PIC::__1_::MasterDelegate<ND>::pass(Domain<ND> const* domain, Species<ND> &sp)
{
    // pass q2-dir
    //
    _pass(domain, sp, Direction::q2);

    // pass q1-dir
    //
    _pass(domain, sp, Direction::q1);
}
void PIC::__1_::MasterDelegate<ND>::pass(Domain<ND> const* domain, BField<ND> &bfield)
{
    constexpr std::integral_constant<long, 0> tag{};

    delegate->pass(domain, bfield);
    broadcast_to_workers(tag, bfield->contr);
}
void PIC::__1_::MasterDelegate<ND>::pass(Domain<ND> const* domain, EField<ND> &efield)
{
    constexpr std::integral_constant<long, 0> tag{};

    delegate->pass(domain, efield);
    broadcast_to_workers(tag, efield->contr);
}
void PIC::__1_::MasterDelegate<ND>::pass(Domain<ND> const* domain, Current<ND> &current)
{
    constexpr std::integral_constant<long, 0> tag{};

    delegate->pass(domain, current);
    broadcast_to_workers(tag, current->mfa);
}
void PIC::__1_::MasterDelegate<ND>::gather(Domain<ND> const* domain, Current<ND> &current)
{
    constexpr std::integral_constant<long, 0> tag{};

    collect_from_workers(tag, current->mfa);
    delegate->gather(domain, current);
    broadcast_to_workers(tag, current->mfa);
}
void PIC::__1_::MasterDelegate<ND>::gather(Domain<ND> const* domain, Species<ND> &sp)
{
    constexpr std::integral_constant<long, 0> tag{};

    {
        collect_from_workers(tag, sp.moment<0>());
        collect_from_workers(tag, sp.moment<1>());
        collect_from_workers(tag, sp.moment<2>());
    }
    delegate->gather(domain, sp);
}
