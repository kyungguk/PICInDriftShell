//
//  PICDomain__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICDomain__ND_hh
#define PICDomain__ND_hh

#include "PICDomain__ND.h"
#include <stdexcept>
#include <string>

// MARK: ArrayND Operator Overloadings
//
namespace {
    template <class T, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad>& operator+=(UTL::ArrayND<T, ND, Pad>& lhs, UTL::ArrayND<T, ND, Pad> const& rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        auto r_first = rhs.pad_begin();
        while (l_first != l_last) {
            *l_first++ += *r_first++;
        }
        return lhs;
    }
    template <class T, class U, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad>& operator*=(UTL::ArrayND<T, ND, Pad>& lhs, U const& rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        while (l_first != l_last) {
            *l_first++ *= rhs;
        }
        return lhs;
    }
}


// MARK:- PIC::__1_::_Domain<ND>
//
template <long ND>
PIC::__1_::_Domain<ND>::_Domain(ParameterSet<ND> const& params, value_type const dt, WaveDamper<ND> const& damper)
: _params{params}, _damper{damper}, _dt{dt} {
    // argument checks
    //
    if (dt <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid argument(s)");
    }
    // allocations
    //
    std::get<0>(_bfield).reset(new BField<ND>{params, damper});
    std::get<1>(_bfield).reset(new BField<ND>{params, damper});
    std::get<0>(_efield).reset(new EField<ND>{params, damper});
    std::get<0>(_current).reset(new Current<ND>{params});
    std::get<1>(_current).reset(new Current<ND>{params});
    _species.reset(new _SpeciesArray);
    _cold_fluids.reset(new _ColdFluidArray);
}

template <long ND>
void PIC::__1_::_Domain<ND>::_advance_by(const size_type n_steps)
{
    Domain<ND> *domain = static_cast<Domain<ND> *>(this);
    // pre-process
    //
    if (_is_first_pass) {
        _is_first_pass = false;
        if (nullptr == delegate()) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - no delegate is set");
        }
        delegate()->once(domain);
        // NOTE: pass may not be needed for BC other than periodic
        delegate()->pass(domain, bfield()), bfield().component_transform();
        delegate()->pass(domain, efield()), efield().component_transform();
    }
    // cycle
    //
    for (size_type i_step = 1; delegate()->will_do_next_cycle(domain, i_step-1) && i_step <= n_steps; ++i_step) {
        _cycle(domain);
    }
    // post-process
    //
    for (Species<ND> &sp : species()) {
        sp.collect_all_moments(); // all moments collected at full grid
        delegate()->gather(domain, sp); // should gather all moments
    }
    for (ColdFluid<ND> &cf : cold_fluids()) {
        cf.collect_all_moments(); // all moments collected at full grid
    }
}

template <long ND>
void PIC::__1_::_Domain<ND>::_cycle(Domain<ND> const* domain)
{
    //
    // 1. update B<0> from n-1/2 to n+1/2 using E at n
    //    B<1> = (B(n-1/2) + B(n+1/2))/2, so B<1> is at a full time step (n)
    //
    bfield<1>()->carts = bfield<0>()->carts; // this is for velocity update
    bfield<0>().update(efield(), dt()); // ** Be careful with masking when splitting B update into two dt/2 **
    delegate()->pass(domain, bfield<0>()), bfield<0>().component_transform();
    (bfield<1>()->carts += bfield<0>()->carts) *= 0.5; // this is for velocity update
    //
    // 2 & 3. update velocities and positions by dt and collect current density
    //
    current().reset();
    delegate()->gather(domain, current()); // this is to account for Source term (see test how to setup delegate for source term)
    //
    for (Species<ND> &sp : species()) {
        sp.update(bfield<1>(), efield(), dt()); // v(n-1/2) -> v(n+1/2)
        sp.update(0.5*dt(), 0.5), delegate()->pass(domain, sp); // x(n) -> x(n+1/2)
        sp.collect_vector_moment(), current()->mfa += collect(domain, current<1>(), sp)->mfa; // J(n+1/2)
        sp.update(0.5*dt(), 0.5), delegate()->pass(domain, sp); // x(n+1/2) -> x(n+1)
    }
    for (ColdFluid<ND> &cf : cold_fluids()) {
        cf.update(efield(), dt()), current()->mfa += collect(domain, current<1>(), cf)->mfa; // nV^n-1/2 -> nV^n+1/2; J(n+1/2)
    }
    //
    current().component_transform();
    //
    // 5. update E from n to n+1 using B and J at n+1/2
    //
    efield().update(bfield<0>(), current(), dt());
    delegate()->pass(domain, efield()), efield().component_transform();
}
template <long ND>
template <class Species>
auto PIC::__1_::_Domain<ND>::collect(Domain<ND> const* domain, Current<ND> &J, Species const &sp) const
-> Current<ND> const &{
    J.reset();
    //
    // collect & gather J
    //
    J += sp;
    delegate()->gather(domain, J);
    //
    // optional smoothing
    //
    for (unsigned i = 0; i < sp.number_of_smoothing_passes(); ++i) {
        delegate()->pass(domain, J), J.smooth();
    }
    //
    return J;
}

#endif /* PICDomain__ND_hh */
