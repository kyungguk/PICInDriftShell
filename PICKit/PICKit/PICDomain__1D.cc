//
//  PICDomain__1D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 3/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICDomain__1D.h"
#include "PICDomain__ND.hh"
#include "PICBField__1D.h"
#include "PICEField__1D.h"
#include "PICCurrent__1D.h"
#include "PICSpecies__1D.h"
#include "PICColdFluid__1D.h"
#include "PICDelegate__1D.h"

constexpr long ND = 1;

// MARK:- PIC::__1_::Domain<ND>
//
PIC::__1_::Domain<ND>::~Domain<ND>()
{
}
PIC::__1_::Domain<ND>::Domain() noexcept
: _Domain{} {
}
PIC::__1_::Domain<ND>::Domain(ParameterSet<ND> const& params, value_type const dt, WaveDamper<ND> const& damper)
: _Domain{params, dt, damper} {
}

void PIC::__1_::Domain<ND>::advance_by(size_type const n_steps)
{
    _advance_by(n_steps);
}
