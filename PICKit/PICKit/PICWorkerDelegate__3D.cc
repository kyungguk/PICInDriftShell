//
//  PICWorkerDelegate__3D.c
//  PICKit
//
//  Created by KYUNGGUK MIN on 6/29/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#include "PICWorkerDelegate__3D.h"
#include "PICWorkerDelegate__ND.hh"
#include "PICMasterDelegate__3D.h"
#include "PICDomain__3D.h"
#include "PICBField__3D.h"
#include "PICEField__3D.h"
#include "PICCurrent__3D.h"
#include "PICSpecies__3D.h"
#include "PICColdFluid__3D.h"

constexpr long ND = 3;

// MARK:- PIC::__1_::WorkerDelegate<3D>
//
PIC::__1_::WorkerDelegate<ND>::~WorkerDelegate<ND>()
{
}
PIC::__1_::WorkerDelegate<ND>::WorkerDelegate(long const id, MasterDelegate<ND> *const master)
: _WorkerDelegate{id, master}
{
    if (!master) {
        throw std::invalid_argument{__PRETTY_FUNCTION__};
    }
}

void PIC::__1_::WorkerDelegate<ND>::once(Domain<ND> *domain)
{
    master->delegate->once(domain);

    // zero-out cold fluid plasma frequency to suppress workers' cold fluid contribution
    //
    for (auto &cf : domain->cold_fluids()) {
        cf.set_properties(cf.Oc0ref(), cf.op0ref()*0, cf.number_of_smoothing_passes());
    }
}
bool PIC::__1_::WorkerDelegate<ND>::will_do_next_cycle(Domain<ND> const* domain, long const i_step)
{
    return master->delegate->will_do_next_cycle(domain, i_step);
}

void PIC::__1_::WorkerDelegate<ND>::pass(Domain<ND> const* domain, Species<ND> &sp)
{
    // pass q2-dir
    //
    _pass(domain, sp, Direction::q2);

    // pass q1-dir
    //
    _pass(domain, sp, Direction::q1);
}
void PIC::__1_::WorkerDelegate<ND>::pass(Domain<ND> const*, BField<ND> &bfield)
{
    constexpr std::integral_constant<long, 0> tag{};

    recv_from_master(tag, bfield->contr);
}
void PIC::__1_::WorkerDelegate<ND>::pass(Domain<ND> const*, EField<ND> &efield)
{
    constexpr std::integral_constant<long, 0> tag{};

    recv_from_master(tag, efield->contr);
}
void PIC::__1_::WorkerDelegate<ND>::pass(Domain<ND> const*, Current<ND> &current)
{
    constexpr std::integral_constant<long, 0> tag{};

    recv_from_master(tag, current->mfa);
}
void PIC::__1_::WorkerDelegate<ND>::gather(Domain<ND> const*, Current<ND> &current)
{
    constexpr std::integral_constant<long, 0> tag{};

    reduce_to_master(tag, current->mfa);
    recv_from_master(tag, current->mfa);
}
void PIC::__1_::WorkerDelegate<ND>::gather(Domain<ND> const*, Species<ND> &sp)
{
    constexpr std::integral_constant<long, 0> tag{};

    {
        reduce_to_master(tag, sp.moment<0>());
        reduce_to_master(tag, sp.moment<1>());
        reduce_to_master(tag, sp.moment<2>());
    }
}
