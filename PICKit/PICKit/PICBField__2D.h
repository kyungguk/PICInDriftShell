//
//  PICBField__2D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICBField__2D_h
#define PICBField__2D_h

#include <PICKit/PICBField__ND.h>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- PIC::__1_::BField<ND>
    //
    template <>
    class BField<2L> : public _BField<2L> {
        BField(BField &&) = default;
        BField &operator=(BField &&) = default;

    public:
        ~BField();
        explicit BField() noexcept;
        explicit BField(ParameterSet<ND> const& params, WaveDamper<ND> const &damper = {});
        explicit BField(BField&& old, WaveDamper<ND> const &damper);

        WaveDamper<ND> const &damper() const noexcept { return _damper; }

        // update
        //
        virtual void update(EField<ND> const &efield, value_type const dt);
        virtual void component_transform();
    private:
        inline void _component_transform(_BField &B) const;
        inline void _update(magnetic_field_type &B, electric_field_type const &E, value_type cdtOsqrtg) const;

        WaveDamper<ND> _damper;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICBField__2D_h */
