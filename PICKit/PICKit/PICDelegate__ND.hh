//
//  PICDelegate__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 11/7/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICDelegate__ND_hh
#define PICDelegate__ND_hh

#include "PICDelegate__ND.h"
#include <type_traits>
#include <algorithm>
#include <stdexcept>
#include <iterator>
#include <string>
#include <cmath>

// MARK: ArrayND Operator Overloadings
//
namespace {
    template <class T, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad>& operator+=(UTL::ArrayND<T, ND, Pad>& lhs, UTL::ArrayND<T, ND, Pad> const& rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        auto r_first = rhs.pad_begin();
        while (l_first != l_last) {
            *l_first++ += *r_first++;
        }
        return lhs;
    }
}

// MARK: Periodic BC
//
namespace {
    template <class T, long Rank, long Pad>
    inline void periodic_pass(UTL::ArrayND<T, Rank, Pad> &A) {
        static_assert(Pad >= 1 && Pad <= 3, "invalid padding");
        for (long i = 0; i < Pad; ++i) {
            // pass to left
            A.end()[i] = A[i];
            // pass to right
            A[-1-i] = A.end()[-1-i];
        }
    }
    template <class T, long Rank, long Pad>
    inline void periodic_gather(UTL::ArrayND<T, Rank, Pad> &A) {
        static_assert(Pad >= 1 && Pad <= 3, "invalid padding");
        for (long i = Pad - 1; i >= 0; --i) {
            // get from right
            A[i] += A.end()[i];
            // get from left
            A.end()[-1-i] += A[-1-i];
        }
    }
}

// MARK: Symmetric BC
//
namespace {
    template <class T, long Rank, long Pad>
    inline void symmetric_pass(UTL::ArrayND<T, Rank, Pad> &A) {
        static_assert(Pad >= 1 && Pad <= 3, "invalid padding");
        for (long i = 0; i < Pad; ++i) {
            // inner ones first
            A[-1-i] = A[i];
            A.end()[i] = A.end()[-1-i];
        }
    }
    template <class T, long Rank, long Pad>
    inline void symmetric_gather(UTL::ArrayND<T, Rank, Pad> &A) {
        static_assert(Pad >= 1 && Pad <= 3, "invalid padding");
        // Note that ghost grid is collected to boundary grid!!
        for (long i = Pad - 1; i >= 0; --i) {
            // outer ones first
            A[0] += A[-1-i];
            A.end()[-1] += A.end()[i];
        }
    }
}

// MARK: Partition
//
namespace {
    template <long i, long ND>
    inline void partition(PIC::__1_::Species<ND>& sp, UTL::DynamicArray<PIC::__1_::Particle<ND>> &L_bucket, UTL::DynamicArray<PIC::__1_::Particle<ND>> &R_bucket)
    {
        using species_type = typename std::remove_reference<decltype(sp)>::type;
        using particle_type = typename species_type::particle_type;
        static_assert(i >= 0 && i < ND, "invalid index");
        auto &sp_bucket = sp.bucket();

        // group particles that have crossed left boundary
        //
        {
            auto const q_min = sp.params().ptl_q_min();
            auto it = std::partition(sp_bucket.begin(), sp_bucket.end(), [q_min](particle_type const &ptl) noexcept->bool {
                return std::get<i>(ptl.pos) >= std::get<i>(q_min);
            });
            std::move(it, sp_bucket.end(), std::back_inserter(L_bucket));
            sp_bucket.erase(it, sp_bucket.end());
        }

        // group particles that have crossed right boundary
        //
        {
            auto const q_max = sp.params().ptl_q_max();
            auto it = std::partition(sp_bucket.begin(), sp_bucket.end(), [q_max](particle_type const &ptl) noexcept->bool {
                return std::get<i>(ptl.pos) < std::get<i>(q_max);
            });
            std::move(it, sp_bucket.end(), std::back_inserter(R_bucket));
            sp_bucket.erase(it, sp_bucket.end());
        }
    }
}

#endif /* PICDelegate__ND_hh */
