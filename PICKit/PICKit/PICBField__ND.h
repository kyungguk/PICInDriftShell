//
//  PICBField__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICBField__ND_h
#define PICBField__ND_h

#include <PICKit/PICKit-config.h>
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <memory>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class BField;
    template <long ND> class EField;

    // MARK:- PIC::__1_::_BField<ND>
    //
    template <long _ND>
    class _BField {
    public:
        static constexpr long ND = _ND;
        static constexpr long reducedND = ND == 3 ? ND - 1 : ND;
        static constexpr long pad_size = 3;

        // types
        //
        using size_type = long;
        using value_type = double;
        using vector_type = UTL::Vector<value_type, 3L>;
        using position_type = UTL::Vector<value_type, reducedND>;
        using size_vector_type = UTL::Vector<size_type, reducedND>;
        using magnetic_field_type = UTL::ArrayND<vector_type, reducedND, pad_size>;
        using electric_field_type = UTL::ArrayND<vector_type, reducedND, pad_size>;
        struct triplet_type { // at cell center for collocated grid; at cell edge (shifted by +0.5) for staggered grid
            magnetic_field_type contr, covar, carts;
            explicit triplet_type(size_vector_type const &N) : contr{N}, covar{N}, carts{N} {}
        };

        // accessors
        //
        ParameterSet<ND> const& params() const noexcept { return _params; }

        triplet_type const* operator->() const noexcept { return _B.get(); }
        triplet_type      * operator->()       noexcept { return _B.get(); }

    protected:
        _BField(_BField &&) = default;
        _BField &operator=(_BField &&) = default;
        virtual ~_BField() {}
        explicit _BField() noexcept {}
        explicit _BField(ParameterSet<ND> const& params);

    private:
        ParameterSet<ND> _params;
        std::unique_ptr<triplet_type> _B;
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICBField__ND_h */
