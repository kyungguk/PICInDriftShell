//
//  PICSpecies__3D.cc
//  PICKit
//
//  Created by Kyungguk Min on 4/2/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICSpecies__3D.h"
#include "PICSpecies__ND.hh"
#include "PICBField__3D.h"
#include "PICEField__3D.h"

constexpr long ND = 3;

// MARK:- PIC::__1_::Species<ND>
//
PIC::__1_::Species<ND>::~Species<ND>()
{
}
PIC::__1_::Species<ND>::Species() noexcept
: _Species{} {
}
PIC::__1_::Species<ND>::Species(ParameterSet<ND> const& params)
: _Species{params} {
}

void PIC::__1_::Species<ND>::set_properties(value_type const Oc0ref, value_type const op0ref, value_type const Nc0ref, ShapeOrder const shape_order, NSmooth const n_smooth)
{
    _Species::set_properties(Oc0ref, op0ref, Nc0ref, shape_order, n_smooth);
    //
    switch (shape_order) {
        case 1:
            _update_velocity = &Species::_update_velocity_<1>;
            _collect_V       = &Species::_collect_V_<1>;
            break;
        case 2:
            _update_velocity = &Species::_update_velocity_<2>;
            _collect_V       = &Species::_collect_V_<2>;
            break;
        case 3:
            _update_velocity = &Species::_update_velocity_<3>;
            _collect_V       = &Species::_collect_V_<3>;
            break;
        default:
            throw std::invalid_argument{std::string{__PRETTY_FUNCTION__} + " - not supported shape order"};
    }
}
void PIC::__1_::Species<ND>::load(value_type const Oc0, value_type const op0, size_type const Nc, ShapeOrder const shape_order, NSmooth const n_smooth, ParticleLoader<ND> const& loader)
{
    // set equatorial species properties
    //
    size_vector_type const Nxy = loader.N().take<2>(); // whole simulation domain sizes
    size_type const Ntot = UTL::reduce_prod(Nxy)*Nc; // this is the total number of particles in the entire domain
    set_properties(Oc0, op0, Ntot*loader.NcellOverNtotal()/*this is the number of particles in the cell at (0, 0)*/, shape_order, n_smooth);
    size_type const n_threads = params().number_of_worker_threads() + 1;
    if (Ntot % n_threads) {
        throw std::invalid_argument{std::string{__PRETTY_FUNCTION__} + " - the total number of particles are not divisible by the number of threads"};
    }
    //
    // particle load
    //
    velocity_type const Orot = params().Orot();
    CoordSystem const &cs = params().coord_system();
    ParameterSet<ND>::position_type const q_min = params().ptl_q_min();
    ParameterSet<ND>::position_type const q_max = params().ptl_q_max();
    size_vector_type const N = params().N().take<2>(); // subdomain sizes
    bucket().reserve(UTL::reduce_prod(N)*Nc/n_threads); // rough estimation of Nptls in this subdomain
    for (size_type i = 0, n = Ntot/n_threads; i < n; ++i) {
        auto const &loaded = loader();
        // filter those that belong here
        if (UTL::reduce_bit_and(loaded.pos >= q_min) && UTL::reduce_bit_and(loaded.pos < q_max)) {
            particle_type &ptl = bucket().emplace_back(particle_type{loaded.vel, loaded.pos});

            // add drift velocity
            //
            CurviCoord const &curvi = ptl.pos; // radial coordinate of the gyro center should be at reference field line
            velocity_type const Oc0mom1 = loader.normalized_current_density(curvi);
            value_type const eta = loader.vdf().nOn0(cs.coord_trans(curvi));
            velocity_type const Vdrift = Oc0mom1/(Oc0*eta); // divided by density so that this is average drift velocity
            ptl.vel += Vdrift;

            // change to rotating frame
            //
            velocity_type const Urot = cross(Orot, cs.position(ptl.pos)); // at the particle position
            ptl.vel -= Urot;
        }
    }
    //
    // force balancing current
    //
    if (params().enforce_force_balance()) {
        ParameterSet<ND>::position_type const &q_min = params().grid_q_min(); // at cell center
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            for (long j = 0; j < N.y; ++j) {
                curvi.y = j + q_min.y;
                ((*this)->force_balancing_mom1[i][j] = loader.normalized_current_density(curvi)) /= -Oc0 * n_threads; // -Oc,ref*nV/Oc, where nV = <v>
            }
        }
    }
}

void PIC::__1_::Species<ND>::update(value_type const dt, value_type const fraction_of_cell_size_allowed_to_travel)
{
    // 1. update position by full dt; x^n -> x^n+1
    //
    if (!_update_position(bucket(), dt, 1.0/fraction_of_cell_size_allowed_to_travel)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - particle(s) moved too far");
    }
}
void PIC::__1_::Species<ND>::update(BField<ND> const &bfield, EField<ND> const &efield, value_type const dt)
{
    // 1. update velocity by full dt; v^n -> v^n+1
    // ** Bcarts is at cell edge for staggered grid, and at cell center for collocated grid **
    //
    (this->*_update_velocity)(bucket(), _full_dB(bfield->carts), efield->carts, dt);
}
void PIC::__1_::Species<ND>::collect_vector_moment()
{
    (this->*_collect_V)(moment<1>(), bucket());
    //
    // subtract ring current contribution
    //
    moment<1>() += force_balancing_moment1();
}
void PIC::__1_::Species<ND>::collect_all_moments()
{
    _collect_all(moment<0>(), moment<1>(), moment<2>(), bucket());
}

auto PIC::__1_::Species<ND>::_full_dB(magnetic_field_type const& H)
-> magnetic_field_type const& {
    magnetic_field_type &F = moment<1>();
    constexpr long pad = magnetic_field_type::pad_size();
    static_assert(pad >= 2, "padding should be greater than 1");
    size_vector_type const N = H.dims() + pad;
    long const _1 = +long{params().grid_strategy() > 0}; // +1 when cell edge is leading; otherwise -0
    long const _0 = -long{params().grid_strategy() < 0}; // +0 when cell edge is leading; otherwise -1
    for (long i = -pad+_1; i < N.x+_0; ++i) {
        for (long j = -pad+_1; j < N.y+_0; ++j) {
            (F[i][j] = (H[i-_0][j-_0] + H[i-_0][j-_1]) + (H[i-_1][j-_0] + H[i-_1][j-_1])) *= .25; // interpolation onto cell center
        }
    }
    return F;
}

bool PIC::__1_::Species<ND>::_update_position(bucket_type &bucket, value_type const dt, value_type const travel_scale_factor) const
{
    long has_moved_too_far{0};
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // at cell center
        auto const v_trans = [&metric](velocity_type const &vel, CurviCoord const &curvi)->velocity_type {
            velocity_type h3 = metric.covar_to_carts({0, 0, 1}, curvi);
            return (h3 *= -UTL::reduce_plus(vel*h3)/UTL::reduce_plus(h3*h3)) += vel; // velocity projected in q1-q2 plane
        };
        for (particle_type &ptl : bucket) {
            CurviCoord const &curvi = ptl.pos;

            // move; project velocity in the q1-q2 plane
            //
            velocity_type moved = metric.carts_to_contr(v_trans(ptl.vel, curvi), curvi);
            ptl.pos += moved *= dt;

            // travel distance check
            //
            moved *= travel_scale_factor;
            has_moved_too_far |= UTL::reduce_bit_or(d2i_vector(moved));
        }
    } else {
        CoordSystem const& cs = params().coord_system();
        auto const v_trans = [&cs](velocity_type const &vel, CurviCoord const &curvi)->velocity_type {
            velocity_type e2 = cs.mfa_basis<2>(curvi);
            return (e2 *= -UTL::reduce_plus(vel*e2)) += vel; // velocity projected in q1-q2 plane
        };
        for (particle_type &ptl : bucket) {
            CurviCoord const &curvi = ptl.pos;

            // move; project velocity in the q1-q2 plane
            //
            velocity_type moved = cs.carts_to_contr(v_trans(ptl.vel, curvi), curvi);
            ptl.pos += moved *= dt;

            // travel distance check
            //
            moved *= travel_scale_factor;
            has_moved_too_far |= UTL::reduce_bit_or(d2i_vector(moved));
        }
    }
    return !has_moved_too_far;
}

template <long Order>
void PIC::__1_::Species<ND>::_update_velocity_(bucket_type &bucket, magnetic_field_type const &dB, electric_field_type const &dE, value_type const dt) const
{
    static_assert(Order == 1 || Order == 2 || Order == 3, "Shape order should be either 1, 2, or 3");
    static_assert(Order <= magnetic_field_type::pad_size(), "Shape order should be greater than the pad size");
    static_assert(Order <= electric_field_type::pad_size(), "Shape order should be greater than the pad size");
    ParameterSet<ND>::position_type const &q_min = params().grid_q_min(); // at cell center
    value_type const O0ref = params().O0ref();
    BorisPush const boris_nonrelativistic{dt, params().c(), O0ref, Oc0ref()};
    velocity_type const Orot_c = params().Orot()/params().c(), OrotO0OOc = params().Orot()*O0ref/Oc0ref();
    UTL::Shape<value_type, Order> sx, sy;
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // at cell center
        auto const is_valid_q3 = [&metric](CurviCoord const &curvi)->bool {
            return (curvi.q3() > std::get<2>(metric.q_min()) - .5)
            &&     (curvi.q3() < std::get<2>(metric.q_max()) + .5);
        };
        for (particle_type &ptl : bucket) {
            sx(ptl.pos.x - q_min.x);
            sy(ptl.pos.y - q_min.y);
            //
            // 1. interpolate perturbed fields
            //
            velocity_type Ei = interp(dE, sx, sy);
            velocity_type Bi = interp(dB, sx, sy);
            //
            // 2. add background field at r = Rg + rho_g
            //
            CurviCoord curvi = ptl.pos; // at the reference field line; curvi.q3() should be 0
            velocity_type const b = metric.BcartsOverB0ref(curvi);
            velocity_type const rho = cross(b, ptl.vel)/(Oc0ref()*UTL::reduce_plus(b*b)); // gyro radius vector
            curvi.drop<1>() += metric.carts_to_contr(rho, curvi).drop<1>(); // adjust all but q1 coordinates
            if (!is_valid_q3(curvi)) {
                throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - particle's gyro radius is too large: crossed q3 boundary");
            }
            velocity_type const Bdip = metric.BcartsOverB0ref(curvi)*O0ref;
            Bi += Bdip;
            //
            // 2.1 change to rotating frame
            //
            Bi += OrotO0OOc;
            Ei += cross(cross(Orot_c, metric.position(curvi)), Bdip); // U/c x Bdip
            //
            // 3. update velocity
            //
            boris_nonrelativistic(ptl.vel, Bi, Ei);
        }
    } else {
        CoordSystem const& cs = params().coord_system();
        for (particle_type &ptl : bucket) {
            sx(ptl.pos.x - q_min.x);
            sy(ptl.pos.y - q_min.y);
            //
            // 1. interpolate perturbed fields
            //
            velocity_type Ei = interp(dE, sx, sy);
            velocity_type Bi = interp(dB, sx, sy);
            //
            // 2. add background field at r = Rg + rho_g
            //
            CurviCoord curvi = ptl.pos; // at the reference field line; curvi.q3() should be 0
            velocity_type const b = cs.BcartsOverB0ref(curvi);
            velocity_type const rho = cross(b, ptl.vel)/(Oc0ref()*UTL::reduce_plus(b*b)); // gyro radius vector
            curvi.drop<1>() += cs.carts_to_contr(rho, curvi).drop<1>(); // adjust all but q1 coordinates
            velocity_type const Bdip = cs.BcartsOverB0ref(curvi)*O0ref;
            Bi += Bdip;
            //
            // 2.1 change to rotating frame
            //
            Bi += OrotO0OOc;
            Ei += cross(cross(Orot_c, cs.position(curvi)), Bdip); // U/c x Bdip
            //
            // 3. update velocity
            //
            boris_nonrelativistic(ptl.vel, Bi, Ei);
        }
    }
}

template <long Order>
void PIC::__1_::Species<ND>::_collect_V_(vector_moment_type &nV, bucket_type const &bucket) const
{
    static_assert(Order == 1 || Order == 2 || Order == 3, "Shape order should be either 1, 2, or 3");
    static_assert(Order <= vector_moment_type::pad_size(), "Shape order should be greater than the pad size");
    nV.fill(vector_moment_type::value_type{0});
    ParameterSet<ND>::position_type const& q_min = params().grid_q_min(); // at cell center
    UTL::Shape<value_type, Order> sx, sy;
    for (particle_type const &ptl : bucket) {
        sx(ptl.pos.x - q_min.x);
        sy(ptl.pos.y - q_min.y);
        deposit(nV, sx, sy, ptl.vel);
    }
    nV *= 1./Nc0ref();
}
void PIC::__1_::Species<ND>::_collect_all(scalar_moment_type &n, vector_moment_type &nV, tensor_moment_type &nvv, bucket_type const &bucket) const
{
    n.fill(scalar_moment_type::value_type{0});
    nV.fill(vector_moment_type::value_type{0});
    nvv.fill(tensor_moment_type::value_type{0});
    ParameterSet<ND>::position_type const& q_min = params().grid_q_min(); // at cell center
    scalar_moment_type::value_type const one{1};
    tensor_moment_type::value_type tmp{0};
    UTL::Shape<value_type, 1> sx, sy;
    for (particle_type const &ptl : bucket) {
        sx(ptl.pos.x - q_min.x);
        sy(ptl.pos.y - q_min.y);
        deposit(n, sx, sy, one);
        deposit(nV, sx, sy, ptl.vel);
        tmp.hi() = tmp.lo() = ptl.vel;
        tmp.lo() *= ptl.vel;
        tmp.hi() *= {ptl.vel.y, ptl.vel.z, ptl.vel.x};
        deposit(nvv, sx, sy, tmp);
    }
    n *= 1./Nc0ref();
    nV *= 1./Nc0ref();
    nvv *= 1./Nc0ref();
}
