//
//  PICMasterDelegate__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 7/1/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef PICMasterDelegate__ND_hh
#define PICMasterDelegate__ND_hh

#include "PICMasterDelegate__ND.h"
#include <stdexcept>
#include <algorithm>
#include <iterator>

// MARK:- PIC::__1_::_MasterDelegate<ND>
//
template <long ND>
PIC::__1_::_MasterDelegate<ND>::_MasterDelegate(Delegate<ND> *const delegate)
: delegate{delegate} {
}

template <long ND>
void PIC::__1_::_MasterDelegate<ND>::_pass(Domain<ND> const* domain, Species<ND> &sp, Direction const dir)
{
    MasterDelegate<ND> *self = static_cast<MasterDelegate<ND> *>(this);
    constexpr std::integral_constant<long, 0> tag{};
    using Bucket = typename std::remove_reference<decltype(sp.bucket())>::type;

#if defined(PICKIT_MULTI_THREAD_AGGREGATE_PARTICLE_PASS) && PICKIT_MULTI_THREAD_AGGREGATE_PARTICLE_PASS
    L_bucket.clear();
    R_bucket.clear();
    delegate->partition(sp, dir, L_bucket, R_bucket);

    // 1. gather
    //
    UTL::DynamicArray<long> remaining;
    {
        remaining.reserve(workers().size() + 1);
        for (WorkerDelegate<ND> &worker : workers()) {
            using Payload = std::tuple<long, Bucket const*, Bucket const*>;
            auto const pkt = worker.worker_to_master.recv(*self, tag);
            Payload const payload = pkt.template payload<Payload>();
            remaining.push_back(std::get<0>(payload));
            std::copy(std::get<1>(payload)->begin(), std::get<1>(payload)->end(), std::back_inserter(L_bucket));
            std::copy(std::get<2>(payload)->begin(), std::get<2>(payload)->end(), std::back_inserter(R_bucket));
        }
    }
    remaining.push_back(sp.bucket().size());

    // 2. boundary pass
    //
    delegate->pass(domain, dir, L_bucket, R_bucket);

    // 3. distribute
    //
    {
        long const total = std::accumulate(remaining.begin(), remaining.end(), L_bucket.size() + R_bucket.size());
        long const quotient = total/remaining.size();
        long offset = 0;
        for (long length = 0, i = 0, n = workers().size(); i < n; ++i, offset += length) {
            length = quotient - remaining[i];
            length *= length >= 0;
            auto tk = workers()[i].master_to_worker.send(*self, tag, std::make_tuple(offset, length, static_cast<Bucket const*>(&L_bucket), static_cast<Bucket const*>(&R_bucket)));
            tickets.push_back(std::move(tk));
        }
        for (long const n = L_bucket.size(); offset < n; ++offset) {
            sp.bucket().push_back(L_bucket[offset]);
        }
        offset -= L_bucket.size();
        for (long const n = R_bucket.size(); offset < n; ++offset) {
            sp.bucket().push_back(R_bucket[offset]);
        }
    }
    tickets.clear();
#else
    L_bucket.clear();
    R_bucket.clear();
    delegate->partition(sp, dir, L_bucket, R_bucket);
    for (WorkerDelegate<ND> &worker : workers()) {
        using Payload = std::tuple<Bucket*, Bucket*>;
        auto const pkt = worker.worker_to_master.recv(*self, tag);
        Payload const payload = pkt.template payload<Payload>();
        delegate->pass(domain, dir, *std::get<0>(payload), *std::get<1>(payload));
    }
    delegate->pass(domain, dir, L_bucket, R_bucket);
    std::move(L_bucket.begin(), L_bucket.end(), std::back_inserter(sp.bucket()));
    std::move(R_bucket.begin(), R_bucket.end(), std::back_inserter(sp.bucket()));
#endif
}

template <long ND>
template <long i, class T, long Rank, long Pad>
void PIC::__1_::_MasterDelegate<ND>::broadcast_to_workers(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> const &payload)
{
    MasterDelegate<ND> *self = static_cast<MasterDelegate<ND> *>(this);

    for (WorkerDelegate<ND> &worker : workers()) {
        tickets.push_back(worker.master_to_worker.send(*self, tag, &payload));
    }
    tickets.clear();
}
template <long ND>
template <long i, class T, long Rank, long Pad>
void PIC::__1_::_MasterDelegate<ND>::collect_from_workers(std::integral_constant<long, i> tag, UTL::ArrayND<T, Rank, Pad> &buffer)
{
    MasterDelegate<ND> *self = static_cast<MasterDelegate<ND> *>(this);

    // the first worker will collect all workers'
    //
    if (!workers().empty()) {
        workers().front().master_to_worker.send(*self, tag, &buffer).wait();
    }
}

#endif /* PICMasterDelegate__ND_hh */
