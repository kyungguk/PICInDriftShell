//
//  PICDomain__ND.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICDomain__ND_h
#define PICDomain__ND_h

#include <PICKit/PICKit-config.h>
#include <GeometryKit/GeometryKit.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <utility>
#include <memory>
#include <array>

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class BField;
    template <long ND> class EField;
    template <long ND> class Current;
    template <long ND> class Species;
    template <long ND> class ColdFluid;
    template <long ND> class Domain;
    template <long ND> class Delegate;

    // MARK:- PIC::__1_::_Domain<ND>
    //
    template <long _ND>
    class _Domain {
    public:
        static constexpr long ND = _ND;

        // types
        //
        using size_type = long;
        using value_type = double;
        using size_vector_type = UTL::Vector<size_type, ND>;

        using delegate_type = Delegate<ND>;
        using bfield_type = BField<ND>;
        using efield_type = EField<ND>;
        using current_type = Current<ND>;
        using species_type = Species<ND>;
        using cold_fluid_type = ColdFluid<ND>;

        // accessors
        //
        ParameterSet<ND> const& params() const noexcept { return _params; }
        WaveDamper<ND> const& damper() const noexcept { return _damper; }
        value_type const& dt() const noexcept { return _dt; } //!< Integration time step.

        EField<ND>       &efield()       noexcept { return efield<0>(); }
        EField<ND> const &efield() const noexcept { return efield<0>(); }

        BField<ND>       &bfield()       noexcept { return bfield<0>(); }
        BField<ND> const &bfield() const noexcept { return bfield<0>(); }

        Current<ND>       &current()       noexcept { return current<0>(); }
        Current<ND> const &current() const noexcept { return current<0>(); }

        UTL::Array<Species<ND>>       &species()       noexcept { return *_species; }
        UTL::Array<Species<ND>> const &species() const noexcept { return *_species; }

        UTL::Array<ColdFluid<ND>>       &cold_fluids()       noexcept { return *_cold_fluids; }
        UTL::Array<ColdFluid<ND>> const &cold_fluids() const noexcept { return *_cold_fluids; }

        Delegate<ND>* delegate() const noexcept { return _delegate; }
        void set_delegate(Delegate<ND>* delegate) noexcept { _delegate = delegate; }

        // add species
        //
        Species<ND> &add_species() { return _species->emplace_back(params()); }
        ColdFluid<ND> &add_cold_fluid() {return _cold_fluids->emplace_back(params()); }

    protected:
        _Domain(_Domain &&) = default;
        _Domain &operator=(_Domain &&) = default;
        virtual ~_Domain() {}
        explicit _Domain() noexcept {}
        explicit _Domain(ParameterSet<ND> const& params, value_type const dt, WaveDamper<ND> const& damper);

        // update
        //
        virtual void advance_by(size_type const n_steps) = 0;
        void _advance_by(size_type const n_steps);

    private:
        // update helpers
        //
        void _cycle(Domain<ND> const*);
        template <class Species>
        Current<ND> const& collect(Domain<ND> const* domain, Current<ND> &J, Species const &sp) const;

        // private accessors
        //
        template <long i> EField<ND> &efield() const noexcept { return *std::get<i>(_efield); }
        template <long i> BField<ND> &bfield() const noexcept { return *std::get<i>(_bfield); }
        template <long i> Current<ND> &current() const noexcept { return *std::get<i>(_current); }

    private:
        using _SpeciesArray = UTL::StaticArray<Species<ND>, 16>;
        using _ColdFluidArray = UTL::StaticArray<ColdFluid<ND>, 16>;

        // member variables
        //
        ParameterSet<ND> _params{};
        WaveDamper<ND> _damper{};
        value_type _dt{};
        std::array<std::unique_ptr<BField<ND>>, 2> _bfield{};
        std::array<std::unique_ptr<EField<ND>>, 1> _efield{};
        std::array<std::unique_ptr<Current<ND>>, 2> _current{};
        std::unique_ptr<_SpeciesArray> _species{};
        std::unique_ptr<_ColdFluidArray> _cold_fluids{};
        Delegate<ND> *_delegate{nullptr};
        bool _is_first_pass{true};
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICDomain__ND_h */
