//
//  PICColdFluid__ND.hh
//  PICKit
//
//  Created by KYUNGGUK MIN on 9/19/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef PICColdFluid__ND_hh
#define PICColdFluid__ND_hh

#include "PICColdFluid__ND.h"
#include "PICParticlePush.h"
#include <type_traits>
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath>

// MARK: Helpers
//
namespace {
    using BorisPush = Boris_C;
    constexpr double quiet_nan = std::numeric_limits<double>::quiet_NaN();
    //
    template <class T>
    inline UTL::Vector<T, 3> cross(UTL::Vector<T, 3> const &A, UTL::Vector<T, 3> const &B) noexcept {
        return {-A.z*B.y + A.y*B.z, A.z*B.x - A.x*B.z, -A.y*B.x + A.x*B.y};
    }
}


// MARK:- PIC::__1_::_ColdFluid<ND>
//
template <long ND>
PIC::__1_::_ColdFluid<ND>::_ColdFluid(ParameterSet<ND> const& params)
: _params{params}, _Oc0ref{quiet_nan}, _op0ref{quiet_nan}, _pkg{new package_type{params.N().template take<reducedND>()}} {
    static_assert(vector_moment_type::pad_size() <= ParameterSet<ND>::pad_size, "insufficient padding");
    moment<0>().fill(quiet_nan);
}
template <long ND>
void PIC::__1_::_ColdFluid<ND>::set_properties(value_type const Oc0ref, value_type const op0ref, NSmooth const n_smooth)
{
    _Oc0ref = Oc0ref;
    _op0ref = op0ref;
    _n_smooth = n_smooth;
}

#endif /* PICColdFluid__ND_hh */
