//
//  PICDelegate__2D.h
//  PICKit
//
//  Created by KYUNGGUK MIN on 11/7/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef PICDelegate__2D_h
#define PICDelegate__2D_h

#include "PICDelegate__ND.h"

PICKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class TestDelegate;

    // MARK:- PIC::__1_::TestDelegate<ND>
    //
    template <>
    class TestDelegate<2L> : public Delegate<2L> {
    public:
        enum BC : long { periodic = 0, reflective = 1 };
        using boundary_type = UTL::Vector<BC, ND>;

        explicit TestDelegate(boundary_type const bc) noexcept;

        // communication
        //
        void pass(Domain<ND> const*, Species<ND> &sp) override;
        void pass(Domain<ND> const*, BField<ND> &bfield) override;
        void pass(Domain<ND> const*, EField<ND> &efield) override;
        void pass(Domain<ND> const*, Current<ND> &current) override;
        void gather(Domain<ND> const*, Current<ND> &current) override;
        void gather(Domain<ND> const*, Species<ND> &sp) override;

        void partition(Species<ND>&, Direction const, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket) override;
        void pass(Domain<ND> const*, Direction const, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket) override;

    private:
        boundary_type _bc;

        template <Direction dir>
        BC const &bc() const noexcept { return std::get<dir>(_bc); }

        template <long i>
        inline static void _periodic_pass(Species<ND> const &sp, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket);
        template <long i>
        inline static void _reflect_pass(Species<ND> const &sp, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket);
    };
} // namespace __1_
PICKIT_END_NAMESPACE

#endif /* PICDelegate__2D_h */
