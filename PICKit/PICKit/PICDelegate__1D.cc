//
//  PICDelegate__1D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 3/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICDelegate__1D.h"
#include "PICDelegate__ND.hh"
#include "PICBField__1D.h"
#include "PICEField__1D.h"
#include "PICCurrent__1D.h"
#include "PICSpecies__1D.h"
#include "PICDomain__1D.h"

constexpr long ND = 1;

// MARK:- PIC::__1_::TestDelegate<ND>
//
PIC::__1_::TestDelegate<ND>::TestDelegate(boundary_type const bc) noexcept
: Delegate{}, _bc{bc} {
}

void PIC::__1_::TestDelegate<ND>::pass(Domain<ND> const* domain, Species<ND> &sp)
{
    // pass q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        Species<ND>::bucket_type L_bucket, R_bucket;
        partition(sp, dir, L_bucket, R_bucket);
        pass(domain, dir, L_bucket, R_bucket);
        std::move(L_bucket.begin(), L_bucket.end(), std::back_inserter(sp.bucket()));
        std::move(R_bucket.begin(), R_bucket.end(), std::back_inserter(sp.bucket()));
    }
}
template <long i>
void PIC::__1_::TestDelegate<ND>::_periodic_pass(Species<ND> const &sp, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket)
{
    static_assert(i >= 0 && i < ND, "invalid index");
    ParameterSet<ND>::position_type const q_len = sp.params().ptl_q_max() - sp.params().ptl_q_min();
    CoordSystem const& cs = sp.params().coord_system();
    //
    // note that velocity is in cartesian coordinates
    // pass mfa velocity components through the boundaries
    //
    for (Particle<ND> &ptl : R_bucket) { // upper boundary
        { // pre-pass:
            CoordSystem::tensor_type const &e = cs.mfa_basis<0>(CurviCoord{ptl.pos, nullptr});
            ptl.vel = { UTL::reduce_plus(ptl.vel*e.x), UTL::reduce_plus(ptl.vel*e.y), UTL::reduce_plus(ptl.vel*e.z) };
        }
        // pass:
        std::get<i>(ptl.pos) -= std::get<i>(q_len);
        { // post-pass:
            CoordSystem::tensor_type const &e = cs.mfa_basis<0>(CurviCoord{ptl.pos, nullptr});
            ptl.vel = ptl.vel.x*e.x + ptl.vel.y*e.y + ptl.vel.z*e.z;
        }
    }
    for (Particle<ND> &ptl : L_bucket) { // lower boundary
        { // pre-pass:
            CoordSystem::tensor_type const &e = cs.mfa_basis<0>(CurviCoord{ptl.pos, nullptr});
            ptl.vel = { UTL::reduce_plus(ptl.vel*e.x), UTL::reduce_plus(ptl.vel*e.y), UTL::reduce_plus(ptl.vel*e.z) };
        }
        // pass:
        std::get<i>(ptl.pos) += std::get<i>(q_len);
        { // post-pass:
            CoordSystem::tensor_type const &e = cs.mfa_basis<0>(CurviCoord{ptl.pos, nullptr});
            ptl.vel = ptl.vel.x*e.x + ptl.vel.y*e.y + ptl.vel.z*e.z;
        }
    }
}
template <long i>
void PIC::__1_::TestDelegate<ND>::_reflect_pass(Species<ND> const &sp, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket)
{
    static_assert(i >= 0 && i < ND, "invalid index");
    ParameterSet<ND>::position_type const q_min = sp.params().ptl_q_min();
    ParameterSet<ND>::position_type const q_max = sp.params().ptl_q_max();
    CoordSystem const& cs = sp.params().coord_system();
    //
    // note that velocity is in cartesian coordinates
    // co/contra-variant basis vectors are obtained using mid-point of position before and after reflection
    //
    constexpr long e1 = Direction::q1 + 1;
    constexpr long sign = i == e1 ? 1 /*parallel reflect: v2 - v1*/ : -1 /*perpendicular reflect: v1 - v2*/;
    for (Particle<ND> &ptl : R_bucket) { // upper boundary
        // position:
        CurviCoord curvi{ptl.pos, nullptr};
        std::get<i>(ptl.pos) += 2*(std::get<i>(q_max) - std::get<i>(ptl.pos));
        (curvi.take<1>() += ptl.pos) *= .5; // choose middle point
        // velocity:
        CoordSystem::vector_type &vel = ptl.vel, v1 = cs.mfa_basis<1>(curvi); // local unit vector parallel to the field line
        v1 *= UTL::reduce_plus(v1 * vel); // parallel component of velocity
        vel -= v1 *= 2; // v2 - v1
        vel *= sign;
    }
    for (Particle<ND> &ptl : L_bucket) { // lower boundary
        // position:
        CurviCoord curvi{ptl.pos, nullptr};
        std::get<i>(ptl.pos) += 2*(std::get<i>(q_min) - std::get<i>(ptl.pos));
        (curvi.take<1>() += ptl.pos) *= .5; // choose middle point
        // velocity:
        CoordSystem::vector_type &vel = ptl.vel, v1 = cs.mfa_basis<1>(curvi); // local unit vector parallel to the field line
        v1 *= UTL::reduce_plus(v1 * vel); // parallel component of velocity
        vel -= v1 *= 2; // v2 - v1
        vel *= sign;
    }
}

void PIC::__1_::TestDelegate<ND>::pass(Domain<ND> const *, BField<ND> &bfield)
{
    // pass q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        switch (bc<dir>()) {
            case periodic:
                ::periodic_pass(bfield->contr);
                break;
            case reflective:
                ::symmetric_pass(bfield->contr);
                break;
        }
    }
}
void PIC::__1_::TestDelegate<ND>::pass(Domain<ND> const *, EField<ND> &efield)
{
    // pass q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        switch (bc<dir>()) {
            case periodic:
                ::periodic_pass(efield->contr);
                break;
            case reflective:
                ::symmetric_pass(efield->contr);
                break;
        }
    }
}
void PIC::__1_::TestDelegate<ND>::pass(Domain<ND> const *, Current<ND> &current)
{
    // pass q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        switch (bc<dir>()) {
            case periodic:
                ::periodic_pass(current->mfa);
                break;
            case reflective:
                ::symmetric_pass(current->mfa);
                break;
        }
    }
}
void PIC::__1_::TestDelegate<ND>::gather(Domain<ND> const *, Current<ND> &current)
{
    // gather q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        switch (bc<dir>()) {
            case periodic:
                ::periodic_gather(current->mfa);
                break;
            case reflective:
                ::symmetric_gather(current->mfa);
                break;
        }
    }
}
void PIC::__1_::TestDelegate<ND>::gather(Domain<ND> const *, Species<ND> &sp)
{
    // gather q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        switch (bc<dir>()) {
            case periodic: {
                ::periodic_gather(sp.moment<0>());
                ::periodic_gather(sp.moment<1>());
                ::periodic_gather(sp.moment<2>());
                break;
            }
            case reflective: {
                ::symmetric_gather(sp.moment<0>());
                ::symmetric_gather(sp.moment<1>());
                ::symmetric_gather(sp.moment<2>());
                break;
            }
        }
    }
}

void PIC::__1_::TestDelegate<ND>::partition(Species<ND>& sp, Direction const dir, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket)
{
    switch (dir) {
        case Direction::q1: {
            constexpr Direction dir = Direction::q1;
            ::partition<dir>(sp, L_bucket, R_bucket);
            switch (bc<dir>()) {
                case periodic:
                    _periodic_pass<dir>(sp, L_bucket, R_bucket);
                    break;
                case reflective:
                    _reflect_pass<dir>(sp, L_bucket, R_bucket);
                    break;
            }
            break;
        }
        case Direction::q2:
            throw std::invalid_argument{__PRETTY_FUNCTION__};
        case Direction::q3:
            throw std::invalid_argument{__PRETTY_FUNCTION__};
    }
}
void PIC::__1_::TestDelegate<ND>::pass(Domain<ND> const*, Direction const dir, UTL::DynamicArray<Particle<ND>> &L_bucket, UTL::DynamicArray<Particle<ND>> &R_bucket)
{
    switch (dir) {
        case Direction::q1:
            L_bucket.swap(R_bucket);
            break;
        case Direction::q2:
            throw std::invalid_argument{__PRETTY_FUNCTION__};
        case Direction::q3:
            throw std::invalid_argument{__PRETTY_FUNCTION__};
    }
}
