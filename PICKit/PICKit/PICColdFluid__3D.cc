//
//  PICColdFluid__3D.cc
//  PICKit
//
//  Created by KYUNGGUK MIN on 9/19/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "PICColdFluid__3D.h"
#include "PICColdFluid__ND.hh"
#include "PICEField__3D.h"

constexpr long ND = 3;

// MARK:- PIC::__1_::ColdFluid<ND>
//
PIC::__1_::ColdFluid<ND>::~ColdFluid<ND>()
{
}
PIC::__1_::ColdFluid<ND>::ColdFluid() noexcept
: _ColdFluid{} {
}
PIC::__1_::ColdFluid<ND>::ColdFluid(ParameterSet<ND> const& params)
: _ColdFluid{params} {
}

void PIC::__1_::ColdFluid<ND>::set_properties(value_type const Oc0ref, value_type const op0ref, NSmooth const n_smooth)
{
    _ColdFluid::set_properties(Oc0ref, op0ref, n_smooth);
}
void PIC::__1_::ColdFluid<ND>::load(value_type const Oc0, value_type const op0, NSmooth const n_smooth)
{
    set_properties(Oc0, op0, n_smooth);

    // zero-out moments
    //
    moment<0>().fill(0);
    moment<1>().fill({0, 0, 0});
    moment<2>().fill({0, 0, 0, 0, 0, 0});

    // density & velocity
    //
    scalar_moment_type &n  = moment<0>();
    vector_moment_type &nV = moment<1>();
    //
    constexpr value_type n0 = 1;
    ParameterSet<ND>::size_vector_type const N = params().N();
    ParameterSet<ND>::position_type const q_min = params().grid_q_min(); // at cell center
    CoordSystem const &cs = params().coord_system();
    vector_type const Orot = params().Orot();
    CurviCoord curvi{0}; // at reference meridian
    for (long i = 0; i < N.x; ++i) {
        curvi.x = i + q_min.x;
        for (long j = 0; j < N.y; ++j) {
            curvi.y = j + q_min.y;
            n[i][j] = n0;
            nV[i][j] = -n[i][j].x*cross(Orot, cs.position(curvi));
        }
    }
}

void PIC::__1_::ColdFluid<ND>::update(EField<ND> const &efield, value_type const dt)
{
    // update flow velocity by dt; nV^n-1/2 -> nV^n+1/2
    // update cartesian components of V
    //
    _update_V(moment<1>(), moment<0>(), efield->carts, dt);
}
void PIC::__1_::ColdFluid<ND>::_update_V(vector_moment_type &nVcarts, scalar_moment_type const &n0, electric_field_type const &Ecarts, value_type const dt) const
{
    value_type const O0ref = params().O0ref();
    BorisPush const boris_nonrelativistic{dt, params().c(), O0ref, Oc0ref()};
    vector_type const Orot_c = params().Orot()/params().c();
    size_vector_type const N = nVcarts.dims();
    if (params().use_metric_table()) {
        MetricTable<ND> const &metric = params().metric_table<ParameterSet<ND>::cell_cent>(); // at cell center
        MetricTable<ND>::size_vector_type ijk = {0, 0, std::get<2>(params().N())/2}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            ijk.x = i;
            for (long j = 0; j < N.y; ++j) {
                ijk.y = j;
                //
                vector_type const Bdip = metric.BcartsOverB0ref(ijk)*O0ref;
                vector_type Bi = Bdip;
                vector_type Ei = Ecarts[i][j];
                Ei += cross(cross(Orot_c, metric.position(ijk)), Bdip); // U/c x Bdip
                vector_type &nV = nVcarts[i][j];
                boris_nonrelativistic(nV, Bi, Ei *= n0[i][j].x);
            }
        }
    } else {
        ParameterSet<ND>::position_type const& q_min = params().grid_q_min(); // at cell center
        CoordSystem const& cs = params().coord_system();
        CurviCoord curvi{0}; // at r0ref and equator
        for (long i = 0; i < N.x; ++i) {
            curvi.x = i + q_min.x;
            for (long j = 0; j < N.y; ++j) {
                curvi.y = j + q_min.y;
                //
                vector_type const Bdip = cs.BcartsOverB0ref(curvi)*O0ref;
                vector_type Bi = Bdip;
                vector_type Ei = Ecarts[i][j];
                Ei += cross(cross(Orot_c, cs.position(curvi)), Bdip); // U/c x Bdip
                vector_type &nV = nVcarts[i][j];
                boris_nonrelativistic(nV, Bi, Ei *= n0[i][j].x);
            }
        }
    }
}

void PIC::__1_::ColdFluid<ND>::collect_all_moments()
{
    // calculate 2nd moment weighted by density
    //
    _collect_vv(moment<2>(), moment<0>(), moment<1>());
}
void PIC::__1_::ColdFluid<ND>::_collect_vv(tensor_moment_type &nvvcarts, scalar_moment_type const &n0, vector_moment_type const &nVcarts) const
{
    size_vector_type const N = nvvcarts.dims();
    for (long i = 0; i < N.x; ++i) {
        for (long j = 0; j < N.y; ++j) {
            tensor_moment_type::value_type &nvv = nvvcarts[i][j];
            vector_type const &nV = nVcarts[i][j];
            nvv.hi() = nvv.lo() = nV;
            nvv.lo() *= nV;
            nvv.hi() *= {nV.y, nV.z, nV.x};
            nvv /= n0[i][j].x;
        }
    }
}
