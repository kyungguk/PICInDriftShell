//
//  GeometryKit.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#if defined(__APPLE__) && defined(__OBJC__)
#import <Cocoa/Cocoa.h>

//! Project version number for GeometryKit.
FOUNDATION_EXPORT double GeometryKitVersionNumber;

//! Project version string for GeometryKit.
FOUNDATION_EXPORT const unsigned char GeometryKitVersionString[];
#endif

// In this header, you should import all the public headers of your framework using statements like #import <GeometryKit/PublicHeader.h>


#if defined(__cplusplus)

#include <GeometryKit/GEOCoordWrapper.h>
#include <GeometryKit/GEOGeometry.h>
#include <GeometryKit/GEOCoordSystem.h>
#include <GeometryKit/GEOMetricTable.h>
#include <GeometryKit/GEOParameterSet.h>
#include <GeometryKit/GEOWaveDamper.h>
#include <GeometryKit/GEOParticleLoader.h>

#endif
