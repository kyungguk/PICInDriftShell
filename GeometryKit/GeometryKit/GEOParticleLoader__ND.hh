//
//  GEOParticleLoader__ND.hh
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOParticleLoader__ND_hh
#define GEOParticleLoader__ND_hh

#include "GEOParticleLoader__ND.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath>

// helpers
//
namespace {
    constexpr long pad = 1; // particle boundary is grid_q ± 0.5
    constexpr double quiet_nan = std::numeric_limits<double>::quiet_NaN();
    //
    template <long S>
    inline UTL::Vector<long, S> isfinite(UTL::Vector<double, S> const &a) noexcept {
        UTL::Vector<long, S> res{-1};
        for (long i = 0; i < S; ++i) {
            res[i] *= std::isfinite(a[i]);
        }
        return res;
    }
    //
    template <class T>
    inline UTL::Vector<T, 3> cross(UTL::Vector<T, 3> const &A, UTL::Vector<T, 3> const &B) noexcept {
        return {-A.z*B.y + A.y*B.z, A.z*B.x - A.x*B.z, -A.y*B.x + A.x*B.y};
    }
}

// MARK: GEO::__1_::ParticleLoader<ND>
//
template <long ND>
GEO::__1_::ParticleLoader<ND>::ParticleLoader<ND>::~ParticleLoader()
{
}
template <long ND>
GEO::__1_::ParticleLoader<ND>::ParticleLoader() noexcept
: _cs{}, _N{}, _q_min{quiet_nan}, _q_max{quiet_nan} {
}
template <long ND>
GEO::__1_::ParticleLoader<ND>::ParticleLoader(CoordSystem const& cs, position_type const& q_min, size_vector_type const& N)
: _cs{cs}, _N{N}, _q_min{q_min}, _q_max{q_min + position_type{N - 1L}} {
    if (UTL::reduce_bit_or(N <= size_vector_type{0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid number of cells");
    }
    if (!UTL::reduce_bit_and(isfinite(q_min))) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - q_min not finite");
    }
    range_check(cs, grid_q_min() - value_type{pad}, grid_q_max() + value_type{pad});
}
template <long ND>
void GEO::__1_::ParticleLoader<ND>::range_check(CoordSystem const &cs, UTL::Vector<value_type, 3> const &q_min, UTL::Vector<value_type, 3> const &q_max)
{
    // q_min and q_max should include any paddings
    //
    // check q1
    //
    if (!cs.is_valid(CurviCoord{q_min.x, 0, 0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid minimum of q1");
    }
    if (!cs.is_valid(CurviCoord{q_max.x, 0, 0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid maximum of q1");
    }
    // check q2 and q3
    //
    { // at minimum q3 coordinate
        CurviCoord curvi{0, 0, q_min.z};
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid minimum of q3");
        }
        curvi.q2() = q_max.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - maximum of q2 at q3_min is too large");
        }
        curvi.q2() = q_min.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - minimum of q2 at q3_min is too small");
        }
    }
    { // at maximum q3 coordinate
        CurviCoord curvi{0, 0, q_max.z};
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid maximum of q3");
        }
        curvi.q2() = q_max.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - maximum of q2 at q3_max is too large");
        }
        curvi.q2() = q_min.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - minimum of q2 at q3_max is too small");
        }
    }
}

template <long ND>
auto GEO::__1_::ParticleLoader<ND>::normalized_current_density(CurviCoord const &curvi) const
-> velocity_type {
    PolarCoord const polar = coord_system().coord_trans(curvi);
    velocity_type const BOB0ref = coord_system().BcartsOverB0ref(polar);
    value_type const Jphi = vdf().JphixBOB0ref(polar)/std::sqrt(UTL::reduce_plus(BOB0ref*BOB0ref));
    return coord_system().translate_carts_from_reference_meridian({Jphi, 0, 0}, polar);
}

template <long ND>
auto GEO::__1_::ParticleLoader<ND>::nOn0(value_type const q2) const
-> value_type {
    CurviCoord const curvi{0, q2, 0};
    PolarCoord const polar = coord_system().coord_trans(curvi);
    return vdf().nOn0(polar);
}
template <long ND>
auto GEO::__1_::ParticleLoader<ND>::nOn0(std::pair<value_type, value_type> const& q2lim) const
-> value_type {
    static const Integrator integrate;
    return integrate([this](value_type const q2)->value_type {
        return this->nOn0(q2);
    }, q2lim.first, q2lim.second);
}

#endif /* GEOParticleLoader__ND_hh */
