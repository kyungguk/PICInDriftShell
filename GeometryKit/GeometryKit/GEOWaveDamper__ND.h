//
//  GEOWaveDamper__ND.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOWaveDamper__ND_h
#define GEOWaveDamper__ND_h

#include <GeometryKit/GeometryKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits> // std::integral_constant

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class WaveDamper;

    // MARK:- GEO::__1_::_WaveDamper<ND>
    //
    template <long _ND>
    class _WaveDamper {
    public:
        static constexpr long ND = _ND;
        static constexpr long pad_size = 3;

        // types
        //
        using size_type = long;
        using value_type = double;
        using position_type = UTL::Vector<value_type, ND>;
        using size_vector_type = UTL::Vector<size_type, ND>;

        // properties
        //
        size_vector_type const &masking_inset() const noexcept { return _masking_inset; }
        position_type const &amplitude_damping() const noexcept { return _amplitude_damping; }
        position_type const &phase_retardation() const noexcept { return _phase_retardation; }

        size_vector_type const &lo_boundary_mask() const noexcept { return _lo_boundary_mask; }
        size_vector_type const &hi_boundary_mask() const noexcept { return _hi_boundary_mask; }

    private:
        size_vector_type _masking_inset; //!< Length of damping region (should not be greater than the dimension sizes).
        position_type _amplitude_damping; //!< Conversion factor for effective amplitude damping lentgh = _masking_inset/_amplitude_damping
        position_type _phase_retardation; //!< Conversion factor for effective phase retardation lentgh = _masking_inset/_phase_retardation
        size_vector_type _lo_boundary_mask{}; //!< Lower boundary mask.
        size_vector_type _hi_boundary_mask{}; //!< Upper boundary mask.

    protected:
        _WaveDamper(_WaveDamper const&) = default;
        _WaveDamper &operator=(_WaveDamper const&) = default;
        _WaveDamper(_WaveDamper&&) noexcept = default;
        _WaveDamper &operator=(_WaveDamper&&) noexcept = default;

        _WaveDamper() noexcept;
        explicit _WaveDamper(size_vector_type const& inset, position_type const& amplitude_damping, position_type const& phase_retardation);

        template <long Rank>
        void _set_boundary_mask(size_type const mask, std::integral_constant<long, Rank>) noexcept;
        void _set_boundary_mask(size_type const, std::integral_constant<long, 0>) noexcept {}

        template <class T, long Pad>
        inline void _damp_q1(UTL::ArrayND<T, ND, Pad> &A, position_type const masking_factor) const;
        template <class T, long Pad>
        inline void _damp_q2(UTL::ArrayND<T, ND, Pad> &A, position_type const masking_factor) const;
        template <class T, long Pad>
        inline void _damp_q3(UTL::ArrayND<T, ND, Pad> &A, position_type const masking_factor) const;

    public:
        // boundary masks
        //
        enum Boundary : long { // bit-or'ed boundary identifiers
            q1_lo = 01 << 0,
            q1_hi = 01 << 1,
            q2_lo = 01 << 2,
            q2_hi = 01 << 3,
            q3_lo = 01 << 4,
            q3_hi = 01 << 5,
            none = 00
        };
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOWaveDamper__ND_h */
