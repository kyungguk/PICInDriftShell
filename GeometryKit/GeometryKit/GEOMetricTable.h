//
//  GEOMetricTable.h
//  GeometryKit
//
//  Created by Kyungguk Min on 3/28/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOMetricTable_h
#define GEOMetricTable_h

#include <GeometryKit/GEOMetricTable__ND.h>
#include <GeometryKit/GEOMetricTable__1D.h>
#include <GeometryKit/GEOMetricTable__2D.h>
#include <GeometryKit/GEOMetricTable__3D.h>

#endif /* GEOMetricTable_h */
