//
//  GEOWaveDamper__3D.cc
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEOWaveDamper__3D.h"
#include "GEOWaveDamper__ND.hh"

constexpr long ND = 3;

// MARK:- GEO::__1_::WaveDamper<ND>
//
GEO::__1_::WaveDamper<ND>::~WaveDamper<ND>()
{
}
GEO::__1_::WaveDamper<ND>::WaveDamper() noexcept
: _WaveDamper{} {
}
GEO::__1_::WaveDamper<ND>::WaveDamper(size_vector_type const& inset, position_type const& amplitude_damping, position_type const& phase_retardation)
: _WaveDamper{inset, amplitude_damping, phase_retardation} {
}

void GEO::__1_::WaveDamper<ND>::set_boundary_mask(long const mask) noexcept
{
    _set_boundary_mask(mask, std::integral_constant<long, ND>{});
}

void GEO::__1_::WaveDamper<ND>::operator()(UTL::ArrayND<UTL::Vector<value_type, 1L>, ND, pad_size> &A, position_type const r) const
{
    _WaveDamper::_damp_q3(A, r);
    _WaveDamper::_damp_q2(A, r);
    _WaveDamper::_damp_q1(A, r);
}
void GEO::__1_::WaveDamper<ND>::operator()(UTL::ArrayND<UTL::Vector<value_type, 2L>, ND, pad_size> &A, position_type const r) const
{
    _WaveDamper::_damp_q3(A, r);
    _WaveDamper::_damp_q2(A, r);
    _WaveDamper::_damp_q1(A, r);
}
void GEO::__1_::WaveDamper<ND>::operator()(UTL::ArrayND<UTL::Vector<value_type, 3L>, ND, pad_size> &A, position_type const r) const
{
    _WaveDamper::_damp_q3(A, r);
    _WaveDamper::_damp_q2(A, r);
    _WaveDamper::_damp_q1(A, r);
}
