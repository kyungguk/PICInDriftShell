//
//  GEOParameterSet__ND.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOParameterSet__ND_h
#define GEOParameterSet__ND_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEOCoordSystem.h>
#include <GeometryKit/GEOMetricTable.h>
#include <UtilityKit/UtilityKit.h>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class ParameterSet;

    // MARK:- GEO::__1_::_ParameterSet<ND>
    //
    template <long _ND>
    class _ParameterSet {
        static constexpr long _leading_cell_center = +1;
        static constexpr long _lagging_cell_center = -1;

    public:
        static constexpr long ND = _ND;
        static constexpr long pad_size = 3;
        static_assert(pad_size >= MetricTable<ND>::pad_size,
                      "ParameterSet::pad_size should be greater than or equal to MetricTable::pad_size");

        // types
        //
        using size_type = long;
        using value_type = double;
        using position_type = UTL::Vector<value_type, ND>;
        using velocity_type = UTL::Vector<value_type, 3L>;
        using size_vector_type = UTL::Vector<size_type, ND>;
        enum GridStrategy : long { collocate = 0, staggered = _lagging_cell_center };
        enum CellLocation : long { cell_cent = 0, cell_edge = 1 };
        struct OptionSet {
            value_type Orot{}; //!< rotational angular speed in radians (z-direction).
            unsigned number_of_worker_threads{0}; //!< number of worker threads for parallelization excluding the main thread.
            bool use_metric_table{false};
            bool enforce_force_balance{false};
            explicit OptionSet() noexcept {}
        };

        // properties
        //
        CoordSystem const& coord_system() const noexcept { return _cs; }
        size_vector_type const &N() const noexcept { return _N; }
        position_type const& grid_q_min() const noexcept { return _q_min; }
        position_type const& grid_q_max() const noexcept { return _q_max; }
        position_type ptl_q_min() const noexcept { return _q_min - .5; }
        position_type ptl_q_max() const noexcept { return _q_max + .5; }
        value_type const& c() const noexcept { return _c; }
        value_type const& O0ref() const noexcept { return _O0ref; }
        static GridStrategy grid_strategy() noexcept { return staggered; }
        bool use_metric_table() const noexcept { return _opts.use_metric_table; }
        bool enforce_force_balance() const noexcept { return _opts.enforce_force_balance; }
        unsigned number_of_worker_threads() const noexcept { return _opts.number_of_worker_threads; }
        velocity_type Orot() const noexcept { return {0, _opts.Orot, 0}; }

    protected:
        _ParameterSet(_ParameterSet const&) = default;
        _ParameterSet &operator=(_ParameterSet const&) = default;
        _ParameterSet(_ParameterSet&&) = default;
        _ParameterSet &operator=(_ParameterSet&&) = default;

        explicit _ParameterSet() noexcept;
        explicit _ParameterSet(CoordSystem const& cs, position_type const& q_min, size_vector_type const& N, value_type const c, value_type const O0ref, OptionSet const &opts);

        static void range_check(CoordSystem const &cs, UTL::Vector<value_type, 3> const &q_min, UTL::Vector<value_type, 3> const &q_max);
        template <long S>
        static void range_check(CoordSystem const &cs, UTL::Vector<value_type, S> const &q_min, UTL::Vector<value_type, S> const &q_max) { range_check(cs, q_min.append(-pad_size), q_max.append(pad_size)); }

        OptionSet const &option_set() const noexcept { return _opts; }

    private:
        CoordSystem _cs; //!< Coordinate system.
        size_vector_type _N; //!< number of cells.
        position_type _q_min; //!< cell-center curvilinear coordinate min.
        position_type _q_max; //!< cell-center curvilinear coordinate max.
        value_type _c; //!< Speed of light.
        value_type _O0ref; //!< Magnetic field strength at reference point (0, 0).
        OptionSet _opts;
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOParameterSet__ND_h */
