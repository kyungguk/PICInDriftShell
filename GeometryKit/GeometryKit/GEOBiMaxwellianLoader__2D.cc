//
//  GEOBiMaxwellianLoader__2D.cc
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEOBiMaxwellianLoader__2D.h"
#include "GEOParticleLoader__ND.hh"

constexpr long ND = 2;

// MARK:- GEO::__1_::BiMaxwellianLoader<2D>
//
GEO::__1_::BiMaxwellianLoader<ND>::~BiMaxwellianLoader<ND>()
{
}
GEO::__1_::BiMaxwellianLoader<ND>::BiMaxwellianLoader() noexcept
: ParticleLoader{} {
}
GEO::__1_::BiMaxwellianLoader<ND>::BiMaxwellianLoader(CoordSystem const &cs, position_type const &q_min, size_vector_type const &N, param_type &&param)
: ParticleLoader{cs, q_min, N}, _param{std::move(param)} {
    // init vdist
    //
    _dist_v1.reset(new UTL::ParallelMaxwellianDistribution{1});
    _dist_v2.reset(new UTL::PerpendicularMaxwellianDistribution{1});
    _dist_phi.reset(new UTL::UniformDistribution{0, 2*M_PI}); // perp velocity phase

    // q1 distribution
    //
    _dist_q1.reset(new UTL::UniformDistribution{ptl_q_min().x, ptl_q_max().x});

    // q2 distribution
    //
    _dist_q2.reset(new UTL::CustomDistribution{ptl_q_min().y, ptl_q_max().y, [this](value_type const q2)->value_type {
        return this->nOn0(q2);
    }, true});
}

auto GEO::__1_::BiMaxwellianLoader<ND>::NcellOverNtotal() const
-> value_type {
    value_type NcellOverNtotal{1};
    CurviCoord const q_min{-.5}, q_max{.5}; // unit box centered at (0, 0, 0)
    NcellOverNtotal *= _dist_q1->cdf(q_max.q1()) - _dist_q1->cdf(q_min.q1());
    NcellOverNtotal *= _dist_q2->cdf(q_max.q2()) - _dist_q2->cdf(q_min.q2());
    return NcellOverNtotal;
}

auto GEO::__1_::BiMaxwellianLoader<ND>::operator()() const
-> particle_type {
    particle_type ptl = this->_load();
    ptl.vel *= _param.vdf.vth1();
    return ptl;
}
auto GEO::__1_::BiMaxwellianLoader<ND>::_load() const
-> particle_type {
    RandomRealPool &rng = this->rng();
    //
    // position
    //
    CurviCoord const curvi{_dist_q1->icdf(rng.q1()), _dist_q2->icdf(rng.q2()), 0};
    //
    // orthogonal unit vectors in local field-aligned reference
    //
    PolarCoord const polar = coord_system().coord_trans(curvi);
    CoordSystem::tensor_type const e = coord_system().mfa_basis<0>(polar); // unit vector {parallel, in-plane perp, out-of-plane perp} to B
    //
    // velocity
    //
    value_type const ph = _dist_phi->icdf(rng.phi());
    value_type const v1 = _dist_v1->icdf(rng.v1()); // normalized to vth1_0
    value_type const v2 = _dist_v2->icdf(rng.v2()) * std::sqrt(_param.vdf.T2OT1(polar)); // normalized to vth1_0
    velocity_type vel{v1, v2*std::cos(ph), v2*std::sin(ph)};
    vel = vel.x*e.x + vel.y*e.y + vel.z*e.z;
    return particle_type{vel, curvi.take<2>()};
}

auto GEO::__1_::BiMaxwellianLoader<ND>::normalized_current_density(CurviCoord const &curvi) const
-> velocity_type {
    return ParticleLoader::normalized_current_density(curvi);
}
