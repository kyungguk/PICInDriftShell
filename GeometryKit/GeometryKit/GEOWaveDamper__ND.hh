//
//  GEOWaveDamper__ND.hh
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOWaveDamper__ND_hh
#define GEOWaveDamper__ND_hh

#include "GEOWaveDamper__ND.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <array>

// MARK: Helpers
//
namespace {
    constexpr double quiet_nan = std::numeric_limits<double>::quiet_NaN();
    //
    template <class T, class U, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad>& operator*=(UTL::ArrayND<T, ND, Pad>& lhs, U const& rhs) {
        auto l_first = lhs.pad_begin(), l_last = lhs.pad_end();
        while (l_first != l_last) {
            *l_first++ *= rhs;
        }
        return lhs;
    }
}

// MARK:- GEO::__1_::_WaveDamper<ND>
//
template <long ND>
GEO::__1_::_WaveDamper<ND>::_WaveDamper() noexcept
: _masking_inset{}, _amplitude_damping{quiet_nan}, _phase_retardation{quiet_nan} {
}
template <long ND>
GEO::__1_::_WaveDamper<ND>::_WaveDamper(size_vector_type const& inset, position_type const& amplitude_damping, position_type const& phase_retardation)
: _masking_inset{inset}, _amplitude_damping{amplitude_damping}, _phase_retardation{phase_retardation} {
    if (UTL::reduce_bit_or(inset < size_vector_type{0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid masking inset");
    }
    if (UTL::reduce_bit_or(amplitude_damping < position_type{0}) ||
        UTL::reduce_bit_or(amplitude_damping > position_type{1})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid amplitude damping factor");
    }
    if (UTL::reduce_bit_or(phase_retardation < position_type{0}) ||
        UTL::reduce_bit_or(phase_retardation > position_type{1})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid phase retardation factor");
    }
}

template <long ND>
template <long Rank>
void GEO::__1_::_WaveDamper<ND>::_set_boundary_mask(size_type const mask, std::integral_constant<long, Rank>) noexcept
{
    static_assert(Rank > 0 && Rank <= ND, "invalid rank");
    constexpr long I = Rank - 1;
    constexpr std::array<Boundary, 3> q_lo{q1_lo, q2_lo, q3_lo};
    constexpr std::array<Boundary, 3> q_hi{q1_hi, q2_hi, q3_hi};
    {
        std::get<I>(_lo_boundary_mask) = mask & std::get<I>(q_lo);
        std::get<I>(_hi_boundary_mask) = mask & std::get<I>(q_hi);
    }
    _set_boundary_mask(mask, std::integral_constant<long, I>{});
}

template <long ND>
template <class T, long Pad>
void GEO::__1_::_WaveDamper<ND>::_damp_q1(UTL::ArrayND<T, ND, Pad> &A, position_type const masking_factor) const
{
    size_vector_type const N = A.dims();
    for (long il = 0, ih = N.x - 1; il < masking_inset().x; ++il, --ih) {
        value_type const tmp = (il - masking_inset().x)*masking_factor.x/masking_inset().x;
        value_type const damper = (1 - tmp)*(1 + tmp);
        if (lo_boundary_mask().x) A[il] *= damper;
        if (hi_boundary_mask().x) A[ih] *= damper;
    }
}
template <long ND>
template <class T, long Pad>
void GEO::__1_::_WaveDamper<ND>::_damp_q2(UTL::ArrayND<T, ND, Pad> &A, position_type const masking_factor) const
{
    size_vector_type const N = A.dims();
    for (long i = 0; i < N.x; ++i) {
        for (long jl = 0, jh = N.y - 1; jl < masking_inset().y; ++jl, --jh) {
            value_type const tmp = (jl - masking_inset().y)*masking_factor.y/masking_inset().y;
            value_type const damper = (1 - tmp)*(1 + tmp);
            if (lo_boundary_mask().y) A[i][jl] *= damper;
            if (hi_boundary_mask().y) A[i][jh] *= damper;
        }
    }
}
template <long ND>
template <class T, long Pad>
void GEO::__1_::_WaveDamper<ND>::_damp_q3(UTL::ArrayND<T, ND, Pad> &A, position_type const masking_factor) const
{
    size_vector_type const N = A.dims();
    for (long i = 0; i < N.x; ++i) {
        for (long j = 0; j < N.y; ++j) {
            for (long kl = 0, kh = N.z - 1; kl < masking_inset().z; ++kl, --kh) {
                value_type const tmp = (kl - masking_inset().z)*masking_factor.z/masking_inset().z;
                value_type const damper = (1 - tmp)*(1 + tmp);
                if (lo_boundary_mask().z) A[i][j][kl] *= damper;
                if (hi_boundary_mask().z) A[i][j][kh] *= damper;
            }
        }
    }
}

#endif /* GEOWaveDamper__ND_hh */
