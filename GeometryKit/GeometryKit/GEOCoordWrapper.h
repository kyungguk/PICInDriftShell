//
//  GEOCoordWrapper.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOCoordWrapper_h
#define GEOCoordWrapper_h

#include <GeometryKit/GeometryKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    /**
     Polar coordinate wrapper.
     */
    struct PolarCoord : public UTL::Vector<double, 3> {
        using Base = UTL::Vector<value_type, size()>;
        //
        explicit PolarCoord() noexcept : Base{} {}
        explicit PolarCoord(Base const &pt) noexcept : Base{pt} {}
        explicit PolarCoord(value_type const x) noexcept : Base{x} {}
        explicit PolarCoord(value_type const phi, value_type const xi, value_type const r) noexcept : Base{phi, xi, r} {}
        //
        value_type const &phi() const noexcept { return x; }
        value_type const &xi() const noexcept { return y; }
        value_type const &r() const noexcept { return z; }
        //
        value_type &phi() noexcept { return x; }
        value_type &xi() noexcept { return y; }
        value_type &r() noexcept { return z; }
    };
    /**
     Curvilinear coordinate wrapper.
     */
    struct CurviCoord : public UTL::Vector<double, 3> {
        using Base = UTL::Vector<value_type, size()>;
        //
        explicit CurviCoord() noexcept : Base{} {}
        explicit CurviCoord(Base const &pt) noexcept : Base{pt} {}
        explicit CurviCoord(value_type const x) noexcept : Base{x} {}
        explicit CurviCoord(value_type const q1, value_type const q2, value_type const q3) noexcept : Base{q1, q2, q3} {}
        explicit CurviCoord(UTL::Vector<value_type, 3> const &pt, decltype(nullptr)) noexcept : CurviCoord{pt} {}
        explicit CurviCoord(UTL::Vector<value_type, 2> const &pt, decltype(nullptr)) noexcept : CurviCoord{pt.append(0), nullptr} {}
        explicit CurviCoord(UTL::Vector<value_type, 1> const &pt, decltype(nullptr)) noexcept : CurviCoord{pt.append(0), nullptr} {}
        //
        value_type const &q1() const noexcept { return x; }
        value_type const &q2() const noexcept { return y; }
        value_type const &q3() const noexcept { return z; }
        //
        value_type &q1() noexcept { return x; }
        value_type &q2() noexcept { return y; }
        value_type &q3() noexcept { return z; }
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOCoordWrapper_h */
