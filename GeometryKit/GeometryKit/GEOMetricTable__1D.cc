//
//  GEOMetricTable__1D.cc
//  GeometryKit
//
//  Created by Kyungguk Min on 3/28/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEOMetricTable__1D.h"
#include "GEOMetricTable__ND.hh"

constexpr long ND = 1;

// MARK:- GEO::__1_::MetricTable<ND>
//
GEO::__1_::MetricTable<ND>::MetricTable(MetricTable const &o)
: MetricTable{} {
    if (o) {
        *this = MetricTable{o.coord_system(), o.N(), o.q_min()};
    }
}
auto GEO::__1_::MetricTable<ND>::operator=(MetricTable const &o)
-> MetricTable &{
    if (this != &o) {
        if (o) {
            *this = MetricTable{o.coord_system(), o.N(), o.q_min()};
        } else {
            *this = MetricTable{};
        }
    }
    return *this;
}

GEO::__1_::MetricTable<ND>::~MetricTable<ND>()
{
}
GEO::__1_::MetricTable<ND>::MetricTable() noexcept
{
}
GEO::__1_::MetricTable<ND>::MetricTable(CoordSystem const& cs, size_vector_type const& N, position_type const& q_min)
: _MetricTable{cs, N, q_min} {
    if (*this) {
        _init_tables(*this);
    }
}
void GEO::__1_::MetricTable<ND>::_init_tables(_MetricTable &table) const
{
    CoordSystem const &cs = this->coord_system();
    CurviCoord curvi{0}; // at the reference meridian
    {
        table->covar_bases = convert(cs.covar_basis<0>(curvi));
        table->contr_bases = convert(cs.contr_basis<0>(curvi));
        table->mfa_bases = convert_mfa(cs.mfa_basis<0>(curvi));
        //
        table->BOB0 = convert(cs.BcartsOverB0(curvi));
        //
        table->position = convert(cs.position(curvi));
    }
    curvi.q2() = 0; // at equator
    table->r0Or0ref3 = UTL::pow<3>(cs.coord_trans(curvi).r()/cs.r0ref());
}

auto GEO::__1_::MetricTable<ND>::covar_metric(size_vector_type const &) const noexcept
-> tensor_type {
    return convert(metric((*this)->covar_bases));
}
auto GEO::__1_::MetricTable<ND>::contr_metric(size_vector_type const &) const noexcept
-> tensor_type {
    return convert(metric((*this)->contr_bases));
}
auto GEO::__1_::MetricTable<ND>::contr_to_covar(vector_type const& contr, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &covar_metric = this->covar_metric(ijk);
    return CoordSystem::contr_to_covar(contr, covar_metric);
}
auto GEO::__1_::MetricTable<ND>::covar_to_contr(vector_type const& covar, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &contr_metric = this->contr_metric(ijk);
    return CoordSystem::covar_to_contr(covar, contr_metric);
}
auto GEO::__1_::MetricTable<ND>::contr_to_carts(vector_type const& contr, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &covar_bases = convert((*this)->covar_bases);
    return coord_system().translate_carts_from_reference_meridian(CoordSystem::contr_to_carts(contr, covar_bases), CurviCoord{position_type{ijk} += q_min(), nullptr});
}
auto GEO::__1_::MetricTable<ND>::carts_to_contr(vector_type const& carts, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &contr_bases = convert((*this)->contr_bases);
    return CoordSystem::carts_to_contr(coord_system().translate_carts_to_reference_meridian(carts, CurviCoord{position_type{ijk} += q_min(), nullptr}), contr_bases);
}
auto GEO::__1_::MetricTable<ND>::covar_to_carts(vector_type const& covar, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &contr_bases = convert((*this)->contr_bases);
    return coord_system().translate_carts_from_reference_meridian(CoordSystem::covar_to_carts(covar, contr_bases), CurviCoord{position_type{ijk} += q_min(), nullptr});
}
auto GEO::__1_::MetricTable<ND>::carts_to_covar(vector_type const& carts, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &covar_bases = convert((*this)->covar_bases);
    return CoordSystem::carts_to_covar(coord_system().translate_carts_to_reference_meridian(carts, CurviCoord{position_type{ijk} += q_min(), nullptr}), covar_bases);
}
auto GEO::__1_::MetricTable<ND>::mfa_to_carts(vector_type const& mfa, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &mfa_bases = convert_mfa((*this)->mfa_bases);
    return coord_system().translate_carts_from_reference_meridian(CoordSystem::mfa_to_carts(mfa, mfa_bases), CurviCoord{position_type{ijk} += q_min(), nullptr});
}
auto GEO::__1_::MetricTable<ND>::carts_to_mfa(vector_type const& carts, size_vector_type const& ijk) const noexcept
-> vector_type {
    tensor_type const &mfa_bases = convert_mfa((*this)->mfa_bases);
    return CoordSystem::carts_to_mfa(coord_system().translate_carts_to_reference_meridian(carts, CurviCoord{position_type{ijk} += q_min(), nullptr}), mfa_bases);
}

auto GEO::__1_::MetricTable<ND>::contr_to_covar(vector_type const& contr, [[maybe_unused]] position_type const& pos) const noexcept
-> vector_type {
    tensor_type const &covar_metric = convert(metric((*this)->covar_bases));
    return CoordSystem::contr_to_covar(contr, covar_metric);
}
auto GEO::__1_::MetricTable<ND>::covar_to_contr(vector_type const& covar, [[maybe_unused]] position_type const& pos) const noexcept
-> vector_type {
    tensor_type const &contr_metric = convert(metric((*this)->contr_bases));
    return CoordSystem::covar_to_contr(covar, contr_metric);
}
auto GEO::__1_::MetricTable<ND>::contr_to_carts(vector_type const& contr, CurviCoord const& pos) const noexcept
-> vector_type {
    tensor_type const &covar_bases = convert((*this)->covar_bases);
    return coord_system().translate_carts_from_reference_meridian(CoordSystem::contr_to_carts(contr, covar_bases), pos);
}
auto GEO::__1_::MetricTable<ND>::carts_to_contr(vector_type const& carts, CurviCoord const& pos) const noexcept
-> vector_type {
    tensor_type const &contr_bases = convert((*this)->contr_bases);
    return CoordSystem::carts_to_contr(coord_system().translate_carts_to_reference_meridian(carts, pos), contr_bases);
}
auto GEO::__1_::MetricTable<ND>::covar_to_carts(vector_type const& covar, CurviCoord const& pos) const noexcept
-> vector_type {
    tensor_type const &contr_bases = convert((*this)->contr_bases);
    return coord_system().translate_carts_from_reference_meridian(CoordSystem::covar_to_carts(covar, contr_bases), pos);
}
auto GEO::__1_::MetricTable<ND>::carts_to_covar(vector_type const& carts, CurviCoord const& pos) const noexcept
-> vector_type {
    tensor_type const &covar_bases = convert((*this)->covar_bases);
    return CoordSystem::carts_to_covar(coord_system().translate_carts_to_reference_meridian(carts, pos), covar_bases);
}
auto GEO::__1_::MetricTable<ND>::mfa_to_carts(vector_type const& mfa, CurviCoord const& pos) const noexcept
-> vector_type {
    tensor_type const &mfa_bases = convert_mfa((*this)->mfa_bases);
    return coord_system().translate_carts_from_reference_meridian(CoordSystem::mfa_to_carts(mfa, mfa_bases), pos);
}
auto GEO::__1_::MetricTable<ND>::carts_to_mfa(vector_type const& carts, CurviCoord const& pos) const noexcept
-> vector_type {
    tensor_type const &mfa_bases = convert_mfa((*this)->mfa_bases);
    return CoordSystem::carts_to_mfa(coord_system().translate_carts_to_reference_meridian(carts, pos), mfa_bases);
}

auto GEO::__1_::MetricTable<ND>::BcartsOverB0(size_vector_type const& ijk) const noexcept
-> vector_type {
    vector_type const &BOB0 = convert((*this)->BOB0);
    return coord_system().translate_carts_from_reference_meridian(BOB0, CurviCoord{position_type{ijk} += q_min(), nullptr});
}

auto GEO::__1_::MetricTable<ND>::BcartsOverB0(CurviCoord const& pos) const noexcept
-> vector_type {
    vector_type const &BOB0 = convert((*this)->BOB0);
    return coord_system().translate_carts_from_reference_meridian(BOB0, pos);
}

auto GEO::__1_::MetricTable<ND>::position(size_vector_type const &ijk) const noexcept
-> vector_type {
    vector_type const r = convert((*this)->position);
    return coord_system().translate_carts_from_reference_meridian(r, CurviCoord{position_type{ijk} += q_min(), nullptr});
}
auto GEO::__1_::MetricTable<ND>::position(CurviCoord const &curvi) const noexcept
-> vector_type {
    vector_type const r = convert((*this)->position);
    return coord_system().translate_carts_from_reference_meridian(r, curvi);
}
