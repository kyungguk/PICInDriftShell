//
//  GEOParameterSet__1D.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOParameterSet__1D_h
#define GEOParameterSet__1D_h

#include <GeometryKit/GEOParameterSet__ND.h>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::ParameterSet<1D>
    //
    template <>
    class ParameterSet<1L> : public _ParameterSet<1L> {
    public:
        // copy/move
        //
        ParameterSet(ParameterSet const&) = default;
        ParameterSet &operator=(ParameterSet const&) = default;
        ParameterSet(ParameterSet&&) = default;
        ParameterSet &operator=(ParameterSet&&) = default;

        // constructors
        //
        ~ParameterSet();
        explicit ParameterSet() noexcept;
        explicit ParameterSet(CoordSystem const& cs, position_type const& q_min, size_vector_type const& N, value_type const c, value_type const O0ref, OptionSet const &opts);
        explicit ParameterSet(CoordSystem const& cs, position_type const& q_min, size_vector_type const& N, value_type const c, value_type const O0ref) : ParameterSet{cs, q_min, N, c, O0ref, OptionSet{}} {}

        /**
         @brief Construct parameter set sliced off starting at location and by the length.
         @discussion The sliced parameter set will have N() == length and {grid,ptl}_q_{min,max} == this->{grid,ptl}_q_{min,max} + location, with all other parameters being the same.
         */
        ParameterSet slice(size_vector_type const &location, size_vector_type const &length) const { return slice(location, length, false); }
        ParameterSet slice(size_vector_type const &location, size_vector_type const &length, bool const use_metric_table) const;

        /**
         @brief Metric table accessor.
         */
        template <CellLocation loc>
        MetricTable<ND> const &metric_table() const noexcept { return std::get<loc>(_metric_table); }

    private:
        UTL::Vector<MetricTable<ND>, 2> _metric_table{}; // {center, edge}
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOParameterSet__1D_h */
