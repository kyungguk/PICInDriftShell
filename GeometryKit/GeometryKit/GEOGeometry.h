//
//  GEOGeometry.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/3/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOGeometry_h
#define GEOGeometry_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEOCoordWrapper.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <cmath>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::Geometry
    //
    /**
     @brief General curvilinear coordinate system for dipole field geometry.
     @discussion
     q1 coordinate varies in the azimuthal direction.
     q2 coordinate varies along the field line.
     q3 coordinate varies along the equatorial radial direction and is translated such that q3 = 0 correspoinds to r0 = r0ref.
     */
    class Geometry {
    public:
        // types
        //
        using value_type = double;
        using vector_type = UTL::Vector< value_type, 3>;
        using tensor_type = UTL::Vector<vector_type, 3>;
        using spline_coeff_type = UTL::CubicSplineCoefficient<value_type>;
        using interpolator_type = spline_coeff_type::spline_interpolator_type;

        // destructor
        //
        ~Geometry();

        // copy/move
        //
        Geometry(Geometry const &) = default;
        Geometry &operator=(Geometry const &) = default;
        Geometry(Geometry &&) = default;
        Geometry &operator=(Geometry &&) = default;

        // constructors
        //
        explicit Geometry() noexcept;
        /**
         @brief Construct the dipole geometry object.
         @param r0ref Equatorial distance to the reference field line (positive).
         @param D Scale lengths of the (q1, q2, q3) curvilinear coordinates at r0ref (positive).
         If not specified, D2 = D1 and D3 = D2.
         @param n Radial dependence of curvilinear coordinates (n cannot be 3).
         */
        explicit Geometry(value_type const r0ref, UTL::Vector<value_type, 3> const &D, value_type const n);
        explicit Geometry(value_type const r0ref, UTL::Vector<value_type, 2> const &D, value_type const n) : Geometry{r0ref, D.append(D.back()), n} {}
        explicit Geometry(value_type const r0ref, UTL::Vector<value_type, 1> const &D, value_type const n) : Geometry{r0ref, D.append(D.back()), n} {}

        // accessors
        //
        /**
         @brief Reference field line label.
         */
        value_type const& r0ref() const noexcept { return _r0ref; }
        /**
         @brief q3 coordinate (before translation) of the reference field line.
         */
        value_type q3ref() const noexcept { return _q3ref; }
        /**
         @brief Scale factor of curvilinear coordinates.
         */
        vector_type const& D() const noexcept { return _D; }
        /**
         @brief Scale factor of q1 curvilinear coordinate.
         */
        value_type const& D1() const noexcept { return D().x; }
        /**
         @brief Scale factor of q2 curvilinear coordinate.
         */
        value_type const& D2() const noexcept { return D().y; }
        /**
         @brief Scale factor of q3 curvilinear coordinate.
         */
        value_type const& D3() const noexcept { return D().z; }
        /**
         @brief Radial dependence of curvilinear coordinates.
         */
        value_type const& n() const noexcept { return _n; }
        /**
         @brief Determinant of covariant metric tensor.
         @discussion Volumn is √g = g1.(g2 x g3), where gj's are covariant basis vectors.
         */
        value_type const& g() const noexcept { return _det_metric; }

        /**
         @brief Polar coordinate `ξ' inversion interpolator.
         @discussion The abscissa is "scaled" q2 coordinate defined as `∆2*q2*r0ref^(n-1)/r0^n'.
         @note Use this only for debugging purpose.
         */
        interpolator_type const &xi_interp() const noexcept { return _xi_interp; }

        // observers
        //
        /**
         @brief Return true if the given curvilinear/polar coordinates are valid (within the range).
         @discussion The check is made in the following order.

         First, the q1/φ coordinate should be such that |φ| <= π.

         Second, the minimum q3/r coordinate should be such that the corresponding field line label `r0' (equatorial field line distance) is positive.

         Finally, the maximum |q2|/|ξ| coordinate should be such that |ξ| < 1.
         @param at Curvilinear/polar coordinates.
         */
        template <class Coord>
        bool is_valid(Coord const &at) const noexcept { return _check(at); }

        // coordinate transformations
        //
        /**
         @brief Wrap φ/q1 coordinate so that -π <= φ < π.
         @discussion Multiple calls should be made if |φ| > 3π.
         */
        template <class Coord>
        void wrap(Coord *at) const noexcept { _wrap(*at); }
        template <class Coord>
        Coord wrap(Coord at) const noexcept { return wrap(&at), at; }

        /**
         @brief Coordinate transform from polar to curvilinear point.
         @discussion For performance, no check is made of the bound of the polar coordinate values.
         @param polar Polar point.
         @return Curvilinear point.
         */
        CurviCoord coord_trans(PolarCoord const &polar) const noexcept { return _coord_trans(polar); }
        /**
         @brief Coordinate transform from curvilinear to polar point.
         @discussion For performance, no check is made of the bound of the curvilinear coordinate values.
         @param curvi Curvilinear point.
         @return Polar point.
         */
        PolarCoord coord_trans(CurviCoord const &curvi) const noexcept { return _coord_trans(curvi); }

        // cartesian vector translation along q1 (assuming azimuthal symmetry)
        //
        /**
         @brief Translate cartesian vector components from the particle meridian to the reference meridian.
         @param carts Cartesian vector components.
         @param from Polar or curvilinear point.
         */
        template <class Coord>
        vector_type translate_carts_to_reference_meridian(vector_type const &carts, Coord const &from) const noexcept {
            value_type const phi = +this->_phi(from);
            value_type const cosphi = std::cos(phi), sinphi = std::sin(phi);
            return {-carts.z*sinphi + carts.x*cosphi, carts.y, carts.z*cosphi + carts.x*sinphi};
        }

        /**
         @brief Translate cartesian vector components from the reference meridian to the particle meridian.
         @param carts Cartesian vector components.
         @param to Polar or curvilinear point.
         */
        template <class Coord>
        vector_type translate_carts_from_reference_meridian(vector_type const &carts, Coord const &to) const noexcept {
            value_type const phi = -this->_phi(to);
            value_type const cosphi = std::cos(phi), sinphi = std::sin(phi);
            return {-carts.z*sinphi + carts.x*cosphi, carts.y, carts.z*cosphi + carts.x*sinphi};
        }

        // metric tensor
        //
        /**
         @brief The covariant metric tensor at a given point.
         @discussion gij =
         [g1.g1  g1.g2   g1.g3
          g2.g1  g2.g2   g2.g3
          g3.g1  g3.g2   g3.g3],
         where g2 is along the field line direction.
         */
        template <class Coord>
        tensor_type covar_metric(Coord const &at) const noexcept { return _covar_metric(at); }

        /**
         @brief The contravariant metric tensor at a given point.
         @sa covar_metric.
         */
        template <class Coord>
        tensor_type contr_metric(Coord const &at) const noexcept { return _contr_metric(at); }

        // covariant/contravariant bases
        //
        /**
         @brief Cartesian components of the ith covariant basis vector at a given point.
         @discussion All three basis vectors are returned if i == 0.
         */
        template <long i, class Coord>
        typename std::conditional<i == 0, tensor_type, vector_type
        >::type covar_basis(Coord const &at) const noexcept { return _covar_basis(at, std::integral_constant<long, i>{}); }

        /**
         @brief Cartesian components of the ith contravariant basis vector at a given point.
         @discussion All three basis vectors are returned if i == 0.
         */
        template <long i, class Coord>
        typename std::conditional<i == 0, tensor_type, vector_type
        >::type contr_basis(Coord const &at) const noexcept { return _contr_basis(at, std::integral_constant<long, i>{}); }

        // unit vectors in field-aligned coordinate system
        //
        /**
         @brief Cartesian components of the ith unit vectors in the field-aligned coordinate system at a given point.
         @discussion
         i == 0: all three unit vectors, {e1, e2, e3};
         i == 1: parallel to B, e1;
         i == 2: in-plane perpendicular to B, e2;
         i == 3: out-of-plane perpendicular to B, e3 (azimuthal direction).
         */
        template <long i, class Coord>
        typename std::conditional<i == 0, tensor_type, vector_type
        >::type mfa_basis(Coord const &at) const noexcept { return _mfa_basis(at, std::integral_constant<long, i>{}); }

        // normalized magnetic field vector
        //
        /**
         @brief Cartesian components of the dipole magnetic field normalized to the reference value (at r0ref), B/B0ref.
         @param at Polar/curvilinear coordinates.
         */
        template <class Coord>
        vector_type BcartsOverB0ref(Coord const &at) const noexcept { return _BcartsOverB0ref(at); }

        /**
         @brief Cartesian components of the dipole magnetic field normalized to the equatorial value, B/B0.
         @param at Polar/curvilinear coordinates.
         */
        template <class Coord>
        vector_type BcartsOverB0(Coord const &at) const noexcept { return _BcartsOverB0(at); }

    private:
        static constexpr value_type _xi_max = 1;
        vector_type _D;
        vector_type _D_r0ref; // D/r0ref
        value_type _det_metric;
        value_type _r0ref;
        value_type _q3ref;
        value_type _n;
        value_type _curvi_pi; //!< q1 coordinate equivalent to φ = π.
        interpolator_type _xi_interp; // {∆2*q2*r0ref^(n-1)/r0^n, xi}

    private: // implementations
        value_type const &D1_r0ref() const noexcept {
            return _D_r0ref.x;
        }
        value_type const &D2_r0ref() const noexcept {
            return _D_r0ref.y;
        }
        value_type const &D3_r0ref() const noexcept {
            return _D_r0ref.z;
        }

        // integral of (1 - ξ^2)^3 = q2*(∆2/r0ref)*r0ref^n/r0^n
        //
        static value_type scaled_q2(value_type const xi) noexcept {
            static value_type const sqrt_3o5 = std::sqrt(3./5), sqrt_1o7 = std::sqrt(1./7);
            return xi*((1 - xi)*(1 + xi) + UTL::pow<4>(xi)*(sqrt_3o5 - xi*sqrt_1o7)*(sqrt_3o5 + xi*sqrt_1o7));
        }
        value_type scaled_q2(CurviCoord const &curvi) const noexcept {
            value_type const q3 = curvi.q3() + q3ref(); // translate q3 forward by q3ref
            value_type const tmp = (3 - n())*q3*D3_r0ref(); // (r0ref/r0)^n-3
            return std::pow(tmp, n()/(n() - 3.))*curvi.q2()*D2_r0ref();
        }

        // check coord validity
        //
        static bool _check(PolarCoord const &polar) noexcept {
            return (std::abs(polar.phi()) <= M_PI) && (polar.r() > 0) && (std::abs(polar.xi()) < _xi_max);
        }
        bool _check(CurviCoord const &curvi) const noexcept {
            value_type const scaled_q2 = this->scaled_q2(curvi);
            value_type const q3 = curvi.q3() + q3ref(); // translate q3 forward by q3ref
            return ((std::abs(curvi.q1()) <= _curvi_pi) & (q3 > 0)) && (std::abs(scaled_q2) < xi_interp().max_abscissa()); // the short circuit is necessary
        }

        // wrap φ
        //
        static void _wrap(PolarCoord &polar) noexcept {
            constexpr value_type _polar_pi = M_PI;
            if (polar.phi() < -_polar_pi) {
                polar.phi() += 2*_polar_pi;
            } else if (polar.phi() >= _polar_pi) {
                polar.phi() -= 2*_polar_pi;
            }
        }
        void _wrap(CurviCoord &curvi) const noexcept {
            if (curvi.q1() < -_curvi_pi) {
                curvi.q1() += 2*_curvi_pi;
            } else if (curvi.q1() >= _curvi_pi) {
                curvi.q1() -= 2*_curvi_pi;
            }
        }

        // phi
        //
        static value_type _phi(PolarCoord const &polar) noexcept {
            return polar.phi();
        }
        value_type _phi(CurviCoord const &curvi) const noexcept {
            return curvi.q1()*D1_r0ref();
        }

        // parameter pack
        //
        class _ParamPack {
            value_type _xi;
            value_type _scaled_q2;
            value_type _r0_r0ref_n; //!< (r0/r0ref)^n
            value_type _r0_r0ref_3_n; //!< (r0/r0ref)^3-n
        public:
            explicit _ParamPack(Geometry const &cs, PolarCoord const &polar) noexcept
            : _xi{polar.xi()}, _scaled_q2{cs.scaled_q2(polar.xi())} {
                value_type const r0_r0ref = polar.r()/(cos2l()*cs.r0ref());
                _r0_r0ref_n = std::pow(r0_r0ref, cs.n());
                _r0_r0ref_3_n = UTL::pow<3>(r0_r0ref)/_r0_r0ref_n;
            }
            explicit _ParamPack(Geometry const &cs, CurviCoord const &curvi) noexcept {
                value_type const q3 = curvi.q3() + cs.q3ref(); // translate q3 forward by q3ref
                _r0_r0ref_3_n = (3 - cs.n())*q3*cs.D3_r0ref();
                _r0_r0ref_n = std::pow(_r0_r0ref_3_n, cs.n()/(3 - cs.n()));
                _scaled_q2 = curvi.q2()*cs.D2_r0ref()/_r0_r0ref_n;
                _xi = *cs.xi_interp()(_scaled_q2); // assumes is_valid(curvi) == true
            }
            value_type const &sinl() const noexcept {
                return _xi;
            }
            value_type const &scaled_q2() const noexcept {
                return _scaled_q2;
            }
            value_type const &r0_r0ref_3_n() const noexcept {
                return _r0_r0ref_3_n;
            }
            value_type const &r0_r0ref_n() const noexcept {
                return _r0_r0ref_n;
            }
            value_type r0_r0ref_3() const noexcept {
                return r0_r0ref_3_n()*r0_r0ref_n();
            }
            value_type r0_r0ref() const noexcept {
                return std::pow(r0_r0ref_3(), 1./3);
            }
            value_type cos2l() const noexcept {
                return (1 - _xi)*(1 + _xi);
            }
            value_type cosl() const noexcept {
                return std::sqrt(cos2l());
            }
            vector_type exi() const noexcept { // unit vector along xi at reference meridian
                return {0, cosl(), -sinl()};
            }
            vector_type er() const noexcept { // unit vector along r at reference meridian
                return {0, sinl(), cosl()};
            }
            static vector_type ephi() noexcept { // unit vector along phi at reference meridian
                return {1, 0, 0};
            }
        };

        // coordinate transformations
        //
        CurviCoord _coord_trans(PolarCoord const& polar) const noexcept {
            _ParamPack const p{*this, polar};
            return CurviCoord{
                _phi(polar)/D1_r0ref(),
                p.r0_r0ref_n()*p.scaled_q2()/D2_r0ref(),
                p.r0_r0ref_3_n()/((3 - n())*D3_r0ref()) - q3ref() // translate q3 backward by q3ref
            };
        }
        PolarCoord _coord_trans(CurviCoord const& curvi) const noexcept {
            _ParamPack const p{*this, curvi};
            return PolarCoord{
                _phi(curvi),
                p.sinl(), // assumes is_valid(curvi) == true
                p.r0_r0ref()*r0ref()*p.cos2l()
            };
        }

        // metric tensor
        //
        class _MetricHelper : public _ParamPack {
        public:
            value_type cos2lr2_r0ref2; // cos^2λ * (r/r0ref)^2
            value_type h22g33_common; // h22 = h22g33_common/D2^2; g33 = h22g33_common*D3^2/(cos^2λ*r_r0ref_2)
            value_type h33g22_common; // h33 = h33g22_common/D3^2; g22 = h33g22_common*D2^2/(cos^2λ*r_r0ref_2)
            value_type h23g23_common; // h23 = h23g23_common/D2D3; g23 = -h23g23_common*D2D3/(cos^2λ*r_r0ref_2)
            value_type g11; // h11 = 1/g11
            //
            template <class Coord>
            explicit _MetricHelper(Geometry const &cs, Coord const &at) noexcept : _ParamPack{cs, at} {
                value_type const n_scaled_q2 = cs.n()*scaled_q2(), cos2l = this->cos2l();
                value_type const r0_r0ref_3 = this->r0_r0ref_3(), r_r0ref_2 = std::pow(r0_r0ref_3, 2./3)*UTL::pow<2>(cos2l);
                cos2lr2_r0ref2 = cos2l*r_r0ref_2;
                //
                h22g33_common = UTL::pow<2>(r0_r0ref_n())/r_r0ref_2*(UTL::pow<2>(n_scaled_q2) + UTL::pow<2>(2*sinl()*n_scaled_q2/cos2l + UTL::pow<3>(cos2l))*cos2l);
                value_type const factor = (1 + 3*UTL::pow<2>(sinl()))/cos2l;
                h33g22_common = UTL::pow<2>(r0_r0ref_3_n())/r_r0ref_2*factor;
                h23g23_common = (r0_r0ref_3/r_r0ref_2)*(factor*n_scaled_q2 + 2*sinl()*UTL::pow<3>(cos2l));
                g11 = UTL::pow<2>(cs.D1())*cos2lr2_r0ref2;
            }
        };
        template <class Coord>
        tensor_type _covar_metric(Coord const &at) const noexcept {
            _MetricHelper const m{*this, at};
            vector_type const g1{m.g11, 0, 0}; // 1st row
            vector_type g2{0, m.h33g22_common/m.cos2lr2_r0ref2, -m.h23g23_common/m.cos2lr2_r0ref2}; // 2nd row
            vector_type g3{0, g2.z, m.h22g33_common/m.cos2lr2_r0ref2}; // 3rd row
            (g2.rest() *= D2()) *= D().rest();
            (g3.rest() *= D3()) *= D().rest();
            return {g1, g2, g3};
        }
        template <class Coord>
        tensor_type _contr_metric(Coord const &at) const noexcept {
            _MetricHelper const m{*this, at};
            vector_type const h1{1/m.g11, 0, 0}; // 1st row
            vector_type h2{0, m.h22g33_common, m.h23g23_common}; // 2nd row
            vector_type h3{0, h2.z, m.h33g22_common}; // 3rd row
            (h2.rest() /= D2()) /= D().rest();
            (h3.rest() /= D3()) /= D().rest();
            return {h1, h2, h3};
        }

        // covariant/contravariant bases (cartesian components)
        //
        class _CurviBasisHelper : public _ParamPack {
        public:
            value_type coslr_r0ref; // cosλ * (r/r0ref)
            UTL::Vector<value_type, 2> h2g3_common; // h2 = {0, h2g3_common.x*eξ, h2g3_common.y*er}/D2; g3 = {0, -h2g3_common.y*eξ, h2g3_common.x*er}*D3/(cosλ*r_r0ref)
            UTL::Vector<value_type, 2> h3g2_common; // h3 = {0, h3g2_common.x*eξ, h3g2_common.y*er}/D3; g2 = {0, h3g2_common.y*eξ, -h3g2_common.x*er}*D2/(cosλ*r_r0ref)
            value_type g1; // h1 = {1/g1*eφ, 0, 0}; g21 = {g1*eφ, 0, 0}
            //
            template <class Coord>
            explicit _CurviBasisHelper(Geometry const &cs, Coord const &at) noexcept : _ParamPack{cs, at} {
                value_type const n_scaled_q2 = cs.n()*scaled_q2();
                value_type const cos2l = this->cos2l(), cosl = std::sqrt(cos2l);
                value_type const r_r0ref = r0_r0ref()*cos2l;
                coslr_r0ref = cosl*r_r0ref;
                value_type const factor = 2*sinl()/cosl;
                //
                h2g3_common.x = factor*n_scaled_q2 + UTL::pow<3>(cos2l)*cosl;
                h2g3_common.y = n_scaled_q2;
                h2g3_common *= r0_r0ref_n()/r_r0ref;
                //
                h3g2_common.x = factor;
                h3g2_common.y = 1;
                h3g2_common *= r0_r0ref_3_n()/r_r0ref;
                //
                g1 = cs.D1()*coslr_r0ref;
            }
        };
        //
        template <class Coord>
        tensor_type _covar_basis(Coord const &at, std::integral_constant<long, 0L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            UTL::Vector<value_type, 2> g2{m.h3g2_common.y, -m.h3g2_common.x};
            g2 *= D2()/m.coslr_r0ref;
            UTL::Vector<value_type, 2> g3{-m.h2g3_common.y, m.h2g3_common.x};
            g3 *= D3()/m.coslr_r0ref;
            return {
                translate_carts_from_reference_meridian(m.ephi() *= m.g1, at),
                translate_carts_from_reference_meridian(g2.x*m.exi() + g2.y*m.er(), at),
                translate_carts_from_reference_meridian(g3.x*m.exi() + g3.y*m.er(), at)
            };
        }
        template <class Coord>
        vector_type _covar_basis(Coord const &at, std::integral_constant<long, 1L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            return translate_carts_from_reference_meridian(m.ephi() *= m.g1, at);
        }
        template <class Coord>
        vector_type _covar_basis(Coord const &at, std::integral_constant<long, 2L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            UTL::Vector<value_type, 2> g2{m.h3g2_common.y, -m.h3g2_common.x};
            g2 *= D2()/m.coslr_r0ref;
            return translate_carts_from_reference_meridian(g2.x*m.exi() + g2.y*m.er(), at);
        }
        template <class Coord>
        vector_type _covar_basis(Coord const &at, std::integral_constant<long, 3L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            UTL::Vector<value_type, 2> g3{-m.h2g3_common.y, m.h2g3_common.x};
            g3 *= D3()/m.coslr_r0ref;
            return translate_carts_from_reference_meridian(g3.x*m.exi() + g3.y*m.er(), at);
        }
        //
        template <class Coord>
        tensor_type _contr_basis(Coord const &at, std::integral_constant<long, 0L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            UTL::Vector<value_type, 2> h2{m.h2g3_common.x, m.h2g3_common.y};
            h2 /= D2();
            UTL::Vector<value_type, 2> h3{m.h3g2_common.x, m.h3g2_common.y};
            h3 /= D3();
            return {
                translate_carts_from_reference_meridian(m.ephi() /= m.g1, at),
                translate_carts_from_reference_meridian(h2.x*m.exi() + h2.y*m.er(), at),
                translate_carts_from_reference_meridian(h3.x*m.exi() + h3.y*m.er(), at)
            };
        }
        template <class Coord>
        vector_type _contr_basis(Coord const &at, std::integral_constant<long, 1L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            return translate_carts_from_reference_meridian(m.ephi() /= m.g1, at);
        }
        template <class Coord>
        vector_type _contr_basis(Coord const &at, std::integral_constant<long, 2L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            UTL::Vector<value_type, 2> h2{m.h2g3_common.x, m.h2g3_common.y};
            h2 /= D2();
            return translate_carts_from_reference_meridian(h2.x*m.exi() + h2.y*m.er(), at);
        }
        template <class Coord>
        vector_type _contr_basis(Coord const &at, std::integral_constant<long, 3L>) const noexcept {
            _CurviBasisHelper const m{*this, at};
            UTL::Vector<value_type, 2> h3{m.h3g2_common.x, m.h3g2_common.y};
            h3 /= D3();
            return translate_carts_from_reference_meridian(h3.x*m.exi() + h3.y*m.er(), at);
        }

        // unit vectors in field-aligned coordinate system
        //
        class _MFABases : public _ParamPack {
            value_type _e1xi;
            value_type _e1r;
        public:
            template <class Coord>
            explicit _MFABases(Geometry const &cs, Coord const& at) noexcept : _ParamPack{cs, at} {
                _e1xi = _e1r = 1./std::sqrt(1 + 3*UTL::pow<2>(sinl()));
                _e1xi *= cosl();
                _e1r *= -2*sinl();
            }
            vector_type e1() const noexcept { // unit vector parallel to B
                return _e1xi*exi() + _e1r*er();
            }
            vector_type e2() const noexcept { // unit vector perp to B on meridional plane
                return -_e1r*exi() + _e1xi*er();
            }
            static vector_type e3() noexcept {
                return ephi();
            }
        };
        template <class Coord>
        tensor_type _mfa_basis(Coord const &at, std::integral_constant<long, 0L>) const noexcept {
            _MFABases const m{*this, at};
            return {
                translate_carts_from_reference_meridian(m.e1(), at),
                translate_carts_from_reference_meridian(m.e2(), at),
                translate_carts_from_reference_meridian(m.e3(), at)
            };
        }
        template <class Coord>
        vector_type _mfa_basis(Coord const &at, std::integral_constant<long, 1L>) const noexcept {
            return translate_carts_from_reference_meridian(_MFABases{*this, at}.e1(), at);
        }
        template <class Coord>
        vector_type _mfa_basis(Coord const &at, std::integral_constant<long, 2L>) const noexcept {
            return translate_carts_from_reference_meridian(_MFABases{*this, at}.e2(), at);
        }
        template <class Coord>
        vector_type _mfa_basis(Coord const &at, std::integral_constant<long, 3L>) const noexcept {
            return translate_carts_from_reference_meridian(_MFABases::e3(), at);
        }

        // normalized magnetic field vector
        //
        class _BField : public _ParamPack {
            value_type _BxiOB0, _BrOB0;
            value_type _B0OB0ref;
        public:
            template <class Coord>
            explicit _BField(Geometry const &cs, Coord const &at) noexcept : _ParamPack{cs, at} {
                _BxiOB0 = _BrOB0 = 1./UTL::pow<3>(cos2l());
                _BxiOB0 *= cosl();
                _BrOB0 *= -2*sinl();
                _B0OB0ref = 1/r0_r0ref_3();
            }
            vector_type BOB0() const noexcept {
                return _BxiOB0*exi() + _BrOB0*er();
            }
            vector_type BOB0ref() const noexcept {
                return BOB0() *= _B0OB0ref;
            }
        };
        template <class Coord>
        vector_type _BcartsOverB0ref(Coord const &at) const noexcept {
            return translate_carts_from_reference_meridian(_BField{*this, at}.BOB0ref(), at);
        }
        template <class Coord>
        vector_type _BcartsOverB0(Coord const &at) const noexcept {
            return translate_carts_from_reference_meridian(_BField{*this, at}.BOB0(), at);
        }
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOGeometry_h */
