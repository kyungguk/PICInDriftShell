//
//  GEOPartialShellLoader__2D.cc
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEOPartialShellLoader__2D.h"
#include "GEOParticleLoader__ND.hh"

constexpr long ND = 2;

// MARK:- GEO::__1_::PartialShellLoader<2D>
//
GEO::__1_::PartialShellLoader<ND>::~PartialShellLoader<ND>()
{
}
GEO::__1_::PartialShellLoader<ND>::PartialShellLoader() noexcept
: ParticleLoader{} {
}
GEO::__1_::PartialShellLoader<ND>::PartialShellLoader(CoordSystem const &cs, position_type const &q_min, size_vector_type const &N, param_type &&param)
: ParticleLoader{cs, q_min, N}, _param{std::move(param)} {
    // init vdist
    //
    _dist_v.reset(new UTL::ShellDistribution{1, _param.vdf.vsOvth()}); // normalized to vth_0
    _dist_alpha.reset(new UTL::SineAlphaPitchAngleDistribution{_param.vdf.zeta()});
    _dist_phi.reset(new UTL::UniformDistribution{0, 2*M_PI}); // perp velocity phase

    // q1 distribution
    //
    _dist_q1.reset(new UTL::UniformDistribution{ptl_q_min().x, ptl_q_max().x});

    // q2 distribution
    //
    _dist_q2.reset(new UTL::CustomDistribution{ptl_q_min().y, ptl_q_max().y, [this](value_type const q2)->value_type {
        return this->nOn0(q2);
    }, true});
}

auto GEO::__1_::PartialShellLoader<ND>::NcellOverNtotal() const
-> value_type {
    value_type NcellOverNtotal{1};
    CurviCoord const q_min{-.5}, q_max{.5}; // unit box centered at (r0ref, 0)
    NcellOverNtotal *= _dist_q1->cdf(q_max.q1()) - _dist_q1->cdf(q_min.q1());
    NcellOverNtotal *= _dist_q2->cdf(q_max.q2()) - _dist_q2->cdf(q_min.q2());
    return NcellOverNtotal;
}

auto GEO::__1_::PartialShellLoader<ND>::operator()() const
-> particle_type {
    particle_type ptl = this->_load();
    ptl.vel *= _param.vdf.vth();
    return ptl;
}
auto GEO::__1_::PartialShellLoader<ND>::_load() const
-> particle_type {
    RandomRealPool &rng = this->rng();
    //
    // position
    //
    CurviCoord const curvi{_dist_q1->icdf(rng.q1()), _dist_q2->icdf(rng.q2()), 0};
    //
    // orthogonal unit vectors in local field-aligned reference
    //
    PolarCoord const polar = coord_system().coord_trans(curvi);
    CoordSystem::tensor_type const e = coord_system().mfa_basis<0>(polar); // unit vector {parallel, in-plane perp, out-of-plane perp} to B
    //
    // velocity
    //
    value_type const phi = _dist_phi->icdf(rng.phi());
    value_type const v = _dist_v->icdf(rng.v1()); // normalized to vth_0
    value_type const alpha = _dist_alpha->icdf(rng.alpha());
    velocity_type vel{v*std::cos(alpha), v*std::sin(alpha) * std::cos(phi), v*std::sin(alpha) * std::sin(phi)};
    vel = vel.x*e.x + vel.y*e.y + vel.z*e.z;
    return particle_type{vel, curvi.take<2>()};
}

auto GEO::__1_::PartialShellLoader<ND>::normalized_current_density(CurviCoord const &curvi) const
-> velocity_type {
    return ParticleLoader::normalized_current_density(curvi);
}
