//
//  GEOVDFDescriptor.cc
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/9/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEOVDFDescriptor.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath>
#include <tuple> // std::tie

namespace {
    constexpr double quiet_nan = std::numeric_limits<double>::quiet_NaN();
    //
    template <class T>
    inline UTL::Vector<T, 3> cross(UTL::Vector<T, 3> const &A, UTL::Vector<T, 3> const &B) noexcept {
        return {-A.z*B.y + A.y*B.z, A.z*B.x - A.x*B.z, -A.y*B.x + A.x*B.y};
    }
}

// MARK:- GEO::__1_::VDFDescriptor
//
GEO::__1_::VDFDescriptor::~VDFDescriptor()
{
}

auto GEO::__1_::VDFDescriptor::BOB0(PolarCoord const &polar) noexcept
-> value_type {
    return std::sqrt(1 + 3*UTL::pow<2>(polar.xi()))/UTL::pow<3>(cos2l(polar));
}
auto GEO::__1_::VDFDescriptor::BxGBOB2(PolarCoord const &polar) noexcept
-> value_type {
    value_type const denom = 1 + 3*UTL::pow<2>(polar.xi());
    value_type const tmp1 = std::sqrt(cos2l(polar)/denom);
    value_type const tmp2 = (1 + UTL::pow<2>(polar.xi()))/denom;
    return -3/polar.r()*tmp1*tmp2;
}
auto GEO::__1_::VDFDescriptor::finiteGyro(PolarCoord const &polar) noexcept
-> value_type {
    static value_type const sqrt_3 = std::sqrt(3);
    value_type const above = (1 - sqrt_3*polar.xi())*(1 + sqrt_3*polar.xi());
    value_type const below = std::sqrt(cos2l(polar)*(1 + 3*UTL::pow<2>(polar.xi())));
    return -above/(polar.r()*below);
}

auto GEO::__1_::VDFDescriptor::P(PolarCoord const &polar) const
-> std::pair<value_type, value_type> {
    value_type const n0 = this->n0();
    auto P = this->POn0(polar);
    std::get<0>(P) *= n0;
    std::get<1>(P) *= n0;
    return P;
}

auto GEO::__1_::VDFDescriptor::JphixBOB0ref(PolarCoord const &polar) const
-> value_type {
    value_type P1, P2;
    std::tie(P1, P2) = this->P(polar);

    // 1. guiding center term
    //
    value_type const Vd = (P2 + P1)*BxGBOB2(polar);

    // 2. finite gyro radius term
    //
    value_type const Vg = P2*finiteGyro(polar);

    return Vd + Vg;
}

GEO::__1_::BiMaxwellianVDF::BiMaxwellianVDF() noexcept
: VDFDescriptor{}, _A0{quiet_nan}, _vth1{quiet_nan} {
}
GEO::__1_::BiMaxwellianVDF::BiMaxwellianVDF(value_type const vth1_0, value_type const A0, bool const should_ignore_parallel_pressure)
: VDFDescriptor{}, _A0{A0}, _vth1{vth1_0}, should_ignore_parallel_pressure{should_ignore_parallel_pressure} {
    if (vth1_0 <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - non-positive parallel thermal speed");
    }
    if (A0 < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative temperature anisotropy");
    }
}

auto GEO::__1_::BiMaxwellianVDF::POn0(PolarCoord const &polar) const
-> std::pair<value_type, value_type> {
    value_type const eta = this->eta(polar);
    return std::make_pair(eta*T1_0()*(!should_ignore_parallel_pressure), UTL::pow<2>(eta)*T2_0());
}

GEO::__1_::PartialShellVDF::PartialShellVDF() noexcept
: VDFDescriptor{}, _zeta{quiet_nan}, _vs{quiet_nan}, _vth{quiet_nan} {
}
GEO::__1_::PartialShellVDF::PartialShellVDF(value_type const vth, value_type const vs, value_type const zeta, bool const should_ignore_parallel_pressure)
: VDFDescriptor{}, _zeta{zeta}, _vs{vs}, _vth{vth}, should_ignore_parallel_pressure{should_ignore_parallel_pressure} {
    if (vth <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - non-positive thermal speed");
    }
    if (vs < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative shell speed");
    }
    if (zeta < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative anisotropy index");
    }
}

auto GEO::__1_::PartialShellVDF::eta(PolarCoord const &polar) const noexcept
-> value_type {
    return std::pow(BOB0(polar), -.5*zeta());
}

auto GEO::__1_::PartialShellVDF::T0() const noexcept
-> value_type {
    constexpr value_type sqrtpi = 2/M_2_SQRTPI;
    value_type const b = vsOvth(), b2 = b*b;
    value_type const bemb2 = b*std::exp(-b2), erfc = std::erfc(-b);
    value_type const denom = bemb2 + sqrtpi*(.5 + b2)*erfc;
    value_type const numer = bemb2*(2.5 + b2) + sqrtpi*(.75 + b2*(3 + b2))*erfc;
    return UTL::pow<2>(vth())*numer/denom;
}
auto GEO::__1_::PartialShellVDF::T12() const noexcept
-> std::pair<value_type, value_type> {
    value_type const zeta = this->zeta();
    value_type const T1 = T0()/(3 + zeta);
    return std::make_pair(T1, .5*(2 + zeta)*T1);
}
auto GEO::__1_::PartialShellVDF::POn0(PolarCoord const &polar) const
-> std::pair<value_type, value_type> {
    value_type const eta = this->eta(polar);
    auto T = T12();
    T.first *= eta*(!should_ignore_parallel_pressure);
    T.second *= eta;
    return T;
}
