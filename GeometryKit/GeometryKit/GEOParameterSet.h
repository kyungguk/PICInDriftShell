//
//  GEOParameterSet.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOParameterSet_h
#define GEOParameterSet_h

#include <GeometryKit/GEOParameterSet__ND.h>
#include <GeometryKit/GEOParameterSet__1D.h>
#include <GeometryKit/GEOParameterSet__2D.h>
#include <GeometryKit/GEOParameterSet__3D.h>

#endif /* GEOParameterSet_h */
