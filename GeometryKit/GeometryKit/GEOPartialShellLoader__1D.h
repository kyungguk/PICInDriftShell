//
//  GEOPartialShellLoader__1D.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/25/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOPartialShellLoader__1D_h
#define GEOPartialShellLoader__1D_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEOParticleLoader__ND.h>
#include <memory>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::PartialShellLoader<1D>
    //
    template <>
    class PartialShellLoader<1L> final : public ParticleLoader<1L> {
    public:
        using vdf_type = PartialShellVDF;
        struct param_type {
            PartialShellVDF vdf{}; //!< Partial shell VDF descriptor.
            explicit param_type() noexcept {}
        };

        PartialShellLoader(PartialShellLoader&&) = default;
        PartialShellLoader& operator=(PartialShellLoader&&) = default;

        // constructors:
        ~PartialShellLoader();
        explicit PartialShellLoader() noexcept;
        explicit PartialShellLoader(CoordSystem const &cs, position_type const &q_min, size_vector_type const &N, param_type &&param);

        // overloads:
        VDFDescriptor const &vdf() const override { return _param.vdf; }
        particle_type operator()() const override;
        value_type NcellOverNtotal() const override; // !! ratio of N_ptl at cell (0, 0, 0) to N_total
        velocity_type normalized_current_density(CurviCoord const &curvi) const override;

    private:
        param_type _param;
        std::unique_ptr<UTL::UniformDistribution> _dist_q1;
        std::unique_ptr<UTL::UniformDistribution> _dist_phi;
        std::unique_ptr<UTL::ShellDistribution> _dist_v;
        std::unique_ptr<UTL::SineAlphaPitchAngleDistribution> _dist_alpha;

        // load particles
        particle_type _load() const; // velocity is normalized by _vth_0
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOPartialShellLoader__1D_h */
