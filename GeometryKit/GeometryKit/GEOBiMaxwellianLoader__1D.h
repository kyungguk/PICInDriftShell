//
//  GEOBiMaxwellianLoader__1D.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOBiMaxwellianLoader__1D_h
#define GEOBiMaxwellianLoader__1D_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEOParticleLoader__ND.h>
#include <memory>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::BiMaxwellianLoader<1D>
    //
    template <>
    class BiMaxwellianLoader<1L> final : public ParticleLoader<1L> {
    public:
        using vdf_type = BiMaxwellianVDF;
        struct param_type {
            BiMaxwellianVDF vdf{}; //!< Bi-Maxwellian VDF descriptor.
            explicit param_type() noexcept {}
        };

        BiMaxwellianLoader(BiMaxwellianLoader&&) = default;
        BiMaxwellianLoader& operator=(BiMaxwellianLoader&&) = default;

        // constructors:
        ~BiMaxwellianLoader();
        explicit BiMaxwellianLoader() noexcept;
        explicit BiMaxwellianLoader(CoordSystem const &cs, position_type const &q_min, size_vector_type const &N, param_type &&param);

        // overloads:
        VDFDescriptor const &vdf() const override { return _param.vdf; }
        particle_type operator()() const override;
        value_type NcellOverNtotal() const override; // !! ratio of N_ptl at cell (0, 0, 0) to N_total
        velocity_type normalized_current_density(CurviCoord const &curvi) const override;

    private:
        param_type _param;
        std::unique_ptr<UTL::UniformDistribution> _dist_q1;
        std::unique_ptr<UTL::UniformDistribution> _dist_phi;
        std::unique_ptr<UTL::ParallelMaxwellianDistribution> _dist_v1;
        std::unique_ptr<UTL::PerpendicularMaxwellianDistribution> _dist_v2;

        // load particles
        particle_type _load() const; // velocity is normalized by _vth1_0
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOBiMaxwellianLoader__1D_h */
