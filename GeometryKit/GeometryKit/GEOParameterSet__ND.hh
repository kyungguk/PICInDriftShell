//
//  GEOParameterSet__ND.hh
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOParameterSet__ND_hh
#define GEOParameterSet__ND_hh

#include "GEOParameterSet__ND.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath> // std::isfinite

// MARK: Helpers
//
namespace {
    constexpr double quiet_nan = std::numeric_limits<double>::quiet_NaN();
    //
    template <long S>
    inline UTL::Vector<long, S> isfinite(UTL::Vector<double, S> const &a) noexcept {
        UTL::Vector<long, S> result;
        for (long i = 0; i < S; ++i) {
            result[i] = std::isfinite(a[i]);
        }
        return result;
    }
}

// MARK:- GEO::__1_::_ParameterSet<ND>
//
template <long ND>
GEO::__1_::_ParameterSet<ND>::_ParameterSet() noexcept
: _cs{}, _N{0}, _q_min{quiet_nan}, _q_max{quiet_nan}, _c{quiet_nan}, _O0ref{quiet_nan}, _opts{} {
}
template <long ND>
GEO::__1_::_ParameterSet<ND>::_ParameterSet(CoordSystem const& cs, position_type const& q_min, size_vector_type const& N, value_type const c, value_type const O0ref, OptionSet const &opts)
: _cs{cs}, _N{N}, _q_min{q_min}, _q_max{q_min + position_type{N - 1L}}, _c{c}, _O0ref{O0ref}, _opts{opts} {
    // argument checks
    //
    if (UTL::reduce_bit_or(_N <= size_vector_type{0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid number of cells");
    }
    if (!UTL::reduce_bit_and(isfinite(_q_min))) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid q_min");
    }
    if (_c <= 0 || _O0ref <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid c or O0");
    }
    range_check(cs, _q_min - value_type{pad_size}, _q_max + value_type{pad_size});
}
template <long ND>
void GEO::__1_::_ParameterSet<ND>::range_check(CoordSystem const &cs, UTL::Vector<value_type, 3> const &q_min, UTL::Vector<value_type, 3> const &q_max)
{
    // q_min and q_max should include any paddings
    //
    // check q1
    //
    if (!cs.is_valid(CurviCoord{q_min.x, 0, 0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid minimum of q1");
    }
    if (!cs.is_valid(CurviCoord{q_max.x, 0, 0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid maximum of q1");
    }
    // check q2 and q3
    //
    { // at minimum q3 coordinate
        CurviCoord curvi{0, 0, q_min.z};
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid minimum of q3");
        }
        curvi.q2() = q_max.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - maximum of q2 at q3_min is too large");
        }
        curvi.q2() = q_min.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - minimum of q2 at q3_min is too small");
        }
    }
    { // at maximum q3 coordinate
        CurviCoord curvi{0, 0, q_max.z};
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid maximum of q3");
        }
        curvi.q2() = q_max.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - maximum of q2 at q3_max is too large");
        }
        curvi.q2() = q_min.y;
        if (!cs.is_valid(curvi)) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - minimum of q2 at q3_max is too small");
        }
    }
}

#endif /* GEOParameterSet__ND_hh */
