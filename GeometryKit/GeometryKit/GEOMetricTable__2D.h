//
//  GEOMetricTable__2D.h
//  GeometryKit
//
//  Created by Kyungguk Min on 3/28/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOMetricTable__2D_h
#define GEOMetricTable__2D_h

#include <GeometryKit/GEOMetricTable__ND.h>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::MetricTable<2D>
    //
    template <>
    class MetricTable<2L> : public _MetricTable<2L> {
    public:
        MetricTable(MetricTable &&) = default;
        MetricTable &operator=(MetricTable &&) = default;
        MetricTable(MetricTable const&);
        MetricTable &operator=(MetricTable const&);

        // constructors
        //
        ~MetricTable();
        explicit MetricTable() noexcept;
        explicit MetricTable(CoordSystem const& cs, size_vector_type const& N, position_type const& q_min);

        // coordinate tranformation
        // calls are not reentrant-safe (due to shape())
        //
        // ijk should be between 0 and N - 1
        tensor_type covar_metric(size_vector_type const &ijk) const noexcept;
        tensor_type contr_metric(size_vector_type const &ijk) const noexcept;
        vector_type contr_to_covar(vector_type const& contr, size_vector_type const& ijk) const noexcept;
        vector_type covar_to_contr(vector_type const& covar, size_vector_type const& ijk) const noexcept;
        vector_type contr_to_carts(vector_type const& contr, size_vector_type const& ijk) const noexcept;
        vector_type carts_to_contr(vector_type const& carts, size_vector_type const& ijk) const noexcept;
        vector_type covar_to_carts(vector_type const& covar, size_vector_type const& ijk) const noexcept;
        vector_type carts_to_covar(vector_type const& carts, size_vector_type const& ijk) const noexcept;
        vector_type   mfa_to_carts(vector_type const&   mfa, size_vector_type const& ijk) const noexcept;
        vector_type carts_to_mfa  (vector_type const& carts, size_vector_type const& ijk) const noexcept;
        // pos should be between q_min and q_max
        vector_type contr_to_covar(vector_type const& contr, position_type const& pos) const noexcept;
        vector_type covar_to_contr(vector_type const& covar, position_type const& pos) const noexcept;
        vector_type contr_to_carts(vector_type const& contr, position_type const& pos) const noexcept {
            return contr_to_carts(contr, CurviCoord{pos, nullptr});
        }
        vector_type carts_to_contr(vector_type const& carts, position_type const& pos) const noexcept {
            return carts_to_contr(carts, CurviCoord{pos, nullptr});
        }
        vector_type covar_to_carts(vector_type const& covar, position_type const& pos) const noexcept {
            return covar_to_carts(covar, CurviCoord{pos, nullptr});
        }
        vector_type carts_to_covar(vector_type const& carts, position_type const& pos) const noexcept {
            return carts_to_covar(carts, CurviCoord{pos, nullptr});
        }
        vector_type   mfa_to_carts(vector_type const&   mfa, position_type const& pos) const noexcept {
            return mfa_to_carts(mfa, CurviCoord{pos, nullptr});
        }
        vector_type carts_to_mfa  (vector_type const& carts, position_type const& pos) const noexcept {
            return carts_to_mfa(carts, CurviCoord{pos, nullptr});
        }
        // curvi should be between q_min and q_max
        vector_type contr_to_carts(vector_type const& contr, CurviCoord const& curvi) const noexcept;
        vector_type carts_to_contr(vector_type const& carts, CurviCoord const& curvi) const noexcept;
        vector_type covar_to_carts(vector_type const& covar, CurviCoord const& curvi) const noexcept;
        vector_type carts_to_covar(vector_type const& carts, CurviCoord const& curvi) const noexcept;
        vector_type   mfa_to_carts(vector_type const&   mfa, CurviCoord const& curvi) const noexcept;
        vector_type carts_to_mfa  (vector_type const& carts, CurviCoord const& curvi) const noexcept;

        // magnetic field interpolation
        // calls are not reentrant-safe (due to shape())
        //
        // ijk should be between 0 and N - 1
        vector_type BcartsOverB0(size_vector_type const& ijk) const noexcept;
        vector_type BcartsOverB0ref(size_vector_type const& ijk) const noexcept {
            return BcartsOverB0(ijk);
        }
        // pos should be between q_min and q_max
        vector_type BcartsOverB0(position_type const& pos) const noexcept {
            return BcartsOverB0(CurviCoord{pos, nullptr});
        }
        vector_type BcartsOverB0ref(position_type const& pos) const noexcept {
            return BcartsOverB0ref(CurviCoord{pos, nullptr});
        }
        // curvi should be between q_min and q_max
        vector_type BcartsOverB0(CurviCoord const& curvi) const noexcept;
        vector_type BcartsOverB0ref(CurviCoord const& curvi) const noexcept {
            return BcartsOverB0(curvi);
        }

        // position vector
        //
        vector_type position(size_vector_type const &ijk) const noexcept;
        vector_type position(position_type const &pos) const noexcept {
            return position(CurviCoord{pos, nullptr});
        }
        vector_type position(CurviCoord const &curvi) const noexcept;

    private:
        void _init_tables(_MetricTable &table) const;
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOMetricTable__2D_h */
