//
//  GEOWaveDamper.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOWaveDamper_h
#define GEOWaveDamper_h

#include <GeometryKit/GEOWaveDamper__ND.h>
#include <GeometryKit/GEOWaveDamper__1D.h>
#include <GeometryKit/GEOWaveDamper__2D.h>
#include <GeometryKit/GEOWaveDamper__3D.h>

#endif /* GEOWaveDamper_h */
