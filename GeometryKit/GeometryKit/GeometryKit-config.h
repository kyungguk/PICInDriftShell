//
//  GeometryKit-config.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GeometryKit_config_h
#define GeometryKit_config_h


// root namespace
//
#ifndef GEOKIT_NAMESPACE
#define GEOKIT_NAMESPACE GEO
#define GEOKIT_BEGIN_NAMESPACE namespace GEOKIT_NAMESPACE {
#define GEOKIT_END_NAMESPACE }
#endif


// GCC Deprecated attribute
//
#ifndef GEOKIT_DEPRECATED_ATTRIBUTE
#define GEOKIT_DEPRECATED_ATTRIBUTE __attribute__ ((deprecated))
#endif


#endif /* GeometryKit_config_h */
