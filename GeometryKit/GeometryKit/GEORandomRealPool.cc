//
//  GEORandomRealPool.cc
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEORandomRealPool.h"
#include <stdexcept>
#include <string>
#include <cstdio>
#include <random>

// MARK: Random Seed
//
namespace {
    unsigned long seed() {
#if 1
        static std::random_device rd;
        return std::uniform_int_distribution<unsigned>{}(rd);
#else
        union { long i; unsigned long u; } seed;
        seed.i = 0x0;
        {
            std::FILE *fp = std::fopen("/dev/random", "r");
            if (nullptr != fp) {
                for (unsigned i = 0; i < sizeof(seed); ++i) {
                    seed.i <<= 8;
                    seed.i |= std::fgetc(fp);
                }
                std::fclose(fp);
            } else {
                throw std::runtime_error(__PRETTY_FUNCTION__);
            }
        }
        return seed.u;
#endif
    }
}


// MARK:- GEO::__1_::RandomRealPool
//
GEO::__1_::RandomRealPool::RandomRealPool(UTL::Vector<UTL::RandomReal::size_type, RNGPack::size()> const &seeds)
: _rngs() {
    // ** prime numbers should be used in BitReversedRandomReal **
    //
    std::get<0>(_rngs).reset(new UTL::NRRandomReal(             std::get<0>(seeds))); // v1
    std::get<1>(_rngs).reset(new UTL::NRRandomReal(             std::get<1>(seeds))); // v2
    std::get<2>(_rngs).reset(new UTL::BitReversedRandomReal( 2, std::get<2>(seeds))); // phi
    std::get<3>(_rngs).reset(new UTL::BitReversedRandomReal( 3, std::get<3>(seeds))); // alpha
    std::get<4>(_rngs).reset(new UTL::BitReversedRandomReal( 5, std::get<4>(seeds))); // q1
    std::get<5>(_rngs).reset(new UTL::BitReversedRandomReal( 7, std::get<5>(seeds))); // q2
    std::get<6>(_rngs).reset(new UTL::BitReversedRandomReal(11, std::get<6>(seeds))); // q3
}
auto GEO::__1_::RandomRealPool::rng()
-> RandomRealPool &{
    static RandomRealPool rng{};
    return rng;
}
auto GEO::__1_::RandomRealPool::seeds()
-> UTL::Vector<UTL::RandomReal::size_type, RNGPack::size()> {
    return {::seed(), ::seed(), ::seed(), ::seed(), ::seed(), ::seed(), ::seed()};
}
