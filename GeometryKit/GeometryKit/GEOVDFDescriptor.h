//
//  GEOVDFDescriptor.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/9/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOVDFDescriptor_h
#define GEOVDFDescriptor_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEOCoordWrapper.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <utility>
#include <memory>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::VDFDescriptor
    //
    /**
     @brief Base class for velocity distribution function (VDF) descriptor.
     */
    class VDFDescriptor {
    public:
        using value_type = double;

        virtual ~VDFDescriptor();

        /**
         @brief Ratio of the density at a given location to the equatorial density, n(ξ, r, φ)/n0(ξ, r0, φ), where r0 = r/(1 - ξ^2).
         @discussion Synonym for η.
         */
        virtual value_type nOn0(PolarCoord const &polar) const = 0;

        /**
         @brief A pair of parallel and perpendicular components of "pressure."
         @discussion Pressure is actually the 2nd velocity moment, <v*v> - <v>^2/<1>.
         */
        std::pair<value_type, value_type> P(PolarCoord const &polar) const;

        /**
         @brief Perpendicular current density (Cartesian vector components).
         @discussion The current density is normalized such that (Ωs,ref*<v>)*(B/B0,ref) is returned.
         */
        value_type JphixBOB0ref(PolarCoord const &polar) const;

    private: // field-aligned gradient helpers
        constexpr static value_type n0() noexcept { return 1; }
        static value_type cos2l(PolarCoord const &polar) noexcept { return (1 - polar.xi())*(1 + polar.xi()); }
        static value_type r0(PolarCoord const &polar) noexcept { return polar.r()/cos2l(polar); }
        inline static value_type BxGBOB2(PolarCoord const &polar) noexcept; // B x gradB/B^2
        inline static value_type finiteGyro(PolarCoord const &polar) noexcept; // geometric factor of finite gyro radius effect
    protected:
        static value_type BOB0(PolarCoord const &polar) noexcept; // B/B_0, where B_0 is the equatorial value

        VDFDescriptor(VDFDescriptor &&) noexcept = default;
        VDFDescriptor &operator=(VDFDescriptor &&) noexcept = default;
        explicit VDFDescriptor() noexcept {}

        // normalized pressure, P/n0, where n0 is the equatorial density
        //
        virtual std::pair<value_type, value_type> POn0(PolarCoord const &) const = 0;
    };

    /**
     @brief Bi-Maxwellian VDF descriptor.
     */
    class BiMaxwellianVDF final : public VDFDescriptor {
        value_type _A0; //!< T2/T1 - 1 at the equator.
        value_type _vth1; //!< Uniform equatorial parallel thermal speed; vth1 == vth1_0.
        bool should_ignore_parallel_pressure{};

    public:
        BiMaxwellianVDF(BiMaxwellianVDF &&) noexcept = default;
        BiMaxwellianVDF &operator=(BiMaxwellianVDF &&) noexcept = default;
        explicit BiMaxwellianVDF() noexcept;

        /**
         @brief Construct BiMaxwellianVDF object.
         @param vth1_0 Equatorial parallel thermal speed.
         @param A0 Equatorial temperature anisotropy, T2_0/T1_0 - 1.
         */
        explicit BiMaxwellianVDF(value_type const vth1_0, value_type const A0, bool const should_ignore_parallel_pressure = {});

        /**
         @brief Equatorial temperature anisotropy, T2_0/T1_0 - 1.
         */
        value_type A0() const noexcept { return _A0; }
        /**
         @brief Parallel thermal spread.
         */
        value_type vth1() const noexcept { return _vth1; }
        /**
         @brief Ratio of T2 to T1 at a given location.
         */
        value_type T2OT1(PolarCoord const &polar) const noexcept { return eta(polar)*(A0() + 1); }
        /**
         @brief Ratio of the number density at a given location on the field line to the density at the equator.
         @discussion η(ξ) = n(ξ, r, φ)/n0(ξ, r0, φ), where r0 = r/(1 - ξ^2).
         */
        value_type nOn0(PolarCoord const &polar) const override { return eta(polar); }

    private:
        value_type eta(PolarCoord const &polar) const noexcept { return 1/(1 + A0()*(1 - 1/BOB0(polar))); } // n(ξ, r0)/n0(0, r0)
        value_type T1_0() const noexcept { return .5*UTL::pow<2>(vth1()); } // equatorial value
        value_type T2_0() const noexcept { return (A0() + 1)*T1_0(); } // equatorial value
        std::pair<value_type, value_type> POn0(PolarCoord const &) const override;
    };

    /**
     @brief Partial shell VDF descriptor.
     */
    class PartialShellVDF final : public VDFDescriptor {
        value_type _zeta; //!< Equatorial pitch angle anisotropy index, ζ = ζ_0.
        value_type _vs; //!< Equatorial shell speed; vs == vs_0.
        value_type _vth; //!< Equatorial shell thermal spread; vth == vth_0.
        bool should_ignore_parallel_pressure{};

    public:
        PartialShellVDF(PartialShellVDF &&) noexcept = default;
        PartialShellVDF &operator=(PartialShellVDF &&) noexcept = default;
        explicit PartialShellVDF() noexcept;

        /**
         @brief Construct BiMaxwellianVDF object.
         @param vth Equatorial shell thermal spread.
         @param vs Equatorial shell speed.
         @param zeta Equatorial pitch angle anisotropy index.
         */
        explicit PartialShellVDF(value_type const vth, value_type const vs, value_type const zeta, bool const should_ignore_parallel_pressure = {});

        /**
         @brief Pitch angle anisotropy index profile.
         */
        value_type zeta() const noexcept { return _zeta; }
        /**
         @brief Shell thermal spread.
         @discussion Assumed to be spatially uniform.
         */
        value_type vth() const noexcept { return _vth; }
        /**
         @brief Shell speed.
         @discussion Assumed to be spatially uniform.
         */
        value_type vsOvth() const noexcept { return _vs/vth(); }
        /**
         @brief Ratio of the number density at a given location on the field line to the density at the equator.
         @discussion η(ξ) = n(ξ, r, φ)/n0(ξ, r0, φ), where r0 = r/(1 - ξ^2).
         */
        value_type nOn0(PolarCoord const &polar) const override { return eta(polar); }

    private:
        value_type eta(PolarCoord const &polar) const noexcept; // n(ξ, r0)/n0(0, r0)
        inline value_type T0() const noexcept;
        inline std::pair<value_type, value_type> T12() const noexcept;
        std::pair<value_type, value_type> POn0(PolarCoord const &) const override;
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOVDFDescriptor_h */
