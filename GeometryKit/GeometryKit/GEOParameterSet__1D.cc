//
//  GEOParameterSet__1D.cc
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEOParameterSet__1D.h"
#include "GEOParameterSet__ND.hh"

constexpr long ND = 1;

// MARK:- GEO::__1_::ParameterSet<ND>
//
GEO::__1_::ParameterSet<ND>::~ParameterSet<ND>()
{
}
GEO::__1_::ParameterSet<ND>::ParameterSet() noexcept
: _ParameterSet{} {
}
GEO::__1_::ParameterSet<ND>::ParameterSet(CoordSystem const& cs, position_type const& q_min, size_vector_type const& N, value_type const c, value_type const O0ref, OptionSet const &opts)
: _ParameterSet{cs, q_min, N, c, O0ref, opts}, _metric_table{} {
    if (option_set().use_metric_table) {
        std::get<cell_cent>(_metric_table) = MetricTable<ND>{cs, N, q_min};
        std::get<cell_edge>(_metric_table) = MetricTable<ND>{cs, N, q_min + 0.5*long{this->grid_strategy()}};
    }
}

auto GEO::__1_::ParameterSet<ND>::slice(size_vector_type const &location, size_vector_type const &length, bool const use_metric_table) const
-> ParameterSet {
    if (UTL::reduce_bit_or(location < size_vector_type{0}) || UTL::reduce_bit_or(length <= size_vector_type{0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small location or length");
    }
    if (UTL::reduce_bit_or(location + length > N())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too large location or length");
    }
    OptionSet opts{option_set()}; {
        opts.use_metric_table = use_metric_table;
    }
    return ParameterSet{coord_system(), grid_q_min() + position_type{location}, length, c(), O0ref(), opts};
}
