//
//  GEOMetricTable__ND.h
//  GeometryKit
//
//  Created by Kyungguk Min on 3/28/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOMetricTable__ND_h
#define GEOMetricTable__ND_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEOCoordSystem.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <memory>
#include <cmath> // std::sqrt in mfa_basis

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::_MetricTable<ND>
    //
    template <long ND> class MetricTable;

    template <long _ND>
    class _MetricTable {
    public:
        static constexpr long ND = _ND;
        static_assert(ND <= 3, "invalid ND");
        static constexpr long pad_size = 3;

        // types
        //
        using size_type = long;
        using value_type = double;
        using vector_type = CoordSystem::vector_type;
        using tensor_type = CoordSystem::tensor_type;
        using position_type = UTL::Vector<value_type, ND>;
        using size_vector_type = UTL::Vector<size_type, ND>;
        //
        using shape_type = UTL::Shape<value_type, 1L>; // for interpolation
        struct shape_vector {
            typename std::conditional<(ND >= 2), shape_type, decltype(nullptr)>::type y;
            typename std::conditional<(ND >= 3), shape_type, decltype(nullptr)>::type z;
        };
        //
        using compact_vector_type = UTL::SIMDVector<value_type, 2>; // {vy, vz}
        using compact_tensor_type = UTL::Vector<value_type, 5>; // {Txx, Tyy, Tyz, Tzy, Tzz}
        using compact_vector_table = typename std::conditional<ND == 1, compact_vector_type,
        UTL::ArrayND<compact_vector_type, ND - 1, pad_size>>::type;
        using compact_tensor_table = typename std::conditional<ND == 1, compact_tensor_type,
        UTL::ArrayND<compact_tensor_type, ND - 1, pad_size>>::type;
        struct package_type {
            // table of basis vector components at the reference meridian
            // assumes symmetry about (independent of) the 1st (azimuthal) coordinate
            // assumes that the 1st (azimuthal) coordinate is orthogonal to the other coordinates
            //
            typename std::conditional<ND == 3, UTL::ArrayND<value_type, 1, pad_size>, value_type
            >::type r0Or0ref3; // (r0/r0ref)^3 which is roughly proportional to q3; used to calculate B0/B0,ref
            compact_vector_table position, BOB0; // B/B_0
            compact_tensor_table covar_bases, contr_bases, mfa_bases;
            package_type([[maybe_unused]] UTL::Vector<size_type, 1> const &N) : r0Or0ref3{}, position{}, BOB0{},
            covar_bases{}, contr_bases{}, mfa_bases{} {}
            package_type(UTL::Vector<size_type, 2> const &N) : r0Or0ref3{}, position{N.rest()}, BOB0{N.rest()},
            covar_bases{N.rest()}, contr_bases{N.rest()}, mfa_bases{N.rest()} {}
            package_type(UTL::Vector<size_type, 3> const &N) : r0Or0ref3{std::get<2>(N)}, position{N.rest()}, BOB0{N.rest()},
            covar_bases{N.rest()}, contr_bases{N.rest()}, mfa_bases{N.rest()} {}
        };

        // properties
        //
        CoordSystem const& coord_system() const noexcept { return _cs; }
        size_vector_type const &N() const noexcept { return _N; }
        position_type const& q_min() const noexcept { return _q_min; }
        position_type const& q_max() const noexcept { return _q_max; }
        explicit operator bool() const noexcept { return static_cast<bool>(_table); }

        package_type const* operator->() const noexcept { return _table.get(); }
        package_type      * operator->()       noexcept { return _table.get(); }

    protected:
        _MetricTable(_MetricTable &&) = default;
        _MetricTable &operator=(_MetricTable &&) = default;

        ~_MetricTable() {}
        explicit _MetricTable() noexcept;
        explicit _MetricTable(CoordSystem const& cs, size_vector_type const& N, position_type const& q_min);

        shape_vector *shape() const noexcept { return _shape.get(); }

    protected: // helpers
        static compact_vector_type convert(vector_type const &v) noexcept {
            return compact_vector_type{v.y, v.z};
        }
        static vector_type convert(compact_vector_type const &v) noexcept {
            constexpr long y = 0, z = 1;
            return {0, std::get<y>(v), std::get<z>(v)};
        }

        static compact_tensor_type convert(tensor_type const &T) noexcept {
            return {T.x.x, T.y.y, T.y.z, T.z.y, T.z.z};
        }
        static tensor_type convert(compact_tensor_type const &T) noexcept {
            constexpr long xx = 0, yy = 1, yz = 2, zy = 3, zz = 4;
            return {
                vector_type{std::get<xx>(T), 0, 0},
                vector_type{0, std::get<yy>(T), std::get<yz>(T)},
                vector_type{0, std::get<zy>(T), std::get<zz>(T)}
            };
        }

        static compact_tensor_type convert_mfa(tensor_type const &T/*{e1, e2, e3}*/) noexcept {
            return convert(tensor_type{T.z, T.x, T.y});
        }
        static tensor_type convert_mfa(compact_tensor_type const &T) noexcept {
            tensor_type const U = convert(T);
            return {U.y, U.z, U.x}; // {e1, e2, e3}
        }

        static compact_tensor_type metric(compact_tensor_type const &T) noexcept {
            constexpr long xx = 0, yy = 1, yz = 2, zy = 3, zz = 4;
            //
            // g_ij = T_i.T_j
            //
            using std::get;
            return {
                get<xx>(T)*get<xx>(T), // g_11
                get<yy>(T)*get<yy>(T) + get<yz>(T)*get<yz>(T), // g_22
                get<yy>(T)*get<zy>(T) + get<yz>(T)*get<zz>(T), // g_23
                get<zy>(T)*get<yy>(T) + get<zz>(T)*get<yz>(T), // g_32
                get<zy>(T)*get<zy>(T) + get<zz>(T)*get<zz>(T)  // g_33
            };
        }

    private:
        CoordSystem _cs; //!< Coordinate system.
        size_vector_type _N; //!< number of cells.
        position_type _q_min; //!< cell-center curvilinear coordinate min.
        position_type _q_max; //!< cell-center curvilinear coordinate max.
        std::unique_ptr<shape_vector> _shape;
        std::unique_ptr<package_type> _table;

        static void range_check(CoordSystem const &cs, UTL::Vector<value_type, 3> const &q_min, UTL::Vector<value_type, 3> const &q_max);
        template <long S>
        static void range_check(CoordSystem const &cs, UTL::Vector<value_type, S> const &q_min, UTL::Vector<value_type, S> const &q_max) { range_check(cs, q_min.append(0), q_max.append(0)); }
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOMetricTable__ND_h */
