//
//  GEOCoordSystem.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOCoordSystem_h
#define GEOCoordSystem_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEOGeometry.h>
#include <cmath>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::CoordSystem
    //
    /**
     @brief General curvilinear coordinate system for dipole magnetic field geometry.
     @discussion
     q1 coordinate varies in the azimuthal direction.
     q2 coordinate varies along the field line.
     q3 coordinate varies along the equatorial radial direction and is translated such that q3 = 0 correspoinds to r0 = r0ref.
     */
    class CoordSystem : public Geometry {
    public:
        // destructor
        //
        ~CoordSystem();

        // copy/move
        //
        CoordSystem(CoordSystem const &) = default;
        CoordSystem &operator=(CoordSystem const &) = default;
        CoordSystem(CoordSystem &&) = default;
        CoordSystem &operator=(CoordSystem &&) = default;

        // constructors
        //
        explicit CoordSystem() noexcept;
        using Geometry::Geometry;

        // Cartesian components of position vector
        //
        /**
         @brief Returns Cartesian components of the given position vector.
         */
        vector_type position(PolarCoord const &polar) const noexcept {
            return translate_carts_from_reference_meridian({
                0, polar.r()*polar.xi(), polar.r()*std::sqrt((1 - polar.xi())*(1 + polar.xi()))
            }, polar);
        }
        vector_type position(CurviCoord const &curvi) const noexcept { return position(coord_trans(curvi)); }

        // vector component transforms
        //
        /**
         @brief Transformation from contravariant to covariant vector components.
         @param contr Contravariant vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Covariant vector components.
         */
        template <class Coord>
        vector_type contr_to_covar(vector_type const &contr, Coord const &at) const noexcept { return contr_to_covar(contr, covar_metric(at)); }
        /**
         @brief Transformation from contravariant to covariant vector components using the covariant metric tensor.
         */
        static vector_type contr_to_covar(vector_type const &contr, tensor_type const &covar_metric) noexcept { return _dot(covar_metric, contr); }

        /**
         @brief Transformation from covariant to contravariant vector components.
         @param covar Covariant vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Contravariant vector components.
         */
        template <class Coord>
        vector_type covar_to_contr(vector_type const &covar, Coord const &at) const noexcept { return covar_to_contr(covar, contr_metric(at)); }
        /**
         @brief Transformation from covariant to contravariant vector components using the contravariant metric tensor.
         */
        static vector_type covar_to_contr(vector_type const &covar, tensor_type const &contr_metric) noexcept { return _dot(contr_metric, covar); }

        /**
         @brief Transformation from contravariant to cartesian vector components.
         @param contr Contravariant vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Cartesian vector components.
         */
        template <class Coord>
        vector_type contr_to_carts(vector_type const &contr, Coord const &at) const noexcept { return contr_to_carts(contr, covar_basis<0>(at)); }
        /**
         @brief Transformation from contravariant to cartesian vector components using the covariant basis vectors.
         */
        static vector_type contr_to_carts(vector_type const &contr, tensor_type const &covar_bases) noexcept { return _dot(contr, covar_bases); }

        /**
         @brief Transformation from cartesian to contravariant vector components.
         @param carts Cartesian vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Contravariant vector components.
         */
        template <class Coord>
        vector_type carts_to_contr(vector_type const &carts, Coord const &at) const noexcept { return carts_to_contr(carts, contr_basis<0>(at)); }
        /**
         @brief Transformation from cartesian to contravariant vector components using the contravariant basis vectors.
         */
        static vector_type carts_to_contr(vector_type const &carts, tensor_type const &contr_bases) noexcept { return _dot(contr_bases, carts); }

        /**
         @brief Transformation from covariant to cartesian vector components.
         @param covar Covariant vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Cartesian vector components.
         */
        template <class Coord>
        vector_type covar_to_carts(vector_type const &covar, Coord const &at) const noexcept { return covar_to_carts(covar, contr_basis<0>(at)); }
        /**
         @brief Transformation from covariant to cartesian vector components using the contravariant basis vectors.
         */
        static vector_type covar_to_carts(vector_type const &covar, tensor_type const &contr_bases) noexcept { return _dot(covar, contr_bases); }

        /**
         @brief Transformation from cartesian to covariant vector components.
         @param carts Cartesian vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Covariant vector components.
         */
        template <class Coord>
        vector_type carts_to_covar(vector_type const &carts, Coord const &at) const noexcept { return carts_to_covar(carts, covar_basis<0>(at)); }
        /**
         @brief Transformation from cartesian to covariant vector components using the covariant basis vectors.
         */
        static vector_type carts_to_covar(vector_type const &carts, tensor_type const &covar_bases) noexcept { return _dot(covar_bases, carts); }

        /**
         @brief Transformation from field-aligned to cartesian vector components.
         @param mfa Field-aligned vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Cartesian vector components.
         */
        template <class Coord>
        vector_type mfa_to_carts(vector_type const &mfa, Coord const &at) const noexcept { return mfa_to_carts(mfa, mfa_basis<0>(at)); }
        /**
         @brief Transformation from field-aligned to cartesian vector components using the field-aligned basis vectors.
         */
        static vector_type mfa_to_carts(vector_type const &mfa, tensor_type const &mfa_bases) noexcept { return _dot(mfa, mfa_bases); }

        /**
         @brief Transformation from cartesian to field-aligned vector components.
         @param carts Cartesian vector components.
         @param at Polar or curvilinear point at which to perform the transform.
         @return Field-aligned vector components.
         */
        template <class Coord>
        vector_type carts_to_mfa(vector_type const &carts, Coord const &at) const noexcept { return carts_to_mfa(carts, mfa_basis<0>(at)); }
        /**
         @brief Transformation from cartesian to field-aligned vector components using the field-aligned basis vectors.
         */
        static vector_type carts_to_mfa(vector_type const &carts, tensor_type const &mfa_bases) noexcept { return _dot(mfa_bases, carts); }

    private:
        static vector_type _dot(tensor_type const &T, vector_type const &v) noexcept {
            //
            // (T.v)_i = T_ij*v_j
            //
            return {
                T.x.x*v.x + T.x.y*v.y + T.x.z*v.z,
                T.y.x*v.x + T.y.y*v.y + T.y.z*v.z,
                T.z.x*v.x + T.z.y*v.y + T.z.z*v.z
            };
        }
        static vector_type _dot(vector_type const &v, tensor_type const &T) noexcept {
            //
            // (v.T)_j = v_i*T_ij
            //
            return {
                v.x*T.x.x + v.y*T.y.x + v.z*T.z.x,
                v.x*T.x.y + v.y*T.y.y + v.z*T.z.y,
                v.x*T.x.z + v.y*T.y.z + v.z*T.z.z
            };
        }
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOCoordSystem_h */
