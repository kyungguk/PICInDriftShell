//
//  GEOParticleLoader__ND.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOParticleLoader__ND_h
#define GEOParticleLoader__ND_h

#include <GeometryKit/GeometryKit-config.h>
#include <GeometryKit/GEORandomRealPool.h>
#include <GeometryKit/GEOCoordSystem.h>
#include <GeometryKit/GEOVDFDescriptor.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <utility>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class ParticleLoader;
    template <long ND> class BiMaxwellianLoader;
    template <long ND> class PartialShellLoader;

    // MARK:- GEO::__1_::ParticleLoader<ND>
    //
    template <long _ND>
    class ParticleLoader {
    public:
        static constexpr long ND = _ND;

        // types
        //
        using size_type = long;
        using value_type = double;
        using velocity_type = UTL::Vector<value_type, 3L>;
        using position_type = UTL::Vector<value_type, ND>;
        using size_vector_type = UTL::Vector<size_type, ND>;
        struct particle_type {
            velocity_type vel;
            typename std::conditional<ND == 3, CurviCoord, position_type>::type pos;
        };
        static_assert(sizeof(position_type) + sizeof(velocity_type) == sizeof(particle_type), "particle_type size incompatibility");

        // destructor:
        virtual ~ParticleLoader();

        /**
         @brief Coordinate system.
         */
        CoordSystem const& coord_system() const noexcept { return _cs; }

        /**
         @brief Number of cells in the entire domain.
         */
        size_vector_type const &N() const noexcept { return _N; }
        /**
         @brief Lower cell-center boundary.
         */
        position_type const &grid_q_min() const noexcept { return _q_min; }
        /**
         @brief Upper cell-center boundary.
         */
        position_type const &grid_q_max() const noexcept { return _q_max; }

        /**
         @brief Lower particle boundary.
         */
        position_type ptl_q_min() const noexcept { return _q_min - .5; }
        /**
         @brief Upper particle boundary.
         */
        position_type ptl_q_max() const noexcept { return _q_max + .5; }

        /**
         @brief Calculate the ratio of the number of particles in a given cell centered at (0, 0, 0) to the total number of particles.
         */
        virtual value_type NcellOverNtotal() const = 0;

        /**
         @brief Load one particle based on the prescribed distribution.
         @discussion Position is between ptl_q_min() and ptl_q_max().
         */
        virtual particle_type operator()() const = 0;

        /**
         @brief Returns VDF descriptor associated with *this.
         */
        virtual VDFDescriptor const &vdf() const = 0;

        /**
         @brief Density-weighted, normalized flow velocity perpendicular to the background magnetic field caused by pressure gradient and anisotropy.
         @discussion "Density-weighted" means average flow velocity multiplied by the density (1st order moment; <v>), and "normalized" means that the weighted flow velocity is multiplied by Ωs,ref; so the return value is Ωs,ref*<v>.

         Conversion to the current density is J = normalized_current_density()/Ωs,ref*(Ω0,ref/Ωs,ref)*ωs,ref^2/c.
         @param curvi Curvilinear coordinate at which it is evaluated; this should be within the particle boundary.
         @return Cartesian components of the velocity vector.
         */
        virtual velocity_type normalized_current_density(CurviCoord const &curvi) const;

    protected:
        ParticleLoader(ParticleLoader&&) = default;
        ParticleLoader& operator=(ParticleLoader&&) = default;

        explicit ParticleLoader() noexcept;
        explicit ParticleLoader(CoordSystem const& cs, position_type const& q_min/*boundary cell center coord*/, size_vector_type const& N);

        static RandomRealPool &rng() { return RandomRealPool::rng(); }

        // density integral helper
        // azimuthal coordinate is ignorable
        //
        using Integrator = UTL::GaussLegendreIntegrator<value_type, 20>;
        inline value_type nOn0(value_type const q2) const; // n(q2)/n0, where q3 = 0 and n0 is the equatorial density
        inline value_type nOn0(std::pair<value_type, value_type> const &q2lim) const; // integral over given q2 limit; q3 = 0

    private:
        CoordSystem _cs;
        size_vector_type _N;
        position_type _q_min;
        position_type _q_max;

        static void range_check(CoordSystem const &cs, UTL::Vector<value_type, 3> const &q_min, UTL::Vector<value_type, 3> const &q_max);
        template <long S>
        static void range_check(CoordSystem const &cs, UTL::Vector<value_type, S> const &q_min, UTL::Vector<value_type, S> const &q_max) { range_check(cs, q_min.append(0), q_max.append(0)); }
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOParticleLoader__ND_h */
