//
//  GEORandomRealPool.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEORandomRealPool_h
#define GEORandomRealPool_h

#include <GeometryKit/GeometryKit-config.h>
#include <UtilityKit/UtilityKit.h>
#include <memory>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    /**
     @brief Common random number generators for use in its subclasses.
     */
    class RandomRealPool {
        using RNGPack = UTL::Vector<std::unique_ptr<UTL::RandomReal>, 7>;

        RandomRealPool &operator=(RandomRealPool &&) = default;
        explicit RandomRealPool(UTL::Vector<UTL::RandomReal::size_type, RNGPack::size()> const &seeds);
        explicit RandomRealPool() : RandomRealPool({100U, 200U, 1U, 1U, 1U, 1U, 1U}/*default seeds*/) {}

        RNGPack _rngs; // {v1, v2, phi, alpha, q1, q2, q3}

    public:
        static UTL::Vector<UTL::RandomReal::size_type, RNGPack::size()> seeds();
        static void reset(UTL::Vector<UTL::RandomReal::size_type, RNGPack::size()> const &seeds) { rng() = RandomRealPool{seeds}; }
        static void reset() { rng() = RandomRealPool{}; } // with default seeds
        static RandomRealPool &rng();

        UTL::RandomReal::value_type q1() noexcept { return std::get<4>(_rngs)->operator()(); }
        UTL::RandomReal::value_type q2() noexcept { return std::get<5>(_rngs)->operator()(); }
        UTL::RandomReal::value_type q3() noexcept { return std::get<6>(_rngs)->operator()(); }
        UTL::RandomReal::value_type v1() noexcept { return std::get<0>(_rngs)->operator()(); }
        UTL::RandomReal::value_type v2() noexcept { return std::get<1>(_rngs)->operator()(); }
        UTL::RandomReal::value_type phi() noexcept { return std::get<2>(_rngs)->operator()(); }
        UTL::RandomReal::value_type alpha() noexcept { return std::get<3>(_rngs)->operator()(); }
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEORandomRealPool_h */
