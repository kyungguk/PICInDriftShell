//
//  GEOGeometry.cc
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/3/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "GEOGeometry.h"
#include <stdexcept>
#include <utility>
#include <string>
#include <limits>

constexpr double quiet_NaN = std::numeric_limits<double>::quiet_NaN();

// MARK:- GEO::__1_::Geometry
//
GEO::__1_::Geometry::~Geometry()
{
}
GEO::__1_::Geometry::Geometry() noexcept
{
    _D.fill(quiet_NaN);
    _D_r0ref.fill(quiet_NaN);
    _det_metric = quiet_NaN;
    _r0ref = quiet_NaN;
    _q3ref = quiet_NaN;
    _n = quiet_NaN;
    _curvi_pi = quiet_NaN;
}
GEO::__1_::Geometry::Geometry(value_type const r0ref, vector_type const &D, value_type const n)
: Geometry{} {
    // argument check
    //
    if (r0ref <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - r0ref is not positive");
    }
    if (D.x <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - D1 is not positive");
    }
    if (D.y <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - D2 is not positive");
    }
    if (D.z <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - D3 is not positive");
    }
    if (std::abs(n - 3) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - n cannot be 3 (within error of +/- 1e-10)");
    }
    //
    _D = D;
    _D_r0ref = D/r0ref;
    _det_metric = UTL::pow<2>(UTL::reduce_prod(D));
    _r0ref = r0ref;
    _q3ref = r0ref/(D3()*(3 - n));
    _n = n;
    _curvi_pi = M_PI*r0ref/D1();

    // construct xi inversion
    //
    {
        // 1. sample points
        //
        auto const sampler = []{
            UTL::AdaptiveSampling1D<value_type> sampler;
            sampler.set_max_recursion(50);
            sampler.set_initial_points(200);
            sampler.set_accuracy_goal(15);
            return sampler;
        }();
        constexpr value_type xi_max = _xi_max;
        auto points = sampler([](value_type const xi) {
            return scaled_q2(xi);
        }, -xi_max, xi_max, scaled_q2(xi_max));

        // 2. construct interpolator
        //
        for (std::pair<value_type, value_type> &pt : points) {
            std::swap(pt.first, pt.second);
        }
        _xi_interp = spline_coeff_type{points.begin(), points.end()}.interpolator();
    }
}
