//
//  GEOWaveDamper__2D.h
//  GeometryKit
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef GEOWaveDamper__2D_h
#define GEOWaveDamper__2D_h

#include <GeometryKit/GEOWaveDamper__ND.h>

GEOKIT_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- GEO::__1_::_WaveDamper<2D>
    //
    template <>
    class WaveDamper<2L> : public _WaveDamper<2L> {
    public:
        // copy/move
        //
        WaveDamper(WaveDamper const&) = default;
        WaveDamper &operator=(WaveDamper const&) = default;
        WaveDamper(WaveDamper&&) noexcept = default;
        WaveDamper &operator=(WaveDamper&&) noexcept = default;

        // constructors
        //
        ~WaveDamper();
        WaveDamper() noexcept;
        explicit WaveDamper(size_vector_type const& inset, position_type const& amplitude_damping, position_type const& phase_retardation);

        /**
         @brief Set which boundaries to mask.
         @param mask Bit or of Boundary enums.
         */
        void set_boundary_mask(size_type const mask) noexcept;

        // wave masking
        //
        void operator()(UTL::ArrayND<UTL::Vector<value_type, 1L>, ND, pad_size> &A, position_type const masking_factor) const;
        void operator()(UTL::ArrayND<UTL::Vector<value_type, 2L>, ND, pad_size> &A, position_type const masking_factor) const;
        void operator()(UTL::ArrayND<UTL::Vector<value_type, 3L>, ND, pad_size> &A, position_type const masking_factor) const;
    };
} // namespace __1_
GEOKIT_END_NAMESPACE

#endif /* GEOWaveDamper__2D_h */
