//
//  GEOKitTests-WaveDamper.mm
//  GeometryKitTests
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOWaveDamper.h>
#include <iostream>
#include <sstream>

@interface GEOKitTests_WaveDamper : XCTestCase

@end

@implementation GEOKitTests_WaveDamper

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- __1_
- (void)testWaveDamper__1_1D {
    using GEO::__1_::WaveDamper;
    constexpr long ND = 1;

    try {
        WaveDamper<ND>::size_vector_type const inset = {10};
        WaveDamper<ND>::position_type const r_d = {.31};
        WaveDamper<ND>::position_type const r_r = {1};
        WaveDamper<ND> damper{inset, r_d, r_r};
        XCTAssert(damper.masking_inset().x == inset.x);
        XCTAssert(damper.amplitude_damping().x == r_d.x);
        XCTAssert(damper.phase_retardation().x == r_r.x);

        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q1_lo);
        XCTAssert(damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q1_hi);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q2_lo);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q2_hi);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q3_lo);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q3_hi);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);

        damper.set_boundary_mask(damper.q1_lo | damper.q1_hi);
        XCTAssert(damper.lo_boundary_mask().x);
        XCTAssert(damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q2_lo | damper.q2_hi);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
        damper.set_boundary_mask(damper.q3_lo | damper.q3_hi);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
        damper.set_boundary_mask((damper.q1_lo | damper.q1_hi) | (damper.q2_lo | damper.q2_hi) | (damper.q3_lo | damper.q3_hi));
        XCTAssert(damper.lo_boundary_mask().x);
        XCTAssert(damper.hi_boundary_mask().x);

        damper.set_boundary_mask(damper.none);
        XCTAssert(!damper.lo_boundary_mask().x);
        XCTAssert(!damper.hi_boundary_mask().x);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    std::ostringstream os;
    os.setf(os.fixed);

    try {
        WaveDamper<ND>::size_vector_type const inset = {10};
        WaveDamper<ND>::position_type const r = {1};
        WaveDamper<ND> damper{inset, r, r};

        constexpr long pad = WaveDamper<ND>::pad_size;
        using Vector = UTL::Vector<double, 2>;
        Vector one{1};
        WaveDamper<ND>::size_vector_type const N = inset*3L + 1L;
        WaveDamper<ND>::position_type const q_min = {-.5*(N.x - 1)};
        UTL::ArrayND<Vector, ND, pad> A(N);
        {
            damper.set_boundary_mask(damper.q1_lo | damper.q1_hi);

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ1All = ", A, "\n");
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (0) ) {
        std::string const _s = os.str();
        NSString *str = [[NSString stringWithUTF8String:_s.c_str()] stringByReplacingOccurrencesOfString:@"|" withString:@", "];
        NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"damper__1D.m"]];
        NSError *error;
        XCTAssert([str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

- (void)testWaveDamper__1_2D {
    using GEO::__1_::WaveDamper;
    constexpr long ND = 2;

    try {
        WaveDamper<ND>::size_vector_type const inset = {10, 0};
        WaveDamper<ND>::position_type const r_d = {.31, .1};
        WaveDamper<ND>::position_type const r_r = {1, 1};
        WaveDamper<ND> damper{inset, r_d, r_r};
        XCTAssert(damper.masking_inset().x == inset.x && damper.masking_inset().y == inset.y);
        XCTAssert(damper.amplitude_damping().x == r_d.x && damper.amplitude_damping().y == r_d.y);
        XCTAssert(damper.phase_retardation().x == r_r.x && damper.phase_retardation().y == r_r.y);

        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q1_lo);
        XCTAssert(damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q1_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q2_lo);
        XCTAssert(!damper.lo_boundary_mask().x && damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q2_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q3_lo);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q3_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);

        damper.set_boundary_mask(damper.q1_lo | damper.q1_hi);
        XCTAssert(damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q2_lo | damper.q2_hi);
        XCTAssert(!damper.lo_boundary_mask().x && damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && damper.hi_boundary_mask().y);
        damper.set_boundary_mask(damper.q3_lo | damper.q3_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
        damper.set_boundary_mask((damper.q1_lo | damper.q1_hi) | (damper.q2_lo | damper.q2_hi) | (damper.q3_lo | damper.q3_hi));
        XCTAssert(damper.lo_boundary_mask().x && damper.lo_boundary_mask().y);
        XCTAssert(damper.hi_boundary_mask().x && damper.hi_boundary_mask().y);

        damper.set_boundary_mask(damper.none);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    std::ostringstream os;
    os.setf(os.fixed);

    try {
        WaveDamper<ND>::size_vector_type const inset = {10, 9};
        WaveDamper<ND>::position_type const r = {1, 1};
        WaveDamper<ND> damper{inset, r, r};

        constexpr long pad = WaveDamper<ND>::pad_size;
        using Vector = UTL::Vector<double, 2>;
        Vector one{1};
        WaveDamper<ND>::size_vector_type const N = inset*3L + 1L;
        WaveDamper<ND>::position_type const q_min = {-.5*(N.x - 1), -.5*(N.y - 1)};
        UTL::ArrayND<Vector, ND, pad> A(N);
        {
            damper.set_boundary_mask(damper.q1_lo | damper.q1_hi);

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ1All = ", A, "\n");
        }
        {
            damper.set_boundary_mask(damper.q2_lo | damper.q2_hi);

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ2All = ", A, "\n");
        }
        {
            damper.set_boundary_mask((damper.q1_lo | damper.q1_hi) | (damper.q2_lo | damper.q2_hi));

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ1Q2All = ", A, "\n");
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (0) ) {
        std::string const _s = os.str();
        NSString *str = [[NSString stringWithUTF8String:_s.c_str()] stringByReplacingOccurrencesOfString:@"|" withString:@", "];
        NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"damper__2D.m"]];
        NSError *error;
        XCTAssert([str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

- (void)testWaveDamper__1_3D {
    using GEO::__1_::WaveDamper;
    constexpr long ND = 3;

    try {
        WaveDamper<ND>::size_vector_type const inset = {10, 0, 3};
        WaveDamper<ND>::position_type const r_d = {.31, .1, .3};
        WaveDamper<ND>::position_type const r_r = {1, 1, 0};
        WaveDamper<ND> damper{inset, r_d, r_r};
        XCTAssert(damper.masking_inset().x == inset.x && damper.masking_inset().y == inset.y && damper.masking_inset().z == inset.z);
        XCTAssert(damper.amplitude_damping().x == r_d.x && damper.amplitude_damping().y == r_d.y && damper.amplitude_damping().z == r_d.z);
        XCTAssert(damper.phase_retardation().x == r_r.x && damper.phase_retardation().y == r_r.y && damper.phase_retardation().z == r_r.z);

        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q1_lo);
        XCTAssert(damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q1_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q2_lo);
        XCTAssert(!damper.lo_boundary_mask().x && damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q2_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q3_lo);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q3_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && damper.hi_boundary_mask().z);

        damper.set_boundary_mask(damper.q1_lo | damper.q1_hi);
        XCTAssert(damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q2_lo | damper.q2_hi);
        XCTAssert(!damper.lo_boundary_mask().x && damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
        damper.set_boundary_mask(damper.q3_lo | damper.q3_hi);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && damper.hi_boundary_mask().z);
        damper.set_boundary_mask((damper.q1_lo | damper.q1_hi) | (damper.q2_lo | damper.q2_hi) | (damper.q3_lo | damper.q3_hi));
        XCTAssert(damper.lo_boundary_mask().x && damper.lo_boundary_mask().y && damper.lo_boundary_mask().z);
        XCTAssert(damper.hi_boundary_mask().x && damper.hi_boundary_mask().y && damper.hi_boundary_mask().z);

        damper.set_boundary_mask(damper.none);
        XCTAssert(!damper.lo_boundary_mask().x && !damper.lo_boundary_mask().y && !damper.lo_boundary_mask().z);
        XCTAssert(!damper.hi_boundary_mask().x && !damper.hi_boundary_mask().y && !damper.hi_boundary_mask().z);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    std::ostringstream os;
    os.setf(os.fixed);

    try {
        WaveDamper<ND>::size_vector_type const inset = {10, 10, 10};
        WaveDamper<ND>::position_type const r = {1, .95, .9};
        WaveDamper<ND> damper{inset, r, r};

        constexpr long pad = WaveDamper<ND>::pad_size;
        using Vector = UTL::Vector<double, 2>;
        Vector one{1};
        WaveDamper<ND>::size_vector_type const N = inset*3L + 1L;
        WaveDamper<ND>::position_type const q_min = {-.5*(N.x - 1), -.5*(N.y - 1), -.5*(N.z - 1)};
        UTL::ArrayND<Vector, ND, pad> A(N);
        {
            damper.set_boundary_mask(damper.q1_lo | damper.q1_hi);

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ1All = ", A, "\n");
        }
        {
            damper.set_boundary_mask(damper.q2_lo | damper.q2_hi);

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ2All = ", A, "\n");
        }
        {
            damper.set_boundary_mask(damper.q3_lo | damper.q3_hi);

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ3All = ", A, "\n");
        }
        {
            damper.set_boundary_mask((damper.q1_lo | damper.q1_hi) | (damper.q2_lo | damper.q2_hi));

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ1Q2All = ", A, "\n");
        }
        {
            damper.set_boundary_mask((damper.q1_lo | damper.q1_hi) | (damper.q3_lo | damper.q3_hi));

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ1Q3All = ", A, "\n");
        }
        {
            damper.set_boundary_mask((damper.q2_lo | damper.q2_hi) | (damper.q3_lo | damper.q3_hi));

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ2Q3All = ", A, "\n");
        }
        {
            damper.set_boundary_mask((damper.q1_lo | damper.q1_hi) | (damper.q2_lo | damper.q2_hi) | (damper.q3_lo | damper.q3_hi));

            A.fill(one);
            damper(A, r);
            printo(os, "dampQ1Q2Q3All = ", A, "\n");
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (0) ) {
        std::string const _s = os.str();
        NSString *str = [[NSString stringWithUTF8String:_s.c_str()] stringByReplacingOccurrencesOfString:@"|" withString:@", "];
        NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"damper__3D.m"]];
        NSError *error;
        XCTAssert([str writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

@end
