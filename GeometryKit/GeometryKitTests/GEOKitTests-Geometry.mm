//
//  GEOKitTests-Geometry.mm
//  GeometryKitTests
//
//  Created by KYUNGGUK MIN on 9/3/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOGeometry.h>
#include <iostream>
#include <sstream>

@interface GEOKitTests_Geometry : XCTestCase

@end

@implementation GEOKitTests_Geometry

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

namespace {
    double scaled_q2(double const xi) {
        static double const sqrt_3o5 = std::sqrt(3./5), sqrt_1o7 = std::sqrt(1./7);
        double const xi_sqrt_1o7 = xi*sqrt_1o7;
        return xi*((1 - xi)*(1 + xi) + UTL::pow<4>(xi)*(sqrt_3o5 - xi_sqrt_1o7)*(sqrt_3o5 + xi_sqrt_1o7));
    }
}

// MARK:- __1_
- (void)testGeometry__1_ZeroN {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::PolarCoord;
    using GEO::__1_::Geometry;

    try {
        constexpr double n = 0, r0ref = 20, D1 = 3., D2 = 1.13, D3 = .98, q3ref = r0ref/(D3*(3 - n));
        const Geometry::vector_type D = {D1, D2, D3};
        Geometry _cs{r0ref, D, n};
        Geometry const cs{std::move(_cs)};
        XCTAssert(!_cs.xi_interp() && r0ref == cs.r0ref() && std::abs(q3ref - cs.q3ref()) < 1e-15*q3ref && D1 == cs.D1() && D2 == cs.D2() && D3 == cs.D3() && n == cs.n());
        XCTAssert(std::abs(D1*D2*D3 - std::sqrt(cs.g())) < 1e-15);

        // `xi' inversion
        //
        {
            double const max_scaled_q2 = scaled_q2(1);
            auto const xi_interp = cs.xi_interp();
            println(std::cout, "xi interpolator table count = ", xi_interp.spline_coefficient().table().size());

            XCTAssert(std::abs(max_scaled_q2 - xi_interp.max_abscissa()) < 1e-15*max_scaled_q2, @"max_scaled_q2 = %f, max_abscissa = %f, err = %e", max_scaled_q2, xi_interp.max_abscissa(), std::abs(max_scaled_q2 - xi_interp.max_abscissa())/max_scaled_q2);
            XCTAssert(std::abs(-max_scaled_q2 - xi_interp.min_abscissa()) < 1e-15*max_scaled_q2, @"min_scaled_q2 = %f, min_abscissa = %f, err = %e", -max_scaled_q2, xi_interp.min_abscissa(), std::abs(-max_scaled_q2 - xi_interp.min_abscissa())/max_scaled_q2);

            UTL::NRRandomReal rng{100};
            UTL::UniformDistribution const dist{-.9, .9};
            UTL::RandomVariate rv{&rng, &dist};
            constexpr long n = 5000;
            for (long i = 0; i < n; ++i) {
                double const xi = rv();
                double const q2 = scaled_q2(xi);
                auto const approx_xi = xi_interp(q2);
                bool stop;
                stop = !approx_xi;
                XCTAssert(!stop, @"i = %ld, q2 = %f, xi = %f, approx_xi = (nil)", i, q2, xi);
                if (stop) {
                    break;
                }
                stop = !(std::abs(xi - approx_xi()) < 1e-10*max_scaled_q2);
                XCTAssert(!stop, @"i = %ld, q2 = %f, xi = %f, approx_xi = %f, err = %e", i, q2, xi, approx_xi(), std::abs(xi - approx_xi())/max_scaled_q2);
                if (stop) {
                    break;
                }
            }
        }

        // bound checking
        //
        {
            double const r0 = cs.r0ref() - 1;
            double const q1 = M_PI*cs.r0ref()/cs.D1();
            double const q2 = scaled_q2(1)*std::pow(r0/cs.r0ref(), cs.n())*cs.r0ref()/cs.D2() - 1e-10;
            double const q3 = std::pow(r0/cs.r0ref(), 3 - cs.n())*cs.r0ref()/(cs.D3()*(3 - cs.n())) - q3ref;
            XCTAssert(cs.is_valid(CurviCoord{q1, q2, q3}) && cs.is_valid(CurviCoord{-q1, -q2, q3}));
            XCTAssert(!cs.is_valid(CurviCoord{q1, q2 - 1000, q3}));
            XCTAssert(!cs.is_valid(CurviCoord{-q1, q2, q3 - 1000}));
            XCTAssert(!cs.is_valid(CurviCoord{q1 + 1, q2, q3}));

            XCTAssert(cs.is_valid(PolarCoord{M_PI, .99, r0}) && cs.is_valid(PolarCoord{-M_PI, -.99, r0}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI, -1.1, r0}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI, 0, -1}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI + 1, 0, r0}));
        }

        // azimuthal angle wrapping
        //
        {
            PolarCoord p{M_PI + .1, 0, 0};
            double a = -M_PI + .1;
            p = cs.wrap(p);
            XCTAssert(std::abs(a - p.phi()) < 1e-15);
            p.phi() = -M_PI - .1;
            a = M_PI - .1;
            p = cs.wrap(p);
            XCTAssert(std::abs(a - p.phi()) < 1e-15);

            const double pi = M_PI*r0ref/D1;
            CurviCoord q{pi + .1, 0, 0};
            a = -pi + .1;
            q = cs.wrap(q);
            XCTAssert(std::abs(a - q.q1()) < 1e-15);
            q.q1() = -pi - .1;
            a = pi - .1;
            q = cs.wrap(q);
            XCTAssert(std::abs(a - q.q1()) < 1e-15);
        }

        CurviCoord const curvi{1.3, 4, -3};
        PolarCoord const polar{0.195, 0.2392271768621264, 15.53244594222082};
        Geometry::vector_type r;

        // cotrans
        //
        {
            r = cs.coord_trans(polar);
            XCTAssert(std::abs(r.x - curvi.x) < 1e-15*std::abs(curvi.x) &&
                      std::abs(r.y - curvi.y) < 2e-14*std::abs(curvi.y) &&
                      std::abs(r.z - curvi.z) < 2e-15*std::abs(curvi.z),
                      @"err.x = %e, err.y = %e, err.z = %e",
                      std::abs(r.x - curvi.x)/std::abs(curvi.x),
                      std::abs(r.y - curvi.y)/std::abs(curvi.y),
                      std::abs(r.z - curvi.z)/std::abs(curvi.z));
        }
        {
            r = cs.coord_trans(curvi);
            XCTAssert(std::abs(r.x - polar.x) < 1e-15*std::abs(polar.x) &&
                      std::abs(r.y - polar.y) < 2e-14*std::abs(polar.y) &&
                      std::abs(r.z - polar.z) < 3e-15*std::abs(polar.z),
                      @"err.x = %e, err.y = %e, err.z = %e",
                      std::abs(r.x - polar.x)/std::abs(polar.x),
                      std::abs(r.y - polar.y)/std::abs(polar.y),
                      std::abs(r.z - polar.z)/std::abs(polar.z));
        }

        // q1 translation
        //
        {
            Geometry::vector_type const v_ptl{.8, 1.1, 3}, v_ref{0.2035385307523959, 1.1, 3.098156236618669};
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);

            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
        }

        // covar metric
        //
        {
            Geometry::tensor_type m;
            Geometry::tensor_type const covar_metric{
                Geometry::vector_type{5.11762122481927, 0, 0},
                Geometry::vector_type{0, 1.44591102516904, -0.7236511484471655},
                Geometry::vector_type{0, -0.7236511484471655, 1.853737544209652}
            };
            m = cs.covar_metric(polar);
            r = covar_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.x * covar_metric.x)) < 1e-15);
            r = covar_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.y * covar_metric.y)) < 1e-15);
            r = covar_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.z * covar_metric.z)) < 2e-15);
            m = cs.covar_metric(curvi);
            r = covar_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.x * covar_metric.x)) < 1e-14);
            r = covar_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.y * covar_metric.y)) < 2e-14);
            r = covar_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.z * covar_metric.z)) < 1e-14);
        }

        // contr metric
        //
        {
            Geometry::tensor_type m;
            Geometry::tensor_type const contr_metric{
                Geometry::vector_type{0.1954032852510133, 0, 0},
                Geometry::vector_type{0, 0.859537516084983, 0.3355411949179227},
                Geometry::vector_type{0, 0.3355411949179227, 0.6704372875954061}
            };
            m = cs.contr_metric(polar);
            r = contr_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.x * contr_metric.x)) < 1e-15);
            r = contr_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.y * contr_metric.y)) < 1e-15);
            r = contr_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.z * contr_metric.z)) < 2e-15);
            m = cs.contr_metric(curvi);
            r = contr_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.x * contr_metric.x)) < 1e-14);
            r = contr_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.y * contr_metric.y)) < 1e-14);
            r = contr_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.z * contr_metric.z)) < 2e-14);
        }

        // covar basis
        //
        {
            Geometry::vector_type const g1{2.219341733552169, 0, -0.4383417554065824};
            Geometry::vector_type const g2{-0.1499954635256541, 0.920148853462693, -0.759432994781011};
            Geometry::vector_type const g3{0.2561567801421889, 0.3257126590939762, 1.296931961169425};
            //
            r = g1 - cs.covar_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-15);
            r = g2 - cs.covar_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-15);
            r = g3 - cs.covar_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 2e-15);
            r = g1 - cs.covar_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-14);
            r = g2 - cs.covar_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 2e-14);
            r = g3 - cs.covar_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-14);
            //
            r = g1 - cs.covar_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-15);
            r = g2 - cs.covar_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-15);
            r = g3 - cs.covar_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 2e-15);
            r = g1 - cs.covar_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-14);
            r = g2 - cs.covar_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 2e-14);
            r = g3 - cs.covar_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-14);
        }

        // contr basis
        //
        {
            Geometry::vector_type const h1{0.4336666658307728, 0, -0.0856534190691423};
            Geometry::vector_type const h2{-0.04297557604761883, 0.900192474766056, -0.2175870499890168};
            Geometry::vector_type const h3{0.1214073998140362, 0.5271177574916813, 0.6146904917133027};
            //
            r = h1 - cs.contr_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-15);
            r = h2 - cs.contr_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-15);
            r = h1 - cs.contr_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-14);
            r = h2 - cs.contr_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-14);
            r = h3 - cs.contr_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 2e-14);
            //
            r = h1 - cs.contr_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-15);
            r = h2 - cs.contr_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-15);
            r = h1 - cs.contr_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-14);
            r = h2 - cs.contr_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-14);
            r = h3 - cs.contr_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 2e-14);
        }

        // mfa basis
        //
        {
            Geometry::vector_type const e1 = {-0.1247404595809807, 0.7652217484845316, -0.6315659058164887};
            Geometry::vector_type const e2 = {0.1482743666817337, 0.6437667866908608, 0.7507190130394025};
            Geometry::vector_type const e3 = {0.981047669549577, 0., -0.193766534962422};
            //
            r = e1 - cs.mfa_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e2 - cs.mfa_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e3 - cs.mfa_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e1 - cs.mfa_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-14);
            r = e2 - cs.mfa_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-14);
            r = e3 - cs.mfa_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            //
            r = e1 - cs.mfa_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e1 * e1)) < 1e-15);
            r = e2 - cs.mfa_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e2 * e2)) < 1e-15);
            r = e3 - cs.mfa_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e3 * e3)) < 1e-15);
            r = e1 - cs.mfa_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e1 * e1)) < 1e-14);
            r = e2 - cs.mfa_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e2 * e2)) < 1e-14);
            r = e3 - cs.mfa_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e3 * e3)) < 1e-15);
        }

        // B vector
        //
        {
            Geometry::vector_type const BOB0 = {-0.1611371748128468, 0.988497806327986, -0.815843921957958};
            Geometry::vector_type const BOB0ref = {-0.2882597044952536, 1.768332390568848, -1.459470343395274};
            r = cs.BcartsOverB0(polar) - BOB0;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0 * BOB0)) < 1e-15);
            r = cs.BcartsOverB0(curvi) - BOB0;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0 * BOB0)) < 2e-14);

            r = cs.BcartsOverB0ref(polar) - BOB0ref;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0ref * BOB0ref)) < 2e-15);
            r = cs.BcartsOverB0ref(curvi) - BOB0ref;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0ref * BOB0ref)) < 2e-14);
        }
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testGeometry__1_PositiveN {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::PolarCoord;
    using GEO::__1_::Geometry;

    try {
        constexpr double n = 1.1, r0ref = 20, D1 = 3., D2 = 1.13, D3 = .98, q3ref = r0ref/(D3*(3 - n));
        const Geometry::vector_type D = {D1, D2, D3};
        Geometry _cs{r0ref, D, n};
        Geometry const cs{std::move(_cs)};
        XCTAssert(!_cs.xi_interp() && r0ref == cs.r0ref() && std::abs(q3ref - cs.q3ref()) < 1e-15*q3ref && D1 == cs.D1() && D2 == cs.D2() && D3 == cs.D3() && n == cs.n());
        XCTAssert(std::abs(D1*D2*D3 - std::sqrt(cs.g())) < 1e-15);

        // `xi' inversion
        //
        {
            double const max_scaled_q2 = scaled_q2(1);
            auto const xi_interp = cs.xi_interp();
            println(std::cout, "xi interpolator table count = ", xi_interp.spline_coefficient().table().size());

            XCTAssert(std::abs(max_scaled_q2 - xi_interp.max_abscissa()) < 1e-15*max_scaled_q2, @"max_scaled_q2 = %f, max_abscissa = %f, err = %e", max_scaled_q2, xi_interp.max_abscissa(), std::abs(max_scaled_q2 - xi_interp.max_abscissa())/max_scaled_q2);
            XCTAssert(std::abs(-max_scaled_q2 - xi_interp.min_abscissa()) < 1e-15*max_scaled_q2, @"min_scaled_q2 = %f, min_abscissa = %f, err = %e", -max_scaled_q2, xi_interp.min_abscissa(), std::abs(-max_scaled_q2 - xi_interp.min_abscissa())/max_scaled_q2);

            UTL::NRRandomReal rng{100};
            UTL::UniformDistribution const dist{-.9, .9};
            UTL::RandomVariate rv{&rng, &dist};
            constexpr long n = 5000;
            for (long i = 0; i < n; ++i) {
                double const xi = rv();
                double const q2 = scaled_q2(xi);
                auto const approx_xi = xi_interp(q2);
                bool stop;
                stop = !approx_xi;
                XCTAssert(!stop, @"i = %ld, q2 = %f, xi = %f, approx_xi = (nil)", i, q2, xi);
                if (stop) {
                    break;
                }
                stop = !(std::abs(xi - approx_xi()) < 1e-10*max_scaled_q2);
                XCTAssert(!stop, @"i = %ld, q2 = %f, xi = %f, approx_xi = %f, err = %e", i, q2, xi, approx_xi(), std::abs(xi - approx_xi())/max_scaled_q2);
                if (stop) {
                    break;
                }
            }
        }

        // bound checking
        //
        {
            double const r0 = cs.r0ref() - 1;
            double const q1 = M_PI*cs.r0ref()/cs.D1();
            double const q2 = scaled_q2(1)*std::pow(r0/cs.r0ref(), cs.n())*cs.r0ref()/cs.D2() - 1e-10;
            double const q3 = std::pow(r0/cs.r0ref(), 3 - cs.n())*cs.r0ref()/(cs.D3()*(3 - cs.n())) - q3ref;
            XCTAssert(cs.is_valid(CurviCoord{q1, q2, q3}) && cs.is_valid(CurviCoord{-q1, -q2, q3}));
            XCTAssert(!cs.is_valid(CurviCoord{q1, q2 - 1000, q3}));
            XCTAssert(!cs.is_valid(CurviCoord{-q1, q2, q3 - 1000}));
            XCTAssert(!cs.is_valid(CurviCoord{q1 + 1, q2, q3}));

            XCTAssert(cs.is_valid(PolarCoord{M_PI, .99, r0}) && cs.is_valid(PolarCoord{-M_PI, -.99, r0}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI, -1.1, r0}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI, 0, -1}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI + 1, 0, r0}));
        }

        // azimuthal angle wrapping
        //
        {
            PolarCoord p{M_PI + .1, 0, 0};
            double a = -M_PI + .1;
            p = cs.wrap(p);
            XCTAssert(std::abs(a - p.phi()) < 1e-15);
            p.phi() = -M_PI - .1;
            a = M_PI - .1;
            p = cs.wrap(p);
            XCTAssert(std::abs(a - p.phi()) < 1e-15);

            const double pi = M_PI*r0ref/D1;
            CurviCoord q{pi + .1, 0, 0};
            a = -pi + .1;
            q = cs.wrap(q);
            XCTAssert(std::abs(a - q.q1()) < 1e-15);
            q.q1() = -pi - .1;
            a = pi - .1;
            q = cs.wrap(q);
            XCTAssert(std::abs(a - q.q1()) < 1e-15);
        }

        CurviCoord const curvi{1.3, 4, -3};
        PolarCoord const polar{0.195, 0.2983583670125045, 15.33465064511582};
        Geometry::vector_type r;

        // cotrans
        //
        {
            r = cs.coord_trans(polar);
            XCTAssert(std::abs(r.x - curvi.x) < 1e-15*std::abs(curvi.x) &&
                      std::abs(r.y - curvi.y) < 1e-14*std::abs(curvi.y) &&
                      std::abs(r.z - curvi.z) < 1e-14*std::abs(curvi.z),
                      @"err.x = %e, err.y = %e, err.z = %e",
                      std::abs(r.x - curvi.x)/std::abs(curvi.x),
                      std::abs(r.y - curvi.y)/std::abs(curvi.y),
                      std::abs(r.z - curvi.z)/std::abs(curvi.z));
        }
        {
            r = cs.coord_trans(curvi);
            XCTAssert(std::abs(r.x - polar.x) < 1e-15*std::abs(polar.x) &&
                      std::abs(r.y - polar.y) < 1e-14*std::abs(polar.y) &&
                      std::abs(r.z - polar.z) < 1e-14*std::abs(polar.z),
                      @"err.x = %e, err.y = %e, err.z = %e",
                      std::abs(r.x - polar.x)/std::abs(polar.x),
                      std::abs(r.y - polar.y)/std::abs(polar.y),
                      std::abs(r.z - polar.z)/std::abs(polar.z));
        }

        // q1 translation
        //
        {
            Geometry::vector_type const v_ptl{.8, 1.1, 3}, v_ref{0.2035385307523959, 1.1, 3.098156236618669};
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);

            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
        }

        // covar metric
        //
        {
            Geometry::tensor_type m;
            Geometry::tensor_type const covar_metric{
                Geometry::vector_type{4.819924355249979, 0, 0},
                Geometry::vector_type{0, 2.929984592244973, -1.822575949062002},
                Geometry::vector_type{0, -1.822575949062002, 1.915250930278945}
            };
            m = cs.covar_metric(polar);
            r = covar_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.x * covar_metric.x)) < 1e-15);
            r = covar_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.y * covar_metric.y)) < 1e-15);
            r = covar_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.z * covar_metric.z)) < 1e-15);
            m = cs.covar_metric(curvi);
            r = covar_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.x * covar_metric.x)) < 1e-14);
            r = covar_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.y * covar_metric.y)) < 1e-14);
            r = covar_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.z * covar_metric.z)) < 1e-14);
        }

        // contr metric
        //
        {
            Geometry::tensor_type m;
            Geometry::tensor_type const contr_metric{
                Geometry::vector_type{0.2074721357215441, 0, 0},
                Geometry::vector_type{0, 0.836400640199552, 0.7959289649767946},
                Geometry::vector_type{0, 0.7959289649767946, 1.279540424695994}
            };
            m = cs.contr_metric(polar);
            r = contr_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.x * contr_metric.x)) < 1e-15);
            r = contr_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.y * contr_metric.y)) < 1e-15);
            r = contr_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.z * contr_metric.z)) < 1e-15);
            m = cs.contr_metric(curvi);
            r = contr_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.x * contr_metric.x)) < 1e-14);
            r = contr_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.y * contr_metric.y)) < 1e-14);
            r = contr_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.z * contr_metric.z)) < 1e-14);
        }

        // covar basis
        //
        {
            Geometry::vector_type const g1{2.153824047972304, 0, -0.4254013700332637};
            Geometry::vector_type const g2{-0.2517262277161414, 1.114570997406436, -1.27449989810325};
            Geometry::vector_type const g3{0.2681235219089763, -0.02236112405551628, 1.357520050462992};
            //
            r = g1 - cs.covar_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-15);
            r = g2 - cs.covar_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-15);
            r = g3 - cs.covar_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-15);
            r = g1 - cs.covar_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-14);
            r = g2 - cs.covar_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-14);
            r = g3 - cs.covar_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-14);
            //
            r = g1 - cs.covar_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-15);
            r = g2 - cs.covar_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-15);
            r = g3 - cs.covar_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-15);
            r = g1 - cs.covar_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-14);
            r = g2 - cs.covar_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-14);
            r = g3 - cs.covar_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-14);
        }

        // contr basis
        //
        {
            Geometry::vector_type const h1{0.4468584752012354, 0, -0.0882589307796721};
            Geometry::vector_type const h2{0.002863299262145685, 0.914430029453372, 0.01449699799243365};
            Geometry::vector_type const h3{0.1427186892107758, 0.858507378188184, 0.7225903971423935};
            //
            r = h1 - cs.contr_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-15);
            r = h2 - cs.contr_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-15);
            r = h1 - cs.contr_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-14);
            r = h2 - cs.contr_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-14);
            //
            r = h1 - cs.contr_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-15);
            r = h2 - cs.contr_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-15);
            r = h1 - cs.contr_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-14);
            r = h2 - cs.contr_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-14);
        }

        // mfa basis
        //
        {
            Geometry::vector_type const e1 = {-0.1470604191268429, 0.6511410412507584, -0.7445727483094688};
            Geometry::vector_type const e2 = {0.126169343334983, 0.7589567473834582, 0.6388004010671418};
            Geometry::vector_type const e3 = {0.981047669549577, 0., -0.1937665349624219};
            //
            r = e1 - cs.mfa_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e2 - cs.mfa_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e3 - cs.mfa_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e1 - cs.mfa_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-14);
            r = e2 - cs.mfa_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-14);
            r = e3 - cs.mfa_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            //
            r = e1 - cs.mfa_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e1 * e1)) < 1e-15);
            r = e2 - cs.mfa_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e2 * e2)) < 1e-15);
            r = e3 - cs.mfa_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e3 * e3)) < 1e-15);
            r = e1 - cs.mfa_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e1 * e1)) < 1e-14);
            r = e2 - cs.mfa_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e2 * e2)) < 1e-14);
            r = e3 - cs.mfa_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e3 * e3)) < 1e-15);
        }

        // B vector
        //
        {
            Geometry::vector_type const BOB0 = {-0.2189593121474791, 0.969488563610625, -1.108599701956387};
            Geometry::vector_type const BOB0ref = {-0.3672497790265965, 1.626075900873319, -1.859400230935193};
            r = cs.BcartsOverB0(polar) - BOB0;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0 * BOB0)) < 1e-15);
            r = cs.BcartsOverB0(curvi) - BOB0;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0 * BOB0)) < 1e-14);

            r = cs.BcartsOverB0ref(polar) - BOB0ref;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0ref * BOB0ref)) < 1e-15);
            r = cs.BcartsOverB0ref(curvi) - BOB0ref;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0ref * BOB0ref)) < 1e-14);
        }
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testGeometry__1_NegativeN {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::PolarCoord;
    using GEO::__1_::Geometry;

    try {
        constexpr double n = -1.1, r0ref = 20, D1 = 3., D2 = 1.13, D3 = .98, q3ref = r0ref/(D3*(3 - n));
        const Geometry::vector_type D = {D1, D2, D3};
        Geometry _cs{r0ref, D, n};
        Geometry const cs{std::move(_cs)};
        XCTAssert(!_cs.xi_interp() && r0ref == cs.r0ref() && std::abs(q3ref - cs.q3ref()) < 1e-15*q3ref && D1 == cs.D1() && D2 == cs.D2() && D3 == cs.D3() && n == cs.n());
        XCTAssert(std::abs(D1*D2*D3 - std::sqrt(cs.g())) < 1e-15);

        // `xi' inversion
        //
        {
            double const max_scaled_q2 = scaled_q2(1);
            auto const xi_interp = cs.xi_interp();
            println(std::cout, "xi interpolator table count = ", xi_interp.spline_coefficient().table().size());

            XCTAssert(std::abs(max_scaled_q2 - xi_interp.max_abscissa()) < 1e-15*max_scaled_q2, @"max_scaled_q2 = %f, max_abscissa = %f, err = %e", max_scaled_q2, xi_interp.max_abscissa(), std::abs(max_scaled_q2 - xi_interp.max_abscissa())/max_scaled_q2);
            XCTAssert(std::abs(-max_scaled_q2 - xi_interp.min_abscissa()) < 1e-15*max_scaled_q2, @"min_scaled_q2 = %f, min_abscissa = %f, err = %e", -max_scaled_q2, xi_interp.min_abscissa(), std::abs(-max_scaled_q2 - xi_interp.min_abscissa())/max_scaled_q2);

            UTL::NRRandomReal rng{100};
            UTL::UniformDistribution const dist{-.9, .9};
            UTL::RandomVariate rv{&rng, &dist};
            constexpr long n = 5000;
            for (long i = 0; i < n; ++i) {
                double const xi = rv();
                double const q2 = scaled_q2(xi);
                auto const approx_xi = xi_interp(q2);
                bool stop;
                stop = !approx_xi;
                XCTAssert(!stop, @"i = %ld, q2 = %f, xi = %f, approx_xi = (nil)", i, q2, xi);
                if (stop) {
                    break;
                }
                stop = !(std::abs(xi - approx_xi()) < 1e-10*max_scaled_q2);
                XCTAssert(!stop, @"i = %ld, q2 = %f, xi = %f, approx_xi = %f, err = %e", i, q2, xi, approx_xi(), std::abs(xi - approx_xi())/max_scaled_q2);
                if (stop) {
                    break;
                }
            }
        }

        // bound checking
        //
        {
            double const r0 = cs.r0ref() - 1;
            double const q1 = M_PI*cs.r0ref()/cs.D1();
            double const q2 = scaled_q2(1)*std::pow(r0/cs.r0ref(), cs.n())*cs.r0ref()/cs.D2() - 1e-10;
            double const q3 = std::pow(r0/cs.r0ref(), 3 - cs.n())*cs.r0ref()/(cs.D3()*(3 - cs.n())) - q3ref;
            XCTAssert(cs.is_valid(CurviCoord{q1, q2, q3}) && cs.is_valid(CurviCoord{-q1, -q2, q3}));
            XCTAssert(!cs.is_valid(CurviCoord{q1, q2 - 1000, q3}));
            XCTAssert(!cs.is_valid(CurviCoord{-q1, q2, q3 - 1000}));
            XCTAssert(!cs.is_valid(CurviCoord{q1 + 1, q2, q3}));

            XCTAssert(cs.is_valid(PolarCoord{M_PI, .99, r0}) && cs.is_valid(PolarCoord{-M_PI, -.99, r0}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI, -1.1, r0}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI, 0, -1}));
            XCTAssert(!cs.is_valid(PolarCoord{M_PI + 1, 0, r0}));
        }

        // azimuthal angle wrapping
        //
        {
            PolarCoord p{M_PI + .1, 0, 0};
            double a = -M_PI + .1;
            p = cs.wrap(p);
            XCTAssert(std::abs(a - p.phi()) < 1e-15);
            p.phi() = -M_PI - .1;
            a = M_PI - .1;
            p = cs.wrap(p);
            XCTAssert(std::abs(a - p.phi()) < 1e-15);

            const double pi = M_PI*r0ref/D1;
            CurviCoord q{pi + .1, 0, 0};
            a = -pi + .1;
            q = cs.wrap(q);
            XCTAssert(std::abs(a - q.q1()) < 1e-15);
            q.q1() = -pi - .1;
            a = pi - .1;
            q = cs.wrap(q);
            XCTAssert(std::abs(a - q.q1()) < 1e-15);
        }

        CurviCoord const curvi{1.3, 4, -3};
        PolarCoord const polar{0.195, 0.1823680101286019, 15.43705963428362};
        Geometry::vector_type r;

        // cotrans
        //
        {
            r = cs.coord_trans(polar);
            XCTAssert(std::abs(r.x - curvi.x) < 1e-15*std::abs(curvi.x) &&
                      std::abs(r.y - curvi.y) < 1e-14*std::abs(curvi.y) &&
                      std::abs(r.z - curvi.z) < 1e-14*std::abs(curvi.z),
                      @"err.x = %e, err.y = %e, err.z = %e",
                      std::abs(r.x - curvi.x)/std::abs(curvi.x),
                      std::abs(r.y - curvi.y)/std::abs(curvi.y),
                      std::abs(r.z - curvi.z)/std::abs(curvi.z));
        }
        {
            r = cs.coord_trans(curvi);
            XCTAssert(std::abs(r.x - polar.x) < 1e-15*std::abs(polar.x) &&
                      std::abs(r.y - polar.y) < 1e-14*std::abs(polar.y) &&
                      std::abs(r.z - polar.z) < 1e-14*std::abs(polar.z),
                      @"err.x = %e, err.y = %e, err.z = %e",
                      std::abs(r.x - polar.x)/std::abs(polar.x),
                      std::abs(r.y - polar.y)/std::abs(polar.y),
                      std::abs(r.z - polar.z)/std::abs(polar.z));
        }

        // q1 translation
        //
        {
            Geometry::vector_type const v_ptl{.8, 1.1, 3}, v_ref{0.2035385307523959, 1.1, 3.098156236618669};
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);

            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
        }

        // covar metric
        //
        {
            Geometry::tensor_type m;
            Geometry::tensor_type const covar_metric{
                Geometry::vector_type{5.183489555519411, 0, 0},
                Geometry::vector_type{0, 0.6682487215076989, -0.1786661794065889},
                Geometry::vector_type{0, -0.1786661794065889, 3.234102207096898}
            };
            m = cs.covar_metric(polar);
            r = covar_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.x * covar_metric.x)) < 1e-15);
            r = covar_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.y * covar_metric.y)) < 1e-15);
            r = covar_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.z * covar_metric.z)) < 1e-15);
            m = cs.covar_metric(curvi);
            r = covar_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.x * covar_metric.x)) < 1e-14);
            r = covar_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.y * covar_metric.y)) < 1e-14);
            r = covar_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_metric.z * covar_metric.z)) < 1e-14);
        }

        // contr metric
        //
        {
            Geometry::tensor_type m;
            Geometry::tensor_type const contr_metric{
                Geometry::vector_type{0.1929202305298742, 0, 0},
                Geometry::vector_type{0, 1.518883347785346, 0.0839098665829415},
                Geometry::vector_type{0, 0.0839098665829415, 0.3138403767974921}
            };
            m = cs.contr_metric(polar);
            r = contr_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.x * contr_metric.x)) < 1e-15);
            r = contr_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.y * contr_metric.y)) < 1e-15);
            r = contr_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.z * contr_metric.z)) < 2e-15);
            m = cs.contr_metric(curvi);
            r = contr_metric.x - m.x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.x * contr_metric.x)) < 1e-14);
            r = contr_metric.y - m.y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.y * contr_metric.y)) < 1e-14);
            r = contr_metric.z - m.z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_metric.z * contr_metric.z)) < 1e-14);
        }

        // covar basis
        //
        {
            Geometry::vector_type const g1{2.23357851967365, 0, -0.4411536602725581};
            Geometry::vector_type const g2{-0.0812496035094005, 0.7017278454610962, -0.4113699725816068};
            Geometry::vector_type const g3{0.3186327238167859, 0.7280105462388981, 1.613250147675473};
            //
            r = g1 - cs.covar_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-15);
            r = g2 - cs.covar_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-15);
            r = g3 - cs.covar_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-15);
            r = g1 - cs.covar_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-14);
            r = g2 - cs.covar_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-14);
            r = g3 - cs.covar_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-14);
            //
            r = g1 - cs.covar_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-15);
            r = g2 - cs.covar_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-15);
            r = g3 - cs.covar_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-15);
            r = g1 - cs.covar_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g1 * g1)) < 1e-14);
            r = g2 - cs.covar_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g2 * g2)) < 1e-14);
            r = g3 - cs.covar_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(g3 * g3)) < 1e-14);
        }

        // contr basis
        //
        {
            Geometry::vector_type const h1{0.4309024829220157, 0, -0.0851074658388797};
            Geometry::vector_type const h2{-0.0966722404401644, 1.126930006954028, -0.4894553964767575};
            Geometry::vector_type const h3{0.0931821707122806, 0.2873609940343393, 0.4717850346995291};
            //
            r = h1 - cs.contr_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-15);
            r = h2 - cs.contr_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-15);
            r = h1 - cs.contr_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-14);
            r = h2 - cs.contr_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-14);
            r = h3 - cs.contr_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-14);
            //
            r = h1 - cs.contr_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-15);
            r = h2 - cs.contr_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-15);
            r = h3 - cs.contr_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-15);
            r = h1 - cs.contr_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h1 * h1)) < 1e-14);
            r = h2 - cs.contr_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h2 * h2)) < 1e-14);
            r = h3 - cs.contr_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(h3 * h3)) < 1e-14);
        }

        // mfa basis
        //
        {
            Geometry::vector_type const e1 = {-0.0993921721835892, 0.858419633198811, -0.503226519022445};
            Geometry::vector_type const e2 = {0.1663329978686467, 0.5129480805489183, 0.842150580645296};
            Geometry::vector_type const e3 = {0.981047669549577, 0., -0.193766534962422};
            //
            r = e1 - cs.mfa_basis<1>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e2 - cs.mfa_basis<2>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e3 - cs.mfa_basis<3>(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            r = e1 - cs.mfa_basis<1>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-14);
            r = e2 - cs.mfa_basis<2>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-14);
            r = e3 - cs.mfa_basis<3>(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)) < 1e-15);
            //
            r = e1 - cs.mfa_basis<0>(polar).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e1 * e1)) < 1e-15);
            r = e2 - cs.mfa_basis<0>(polar).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e2 * e2)) < 1e-15);
            r = e3 - cs.mfa_basis<0>(polar).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e3 * e3)) < 1e-15);
            r = e1 - cs.mfa_basis<0>(curvi).x;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e1 * e1)) < 1e-14);
            r = e2 - cs.mfa_basis<0>(curvi).y;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e2 * e2)) < 1e-14);
            r = e3 - cs.mfa_basis<0>(curvi).z;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(e3 * e3)) < 1e-15);
        }

        // B vector
        //
        {
            Geometry::vector_type const BOB0 = {-0.1153645356407598, 0.996368025702967, -0.5840952301747341};
            Geometry::vector_type const BOB0ref = {-0.2266732134851047, 1.957706854584739, -1.147655491088811};
            r = cs.BcartsOverB0(polar) - BOB0;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0 * BOB0)) < 1e-15);
            r = cs.BcartsOverB0(curvi) - BOB0;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0 * BOB0)) < 1e-14);

            r = cs.BcartsOverB0ref(polar) - BOB0ref;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0ref * BOB0ref)) < 1e-15);
            r = cs.BcartsOverB0ref(curvi) - BOB0ref;
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(BOB0ref * BOB0ref)) < 1e-14);
        }
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
