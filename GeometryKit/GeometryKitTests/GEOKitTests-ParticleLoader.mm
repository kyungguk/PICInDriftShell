//
//  GEOKitTests-ParticleLoader.mm
//  GeometryKitTests
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOParticleLoader.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <random>

@interface GEOKitTests_ParticleLoader : XCTestCase

@end

@implementation GEOKitTests_ParticleLoader

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    //GEO::RandomRealPool::reset(true);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- __1_
- (void)testRandomRealPool__1_ {
    using GEO::__1_::RandomRealPool;

    try {
        constexpr long n = 5000;
        UTL::DynamicArray<UTL::Vector<double, 7>> bag(n);
        RandomRealPool &rng = RandomRealPool::rng();

        RandomRealPool::reset();
        for (auto &v : bag) {
            std::get<0>(v) = rng.phi();
            std::get<1>(v) = rng.q1();
            std::get<2>(v) = rng.q2();
            std::get<3>(v) = rng.q3();
            std::get<4>(v) = rng.v1();
            std::get<5>(v) = rng.v2();
            std::get<6>(v) = rng.alpha();
        }

        auto const seeds = RandomRealPool::seeds();
        for (long i = 0; i < seeds.size(); ++i) {
            for (long j = i + 1; j < seeds.size(); ++j) {
                XCTAssert(seeds[i] != seeds[j], @"seeds[%ld] == seeds[%ld]", i, j);
            }
        }
        printo(std::cout, "seeds = ", seeds, "\n");

        RandomRealPool::reset(seeds);
        for (auto const &v : bag) {
            XCTAssert(std::get<0>(v) != rng.phi());
            XCTAssert(std::get<1>(v) != rng.q1());
            XCTAssert(std::get<2>(v) != rng.q2());
            XCTAssert(std::get<3>(v) != rng.q3());
            XCTAssert(std::get<4>(v) != rng.v1());
            XCTAssert(std::get<5>(v) != rng.v2());
            XCTAssert(std::get<6>(v) != rng.alpha());
        }

        RandomRealPool::reset();
        for (auto const &v : bag) {
            XCTAssert(std::get<0>(v) == rng.phi());
            XCTAssert(std::get<1>(v) == rng.q1());
            XCTAssert(std::get<2>(v) == rng.q2());
            XCTAssert(std::get<3>(v) == rng.q3());
            XCTAssert(std::get<4>(v) == rng.v1());
            XCTAssert(std::get<5>(v) == rng.v2());
            XCTAssert(std::get<6>(v) == rng.alpha());
        }

        if ( (0) ) {
            std::ofstream os;
            os.precision(12);
            {
                NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] stringByAppendingPathComponent:@"random_real_pool.m"];
                os.open(path.UTF8String);
            }
            printo(os, bag, "\n");
            os.close();
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testBiMaxwellianLoader__1_1D {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::ParticleLoader;
    using GEO::__1_::BiMaxwellianLoader;

    std::ostringstream os;
    os.precision(12);

    try {
        constexpr long ND = 1;
        constexpr double dip_type = 1.5, r0ref = 5.2;
        ParticleLoader<ND>::position_type const D = {r0ref*M_PI/180};
        ParticleLoader<ND>::size_vector_type const N = {201};
        ParticleLoader<ND>::position_type const q_min = -.5*ParticleLoader<ND>::position_type{N - 1L};
        ParticleLoader<ND>::position_type const q_max = q_min + ParticleLoader<ND>::position_type{N - 1L};
        CoordSystem const cs{r0ref, D, dip_type};
        println(os, "r0ref = ", r0ref);
        println(os, "dipIdx = ", cs.n());
        println(os, "\\[CapitalDelta] = ", D);
        println(os, "qmin = ", q_min);
        println(os, "qmax = ", q_max) << std::endl;

        constexpr double beta1_0 = .45, T2OT1_0 = 5, A0 = T2OT1_0 - 1;
        println(os, "beta1 = ", beta1_0);
        println(os, "A0 = ", A0) << std::endl;

        BiMaxwellianLoader<ND>::param_type bimax; {
            bimax.vdf = BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1_0), A0};
        }
        ParticleLoader<ND> const &loader = BiMaxwellianLoader<ND>{cs, q_min, N, std::move(bimax)};
        println(os, "NcONtot = ", loader.NcellOverNtotal()) << std::endl;

        XCTAssert(UTL::reduce_bit_and(loader.N() == N));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_min() == q_min));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_max() == q_max));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_min() == q_min - .5));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_max() == q_max + .5));

        if ( (0) ) {
            CurviCoord q{0};

            UTL::ArrayND<ParticleLoader<ND>::velocity_type, ND, 0> J(N);
            for (long i = -J.pad_size(); i < N.x + J.pad_size(); ++i) {
                q.x = i + q_min.x;
                J[i] = loader.normalized_current_density(q);
            }
            println(os, "\\[CapitalOmega]srefV = ", J) << std::endl;

            const long Nc = 5000, Ntot = UTL::reduce_prod(N)*Nc;
            UTL::DynamicArray<decltype(loader())> bucket;
            bucket.reserve(Ntot);
            for (long i = 0; i < Ntot; ++i) {
                decltype(loader()) ptl = loader();
                // velocity transformation
                q.take<1>() = ptl.pos;
                double const v1 = UTL::reduce_plus(cs.mfa_basis<1>(q) * ptl.vel);
                double const v2 = UTL::reduce_plus(cs.mfa_basis<2>(q) * ptl.vel);
                double const v3 = UTL::reduce_plus(cs.mfa_basis<3>(q) * ptl.vel);
                ptl.vel.x = v1;
                ptl.vel.y = v2;
                ptl.vel.z = v3;
                //
                bucket.push_back(ptl);
            }
            using Array = UTL::Array<UTL::Vector<double, sizeof(decltype(loader()))/sizeof(double)>>;
            println(os, "ptls = ", Array{reinterpret_cast<Array::pointer>(bucket.data()), bucket.size()}) << std::endl;

            {
                NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
                contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
                contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

                NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
                NSError *error;
                XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"bi_maxwellian.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}
- (void)testBiMaxwellianLoader__1_2D {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::ParticleLoader;
    using GEO::__1_::BiMaxwellianLoader;

    std::ostringstream os;
    os.precision(12);

    try {
        constexpr long ND = 2;
        constexpr double dip_type = 1.5, r0ref = 5.2;
        ParticleLoader<ND>::position_type const D = {r0ref*M_PI/180, .11};
        ParticleLoader<ND>::size_vector_type const N = {17, 29};
        ParticleLoader<ND>::position_type const q_min = -.5*ParticleLoader<ND>::position_type{N - 1L};
        ParticleLoader<ND>::position_type const q_max = q_min + ParticleLoader<ND>::position_type{N - 1L};
        CoordSystem const cs{r0ref, D, dip_type};
        println(os, "r0ref = ", r0ref);
        println(os, "dipIdx = ", cs.n());
        println(os, "\\[CapitalDelta] = ", D);
        println(os, "qmin = ", q_min);
        println(os, "qmax = ", q_max) << std::endl;

        constexpr double beta1_0 = .45, T2OT1_0 = 5, A0 = T2OT1_0 - 1;
        println(os, "beta1 = ", beta1_0);
        println(os, "A0 = ", A0) << std::endl;

        BiMaxwellianLoader<ND>::param_type bimax; {
            bimax.vdf = BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1_0), A0};
        }
        ParticleLoader<ND> const &loader = BiMaxwellianLoader<ND>{cs, q_min, N, std::move(bimax)};
        println(os, "NcONtot = ", loader.NcellOverNtotal()) << std::endl;

        XCTAssert(UTL::reduce_bit_and(loader.N() == N));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_min() == q_min));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_max() == q_max));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_min() == q_min - .5));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_max() == q_max + .5));

        if ( (0) ) {
            CurviCoord q{0};

            UTL::ArrayND<ParticleLoader<ND>::velocity_type, ND, 0> J(N);
            for (long i = -J.pad_size(); i < N.x + J.pad_size(); ++i) {
                q.x = i + q_min.x;
                for (long j = -J.pad_size(); j < N.y + J.pad_size(); ++j) {
                    q.y = j + q_min.y;
                    J[i][j] = loader.normalized_current_density(q);
                }
            }
            println(os, "\\[CapitalOmega]srefV = ", J) << std::endl;

            const long Nc = 5000, Ntot = UTL::reduce_prod(N)*Nc;
            UTL::DynamicArray<decltype(loader())> bucket;
            bucket.reserve(Ntot);
            for (long i = 0; i < Ntot; ++i) {
                decltype(loader()) ptl = loader();
                // velocity transformation
                q.most() = ptl.pos;
                double const v1 = UTL::reduce_plus(cs.mfa_basis<1>(q) * ptl.vel);
                double const v2 = UTL::reduce_plus(cs.mfa_basis<2>(q) * ptl.vel);
                double const v3 = UTL::reduce_plus(cs.mfa_basis<3>(q) * ptl.vel);
                ptl.vel.x = v1;
                ptl.vel.y = v2;
                ptl.vel.z = v3;
                //
                bucket.push_back(ptl);
            }
            using Array = UTL::Array<UTL::Vector<double, sizeof(decltype(loader()))/sizeof(double)>>;
            println(os, "ptls = ", Array{reinterpret_cast<Array::pointer>(bucket.data()), bucket.size()}) << std::endl;

            {
                NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
                contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
                contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

                NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
                NSError *error;
                XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"bi_maxwellian.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}
- (void)testBiMaxwellianLoader__1_3D {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::ParticleLoader;
    using GEO::__1_::BiMaxwellianLoader;

    std::ostringstream os;
    os.precision(12);

    try {
        constexpr long ND = 3;
        constexpr double dip_type = 1.5, r0ref = 5.2;
        ParticleLoader<ND>::position_type const D = {r0ref*M_PI/180, .11, .1};
        ParticleLoader<ND>::size_vector_type const N = {17, 29, 1};
        ParticleLoader<ND>::position_type const q_min = -.5*ParticleLoader<ND>::position_type{N - 1L};
        ParticleLoader<ND>::position_type const q_max = q_min + ParticleLoader<ND>::position_type{N - 1L};
        CoordSystem const cs{r0ref, D, dip_type};
        println(os, "r0ref = ", r0ref);
        println(os, "dipIdx = ", cs.n());
        println(os, "\\[CapitalDelta] = ", D);
        println(os, "qmin = ", q_min);
        println(os, "qmax = ", q_max) << std::endl;

        constexpr double beta1_0 = .45, T2OT1_0 = 5, A0 = T2OT1_0 - 1;
        println(os, "beta1 = ", beta1_0);
        println(os, "A0 = ", A0) << std::endl;

        BiMaxwellianLoader<ND>::param_type bimax; {
            bimax.vdf = BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1_0), A0};
        }
        ParticleLoader<ND> const &loader = BiMaxwellianLoader<ND>{cs, q_min, N, std::move(bimax)};
        println(os, "NcONtot = ", loader.NcellOverNtotal()) << std::endl;

        XCTAssert(UTL::reduce_bit_and(loader.N() == N));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_min() == q_min));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_max() == q_max));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_min() == q_min - .5));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_max() == q_max + .5));

        if ( (0) ) {
            CurviCoord q{0};

            UTL::ArrayND<ParticleLoader<ND>::velocity_type, ND, 0> J(N);
            for (long i = -J.pad_size(); i < N.x + J.pad_size(); ++i) {
                q.x = i + q_min.x;
                for (long j = -J.pad_size(); j < N.y + J.pad_size(); ++j) {
                    q.y = j + q_min.y;
                    for (long k = -J.pad_size(); k < N.z + J.pad_size(); ++k) {
                        q.z = k + q_min.z;
                        J[i][j][k] = loader.normalized_current_density(q);
                    }
                }
            }
            println(os, "\\[CapitalOmega]srefV = ", J) << std::endl;

            const long Nc = 5000, Ntot = UTL::reduce_prod(N)*Nc;
            UTL::DynamicArray<decltype(loader())> bucket;
            bucket.reserve(Ntot);
            for (long i = 0; i < Ntot; ++i) {
                decltype(loader()) ptl = loader();
                // velocity transformation
                q = ptl.pos;
                double const v1 = UTL::reduce_plus(cs.mfa_basis<1>(q) * ptl.vel);
                double const v2 = UTL::reduce_plus(cs.mfa_basis<2>(q) * ptl.vel);
                double const v3 = UTL::reduce_plus(cs.mfa_basis<3>(q) * ptl.vel);
                ptl.vel.x = v1;
                ptl.vel.y = v2;
                ptl.vel.z = v3;
                //
                bucket.push_back(ptl);
            }
            using Array = UTL::Array<UTL::Vector<double, sizeof(decltype(loader()))/sizeof(double)>>;
            println(os, "ptls = ", Array{reinterpret_cast<Array::pointer>(bucket.data()), bucket.size()}) << std::endl;

            {
                NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
                contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
                contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

                NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
                NSError *error;
                XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"bi_maxwellian.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testPartialShellLoader__1_1D {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::ParticleLoader;
    using GEO::__1_::PartialShellLoader;

    std::ostringstream os;
    os.precision(12);

    try {
        constexpr long ND = 1;
        constexpr double dip_type = 1.5, r0ref = 5.2;
        ParticleLoader<ND>::position_type const D = {r0ref*M_PI/180};
        ParticleLoader<ND>::size_vector_type const N = {201};
        ParticleLoader<ND>::position_type const q_min = -.5*ParticleLoader<ND>::position_type{N - 1L};
        ParticleLoader<ND>::position_type const q_max = q_min + ParticleLoader<ND>::position_type{N - 1L};
        CoordSystem const cs{r0ref, D, dip_type};
        println(os, "r0ref = ", r0ref);
        println(os, "dipIdx = ", cs.n());
        println(os, "\\[CapitalDelta] = ", D);
        println(os, "qmin = ", q_min);
        println(os, "qmax = ", q_max) << std::endl;

        constexpr double vth = .45, vs = 2, zeta = 5;
        println(os, "vth = ", vth);
        println(os, "vs = ", vs);
        println(os, "zeta = ", zeta) << std::endl;

        PartialShellLoader<ND>::param_type shell; {
            shell.vdf = PartialShellLoader<ND>::vdf_type{vth, vs, zeta};
        }
        ParticleLoader<ND> const &loader = PartialShellLoader<ND>{cs, q_min, N, std::move(shell)};
        println(os, "NcONtot = ", loader.NcellOverNtotal()) << std::endl;

        XCTAssert(UTL::reduce_bit_and(loader.N() == N));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_min() == q_min));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_max() == q_max));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_min() == q_min - .5));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_max() == q_max + .5));

        if ( (0) ) {
            CurviCoord q{0};

            UTL::ArrayND<ParticleLoader<ND>::velocity_type, ND, 0> J(N);
            for (long i = -J.pad_size(); i < N.x + J.pad_size(); ++i) {
                q.x = i + q_min.x;
                J[i] = loader.normalized_current_density(q);
            }
            println(os, "\\[CapitalOmega]srefV = ", J) << std::endl;

            const long Nc = 5000, Ntot = UTL::reduce_prod(N)*Nc;
            UTL::DynamicArray<decltype(loader())> bucket;
            bucket.reserve(Ntot);
            for (long i = 0; i < Ntot; ++i) {
                decltype(loader()) ptl = loader();
                // velocity transformation
                q.take<1>() = ptl.pos;
                double const v1 = UTL::reduce_plus(cs.mfa_basis<1>(q) * ptl.vel);
                double const v2 = UTL::reduce_plus(cs.mfa_basis<2>(q) * ptl.vel);
                double const v3 = UTL::reduce_plus(cs.mfa_basis<3>(q) * ptl.vel);
                ptl.vel.x = v1;
                ptl.vel.y = v2;
                ptl.vel.z = v3;
                //
                bucket.push_back(ptl);
            }
            using Array = UTL::Array<UTL::Vector<double, sizeof(decltype(loader()))/sizeof(double)>>;
            println(os, "ptls = ", Array{reinterpret_cast<Array::pointer>(bucket.data()), bucket.size()}) << std::endl;

            {
                NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
                contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
                contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

                NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
                NSError *error;
                XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"partial_shell.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}
- (void)testPartialShellLoader__1_2D {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::ParticleLoader;
    using GEO::__1_::PartialShellLoader;

    std::ostringstream os;
    os.precision(12);

    try {
        constexpr long ND = 2;
        constexpr double dip_type = 1.5, r0ref = 5.2;
        ParticleLoader<ND>::position_type const D = {r0ref*M_PI/180, .11};
        ParticleLoader<ND>::size_vector_type const N = {17, 29};
        ParticleLoader<ND>::position_type const q_min = -.5*ParticleLoader<ND>::position_type{N - 1L};
        ParticleLoader<ND>::position_type const q_max = q_min + ParticleLoader<ND>::position_type{N - 1L};
        CoordSystem const cs{r0ref, D, dip_type};
        println(os, "r0ref = ", r0ref);
        println(os, "dipIdx = ", cs.n());
        println(os, "\\[CapitalDelta] = ", D);
        println(os, "qmin = ", q_min);
        println(os, "qmax = ", q_max) << std::endl;

        constexpr double vth = .45, vs = 2, zeta = 5;
        println(os, "vth = ", vth);
        println(os, "vs = ", vs);
        println(os, "zeta = ", zeta) << std::endl;

        PartialShellLoader<ND>::param_type shell; {
            shell.vdf = PartialShellLoader<ND>::vdf_type{vth, vs, zeta};
        }
        ParticleLoader<ND> const &loader = PartialShellLoader<ND>{cs, q_min, N, std::move(shell)};
        println(os, "NcONtot = ", loader.NcellOverNtotal()) << std::endl;

        XCTAssert(UTL::reduce_bit_and(loader.N() == N));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_min() == q_min));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_max() == q_max));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_min() == q_min - .5));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_max() == q_max + .5));

        if ( (0) ) {
            CurviCoord q{0};

            UTL::ArrayND<ParticleLoader<ND>::velocity_type, ND, 0> J(N);
            for (long i = -J.pad_size(); i < N.x + J.pad_size(); ++i) {
                q.x = i + q_min.x;
                for (long j = -J.pad_size(); j < N.y + J.pad_size(); ++j) {
                    q.y = j + q_min.y;
                    J[i][j] = loader.normalized_current_density(q);
                }
            }
            println(os, "\\[CapitalOmega]srefV = ", J) << std::endl;

            const long Nc = 5000, Ntot = UTL::reduce_prod(N)*Nc;
            UTL::DynamicArray<decltype(loader())> bucket;
            bucket.reserve(Ntot);
            for (long i = 0; i < Ntot; ++i) {
                decltype(loader()) ptl = loader();
                // velocity transformation
                q.most() = ptl.pos;
                double const v1 = UTL::reduce_plus(cs.mfa_basis<1>(q) * ptl.vel);
                double const v2 = UTL::reduce_plus(cs.mfa_basis<2>(q) * ptl.vel);
                double const v3 = UTL::reduce_plus(cs.mfa_basis<3>(q) * ptl.vel);
                ptl.vel.x = v1;
                ptl.vel.y = v2;
                ptl.vel.z = v3;
                //
                bucket.push_back(ptl);
            }
            using Array = UTL::Array<UTL::Vector<double, sizeof(decltype(loader()))/sizeof(double)>>;
            println(os, "ptls = ", Array{reinterpret_cast<Array::pointer>(bucket.data()), bucket.size()}) << std::endl;

            {
                NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
                contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
                contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

                NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
                NSError *error;
                XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"partial_shell.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}
- (void)testPartialShellLoader__1_3D {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::ParticleLoader;
    using GEO::__1_::PartialShellLoader;

    std::ostringstream os;
    os.precision(12);

    try {
        constexpr long ND = 3;
        constexpr double dip_type = 1.5, r0ref = 5.2;
        ParticleLoader<ND>::position_type const D = {r0ref*M_PI/180, .11, .1};
        ParticleLoader<ND>::size_vector_type const N = {17, 29, 1};
        ParticleLoader<ND>::position_type const q_min = -.5*ParticleLoader<ND>::position_type{N - 1L};
        ParticleLoader<ND>::position_type const q_max = q_min + ParticleLoader<ND>::position_type{N - 1L};
        CoordSystem const cs{r0ref, D, dip_type};
        println(os, "r0ref = ", r0ref);
        println(os, "dipIdx = ", cs.n());
        println(os, "\\[CapitalDelta] = ", D);
        println(os, "qmin = ", q_min);
        println(os, "qmax = ", q_max) << std::endl;

        constexpr double vth = .45, vs = 2, zeta = 5;
        println(os, "vth = ", vth);
        println(os, "vs = ", vs);
        println(os, "zeta = ", zeta) << std::endl;

        PartialShellLoader<ND>::param_type shell; {
            shell.vdf = PartialShellLoader<ND>::vdf_type{vth, vs, zeta};
        }
        ParticleLoader<ND> const &loader = PartialShellLoader<ND>{cs, q_min, N, std::move(shell)};
        println(os, "NcONtot = ", loader.NcellOverNtotal()) << std::endl;

        XCTAssert(UTL::reduce_bit_and(loader.N() == N));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_min() == q_min));
        XCTAssert(UTL::reduce_bit_and(loader.grid_q_max() == q_max));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_min() == q_min - .5));
        XCTAssert(UTL::reduce_bit_and(loader.ptl_q_max() == q_max + .5));

        if ( (0) ) {
            CurviCoord q{0};

            UTL::ArrayND<ParticleLoader<ND>::velocity_type, ND, 0> J(N);
            for (long i = -J.pad_size(); i < N.x + J.pad_size(); ++i) {
                q.x = i + q_min.x;
                for (long j = -J.pad_size(); j < N.y + J.pad_size(); ++j) {
                    q.y = j + q_min.y;
                    for (long k = -J.pad_size(); k < N.z + J.pad_size(); ++k) {
                        q.z = k + q_min.z;
                        J[i][j][k] = loader.normalized_current_density(q);
                    }
                }
            }
            println(os, "\\[CapitalOmega]srefV = ", J) << std::endl;

            const long Nc = 5000, Ntot = UTL::reduce_prod(N)*Nc;
            UTL::DynamicArray<decltype(loader())> bucket;
            bucket.reserve(Ntot);
            for (long i = 0; i < Ntot; ++i) {
                decltype(loader()) ptl = loader();
                // velocity transformation
                q = ptl.pos;
                double const v1 = UTL::reduce_plus(cs.mfa_basis<1>(q) * ptl.vel);
                double const v2 = UTL::reduce_plus(cs.mfa_basis<2>(q) * ptl.vel);
                double const v3 = UTL::reduce_plus(cs.mfa_basis<3>(q) * ptl.vel);
                ptl.vel.x = v1;
                ptl.vel.y = v2;
                ptl.vel.z = v3;
                //
                bucket.push_back(ptl);
            }
            using Array = UTL::Array<UTL::Vector<double, sizeof(decltype(loader()))/sizeof(double)>>;
            println(os, "ptls = ", Array{reinterpret_cast<Array::pointer>(bucket.data()), bucket.size()}) << std::endl;

            {
                NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
                contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
                contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

                NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
                NSError *error;
                XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"partial_shell.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
