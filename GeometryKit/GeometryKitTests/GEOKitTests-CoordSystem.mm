//
//  GEOKitTests-CoordSystem.mm
//  GeometryKitTests
//
//  Created by KYUNGGUK MIN on 3/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOCoordSystem.h>
#include <iostream>
#include <sstream>

@interface GEOKitTests_CoordSystem : XCTestCase

@end

@implementation GEOKitTests_CoordSystem

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- __1_
- (void)testCoordSystem__1_ZeroN {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::PolarCoord;
    using GEO::__1_::CoordSystem;

    try {
        constexpr double n = 0, r0ref = 20;
        CoordSystem::vector_type const D = {3, 1.13, .98};
        CoordSystem _cs1{r0ref, D, n}, _cs2(_cs1), cs;
        XCTAssert(_cs1.xi_interp() && _cs2.xi_interp() && !cs.xi_interp());
        cs = std::move(_cs2);
        XCTAssert(_cs1.xi_interp() && !_cs2.xi_interp() && cs.xi_interp());

        double const q3ref = r0ref/(cs.D3()*(3 - n));
        XCTAssert(std::abs(n - cs.n()) < 1e-15, @"cs.n() = %f", cs.n());
        XCTAssert(UTL::reduce_bit_and(cs.D() == D), @"cs.D() = {%f, %f, %f}", cs.D().x, cs.D().y, cs.D().z);
        XCTAssert(std::abs(UTL::reduce_prod(D) - std::sqrt(cs.g())) < 1e-15);
        XCTAssert(cs.r0ref() == r0ref && std::abs(cs.q3ref() - q3ref) < 1e-15*q3ref);

        CurviCoord const curvi{1.3, 4, -3};
        PolarCoord const polar{0.195, 0.2392271768621264, 15.53244594222082};
        CoordSystem::vector_type r;

        // q1 translation
        //
        {
            CoordSystem::vector_type const v_ptl{.8, 1.1, 3}, v_ref{0.2035385307523959, 1.1, 3.098156236618669};
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);

            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
        }

        // position vector
        //
        {
            CoordSystem::vector_type const carts{2.922278369377216, 3.715783192521076, 14.79561155701446};
            r = carts - cs.position(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)) < 1e-15, @"%e", std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)));
            r = carts - cs.position(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)) < 1e-14, @"%e", std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)));
        }

        // component trans
        //
        {
            CoordSystem::vector_type const carts_v = {.8, 1.1, 3};
            CoordSystem::vector_type const contr_v = {0.0899730754571913, 0.3030701114375157, 2.521026928231986};
            CoordSystem::vector_type const covar_v = {0.4604481206219882, -1.386131616354594, 4.454005232625398};
            CoordSystem::vector_type const   mfa_v = {-1.152746161781266,  3.078919997823541, 0.203538530752396};

            r = contr_v - cs.carts_to_contr(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 2e-15);
            r = covar_v - cs.carts_to_covar(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 2e-15);
            r =   mfa_v - cs.carts_to_mfa(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(  mfa_v *   mfa_v)) < 1e-15);
            r = carts_v - cs.contr_to_carts(contr_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 2e-15);
            r = carts_v - cs.covar_to_carts(covar_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-15);
            r = carts_v - cs.mfa_to_carts(mfa_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-15);
            r = covar_v - cs.contr_to_covar(contr_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 2e-15);
            r = contr_v - cs.covar_to_contr(covar_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 2e-15);
            //
            r = contr_v - cs.carts_to_contr(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-14);
            r = covar_v - cs.carts_to_covar(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r =   mfa_v - cs.carts_to_mfa(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(  mfa_v *   mfa_v)) < 1e-14);
            r = carts_v - cs.contr_to_carts(contr_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = carts_v - cs.covar_to_carts(covar_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 2e-14);
            r = carts_v - cs.mfa_to_carts(mfa_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = covar_v - cs.contr_to_covar(contr_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r = contr_v - cs.covar_to_contr(covar_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 2e-14);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testGeometry__1_PositiveN {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::PolarCoord;
    using GEO::__1_::CoordSystem;

    try {
        constexpr double n = 1.1, r0ref = 20;
        CoordSystem::vector_type const D = {3, 1.13, .98};
        CoordSystem _cs1{r0ref, D, n}, _cs2(_cs1), cs;
        XCTAssert(_cs1.xi_interp() && _cs2.xi_interp() && !cs.xi_interp());
        cs = std::move(_cs2);
        XCTAssert(_cs1.xi_interp() && !_cs2.xi_interp() && cs.xi_interp());

        double const q3ref = r0ref/(cs.D3()*(3 - n));
        XCTAssert(std::abs(n - cs.n()) < 1e-15, @"cs.n() = %f", cs.n());
        XCTAssert(UTL::reduce_bit_and(cs.D() == D), @"cs.D() = {%f, %f, %f}", cs.D().x, cs.D().y, cs.D().z);
        XCTAssert(std::abs(UTL::reduce_prod(D) - std::sqrt(cs.g())) < 1e-15);
        XCTAssert(cs.r0ref() == r0ref && std::abs(cs.q3ref() - q3ref) < 1e-15*q3ref);

        CurviCoord const curvi{1.3, 4, -3};
        PolarCoord const polar{0.195, 0.2983583670125045, 15.33465064511582};
        CoordSystem::vector_type r;

        // q1 translation
        //
        {
            CoordSystem::vector_type const v_ptl{.8, 1.1, 3}, v_ref{0.2035385307523959, 1.1, 3.098156236618669};
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);

            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
        }

        // position vector
        //
        {
            CoordSystem::vector_type const carts{2.836009133555091, 4.575221325184004, 14.35882698648203};
            r = carts - cs.position(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)) < 1e-15, @"%e", std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)));
            r = carts - cs.position(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)) < 1e-14, @"%e", std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)));
        }

        // component trans
        //
        {
            CoordSystem::vector_type const carts_v = {.8, 1.1, 3};
            CoordSystem::vector_type const contr_v = {0.0927099878219721, 1.051654665785727, 3.226304258802803};
            CoordSystem::vector_type const covar_v = {0.4468551282780524, -2.798852579335585, 4.26246173245509};
            CoordSystem::vector_type const   mfa_v = {-1.635111434854046, 2.852189099991215, 0.203538530752396};

            r = contr_v - cs.carts_to_contr(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-15);
            r = covar_v - cs.carts_to_covar(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-15);
            r =   mfa_v - cs.carts_to_mfa(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(  mfa_v *   mfa_v)) < 1e-15);
            r = carts_v - cs.contr_to_carts(contr_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = carts_v - cs.covar_to_carts(covar_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-15);
            r = carts_v - cs.mfa_to_carts(mfa_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-15);
            r = covar_v - cs.contr_to_covar(contr_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r = contr_v - cs.covar_to_contr(covar_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-15);
            //
            r = contr_v - cs.carts_to_contr(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-15);
            r = covar_v - cs.carts_to_covar(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r =   mfa_v - cs.carts_to_mfa(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(  mfa_v *   mfa_v)) < 1e-14);
            r = carts_v - cs.contr_to_carts(contr_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-15);
            r = carts_v - cs.covar_to_carts(covar_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = carts_v - cs.mfa_to_carts(mfa_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = covar_v - cs.contr_to_covar(contr_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r = contr_v - cs.covar_to_contr(covar_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-14);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testGeometry__1_NegativeN {
    using GEO::__1_::CurviCoord;
    using GEO::__1_::PolarCoord;
    using GEO::__1_::CoordSystem;

    try {
        constexpr double n = -1.1, r0ref = 20;
        CoordSystem::vector_type const D = {3, 1.13, .98};
        CoordSystem _cs1{r0ref, D, n}, _cs2(_cs1), cs;
        XCTAssert(_cs1.xi_interp() && _cs2.xi_interp() && !cs.xi_interp());
        cs = std::move(_cs2);
        XCTAssert(_cs1.xi_interp() && !_cs2.xi_interp() && cs.xi_interp());

        double const q3ref = r0ref/(cs.D3()*(3 - n));
        XCTAssert(std::abs(n - cs.n()) < 1e-15, @"cs.n() = %f", cs.n());
        XCTAssert(UTL::reduce_bit_and(cs.D() == D), @"cs.D() = {%f, %f, %f}", cs.D().x, cs.D().y, cs.D().z);
        XCTAssert(std::abs(UTL::reduce_prod(D) - std::sqrt(cs.g())) < 1e-15);
        XCTAssert(cs.r0ref() == r0ref && std::abs(cs.q3ref() - q3ref) < 1e-15*q3ref);

        CurviCoord const curvi{1.3, 4, -3};
        PolarCoord const polar{0.195, 0.1823680101286019, 15.43705963428362};
        CoordSystem::vector_type r;

        // q1 translation
        //
        {
            CoordSystem::vector_type const v_ptl{.8, 1.1, 3}, v_ref{0.2035385307523959, 1.1, 3.098156236618669};
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);
            r = v_ref - cs.translate_carts_to_reference_meridian(v_ptl, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ptl * v_ptl)) < 1e-15);

            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
            r = v_ptl - cs.translate_carts_from_reference_meridian(v_ref, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(v_ref * v_ref)) < 1e-15);
        }

        // position vector
        //
        {
            CoordSystem::vector_type const carts{2.941024401817054, 2.815225847740868, 14.890523464491};
            r = carts - cs.position(polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)) < 1e-15, @"%e", std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)));
            r = carts - cs.position(curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)) < 1e-14, @"%e", std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts * carts)));
        }

        // component trans
        //
        {
            CoordSystem::vector_type const carts_v = {.8, 1.1, 3};
            CoordSystem::vector_type const contr_v = {0.0893995888209735, -0.3060809741329735, 1.805997934106185};
            CoordSystem::vector_type const covar_v = {0.4634018349212459, -0.527208970545135, 5.895468222942635};
            CoordSystem::vector_type const   mfa_v = {-0.6449316982955144, 3.223761028834615, 0.203538530752396};

            r = contr_v - cs.carts_to_contr(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-15);
            r = covar_v - cs.carts_to_covar(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-15);
            r =   mfa_v - cs.carts_to_mfa(carts_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(  mfa_v *   mfa_v)) < 1e-15);
            r = carts_v - cs.contr_to_carts(contr_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = carts_v - cs.covar_to_carts(covar_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-15);
            r = carts_v - cs.mfa_to_carts(mfa_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-15);
            r = covar_v - cs.contr_to_covar(contr_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r = contr_v - cs.covar_to_contr(covar_v, polar);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-15);
            //
            r = contr_v - cs.carts_to_contr(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-14);
            r = covar_v - cs.carts_to_covar(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r =   mfa_v - cs.carts_to_mfa(carts_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(  mfa_v *   mfa_v)) < 1e-14);
            r = carts_v - cs.contr_to_carts(contr_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = carts_v - cs.covar_to_carts(covar_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = carts_v - cs.mfa_to_carts(mfa_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(carts_v * carts_v)) < 1e-14);
            r = covar_v - cs.contr_to_covar(contr_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(covar_v * covar_v)) < 1e-14);
            r = contr_v - cs.covar_to_contr(covar_v, curvi);
            XCTAssert(std::sqrt(UTL::reduce_plus(r * r)/UTL::reduce_plus(contr_v * contr_v)) < 1e-14);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
