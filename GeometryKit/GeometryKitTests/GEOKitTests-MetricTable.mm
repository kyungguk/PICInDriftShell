//
//  GEOKitTests-MetricTable.mm
//  GeometryKit
//
//  Created by Kyungguk Min on 3/28/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOMetricTable.h>
#include <iostream>
#include <sstream>

@interface GEOKitTests_MetricTable : XCTestCase

@end

@implementation GEOKitTests_MetricTable

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- __1_
- (void)testMetricTable__1_1D {
    using GEO::__1_::MetricTable;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::CurviCoord;

    try {
        constexpr long ND = 1;
        constexpr double n = 1.1, r0ref = 110;
        CoordSystem::vector_type const D = {.3, 1.3, 1.0833333333333333};
        CoordSystem const cs{r0ref, D, n};
        MetricTable<ND>::size_vector_type const N = {173};
        MetricTable<ND>::position_type const q_min = -.5*MetricTable<ND>::position_type{N - 1L};
        MetricTable<ND>::position_type const q_max = q_min + MetricTable<ND>::position_type{N - 1L};

        // property test
        //
        MetricTable<ND> metric;
        XCTAssert(!metric);
        metric = MetricTable<ND>{cs, N, q_min};
        XCTAssert(metric);
        XCTAssert(UTL::reduce_bit_and(N == metric.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric.q_max()));
        MetricTable<ND> metric2{metric};
        XCTAssert(metric2);
        XCTAssert(UTL::reduce_bit_and(N == metric2.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric2.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric2.q_max()));
        metric2 = MetricTable<ND>{};
        XCTAssert(!metric2);
        metric2 = metric;
        XCTAssert(metric2);
        XCTAssert(UTL::reduce_bit_and(N == metric2.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric2.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric2.q_max()));

        // table test
        //
        CoordSystem::vector_type const v0{1, 2, 3};
        CoordSystem::vector_type v1, v2, v3;
        MetricTable<ND>::size_vector_type ijk;
        MetricTable<ND>::position_type pos;
        CurviCoord curvi;
        double s;
        bool stop;
        // at grid node
        //
        stop = false;
        for (ijk.x = 0; !stop && ijk.x < metric.N().x; ++ijk.x) {
            pos = metric.q_min() + MetricTable<ND>::position_type{ijk};
            curvi = CurviCoord{pos, nullptr};
            bool test;
            //
            v1 = cs.contr_to_covar(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.contr_to_covar(v0, pos) / s;
            v3 = metric.contr_to_covar(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"contr_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.covar_to_contr(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.covar_to_contr(v0, pos) / s;
            v3 = metric.covar_to_contr(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"covar_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.contr_to_carts(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.contr_to_carts(v0, curvi) / s;
            v3 = metric.contr_to_carts(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"contr_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.covar_to_carts(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.covar_to_carts(v0, curvi) / s;
            v3 = metric.covar_to_carts(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"covar_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.mfa_to_carts(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.mfa_to_carts(v0, curvi) / s;
            v3 = metric.mfa_to_carts(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"mfa_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.carts_to_contr(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.carts_to_contr(v0, curvi) / s;
            v3 = metric.carts_to_contr(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"carts_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.carts_to_covar(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.carts_to_covar(v0, curvi) / s;
            v3 = metric.carts_to_covar(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"carts_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.carts_to_mfa(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.carts_to_mfa(v0, curvi) / s;
            v3 = metric.carts_to_mfa(v0, ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"carts_to_mfa - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.BcartsOverB0(curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.BcartsOverB0(curvi) / s;
            v3 = metric.BcartsOverB0(ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"BcartsOverB0 - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.BcartsOverB0ref(curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.BcartsOverB0ref(curvi) / s;
            v3 = metric.BcartsOverB0ref(ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"BcartsOverB0ref - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
            //
            v1 = cs.position(curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.position(curvi) / s;
            v3 = metric.position(ijk) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                      @"position - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
            stop |= !test;
        }
        // at half grid node
        //
        stop = false;
        for (ijk.x = 0; !stop && ijk.x < metric.N().x; ++ijk.x) {
            pos = metric.q_min() + MetricTable<ND>::position_type{ijk};
            pos += .5;
            curvi = CurviCoord{pos, nullptr};
            bool test;
            //
            v1 = cs.contr_to_covar(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.contr_to_covar(v0, pos) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"contr_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.covar_to_contr(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.covar_to_contr(v0, pos) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"covar_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.contr_to_carts(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.contr_to_carts(v0, curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"contr_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.covar_to_carts(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.covar_to_carts(v0, curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"covar_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.mfa_to_carts(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.mfa_to_carts(v0, curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"mfa_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.carts_to_contr(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.carts_to_contr(v0, curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"carts_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.carts_to_covar(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.carts_to_covar(v0, curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"carts_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.carts_to_mfa(v0, curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.carts_to_mfa(v0, curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"carts_to_mfa - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.BcartsOverB0(curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.BcartsOverB0(curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"BcartsOverB0 - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.BcartsOverB0ref(curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.BcartsOverB0ref(curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"BcartsOverB0ref - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
            //
            v1 = cs.position(curvi);
            s = std::sqrt(UTL::reduce_plus(v1 * v1));
            v1 /= s;
            v2 = metric.position(curvi) / s;
            XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                      @"position - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                      ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
            stop |= !test;
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testMetricTable__1_2D {
    using GEO::__1_::MetricTable;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::CurviCoord;

    try {
        constexpr long ND = 2;
        constexpr double n = 1.1, r0ref = 1100;
        CoordSystem::vector_type const D = {.3, 1.3, 1.0833333333333333};
        CoordSystem const cs{r0ref, D, n};
        MetricTable<ND>::size_vector_type const N = {210, 173};
        MetricTable<ND>::position_type const q_min = -.5*MetricTable<ND>::position_type{N - 1L};
        MetricTable<ND>::position_type const q_max = q_min + MetricTable<ND>::position_type{N - 1L};

        // property test
        //
        MetricTable<ND> metric;
        XCTAssert(!metric);
        metric = MetricTable<ND>{cs, N, q_min};
        XCTAssert(metric);
        XCTAssert(UTL::reduce_bit_and(N == metric.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric.q_max()));
        MetricTable<ND> metric2{metric};
        XCTAssert(metric2);
        XCTAssert(UTL::reduce_bit_and(N == metric2.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric2.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric2.q_max()));
        metric2 = MetricTable<ND>{};
        XCTAssert(!metric2);
        metric2 = metric;
        XCTAssert(metric2);
        XCTAssert(UTL::reduce_bit_and(N == metric2.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric2.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric2.q_max()));

        // table test
        //
        CoordSystem::vector_type const v0{1, 2, 3};
        CoordSystem::vector_type v1, v2, v3;
        MetricTable<ND>::size_vector_type ijk;
        MetricTable<ND>::position_type pos;
        CurviCoord curvi;
        double s;
        bool stop;
        // at grid node
        //
        stop = false;
        for (ijk.x = 0; !stop && ijk.x < metric.N().x; ++ijk.x) {
            for (ijk.y = 0; !stop && ijk.y < metric.N().y; ++ijk.y) {
                pos = metric.q_min() + MetricTable<ND>::position_type{ijk};
                curvi = CurviCoord{pos, nullptr};
                bool test;
                //
                v1 = cs.contr_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_covar(v0, pos) / s;
                v3 = metric.contr_to_covar(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"contr_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_contr(v0, pos) / s;
                v3 = metric.covar_to_contr(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"covar_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.contr_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_carts(v0, curvi) / s;
                v3 = metric.contr_to_carts(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"contr_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_carts(v0, curvi) / s;
                v3 = metric.covar_to_carts(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"covar_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.mfa_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.mfa_to_carts(v0, curvi) / s;
                v3 = metric.mfa_to_carts(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"mfa_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_contr(v0, curvi) / s;
                v3 = metric.carts_to_contr(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"carts_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_covar(v0, curvi) / s;
                v3 = metric.carts_to_covar(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"carts_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_mfa(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_mfa(v0, curvi) / s;
                v3 = metric.carts_to_mfa(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"carts_to_mfa - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0(curvi) / s;
                v3 = metric.BcartsOverB0(ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                          @"BcartsOverB0 - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0ref(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0ref(curvi) / s;
                v3 = metric.BcartsOverB0ref(ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                          @"BcartsOverB0ref - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.position(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.position(curvi) / s;
                v3 = metric.position(ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                          @"position - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
            }
        }
        // at half grid node
        //
        stop = false;
        for (ijk.x = 0; !stop && ijk.x < metric.N().x; ++ijk.x) {
            for (ijk.y = 0; !stop && ijk.y < metric.N().y; ++ijk.y) {
                pos = metric.q_min() + MetricTable<ND>::position_type{ijk};
                pos += .5;
                curvi.most() = pos;
                curvi.q3() = 0;
                bool test;
                //
                v1 = cs.contr_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_covar(v0, pos) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"contr_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_contr(v0, pos) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"covar_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.contr_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_carts(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"contr_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_carts(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"covar_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.mfa_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.mfa_to_carts(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"mfa_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_contr(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"carts_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_covar(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"carts_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_mfa(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_mfa(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"carts_to_mfa - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0(curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"BcartsOverB0 - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0ref(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0ref(curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"BcartsOverB0ref - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.position(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.position(curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"position - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testMetricTable__1_3D {
    using GEO::__1_::MetricTable;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::CurviCoord;

    try {
        constexpr long ND = 3;
        constexpr double n = 1.1, r0ref = 700;
        CoordSystem::vector_type const D = {.3, 1.3, 1.0833333333333333};
        CoordSystem const cs{r0ref, D, n};
        MetricTable<ND>::size_vector_type const N = {36, 41, 35};
        MetricTable<ND>::position_type const q_min = -.5*MetricTable<ND>::position_type{N - 1L};
        MetricTable<ND>::position_type const q_max = q_min + MetricTable<ND>::position_type{N - 1L};

        // property test
        //
        MetricTable<ND> metric;
        XCTAssert(!metric);
        metric = MetricTable<ND>{cs, N, q_min};
        XCTAssert(metric);
        XCTAssert(UTL::reduce_bit_and(N == metric.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric.q_max()));
        MetricTable<ND> metric2{metric};
        XCTAssert(metric2);
        XCTAssert(UTL::reduce_bit_and(N == metric2.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric2.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric2.q_max()));
        metric2 = MetricTable<ND>{};
        XCTAssert(!metric2);
        metric2 = metric;
        XCTAssert(metric2);
        XCTAssert(UTL::reduce_bit_and(N == metric2.N()));
        XCTAssert(UTL::reduce_bit_and(q_min == metric2.q_min()));
        XCTAssert(UTL::reduce_bit_and(q_max == metric2.q_max()));

        // table test
        //
        CoordSystem::vector_type const v0{1, 2, 3};
        CoordSystem::vector_type v1, v2, v3;
        MetricTable<ND>::size_vector_type ijk;
        MetricTable<ND>::position_type pos;
        CurviCoord curvi;
        double s;
        bool stop;
        // at grid node
        //
        stop = false;
        for (ijk.x = 0; !stop && ijk.x < metric.N().x; ++ijk.x) {
            for (ijk.y = 0; !stop && ijk.y < metric.N().y; ++ijk.y) {
                for (ijk.z = 0; !stop && ijk.z < metric.N().z; ++ijk.z) {
                    pos = metric.q_min() + MetricTable<ND>::position_type{ijk};
                    curvi = CurviCoord{pos};
                    bool test;
                    //
                    v1 = cs.contr_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_covar(v0, pos) / s;
                    v3 = metric.contr_to_covar(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"contr_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_contr(v0, pos) / s;
                    v3 = metric.covar_to_contr(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"covar_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.contr_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_carts(v0, curvi) / s;
                    v3 = metric.contr_to_carts(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"contr_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_carts(v0, curvi) / s;
                    v3 = metric.covar_to_carts(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"covar_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.mfa_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.mfa_to_carts(v0, curvi) / s;
                    v3 = metric.mfa_to_carts(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"mfa_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_contr(v0, curvi) / s;
                    v3 = metric.carts_to_contr(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"carts_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_covar(v0, curvi) / s;
                    v3 = metric.carts_to_covar(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"carts_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_mfa(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_mfa(v0, curvi) / s;
                    v3 = metric.carts_to_mfa(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"carts_to_mfa - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0(curvi) / s;
                    v3 = metric.BcartsOverB0(ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"BcartsOverB0 - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0ref(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0ref(curvi) / s;
                    v3 = metric.BcartsOverB0ref(ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"BcartsOverB0ref - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.position(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.position(curvi) / s;
                    v3 = metric.position(ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"position - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                }
            }
        }
        // at half grid node
        //
        stop = false;
        for (ijk.x = 0; !stop && ijk.x < metric.N().x; ++ijk.x) {
            for (ijk.y = 0; !stop && ijk.y < metric.N().y; ++ijk.y) {
                for (ijk.z = 0; !stop && ijk.z < metric.N().z; ++ijk.z) {
                    pos = metric.q_min() + MetricTable<ND>::position_type{ijk};
                    pos += .5;
                    curvi = CurviCoord{pos};
                    bool test;
                    //
                    v1 = cs.contr_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_covar(v0, pos) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"contr_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_contr(v0, pos) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"covar_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.contr_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_carts(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"contr_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_carts(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"covar_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.mfa_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.mfa_to_carts(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"mfa_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_contr(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"carts_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_covar(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"carts_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_mfa(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_mfa(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"carts_to_mfa - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0(curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"BcartsOverB0 - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0ref(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0ref(curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"BcartsOverB0ref - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.position(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.position(curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"position - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                }
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
