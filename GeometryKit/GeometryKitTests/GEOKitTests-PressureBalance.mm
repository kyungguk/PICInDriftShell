//
//  GEOKitTests-PressureBalance.mm
//  GeometryKitTests
//
//  Created by KYUNGGUK MIN on 9/10/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOParticleLoader.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <random>

@interface GEOKitTests_PressureBalance : XCTestCase

@end

@implementation GEOKitTests_PressureBalance

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- __1_
- (void)testBiMaxwellianVDF__1_ {
    using GEO::__1_::PolarCoord;
    using GEO::__1_::VDFDescriptor;
    using GEO::__1_::BiMaxwellianVDF;

    std::ostringstream os;
    os.setf(os.scientific);
    os.precision(20);

    try {
        // VDF
        //
        constexpr double T1_0 = 1.5, A0 = 1.5;
        BiMaxwellianVDF const vdf{std::sqrt(2*T1_0), A0};
        println(os, "T10 = ", T1_0);
        println(os, "A0 = ", A0);

        // test
        //
        constexpr long nsamples = 1000;
        constexpr double r0ref = 5, ximax = .5;
        XCTAssert(std::abs(T1_0 - .5*UTL::pow<2>(vdf.vth1())) < 1e-15*T1_0);
        XCTAssert(vdf.A0() == A0);

        UTL::StaticArray<PolarCoord, nsamples> polar_list;
        UTL::StaticArray<VDFDescriptor::value_type, nsamples> P1_list, P2_list, JphixBOB0ref_list;

        std::mt19937 gen(100);
        std::uniform_real_distribution<> dist_xi(-ximax, ximax);
        for (long i = 0; i < nsamples; ++i) {
            PolarCoord polar;
            polar.xi() = dist_xi(gen);
            polar.r() = r0ref*((1 - polar.xi())*(1 + polar.xi()));
            polar.phi() = 0;
            polar_list.push_back(polar);
            //
            auto const &P = vdf.P(polar);
            P1_list.push_back(P.first);
            P2_list.push_back(P.second);
            //
            JphixBOB0ref_list.push_back(vdf.JphixBOB0ref(polar));
        }

        println(os, "polar = ", polar_list);
        println(os, "Plist = {", P1_list, ", ", P2_list, "}");
        println(os, "J\\[Phi]xBOB0ref = ", JphixBOB0ref_list);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (0) ) {
        NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
        contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
        contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

        NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
        NSError *error;
        XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"BiMaxwellianVDF.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

- (void)testPartialShellVDF__1_ {
    using GEO::__1_::PolarCoord;
    using GEO::__1_::VDFDescriptor;
    using GEO::__1_::PartialShellVDF;

    std::ostringstream os;
    os.setf(os.scientific);
    os.precision(20);

    try {
        // VDF
        //
        constexpr double vth = .4, zeta = 2, vs = 2;
        PartialShellVDF const vdf{vth, vs, zeta};
        println(os, "vth = ", vth);
        println(os, "vs = ", vs);
        println(os, "zeta = ", zeta);

        // test
        //
        constexpr long nsamples = 1000;
        constexpr double r0ref = 5, ximax = .3;
        XCTAssert(vth == vdf.vth());
        XCTAssert(std::abs(vs/vth - vdf.vsOvth()) < 1e-15*vdf.vsOvth());

        UTL::StaticArray<PolarCoord, nsamples> polar_list;
        UTL::StaticArray<VDFDescriptor::value_type, nsamples> P1_list, P2_list, JphixBOB0ref_list;

        std::mt19937 gen(100);
        std::uniform_real_distribution<> dist_xi(-ximax, ximax);
        for (long i = 0; i < nsamples; ++i) {
            PolarCoord polar;
            polar.xi() = dist_xi(gen);
            polar.r() = r0ref*((1 - polar.xi())*(1 + polar.xi()));
            polar.phi() = 0;
            polar_list.push_back(polar);
            //
            auto const &P = vdf.P(polar);
            P1_list.push_back(P.first);
            P2_list.push_back(P.second);
            //
            JphixBOB0ref_list.push_back(vdf.JphixBOB0ref(polar));
        }

        println(os, "polar = ", polar_list);
        println(os, "Plist = {", P1_list, ", ", P2_list, "}");
        println(os, "J\\[Phi]xBOB0ref = ", JphixBOB0ref_list);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    if ( (0) ) {
        NSString *contents = [NSString stringWithFormat:@"%s", os.str().c_str()];
        contents = [contents stringByReplacingOccurrencesOfString:@"e+" withString:@"*^"];
        contents = [contents stringByReplacingOccurrencesOfString:@"e-" withString:@"*^-"];

        NSURL *url = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] isDirectory:YES];
        NSError *error;
        XCTAssert([contents writeToURL:[url URLByAppendingPathComponent:@"PartialShellVDF.m"] atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
    }
}

@end
