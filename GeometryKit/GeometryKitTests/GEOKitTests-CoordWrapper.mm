//
//  GEOKitTests-CoordWrapper.mm
//  GeometryKitTests
//
//  Created by KYUNGGUK MIN on 9/3/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOCoordWrapper.h>
#include <iostream>
#include <sstream>

@interface GEOKitTests_CoordWrapper : XCTestCase

@end

@implementation GEOKitTests_CoordWrapper

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- GEO::__1_
- (void)testPolarCoord__1_ {
    using GEO::__1_::PolarCoord;
    using Vector = UTL::Vector<double, 3>;

    try {
        PolarCoord::value_type _0{0}, _1{}, _2{}, _3{};
        PolarCoord q{};
        XCTAssert(q.xi() == _0 && q.r() == _0 && q.phi() == _0);
        _1 = arc4random();
        q = PolarCoord{_1};
        XCTAssert(q.xi() == _1 && q.r() == _1 && q.phi() == _1);
        _1 = arc4random();
        _2 = arc4random();
        _3 = arc4random();
        q = PolarCoord{_3, _1, _2};
        XCTAssert(q.xi() == _1 && q.r() == _2 && q.phi() == _3);
        _1 = arc4random();
        _2 = arc4random();
        _3 = arc4random();
        q = PolarCoord{Vector{_3, _1, _2}};
        XCTAssert(q.xi() == _1 && q.r() == _2 && q.phi() == _3);
        _1 = arc4random();
        _2 = arc4random();
        _3 = arc4random();
        q.xi() = _1;
        q.r() = _2;
        q.phi() = _3;
        XCTAssert(q.xi() == _1 && q.r() == _2 && q.phi() == _3);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testCurviCoord__1_ {
    using GEO::__1_::CurviCoord;
    using Vector1 = UTL::Vector<double, 1>;
    using Vector2 = UTL::SIMDVector<double, 2>;
    using Vector3 = UTL::Vector<double, 3>;

    try {
        CurviCoord::value_type _0{0}, _1{}, _2{}, _3{};
        CurviCoord q{};
        XCTAssert(q.q1() == _0 && q.q2() == _0 && q.q3() == _0);
        _1 = arc4random();
        q = CurviCoord{_1};
        XCTAssert(q.q1() == _1 && q.q2() == _1 && q.q3() == _1);
        _1 = arc4random();
        _2 = arc4random();
        _3 = arc4random();
        q = CurviCoord{_1, _2, _3};
        XCTAssert(q.q1() == _1 && q.q2() == _2 && q.q3() == _3);
        _1 = arc4random();
        _2 = arc4random();
        _3 = arc4random();
        q = CurviCoord{Vector3{_1, _2, _3}};
        XCTAssert(q.q1() == _1 && q.q2() == _2 && q.q3() == _3);
        _1 = arc4random();
        _2 = arc4random();
        _3 = arc4random();
        q = CurviCoord{Vector3{_1, _2, _3}, nullptr};
        XCTAssert(q.q1() == _1 && q.q2() == _2 && q.q3() == _3);
        _1 = arc4random();
        _2 = arc4random();
        _3 = 0;
        q = CurviCoord{Vector2{_1, _2}, nullptr};
        XCTAssert(q.q1() == _1 && q.q2() == _2 && q.q3() == _3);
        _1 = arc4random();
        _2 = 0;
        _3 = 0;
        q = CurviCoord{Vector1{_1}, nullptr};
        XCTAssert(q.q1() == _1 && q.q2() == _2 && q.q3() == _3);
        _1 = arc4random();
        _2 = arc4random();
        _3 = arc4random();
        q.q1() = _1;
        q.q2() = _2;
        q.q3() = _3;
        XCTAssert(q.q1() == _1 && q.q2() == _2 && q.q3() == _3);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
