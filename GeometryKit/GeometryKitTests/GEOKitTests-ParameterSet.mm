//
//  GEOKitTests-ParameterSet.mm
//  GeometryKitTests
//
//  Created by KYUNGGUK MIN on 9/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <GeometryKit/GEOParameterSet.h>
#include <iostream>
#include <sstream>

@interface GEOKitTests_ParameterSet : XCTestCase

@end

@implementation GEOKitTests_ParameterSet

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// MARK:- __1_
- (void)testParameterSet__1_1D {
    using GEO::__1_::ParameterSet;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::CurviCoord;

    try {
        constexpr long ND = 1;
        constexpr double n = 1.1, r0ref = 1100, c = 1.3948, O0ref = 0.238237;
        CoordSystem::vector_type const D = {.3, 1.3, 1.0833333333333333};
        CoordSystem const cs{r0ref, D, n};
        ParameterSet<ND>::size_vector_type const N = {173};
        ParameterSet<ND>::position_type const grid_q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
        ParameterSet<ND>::position_type const grid_q_max = grid_q_min + ParameterSet<ND>::position_type{N - 1L};
        ParameterSet<ND>::OptionSet opts; {
            opts.number_of_worker_threads = 1;
            opts.Orot = 1.1;
        }
        ParameterSet<ND> params{cs, grid_q_min, N, c, O0ref, opts};
        XCTAssert(c == params.c() && O0ref == params.O0ref());
        XCTAssert(UTL::reduce_bit_and(N == params.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min == params.grid_q_min()));
        XCTAssert(UTL::reduce_bit_and(grid_q_max == params.grid_q_max()));
        XCTAssert(UTL::reduce_bit_and(params.ptl_q_min() == grid_q_min - .5));
        XCTAssert(UTL::reduce_bit_and(params.ptl_q_max() == grid_q_max + .5));
        XCTAssert(params.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!params.use_metric_table());
        XCTAssert(UTL::reduce_plus(params.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));

        // slicing
        //
        ParameterSet<ND>::size_vector_type location, length;
        location = {0L};
        length = {1L};
        ParameterSet<ND> slice = params.slice(location, length);
        XCTAssert(c == slice.c() && O0ref == slice.O0ref());
        XCTAssert(UTL::reduce_bit_and(length == slice.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min + ParameterSet<ND>::position_type{location} == slice.grid_q_min()));
        XCTAssert(slice.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!slice.use_metric_table());
        XCTAssert(UTL::reduce_plus(slice.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));
        location = {1L};
        length = {2L};
        slice = params.slice(location, length);
        XCTAssert(c == slice.c() && O0ref == slice.O0ref());
        XCTAssert(UTL::reduce_bit_and(length == slice.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min + ParameterSet<ND>::position_type{location} == slice.grid_q_min()));
        XCTAssert(slice.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!slice.use_metric_table());
        XCTAssert(UTL::reduce_plus(slice.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));

        // vector trans test
        //
        constexpr bool use_metric_table = true;
        params = params.slice({1L}, params.N() - 1L, use_metric_table);
        XCTAssert(params.use_metric_table());
        XCTAssert(params.metric_table<ParameterSet<ND>::cell_cent>() &&
                  UTL::reduce_bit_and(params.N() == params.metric_table<ParameterSet<ND>::cell_cent>().N()) &&
                  UTL::reduce_bit_and(params.grid_q_min() == params.metric_table<ParameterSet<ND>::cell_cent>().q_min()) &&
                  UTL::reduce_bit_and(params.grid_q_max() == params.metric_table<ParameterSet<ND>::cell_cent>().q_max()) &&
                  params.metric_table<ParameterSet<ND>::cell_edge>() &&
                  UTL::reduce_bit_and(params.N() == params.metric_table<ParameterSet<ND>::cell_edge>().N()) &&
                  UTL::reduce_bit_and(params.grid_q_min() + .5*params.grid_strategy() == params.metric_table<ParameterSet<ND>::cell_edge>().q_min()) &&
                  UTL::reduce_bit_and(params.grid_q_max() + .5*params.grid_strategy() == params.metric_table<ParameterSet<ND>::cell_edge>().q_max()));

        CoordSystem::vector_type const v0{1, 2, 3};
        CoordSystem::vector_type v1, v2, v3;
        ParameterSet<ND>::size_vector_type ijk;
        ParameterSet<ND>::position_type pos;
        CurviCoord curvi;
        double s;
        bool stop;

        // collocate
        //
        if (params.metric_table<ParameterSet<ND>::cell_cent>()) {
            auto const &metric = params.metric_table<ParameterSet<ND>::cell_cent>();
            //at grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                curvi = CurviCoord{pos, nullptr};
                bool test;
                //
                v1 = cs.contr_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_covar(v0, pos) / s;
                v3 = metric.contr_to_covar(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"contr_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_contr(v0, pos) / s;
                v3 = metric.covar_to_contr(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"covar_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.contr_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_carts(v0, curvi) / s;
                v3 = metric.contr_to_carts(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"contr_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_carts(v0, curvi) / s;
                v3 = metric.covar_to_carts(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"covar_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_contr(v0, curvi) / s;
                v3 = metric.carts_to_contr(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"carts_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_covar(v0, curvi) / s;
                v3 = metric.carts_to_covar(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"carts_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0(curvi) / s;
                v3 = metric.BcartsOverB0(ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"BcartsOverB0 - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0ref(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0ref(curvi) / s;
                v3 = metric.BcartsOverB0ref(ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"BcartsOverB0ref - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
            }
            // at half grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                pos += .5;
                curvi = CurviCoord{pos, nullptr};
                bool test;
                //
                v1 = cs.contr_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_covar(v0, pos) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"contr_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_contr(v0, pos) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"covar_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.contr_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_carts(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"contr_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_carts(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"covar_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_contr(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"carts_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_covar(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"carts_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0(curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"BcartsOverB0 - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0ref(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0ref(curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"BcartsOverB0ref - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
            }
        } else {
            XCTAssert(false, @"No metric table for cell center");
        }

        // staggered
        //
        if (params.metric_table<ParameterSet<ND>::cell_edge>()) {
            auto const &metric = params.metric_table<ParameterSet<ND>::cell_edge>();
            //at grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                curvi = CurviCoord{pos, nullptr};
                bool test;
                //
                v1 = cs.contr_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_covar(v0, pos) / s;
                v3 = metric.contr_to_covar(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"contr_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_contr(v0, pos) / s;
                v3 = metric.covar_to_contr(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"covar_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.contr_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_carts(v0, curvi) / s;
                v3 = metric.contr_to_carts(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"contr_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_carts(v0, curvi) / s;
                v3 = metric.covar_to_carts(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"covar_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_contr(v0, curvi) / s;
                v3 = metric.carts_to_contr(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"carts_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_covar(v0, curvi) / s;
                v3 = metric.carts_to_covar(v0, ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"carts_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0(curvi) / s;
                v3 = metric.BcartsOverB0(ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"BcartsOverB0 - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0ref(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0ref(curvi) / s;
                v3 = metric.BcartsOverB0ref(ijk) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                          @"BcartsOverB0ref - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                stop |= !test;
            }
            // at half grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                pos += .5;
                curvi = CurviCoord{pos, nullptr};
                bool test;
                //
                v1 = cs.contr_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_covar(v0, pos) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"contr_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_contr(v0, pos) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"covar_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.contr_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.contr_to_carts(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"contr_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.covar_to_carts(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.covar_to_carts(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"covar_to_carts - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_contr(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_contr(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"carts_to_contr - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.carts_to_covar(v0, curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.carts_to_covar(v0, curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"carts_to_covar - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0(curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"BcartsOverB0 - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
                //
                v1 = cs.BcartsOverB0ref(curvi);
                s = std::sqrt(UTL::reduce_plus(v1 * v1));
                v1 /= s;
                v2 = metric.BcartsOverB0ref(curvi) / s;
                XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                          @"BcartsOverB0ref - ijk = {%ld}, pos = {%f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                          ijk.x, pos.x, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                stop |= !test;
            }
        } else {
            XCTAssert(false, @"No metric table for cell edge");
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testParameterSet__1_2D {
    using GEO::__1_::ParameterSet;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::CurviCoord;

    try {
        constexpr long ND = 2;
        constexpr double n = 1.1, r0ref = 1100, c = 1.3948, O0ref = 0.238237;
        CoordSystem::vector_type const D = {.3, 1.3, 1.0833333333333333};
        CoordSystem const cs{r0ref, D, n};
        ParameterSet<ND>::size_vector_type const N = {173, 210};
        ParameterSet<ND>::position_type const grid_q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
        ParameterSet<ND>::position_type const grid_q_max = grid_q_min + ParameterSet<ND>::position_type{N - 1L};
        ParameterSet<ND>::OptionSet opts; {
            opts.number_of_worker_threads = 1;
            opts.Orot = 1.1;
        }
        ParameterSet<ND> params{cs, grid_q_min, N, c, O0ref, opts};
        XCTAssert(c == params.c() && O0ref == params.O0ref());
        XCTAssert(UTL::reduce_bit_and(N == params.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min == params.grid_q_min()));
        XCTAssert(UTL::reduce_bit_and(grid_q_max == params.grid_q_max()));
        XCTAssert(UTL::reduce_bit_and(params.ptl_q_min() == grid_q_min - .5));
        XCTAssert(UTL::reduce_bit_and(params.ptl_q_max() == grid_q_max + .5));
        XCTAssert(params.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!params.use_metric_table());
        XCTAssert(UTL::reduce_plus(params.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));

        // slicing
        //
        ParameterSet<ND>::size_vector_type location, length;
        location = {0L, 0L};
        length = {1L, 2L};
        ParameterSet<ND> slice = params.slice(location, length);
        XCTAssert(c == slice.c() && O0ref == slice.O0ref());
        XCTAssert(UTL::reduce_bit_and(length == slice.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min + ParameterSet<ND>::position_type{location} == slice.grid_q_min()));
        XCTAssert(slice.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!slice.use_metric_table());
        XCTAssert(UTL::reduce_plus(slice.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));
        location = {1L, 2L};
        length = {2L, 1L};
        slice = params.slice(location, length);
        XCTAssert(c == slice.c() && O0ref == slice.O0ref());
        XCTAssert(UTL::reduce_bit_and(length == slice.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min + ParameterSet<ND>::position_type{location} == slice.grid_q_min()));
        XCTAssert(slice.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!slice.use_metric_table());
        XCTAssert(UTL::reduce_plus(slice.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));

        // vector trans test
        //
        constexpr bool use_metric_table = true;
        params = params.slice({1L, 1L}, params.N() - 1L, use_metric_table);
        XCTAssert(params.use_metric_table());
        XCTAssert(params.metric_table<ParameterSet<ND>::cell_cent>() &&
                  UTL::reduce_bit_and(params.N() == params.metric_table<ParameterSet<ND>::cell_cent>().N()) &&
                  UTL::reduce_bit_and(params.grid_q_min() == params.metric_table<ParameterSet<ND>::cell_cent>().q_min()) &&
                  UTL::reduce_bit_and(params.grid_q_max() == params.metric_table<ParameterSet<ND>::cell_cent>().q_max()) &&
                  params.metric_table<ParameterSet<ND>::cell_edge>() &&
                  UTL::reduce_bit_and(params.N() == params.metric_table<ParameterSet<ND>::cell_edge>().N()) &&
                  UTL::reduce_bit_and(params.grid_q_min() + .5*params.grid_strategy() == params.metric_table<ParameterSet<ND>::cell_edge>().q_min()) &&
                  UTL::reduce_bit_and(params.grid_q_max() + .5*params.grid_strategy() == params.metric_table<ParameterSet<ND>::cell_edge>().q_max()));

        CoordSystem::vector_type const v0{1, 2, 3};
        CoordSystem::vector_type v1, v2, v3;
        ParameterSet<ND>::size_vector_type ijk;
        ParameterSet<ND>::position_type pos;
        CurviCoord curvi;
        double s;
        bool stop;

        // collocate
        //
        if (params.metric_table<ParameterSet<ND>::cell_cent>()) {
            auto const &metric = params.metric_table<ParameterSet<ND>::cell_cent>();
            //at grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                    curvi = CurviCoord{pos, nullptr};
                    bool test;
                    //
                    v1 = cs.contr_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_covar(v0, pos) / s;
                    v3 = metric.contr_to_covar(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"contr_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_contr(v0, pos) / s;
                    v3 = metric.covar_to_contr(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"covar_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.contr_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_carts(v0, curvi) / s;
                    v3 = metric.contr_to_carts(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"contr_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_carts(v0, curvi) / s;
                    v3 = metric.covar_to_carts(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"covar_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_contr(v0, curvi) / s;
                    v3 = metric.carts_to_contr(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"carts_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_covar(v0, curvi) / s;
                    v3 = metric.carts_to_covar(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"carts_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0(curvi) / s;
                    v3 = metric.BcartsOverB0(ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"BcartsOverB0 - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0ref(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0ref(curvi) / s;
                    v3 = metric.BcartsOverB0ref(ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"BcartsOverB0ref - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                }
            }
            // at half grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                    pos += .5;
                    curvi = CurviCoord{pos, nullptr};
                    bool test;
                    //
                    v1 = cs.contr_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_covar(v0, pos) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"contr_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_contr(v0, pos) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"covar_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.contr_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_carts(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"contr_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_carts(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"covar_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_contr(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"carts_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_covar(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"carts_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0(curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"BcartsOverB0 - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0ref(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0ref(curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"BcartsOverB0ref - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                }
            }
        } else {
            XCTAssert(false, @"No metric table for cell center");
        }

        // staggered
        //
        if (params.metric_table<ParameterSet<ND>::cell_edge>()) {
            auto const &metric = params.metric_table<ParameterSet<ND>::cell_edge>();
            //at grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                    curvi = CurviCoord{pos, nullptr};
                    bool test;
                    //
                    v1 = cs.contr_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_covar(v0, pos) / s;
                    v3 = metric.contr_to_covar(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"contr_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_contr(v0, pos) / s;
                    v3 = metric.covar_to_contr(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"covar_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.contr_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_carts(v0, curvi) / s;
                    v3 = metric.contr_to_carts(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"contr_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_carts(v0, curvi) / s;
                    v3 = metric.covar_to_carts(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"covar_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_contr(v0, curvi) / s;
                    v3 = metric.carts_to_contr(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"carts_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_covar(v0, curvi) / s;
                    v3 = metric.carts_to_covar(v0, ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                              @"carts_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0(curvi) / s;
                    v3 = metric.BcartsOverB0(ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"BcartsOverB0 - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0ref(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0ref(curvi) / s;
                    v3 = metric.BcartsOverB0ref(ijk) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                              @"BcartsOverB0ref - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                    stop |= !test;
                }
            }
            // at half grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                    pos += .5;
                    curvi = CurviCoord{pos, nullptr};
                    bool test;
                    //
                    v1 = cs.contr_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_covar(v0, pos) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"contr_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_contr(v0, pos) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"covar_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.contr_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.contr_to_carts(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"contr_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.covar_to_carts(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.covar_to_carts(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"covar_to_carts - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_contr(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_contr(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"carts_to_contr - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.carts_to_covar(v0, curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.carts_to_covar(v0, curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"carts_to_covar - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0(curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"BcartsOverB0 - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                    //
                    v1 = cs.BcartsOverB0ref(curvi);
                    s = std::sqrt(UTL::reduce_plus(v1 * v1));
                    v1 /= s;
                    v2 = metric.BcartsOverB0ref(curvi) / s;
                    XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                              @"BcartsOverB0ref - ijk = {%ld, %ld}, pos = {%f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                              ijk.x, ijk.y, pos.x, pos.y, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                    stop |= !test;
                }
            }
        } else {
            XCTAssert(false, @"No metric table for cell edge");
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testParameterSet__1_3D {
    using GEO::__1_::ParameterSet;
    using GEO::__1_::CoordSystem;
    using GEO::__1_::CurviCoord;

    try {
        constexpr long ND = 3;
        constexpr double n = 1.1, r0ref = 700, c = 1.3948, O0ref = 0.238237;
        CoordSystem::vector_type const D = {.3, 1.3, 1.0833333333333333};
        CoordSystem const cs{r0ref, D, n};
        ParameterSet<ND>::size_vector_type const N = {36, 41, 35};
        ParameterSet<ND>::position_type const grid_q_min = -.5*ParameterSet<ND>::position_type{N - 1L};
        ParameterSet<ND>::position_type const grid_q_max = grid_q_min + ParameterSet<ND>::position_type{N - 1L};
        ParameterSet<ND>::OptionSet opts; {
            opts.number_of_worker_threads = 1;
            opts.Orot = 1.1;
        }
        ParameterSet<ND> params{cs, grid_q_min, N, c, O0ref, opts};
        XCTAssert(c == params.c() && O0ref == params.O0ref());
        XCTAssert(UTL::reduce_bit_and(N == params.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min == params.grid_q_min()));
        XCTAssert(UTL::reduce_bit_and(grid_q_max == params.grid_q_max()));
        XCTAssert(UTL::reduce_bit_and(params.ptl_q_min() == grid_q_min - .5));
        XCTAssert(UTL::reduce_bit_and(params.ptl_q_max() == grid_q_max + .5));
        XCTAssert(params.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!params.use_metric_table());
        XCTAssert(UTL::reduce_plus(params.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));

        // slicing
        //
        ParameterSet<ND>::size_vector_type location, length;
        location = {0L, 0L, 0L};
        length = {1L, 2L, 3L};
        ParameterSet<ND> slice = params.slice(location, length);
        XCTAssert(c == slice.c() && O0ref == slice.O0ref());
        XCTAssert(UTL::reduce_bit_and(length == slice.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min + ParameterSet<ND>::position_type{location} == slice.grid_q_min()));
        XCTAssert(slice.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!slice.use_metric_table());
        XCTAssert(UTL::reduce_plus(slice.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));
        location = {1L, 2L, 4L};
        length = {2L, 1L, 3L};
        slice = params.slice(location, length);
        XCTAssert(c == slice.c() && O0ref == slice.O0ref());
        XCTAssert(UTL::reduce_bit_and(length == slice.N()));
        XCTAssert(UTL::reduce_bit_and(grid_q_min + ParameterSet<ND>::position_type{location} == slice.grid_q_min()));
        XCTAssert(slice.number_of_worker_threads() == opts.number_of_worker_threads);
        XCTAssert(!slice.use_metric_table());
        XCTAssert(UTL::reduce_plus(slice.Orot()*cs.mfa_basis<3>(CurviCoord{1, 0, 0})) <= 1e-14*UTL::pow<2>(opts.Orot));

        // vector trans test
        //
        constexpr bool use_metric_table = true;
        params = params.slice({1L, 1L, 1L}, params.N() - 2L, use_metric_table);
        XCTAssert(params.use_metric_table());
        XCTAssert(params.metric_table<ParameterSet<ND>::cell_cent>() &&
                  UTL::reduce_bit_and(params.N() == params.metric_table<ParameterSet<ND>::cell_cent>().N()) &&
                  UTL::reduce_bit_and(params.grid_q_min() == params.metric_table<ParameterSet<ND>::cell_cent>().q_min()) &&
                  UTL::reduce_bit_and(params.grid_q_max() == params.metric_table<ParameterSet<ND>::cell_cent>().q_max()) &&
                  params.metric_table<ParameterSet<ND>::cell_edge>() &&
                  UTL::reduce_bit_and(params.N() == params.metric_table<ParameterSet<ND>::cell_edge>().N()) &&
                  UTL::reduce_bit_and(params.grid_q_min() + .5*params.grid_strategy() == params.metric_table<ParameterSet<ND>::cell_edge>().q_min()) &&
                  UTL::reduce_bit_and(params.grid_q_max() + .5*params.grid_strategy() == params.metric_table<ParameterSet<ND>::cell_edge>().q_max()));

        CoordSystem::vector_type const v0{1, 2, 3};
        CoordSystem::vector_type v1, v2, v3;
        ParameterSet<ND>::size_vector_type ijk;
        ParameterSet<ND>::position_type pos;
        CurviCoord curvi;
        double s;
        bool stop;

        // collocate
        //
        if (params.metric_table<ParameterSet<ND>::cell_cent>()) {
            auto const &metric = params.metric_table<ParameterSet<ND>::cell_cent>();
            //at grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    for (ijk.z = 0; !stop && ijk.z < params.N().z; ++ijk.z) {
                        pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                        curvi = CurviCoord{pos};
                        bool test;
                        //
                        v1 = cs.contr_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_covar(v0, pos) / s;
                        v3 = metric.contr_to_covar(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"contr_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_contr(v0, pos) / s;
                        v3 = metric.covar_to_contr(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"covar_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.contr_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_carts(v0, curvi) / s;
                        v3 = metric.contr_to_carts(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"contr_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_carts(v0, curvi) / s;
                        v3 = metric.covar_to_carts(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"covar_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_contr(v0, curvi) / s;
                        v3 = metric.carts_to_contr(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"carts_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_covar(v0, curvi) / s;
                        v3 = metric.carts_to_covar(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"carts_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0(curvi) / s;
                        v3 = metric.BcartsOverB0(ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"BcartsOverB0 - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0ref(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0ref(curvi) / s;
                        v3 = metric.BcartsOverB0ref(ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"BcartsOverB0ref - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                    }
                }
            }
            // at half grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    for (ijk.z = 0; !stop && ijk.z < params.N().z; ++ijk.z) {
                        pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                        pos += .5;
                        curvi = CurviCoord{pos};
                        bool test;
                        //
                        v1 = cs.contr_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_covar(v0, pos) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"contr_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_contr(v0, pos) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"covar_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.contr_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_carts(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"contr_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_carts(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"covar_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_contr(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"carts_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_covar(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"carts_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0(curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"BcartsOverB0 - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0ref(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0ref(curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"BcartsOverB0ref - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                    }
                }
            }
        } else {
            XCTAssert(false, @"No metric table for cell center");
        }

        // staggered
        //
        if (params.metric_table<ParameterSet<ND>::cell_edge>()) {
            auto const &metric = params.metric_table<ParameterSet<ND>::cell_edge>();
            //at grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    for (ijk.z = 0; !stop && ijk.z < params.N().z; ++ijk.z) {
                        pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                        curvi = CurviCoord{pos};
                        bool test;
                        //
                        v1 = cs.contr_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_covar(v0, pos) / s;
                        v3 = metric.contr_to_covar(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"contr_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_contr(v0, pos) / s;
                        v3 = metric.covar_to_contr(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"covar_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.contr_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_carts(v0, curvi) / s;
                        v3 = metric.contr_to_carts(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"contr_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_carts(v0, curvi) / s;
                        v3 = metric.covar_to_carts(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"covar_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_contr(v0, curvi) / s;
                        v3 = metric.carts_to_contr(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 1e-15),
                                  @"carts_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_covar(v0, curvi) / s;
                        v3 = metric.carts_to_covar(v0, ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"carts_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0(curvi) / s;
                        v3 = metric.BcartsOverB0(ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"BcartsOverB0 - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0ref(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0ref(curvi) / s;
                        v3 = metric.BcartsOverB0ref(ijk) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 2e-15 && std::abs(UTL::reduce_plus(v1 * v3) - 1) < 2e-15),
                                  @"BcartsOverB0ref - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, v3 = {%f, %f, %f}, err12 = %e, err13 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, std::abs(UTL::reduce_plus(v1 * v2) - 1), std::abs(UTL::reduce_plus(v1 * v3) - 1));
                        stop |= !test;
                    }
                }
            }
            // at half grid node
            //
            stop = false;
            for (ijk.x = 0; !stop && ijk.x < params.N().x; ++ijk.x) {
                for (ijk.y = 0; !stop && ijk.y < params.N().y; ++ijk.y) {
                    for (ijk.z = 0; !stop && ijk.z < params.N().z; ++ijk.z) {
                        pos = metric.q_min() + ParameterSet<ND>::position_type{ijk};
                        pos += .5;
                        curvi = CurviCoord{pos};
                        bool test;
                        //
                        v1 = cs.contr_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_covar(v0, pos) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"contr_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_contr(v0, pos) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"covar_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.contr_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.contr_to_carts(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"contr_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.covar_to_carts(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.covar_to_carts(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"covar_to_carts - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_contr(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_contr(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"carts_to_contr - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.carts_to_covar(v0, curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.carts_to_covar(v0, curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"carts_to_covar - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0(curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"BcartsOverB0 - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                        //
                        v1 = cs.BcartsOverB0ref(curvi);
                        s = std::sqrt(UTL::reduce_plus(v1 * v1));
                        v1 /= s;
                        v2 = metric.BcartsOverB0ref(curvi) / s;
                        XCTAssert((test = std::abs(UTL::reduce_plus(v1 * v2) - 1) < 1e-5),
                                  @"BcartsOverB0ref - ijk = {%ld, %ld, %ld}, pos = {%f, %f, %f}, v1 = {%f, %f, %f}, v2 = {%f, %f, %f}, err12 = %e",
                                  ijk.x, ijk.y, ijk.z, pos.x, pos.y, pos.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, std::abs(UTL::reduce_plus(v1 * v2) - 1));
                        stop |= !test;
                    }
                }
            }
        } else {
            XCTAssert(false, @"No metric table for cell edge");
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
