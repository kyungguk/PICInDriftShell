# PICInDriftShell

Particle-in-cell code in dipole geometry

## Dependencies
- HDF5
- MPI
- [jsoncpp](https://github.com/open-source-parsers/jsoncpp) (automatically downloaded)

## Compilation

1. Clone the project

        git clone https://gitlab.com/kyungguk/PICInDriftShell.git

2. Initialize submodules (skip this if already done so)

        cd PICInDriftShell
        git submodule init

3. Update the submodules

        git submodule update

4. `cd` into the project directory and setup a build system

        cmake -S $PWD -B build -DCMAKE_BUILD_TYPE=Release -DENABLE_IPO=On -G "Ninja"

> You may need to make necessary changes in the `CMakeLists.txt` file that suite your environment.

5. Build

        cmake --build build

After building the project, the main executable binary will be located in `build/mpic-dipole_field/mpic-dipole_field`.


Since all simulation parameters are specified in a **JSON**-formatted text file and as command line arguments, no further recompilation of the code is needed.

## Sample Job Script

For the **PBS** job scheduler, the job script would look something like this

    #!/bin/sh

    #PBS -N DS-r6675
    #PBS -V
    #PBS -j oe
    #PBS -m abe
    #PBS -M your@mail.com
    #PBS -q normal
    #PBS -l select=8:ncpus=40:mpiprocs=40:ompthreads=1
    #PBS -l walltime=48:00:00

    module load [whatever modules needed & dependent libs]

    cd $PBS_O_WORKDIR

    mpiexec $HOME/executable_path/mpic-dipole_field -i ./0_input.drift_shell.PIC.json -O ./ 2>&1 | tee stdout.log

The `-i` option specifies the path to the **JSON** input text file and the `-O` option specifies the simulation output path.

The last part `... 2>&1 | tee stdout.log` is optional; it redirects the standard output and error to a file called `stdout.log` and is a convenient way of monitoring the progress in realtime.


## Research Papers

- Min, K., Liu, K., Denton, R. E., Nemec, F., Boardsen, S. A., & Miyoshi, Y. (2020). Two-dimensional hybrid particle-in-cell simulations of magnetosonic waves in the dipole magnetic field: On a constant L-shell. Journal of Geophysical Research: Space Physics, 125, e2020JA028414. https://doi.org/10.1029/2020JA028414.
- Min, K., Němec, F., Liu, K., Denton, R. E., & Boardsen, S. A. (2019). Equatorial propagation of the magnetosonic mode across the plasmapause: 2-D PIC simulations. Journal of Geophysical Research: Space Physics 124 (6), 4424-4444. https://doi.org/10.1029/2019JA026567.
- Min, K., Boardsen, S. A., Denton, R. E., & Liu, K. (2018). Equatorial evolution of the fast magnetosonic mode in the source region: Observation-simulation comparison of the preferential propagation direction. Journal of Geophysical Research: Space Physics, 123, 9532–9544. https://doi.org/10.1029/2018JA026037.
- Min, K., Liu, K., Denton, R. E., & Boardsen, S. A. (2018). Particle-in-cell simulations of the fast magnetosonic mode in a dipole magnetic field: 1-D along the radial direction. Journal of Geophysical Research: Space Physics, 123, 7424–7440. https://doi.org/10.1029/2018JA025666.
