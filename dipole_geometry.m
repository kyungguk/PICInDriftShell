(* ::Package:: *)

(*M.y''=b*)
Clear[cubicSplineSolve2ndD]
cubicSplineSolve2ndD[pts:{{_Real,_Real}..}]/;Length[pts]>=3:=Module[{n=Length[pts],b,xj,m},
b=Subtract[
(Divide@@@(Rest[#]-Most[#]))&@Rest[Reverse/@pts],
(Divide@@@(Rest[#]-Most[#]))&@Most[Reverse/@pts]
];
xj=First/@pts;
m=SparseArray[Table[{i,i-1},{i,2,n-2}]->Differences[xj[[2;;-2]]]/6,{1,1}n-2];
m=SparseArray[Table[{i,i},{i,1,n-2}]->(Drop[xj,2]-Drop[xj,-2])/3]+m+Transpose[m];
Flatten[{0.,LinearSolve[m,b],0.}]
]


Clear[cubicSplineCoefs]
cubicSplineCoefs[pts:{{_Real,_Real}..}]/;Length[pts]>=3:=With[{yppj=cubicSplineSolve2ndD[pts]},
Module[{xj,yj,coefs,dxj},
{xj,yj}=Transpose[pts];
coefs=MapThread[Function[{x0,x1,y0,y1,ypp0,ypp1},
{y0,-(y0-y1)-((x0-x1)^2 (2 ypp0+ypp1))/6,(x0-x1)^2 ypp0/2,-(((x0-x1)^2 (ypp0-ypp1))/6)}
],{Most[xj],Rest[xj],Most[yj],Rest[yj],Most[yppj],Rest[yppj]}];
dxj=Differences[xj];
If[And@@Thread[Equal[0,Chop[Differences[dxj]/Mean[dxj]]]],
(*regular spacing*)
coefs,
(*irregular spacing*)
(Print["cubicSplineCoefs::irregular spacing"];Abort[])
]
]
]


Clear[scaledq2,\[Xi]inversion]
scaledq2=Function[\[Xi],-\[Xi]/35(35(-1+\[Xi])(1+\[Xi])+\[Xi]^4(-21+5\[Xi]^2)),{Listable}];
\[Xi]inversion=With[{xmax=2000,\[Xi]max=1,scaledq2=scaledq2},
Module[{q2s,\[Xi],\[Xi]s},
q2s=scaledq2[\[Xi]max]//N;
q2s=Range[0,xmax]q2s/xmax;
\[Xi]s=FoldList[Replace[\[Xi],FindRoot[scaledq2[\[Xi]]==#2,{\[Xi],#}]]&,0.,q2s[[2;;-2]]]~Append~N[\[Xi]max];
With[{\[CapitalDelta]q2=q2s[[2]]-q2s[[1]],coefs=cubicSplineCoefs[Thread[{Union[Flatten[{-q2s,q2s}]],Union[Flatten[{-\[Xi]s,\[Xi]s}]]}]]},
Function[q2,Module[{x=q2/\[CapitalDelta]q2,i0,t},
If[-xmax<=x<xmax,
i0=Floor[x+xmax];
t=x-(i0-xmax);
Total[coefs[[i0+1]]{1,t,t^2,t^3}],
(*else: returns very large number for out-of-range argument*)
\[Xi]max*Sign[q2]+1*^30
]
],{Listable}]
]
]
];


Clear[curviToPolar,polarToCarts]
curviToPolar[n_?NumberQ/;Chop[n-3]!=0,r0ref_Real,{\[CapitalDelta]1_Real,\[CapitalDelta]2_Real,\[CapitalDelta]3_Real},{q1_Real,q2_Real,q3_Real}]:=With[{q3ref=N@Divide[r0ref,\[CapitalDelta]3(3-n)],\[Xi]inversion=\[Xi]inversion},
Module[{r0,\[Xi],r,\[Phi]},
r0=Power[(3-n)\[CapitalDelta]3/r0ref(q3+q3ref),1/(3-n)]r0ref;
\[Xi]=\[Xi]inversion[(r0ref/r0)^n \[CapitalDelta]2/r0ref q2];
r=r0(1-\[Xi])(1+\[Xi]);
\[Phi]=\[CapitalDelta]1/r0ref q1;
{\[Phi],\[Xi],r}
]
]
(*polarToCarts[{\[Phi]_Real,\[Xi]_Real,r_Real}]:={rSqrt[1-\[Xi]^2]Sin[\[Phi]],r \[Xi],rSqrt[1-\[Xi]^2]Cos[\[Phi]]}*)
polarToCarts=Compile[{{\[Phi]\[Xi]r,_Real,1}},
Module[{\[Xi],r,\[Phi],cos\[Lambda]},
{\[Phi],\[Xi],r}=\[Phi]\[Xi]r;
cos\[Lambda]=Sqrt[(1-\[Xi])(1+\[Xi])];
{r cos\[Lambda] Sin[\[Phi]],r \[Xi],r cos\[Lambda] Cos[\[Phi]]}
],
RuntimeAttributes->{Listable},
Parallelization->True
];


Clear[compileCurviToPolar]
compileCurviToPolar[n_?NumberQ/;Chop[n-3]!=0,r0ref_Real,{\[CapitalDelta]1_Real,\[CapitalDelta]2_Real,\[CapitalDelta]3_Real}]:=With[{\[Xi]inversion=\[Xi]inversion},
Compile[{{curvi,_Real,1}},
Module[{q1,q2,q3,q3ref,r0,\[Xi],r,\[Phi]},
{q1,q2,q3}=curvi;
q3ref=Divide[r0ref,\[CapitalDelta]3(3-n)];
r0=Power[(3-n)\[CapitalDelta]3/r0ref(q3+q3ref),1/(3-n)]r0ref;
\[Xi]=\[Xi]inversion[(r0ref/r0)^n \[CapitalDelta]2/r0ref q2];
r=r0(1-\[Xi])(1+\[Xi]);
\[Phi]=\[CapitalDelta]1/r0ref q1;
{\[Phi],\[Xi],r}
],
RuntimeAttributes->{Listable},
Parallelization->True
]
]


Clear[cartsToPolar,polarToCurvi]
polarToCurvi[n_?NumberQ/;Chop[n-3]!=0,r0ref_Real,{\[CapitalDelta]1_Real,\[CapitalDelta]2_Real,\[CapitalDelta]3_Real},{\[Phi]_Real,\[Xi]_Real,r_Real}]:=With[{q3ref=N@Divide[r0ref,\[CapitalDelta]3(3-n)],scaledq2=scaledq2},
Module[{r0,q1,q2,q3},
r0=Divide[r,(1-\[Xi])(1+\[Xi])];
q1=\[Phi] r0ref/\[CapitalDelta]1;
q2=r0ref/\[CapitalDelta]2 Power[r0/r0ref,n]scaledq2[\[Xi]];
q3=Subtract[r0ref/\[CapitalDelta]3 Power[r0/r0ref,3-n]/(3-n),q3ref];
{q1,q2,q3}
]
]
(*cartsToPolar[{z_Real,x_Real,y_Real}]:=With[{r=Sqrt[x^2+y^2+z^2]},{ArcTan[y,z],x/r,r}]*)
cartsToPolar=Compile[{{zxy,_Real,1}},
Module[{x,y,z,r},
{z,x,y}=zxy;
r=Sqrt[zxy.zxy];
(*{\[Phi],\[Xi],r}*)
{ArcTan[y,z],x/r,r}
],
RuntimeAttributes->{Listable},
Parallelization->True
];


Clear[compilePolarToCurvi]
compilePolarToCurvi[n_?NumberQ/;Chop[n-3]!=0,r0ref_Real,{\[CapitalDelta]1_Real,\[CapitalDelta]2_Real,\[CapitalDelta]3_Real}]:=With[{q3ref=N@Divide[r0ref,\[CapitalDelta]3(3-n)],scaledq2=scaledq2},
Compile[{{\[Phi]\[Xi]r,_Real,1}},
Module[{\[Xi],r,\[Phi],r0,q1,q2,q3},
{\[Phi],\[Xi],r}=\[Phi]\[Xi]r;
r0=Divide[r,(1-\[Xi])(1+\[Xi])];
q1=\[Phi] r0ref/\[CapitalDelta]1;
q3=Subtract[r0ref/\[CapitalDelta]3 Power[r0/r0ref,3-n]/(3-n),q3ref];
q2=r0ref/\[CapitalDelta]2 Power[r0/r0ref,n]scaledq2[\[Xi]];
{q1,q2,q3}
],
RuntimeAttributes->{Listable},
Parallelization->True
]
]


Clear[vectorTransform]
vectorTransform=Compile[{{v,_Real,1},{polar,_Real,1}},
Module[{\[Xi],r,\[Phi],e1={0.},e2={0.},e3={0.},cos\[Lambda]2,cos\[Lambda]},
{\[Phi],\[Xi],r}=polar;
e3={Cos[\[Phi]],0.,-Sin[\[Phi]]};
cos\[Lambda]2=(1-\[Xi])(1+\[Xi]);
cos\[Lambda]=Sqrt[cos\[Lambda]2];
e1=cos\[Lambda]{-\[Xi] Sin[\[Phi]],cos\[Lambda],-\[Xi] Cos[\[Phi]]}-2\[Xi]{cos\[Lambda] Sin[\[Phi]],\[Xi],cos\[Lambda] Cos[\[Phi]]};
e1/=Sqrt[1+3\[Xi]^2];
e2=Cross[e3,e1];
{v.e1,v.e2,v.e3}
],
RuntimeAttributes->{Listable},
Parallelization->True
];


Clear[tensorTransform]
tensorTransform=Compile[{{vv,_Real,1},{polar,_Real,1}},
Module[{\[Xi],r,\[Phi],e1={0.},e2={0.},e3={0.},cos\[Lambda]2,cos\[Lambda]},
{\[Phi],\[Xi],r}=polar;
e3={Cos[\[Phi]],0.,-Sin[\[Phi]]};
cos\[Lambda]2=(1-\[Xi])(1+\[Xi]);
cos\[Lambda]=Sqrt[cos\[Lambda]2];
e1=cos\[Lambda]{-\[Xi] Sin[\[Phi]],cos\[Lambda],-\[Xi] Cos[\[Phi]]}-2\[Xi]{cos\[Lambda] Sin[\[Phi]],\[Xi],cos\[Lambda] Cos[\[Phi]]};
e1/=Sqrt[1+3\[Xi]^2];
e2=Cross[e3,e1];
{vv.{e1[[1]]^2,e1[[2]]^2,e1[[3]]^2,2e1[[1]]e1[[2]],2e1[[2]]e1[[3]],2e1[[1]]e1[[3]]},
vv.{e2[[1]]^2,e2[[2]]^2,e2[[3]]^2,2e2[[1]]e2[[2]],2e2[[2]]e2[[3]],2e2[[1]]e2[[3]]},
vv.{e3[[1]]^2,e3[[2]]^2,e3[[3]]^2,2e3[[1]]e3[[2]],2e3[[2]]e3[[3]],2e3[[1]]e3[[3]]}}
],
RuntimeAttributes->{Listable},
Parallelization->True
];


Clear[compileCovarBases]
compileCovarBases[n_?NumberQ/;Chop[n-3]!=0,r0ref_Real,{\[CapitalDelta]1_Real,\[CapitalDelta]2_Real,\[CapitalDelta]3_Real}]:=With[{scaledq2=scaledq2},
Compile[{{polar,_Real,1}},
Module[{\[Xi],r,\[Phi],cos\[Lambda]2,cos\[Lambda],r0,e\[Xi]={0.},er={0.},e\[Phi]={0.},g1={0.},g2={0.},g3={0.}},
{\[Phi],\[Xi],r}=polar;
cos\[Lambda]2=(1-\[Xi])(1+\[Xi]);
cos\[Lambda]=Sqrt[cos\[Lambda]2];
r/=r0ref;
r0=r/cos\[Lambda]2;
e\[Phi]={Cos[\[Phi]],0.,-Sin[\[Phi]]};
e\[Xi]={-\[Xi] Sin[\[Phi]],cos\[Lambda],-\[Xi] Cos[\[Phi]]};
er={cos\[Lambda] Sin[\[Phi]],\[Xi],cos\[Lambda] Cos[\[Phi]]};
g1=\[CapitalDelta]1 r cos\[Lambda] e\[Phi];
g2=(\[CapitalDelta]2/cos\[Lambda])(Power[r0,3-n]/r^2)(e\[Xi]-(2\[Xi]/cos\[Lambda])er);
g3=(\[CapitalDelta]3/cos\[Lambda])(Power[r0,n]/r^2)(-n scaledq2[\[Xi]]e\[Xi]+(2n \[Xi]/cos\[Lambda] scaledq2[\[Xi]]+Power[cos\[Lambda]2,3.5])er);
{g1,g2,g3}
],
RuntimeAttributes->{Listable},
Parallelization->True
]
]


Clear[compileContrBases]
compileContrBases[n_?NumberQ/;Chop[n-3]!=0,r0ref_Real,{\[CapitalDelta]1_Real,\[CapitalDelta]2_Real,\[CapitalDelta]3_Real}]:=With[{scaledq2=scaledq2},
Compile[{{polar,_Real,1}},
Module[{\[Xi],r,\[Phi],cos\[Lambda]2,cos\[Lambda],r0,e\[Xi]={0.},er={0.},e\[Phi]={0.},h1={0.},h2={0.},h3={0.}},
{\[Phi],\[Xi],r}=polar;
cos\[Lambda]2=(1-\[Xi])(1+\[Xi]);
cos\[Lambda]=Sqrt[cos\[Lambda]2];
r/=r0ref;
r0=r/cos\[Lambda]2;
e\[Phi]={Cos[\[Phi]],0.,-Sin[\[Phi]]};
e\[Xi]={-\[Xi] Sin[\[Phi]],cos\[Lambda],-\[Xi] Cos[\[Phi]]};
er={cos\[Lambda] Sin[\[Phi]],\[Xi],cos\[Lambda] Cos[\[Phi]]};
h1=Divide[1,\[CapitalDelta]1 r cos\[Lambda]]e\[Phi];
h2=(1/\[CapitalDelta]2)(Power[r0,n]/r)((2n \[Xi]/cos\[Lambda] scaledq2[\[Xi]]+Power[cos\[Lambda]2,3.5])e\[Xi]+n scaledq2[\[Xi]]er);
h3=(1/\[CapitalDelta]3)(Power[r0,3-n]/r)((2\[Xi]/cos\[Lambda])e\[Xi]+er);
{h1,h2,h3}
],
RuntimeAttributes->{Listable},
Parallelization->True
]
]



