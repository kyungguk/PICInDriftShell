//
//  MPICDiagnostic__3D.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 4/3/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPICDiagnostic__3D.h"
#include "MPICDiagnostic__ND.hh"

constexpr long ND = 3;

// MARK:- MPIC::__1_::Diagnostic<ND>
//
MPIC::__1_::Diagnostic<ND>::Diagnostic(long const Nspecies)
: _Diagnostic{Nspecies} {
}

void MPIC::__1_::Diagnostic<ND>::update(MPK::Comm const &comm, PIC::Domain<ND> const &domain)
{
    auto const &params = domain.params();
    static_assert(ND == 3, "ND != 3");
    auto const N = params.N().take<2>(); // effectively 2D
    double const divisor = 2*comm.size()*UTL::reduce_prod(N); // 2*total_number_of_cells

    // average field energy
    //
    double const B_offset = .5*long{params.grid_strategy()};
    double const E_offset = 0;
    //
    dB2Over2 = reduce_plus(comm, _accumulate(params.coord_system(), params.grid_q_min() + B_offset, domain.bfield()->carts))/divisor;
    dE2Over2 = reduce_plus(comm, _accumulate(params.coord_system(), params.grid_q_min() + E_offset, domain.efield()->carts))/divisor;

    // average particle energy
    //
    mv2Over2.fill(0);
    long idx = 0;
    for (auto const &sp : domain.species()) {
        double const scale_factor = sp.energy_density_conversion_factor();
        msU2Over2.at(idx) = reduce_plus(comm, _accumulate(params.coord_system(), params.grid_q_min(), sp.moment<0>(), sp.moment<1>()))*(scale_factor/divisor);
        msv2Over2.at(idx) = reduce_plus(comm, _accumulate(params.coord_system(), params.grid_q_min(), sp.moment<2>()))*(scale_factor/divisor);
        mv2Over2 += msv2Over2.at(idx); // total kinetic energy
        ++idx;
    }
    for (auto const &cf : domain.cold_fluids()) {
        double const scale_factor = cf.energy_density_conversion_factor();
        msU2Over2.at(idx) = reduce_plus(comm, _accumulate(params.coord_system(), params.grid_q_min(), cf.moment<0>(), cf.moment<1>()))*(scale_factor/divisor);
        msv2Over2.at(idx) = reduce_plus(comm, _accumulate(params.coord_system(), params.grid_q_min(), cf.moment<2>()))*(scale_factor/divisor);
        mv2Over2 += msv2Over2.at(idx); // total kinetic energy
        ++idx;
    }
}
template <long Pad1>
auto MPIC::__1_::Diagnostic<ND>::_accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<vector_type, ND - 1, Pad1> const &A)
-> vector_type {
    auto const N = A.dims();
    vector_type V2{0};
    PIC::CurviCoord curvi{0}; // at the reference meridian
    for (long i = 0; i < N.x; ++i) {
        curvi.x = i + q_min.x;
        for (long j = 0; j < N.y; ++j) {
            curvi.y = j + q_min.y;
            //
            auto e = cs.mfa_basis<0>(curvi); // field-aligned unit vector
            vector_type const &carts = A[i][j];
            V2.x += UTL::pow<2>(UTL::reduce_plus(e.x *= carts)); // (e1•carts)^2
            V2.y += UTL::pow<2>(UTL::reduce_plus(e.y *= carts)); // (e2•carts)^2
            V2.z += UTL::pow<2>(UTL::reduce_plus(e.z *= carts)); // (e3•carts)^2
        }
    }
    return V2;
}
template <long Pad0, long Pad1>
auto MPIC::__1_::Diagnostic<ND>::_accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<scalar_type, ND - 1, Pad0> const &n_arr, UTL::ArrayND<vector_type, ND - 1, Pad1> const &nV_arr)
-> vector_type {
    auto const N = nV_arr.dims();
    vector_type V2{0};
    PIC::CurviCoord curvi{0}; // at the reference meridian
    for (long i = 0; i < N.x; ++i) {
        curvi.x = i + q_min.x;
        for (long j = 0; j < N.y; ++j) {
            curvi.y = j + q_min.y;
            //
            auto e = cs.mfa_basis<0>(curvi); // field-aligned unit vector
            scalar_type const &n = n_arr[i][j];
            vector_type const &nVcarts = nV_arr[i][j];
            V2.x += UTL::pow<2>(UTL::reduce_plus(e.x *= nVcarts))/n.x; // (e1•carts)^2
            V2.y += UTL::pow<2>(UTL::reduce_plus(e.y *= nVcarts))/n.x; // (e2•carts)^2
            V2.z += UTL::pow<2>(UTL::reduce_plus(e.z *= nVcarts))/n.x; // (e3•carts)^2
        }
    }
    return V2;
}
template <long Pad2>
auto MPIC::__1_::Diagnostic<ND>::_accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<compact_tensor_type, ND - 1, Pad2> const &nvv_arr)
-> vector_type {
    auto const N = nvv_arr.dims();
    vector_type V2{0};
    PIC::CurviCoord curvi{0}; // at the reference meridian
    for (long i = 0; i < N.x; ++i) {
        curvi.x = i + q_min.x;
        for (long j = 0; j < N.y; ++j) {
            curvi.y = j + q_min.y;
            //
            auto const e = cs.mfa_basis<0>(curvi); // field-aligned unit vector
            compact_tensor_type const &vv = nvv_arr[i][j];
            V2.x += vv.xx()*e.x.x*e.x.x + vv.yy()*e.x.y*e.x.y + vv.zz()*e.x.z*e.x.z + 2*(vv.xy()*e.x.x*e.x.y + vv.zx()*e.x.x*e.x.z + vv.yz()*e.x.y*e.x.z);
            V2.y += vv.xx()*e.y.x*e.y.x + vv.yy()*e.y.y*e.y.y + vv.zz()*e.y.z*e.y.z + 2*(vv.xy()*e.y.x*e.y.y + vv.zx()*e.y.x*e.y.z + vv.yz()*e.y.y*e.y.z);
            V2.z += vv.xx()*e.z.x*e.z.x + vv.yy()*e.z.y*e.z.y + vv.zz()*e.z.z*e.z.z + 2*(vv.xy()*e.z.x*e.z.y + vv.zx()*e.z.x*e.z.z + vv.yz()*e.z.y*e.z.z);
        }
    }
    return V2;
}
