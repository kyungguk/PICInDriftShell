//
//  MPICTimeseriesRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICTimeseriesRecorder_h
#define MPICTimeseriesRecorder_h

#include "MPICRecorder.h"
#include <json/json.h>
#include <string>
#include <map>

MPIC_BEGIN_NAMESPACE
class TimeseriesRecorder : public Recorder {
public:
    explicit TimeseriesRecorder(char const *output_directory, Json::Value const json);
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate) override;
    void operator<<(__1_::PICDelegate<2L> const &delegate) override;
    void operator<<(__1_::PICDelegate<3L> const &delegate) override;

private:
    std::map<int, std::string> _paths; // a map of pid and filename
    bool _should_record_moments;

    template <class Delegate>
    void record(Delegate const &delegate, Identifier::Any) const;

    template <class T, long S>
    static UTL::Vector<T, S> const *begin(UTL::Vector<T, S> const &v) { return &v; }
    template <class T, long Rank, long Pad>
    static T const *begin(UTL::ArrayND<T, Rank, Pad> const &a) { return begin(*a.begin()); }
};
MPIC_END_NAMESPACE

#endif /* MPICTimeseriesRecorder_h */
