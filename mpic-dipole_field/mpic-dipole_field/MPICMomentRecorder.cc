//
//  MPICMomentRecorder.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICMomentRecorder.h"

#include "MPICDelegate.h"
#include <stdexcept>
#include <string>

MPIC::MomentRecorder::MomentRecorder(char const *output_directory, Json::Value const json)
: Recorder{json.get("recording_frequency", unsigned{1}).asUInt()}
, _output_directory{output_directory}
, _filename_prefix{json.get("filename_prefix", "moment").asString()} {
}
//
void MPIC::MomentRecorder::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::MomentRecorder::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::MomentRecorder::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    record(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::MomentRecorder::record(Delegate const &delegate, Identifier::Any) const
{
    try {
        if (delegate.comm().is_master()) { // in master process
            //
            // create hdf5 file
            //
            H5::File root = [](std::string const path) {
                return H5::File{H5::File::Trunc{}, path.c_str()};
            }(_output_directory + "/" + _filename_prefix + "_" + std::to_string(delegate.cur_step()) + ".h5");
            //
            // record particle moments
            //
            record_particles(root, delegate, Delegate::identifier);
            //
            // record cold_fluid moments
            //
            record_cold_fluids(root, delegate, Delegate::identifier);
            //
            // flush
            //
            root.flush();
        } else { // in non-master processes
            //
            // record particle moments
            //
            record_particles(delegate, Delegate::identifier);
            //
            // record cold_fluid moments
            //
            record_cold_fluids(delegate, Delegate::identifier);
        }
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

template <class Delegate>
void MPIC::MomentRecorder::record_particles(H5::File &file, Delegate const &delegate, Identifier::Any) const
{
    using species_type = typename Delegate::species_type;
    using scalar_moment_type = typename species_type::scalar_moment_type;
    using vector_moment_type = typename species_type::vector_moment_type;
    using tensor_moment_type = typename species_type::tensor_moment_type;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    long const Ns = domain.species().size();
    //
    // check if container is empty
    //
    if (Ns <= 0) {
        return;
    }
    //
    // create group and populate attributes
    //
    H5::Group root = file.group("particles", H5::PList{}, H5::PList{});
    write_attr(root, "Nspecies", Ns);
    write_common_attr(root, delegate, delegate.identifier);
    write_species_attr(root, domain, delegate.identifier);
    //
    // create file dataspace
    //
    H5::Space mom0_fspace, mom1_fspace, mom2_fspace; {
        auto const N = delegate.simulation_size().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type dims{N.begin(), N.end()};
        dims.insert(dims.begin(), static_cast<unsigned long>(Ns)); // first dimension is species
        mom0_fspace = H5::Space{dims};
        dims.push_back(vector_moment_type::value_type::size());
        mom1_fspace = H5::Space{dims};
        dims.back() = tensor_moment_type::value_type::size();
        mom2_fspace = H5::Space{dims};
    }
    //
    // create datasets and populate attributes
    //
    H5::Dataset n, V, vv; {
        n = root.dataset("n", H5::Type::native<double>(), mom0_fspace);
        write_common_attr(n, delegate, delegate.identifier);
        write_species_attr(n, domain, delegate.identifier);
        V = root.dataset("nV", H5::Type::native<double>(), mom1_fspace);
        write_common_attr(V, delegate, delegate.identifier);
        write_species_attr(V, domain, delegate.identifier);
        vv = root.dataset("nvv", H5::Type::native<double>(), mom2_fspace);
        write_common_attr(vv, delegate, delegate.identifier);
        write_species_attr(vv, domain, delegate.identifier);
    }
    //
    // memory dataspace
    //
    H5::Space::size_array_type mom0_count, mom1_count, mom2_count;
    H5::Space mom0_mspace, mom1_mspace, mom2_mspace; {
        auto const N = params.N().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type mom0_dims{N.begin(), N.end()}, mom1_dims = mom0_dims, mom2_dims = mom0_dims;
        for (long i = 0; i < mom0_dims.size(); ++i) {
            mom0_count.push_back(mom0_dims[i]);
            mom0_dims[i] += 2*scalar_moment_type::pad_size();
            mom1_dims[i] += 2*vector_moment_type::pad_size();
            mom2_dims[i] += 2*tensor_moment_type::pad_size();
        }
        mom2_count = mom1_count = mom0_count;
        mom1_dims.push_back(vector_moment_type::value_type::size());
        mom1_count.push_back(mom1_dims.back());
        mom2_dims.push_back(tensor_moment_type::value_type::size());
        mom2_count.push_back(mom2_dims.back());
        //
        H5::Space::size_array_type mom0_start(N.size(), scalar_moment_type::pad_size());
        mom0_start.push_back(0); // this will be ignored
        (mom0_mspace = H5::Space{mom0_dims}).select(H5S_SELECT_SET, mom0_start, mom0_count);
        H5::Space::size_array_type mom1_start(N.size(), vector_moment_type::pad_size());
        mom1_start.push_back(0);
        (mom1_mspace = H5::Space{mom1_dims}).select(H5S_SELECT_SET, mom1_start, mom1_count);
        H5::Space::size_array_type mom2_start(N.size(), tensor_moment_type::pad_size());
        mom2_start.push_back(0);
        (mom2_mspace = H5::Space{mom2_dims}).select(H5S_SELECT_SET, mom2_start, mom2_count);
    }
    mom0_count.insert(mom0_count.begin(), 1); // first dimension is species
    mom1_count.insert(mom1_count.begin(), 1);
    mom2_count.insert(mom2_count.begin(), 1);
    //
    // record master domain moments
    //
    auto offset = delegate.domain_offset();
    scalar_moment_type mom0{params.N().template take<scalar_moment_type::rank()>()};
    vector_moment_type mom1{params.N().template take<scalar_moment_type::rank()>()};
    tensor_moment_type mom2{params.N().template take<scalar_moment_type::rank()>()};
    for (long i = 0; i < Ns; ++i) {
        species_type const &sp = domain.species().at(i);
        H5::Space::size_array_type start{offset.begin(), offset.end()};
        start.insert(start.begin(), static_cast<unsigned long>(i)); // first dimension is species
        start.push_back(0);
        // moment<0>
        mom0_fspace.select(H5S_SELECT_SET, start, mom0_count);
        n.write(mom0_fspace, data(mom0 = sp.template moment<0>()), mom0_mspace);
        // moment<1>
        mom1_fspace.select(H5S_SELECT_SET, start, mom1_count);
        V.write(mom1_fspace, data(mom1 = sp.template moment<1>()), mom1_mspace);
        // moment<2>
        mom2_fspace.select(H5S_SELECT_SET, start, mom2_count);
        vv.write(mom2_fspace, data(mom2 = sp.template moment<2>()), mom2_mspace);
    }
    //
    // record slave domain moments
    //
    UTL::Vector<MPK::Tag, 4> const tags{tag + 0, tag + 1, tag + 2, tag + 3};
    for (int rank = delegate.comm()->size() - 1; rank >= 0; --rank) {
        if (delegate.comm().master() == rank) continue; // skip master
        //
        // first receive is offset
        //
        delegate.comm()->recv(offset, {MPK::Rank{rank}, std::get<0>(tags)});
        H5::Space::size_array_type start{offset.begin(), offset.end()};
        start.insert(start.begin(), 0); // first dimension is species
        start.push_back(0);
        //
        // enumerate species
        //
        for (long i = 0; i < Ns; ++i) {
            start[0] = static_cast<unsigned long>(i);
            //
            // second receive is moment<0>
            //
            delegate.comm()->recv(mom0, {MPK::Rank{rank}, std::get<1>(tags)});
            mom0_fspace.select(H5S_SELECT_SET, start, mom0_count);
            n.write(mom0_fspace, data(mom0), mom0_mspace);
            //
            // third receive is moment<1>
            //
            delegate.comm()->recv(mom1, {MPK::Rank{rank}, std::get<2>(tags)});
            mom1_fspace.select(H5S_SELECT_SET, start, mom1_count);
            V.write(mom1_fspace, data(mom1), mom1_mspace);
            //
            // fourth receive is moment<2>
            //
            delegate.comm()->recv(mom2, {MPK::Rank{rank}, std::get<3>(tags)});
            mom2_fspace.select(H5S_SELECT_SET, start, mom2_count);
            vv.write(mom2_fspace, data(mom2), mom2_mspace);
        }
    }
}
template <class Delegate>
void MPIC::MomentRecorder::record_particles(Delegate const &delegate, Identifier::Any) const
{
    using species_type = typename Delegate::species_type;
    using scalar_moment_type = typename species_type::scalar_moment_type;
    using vector_moment_type = typename species_type::vector_moment_type;
    using tensor_moment_type = typename species_type::tensor_moment_type;
    auto const &domain = delegate.domain();
    long const Ns = domain.species().size();
    UTL::Vector<MPK::Tag, 4> const tags{tag + 0, tag + 1, tag + 2, tag + 3};
    //
    // check if container is empty
    //
    if (Ns <= 0) {
        return;
    }
    //
    // first send is offset
    //
    auto offset = delegate.domain_offset();
    delegate.comm()->issend(offset, {delegate.comm().master(), std::get<0>(tags)}).wait();
    //
    // enumerate species
    //
    for (long i = 0; i < Ns; ++i) {
        species_type const &sp = domain.species().at(i);
        //
        // second send is moment<0>
        //
        scalar_moment_type const &mom0 = sp.template moment<0>();
        MPK::Request req0 = delegate.comm()->issend(mom0, {delegate.comm().master(), std::get<1>(tags)});
        //
        // third send is moment<1>
        //
        vector_moment_type const &mom1 = sp.template moment<1>();
        MPK::Request req1 = delegate.comm()->issend(mom1, {delegate.comm().master(), std::get<2>(tags)});
        //
        // fourth send is moment<2>
        //
        tensor_moment_type const &mom2 = sp.template moment<2>();
        MPK::Request req2 = delegate.comm()->issend(mom2, {delegate.comm().master(), std::get<3>(tags)});
        //
        // block until sent
        //
        req0.wait();
        req1.wait();
        req2.wait();
    }
}

template <class Delegate>
void MPIC::MomentRecorder::record_cold_fluids(H5::File &file, Delegate const &delegate, Identifier::Any) const
{
    using cold_fluid_type = typename Delegate::cold_fluid_type;
    using scalar_moment_type = typename cold_fluid_type::scalar_moment_type;
    using vector_moment_type = typename cold_fluid_type::vector_moment_type;
    using tensor_moment_type = typename cold_fluid_type::tensor_moment_type;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    long const Ns = domain.cold_fluids().size();
    //
    // check if container is empty
    //
    if (Ns <= 0) {
        return;
    }
    //
    // create group and populate attributes
    //
    H5::Group root = file.group("cold_fluids", H5::PList{}, H5::PList{});
    write_attr(root, "Nspecies", Ns);
    write_common_attr(root, delegate, delegate.identifier);
    write_cold_fluid_attr(root, domain, delegate.identifier);
    //
    // create file dataspace
    //
    H5::Space mom0_fspace, mom1_fspace, mom2_fspace; {
        auto const N = delegate.simulation_size().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type dims{N.begin(), N.end()};
        dims.insert(dims.begin(), static_cast<unsigned long>(Ns)); // first dimension is cold fluids
        mom0_fspace = H5::Space{dims};
        dims.push_back(vector_moment_type::value_type::size());
        mom1_fspace = H5::Space{dims};
        dims.back() = tensor_moment_type::value_type::size();
        mom2_fspace = H5::Space{dims};
    }
    //
    // create datasets and populate attributes
    //
    H5::Dataset n, V, vv; {
        n = root.dataset("n", H5::Type::native<double>(), mom0_fspace);
        write_common_attr(n, delegate, delegate.identifier);
        write_cold_fluid_attr(n, domain, delegate.identifier);
        V = root.dataset("nV", H5::Type::native<double>(), mom1_fspace);
        write_common_attr(V, delegate, delegate.identifier);
        write_cold_fluid_attr(V, domain, delegate.identifier);
        vv = root.dataset("nvv", H5::Type::native<double>(), mom2_fspace);
        write_common_attr(vv, delegate, delegate.identifier);
        write_cold_fluid_attr(vv, domain, delegate.identifier);
    }
    //
    // memory dataspace
    //
    H5::Space::size_array_type mom0_count, mom1_count, mom2_count;
    H5::Space mom0_mspace, mom1_mspace, mom2_mspace; {
        auto const N = params.N().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type mom0_dims{N.begin(), N.end()}, mom1_dims = mom0_dims, mom2_dims = mom0_dims;
        for (long i = 0; i < mom0_dims.size(); ++i) {
            mom0_count.push_back(mom0_dims[i]);
            mom0_dims[i] += 2*scalar_moment_type::pad_size();
            mom1_dims[i] += 2*vector_moment_type::pad_size();
            mom2_dims[i] += 2*tensor_moment_type::pad_size();
        }
        mom2_count = mom1_count = mom0_count;
        mom1_dims.push_back(vector_moment_type::value_type::size());
        mom1_count.push_back(mom1_dims.back());
        mom2_dims.push_back(tensor_moment_type::value_type::size());
        mom2_count.push_back(mom2_dims.back());
        //
        H5::Space::size_array_type mom0_start(N.size(), scalar_moment_type::pad_size());
        mom0_start.push_back(0); // this will be ignored
        (mom0_mspace = H5::Space{mom0_dims}).select(H5S_SELECT_SET, mom0_start, mom0_count);
        H5::Space::size_array_type mom1_start(N.size(), vector_moment_type::pad_size());
        mom1_start.push_back(0);
        (mom1_mspace = H5::Space{mom1_dims}).select(H5S_SELECT_SET, mom1_start, mom1_count);
        H5::Space::size_array_type mom2_start(N.size(), tensor_moment_type::pad_size());
        mom2_start.push_back(0);
        (mom2_mspace = H5::Space{mom2_dims}).select(H5S_SELECT_SET, mom2_start, mom2_count);
    }
    mom0_count.insert(mom0_count.begin(), 1); // first dimension is cold fluids
    mom1_count.insert(mom1_count.begin(), 1);
    mom2_count.insert(mom2_count.begin(), 1);
    //
    // record master domain moments
    //
    auto offset = delegate.domain_offset();
    scalar_moment_type mom0{params.N().template take<scalar_moment_type::rank()>()};
    vector_moment_type mom1{params.N().template take<scalar_moment_type::rank()>()};
    tensor_moment_type mom2{params.N().template take<scalar_moment_type::rank()>()};
    for (long i = 0; i < Ns; ++i) {
        cold_fluid_type const &cf = domain.cold_fluids().at(i);
        H5::Space::size_array_type start{offset.begin(), offset.end()};
        start.insert(start.begin(), static_cast<unsigned long>(i)); // first dimension is cold fluids
        start.push_back(0);
        // moment<0>
        mom0_fspace.select(H5S_SELECT_SET, start, mom0_count);
        n.write(mom0_fspace, data(mom0 = cf.template moment<0>()), mom0_mspace);
        // moment<1>
        mom1_fspace.select(H5S_SELECT_SET, start, mom1_count);
        V.write(mom1_fspace, data(mom1 = cf.template moment<1>()), mom1_mspace);
        // moment<2>
        mom2_fspace.select(H5S_SELECT_SET, start, mom2_count);
        vv.write(mom2_fspace, data(mom2 = cf.template moment<2>()), mom2_mspace);
    }
    //
    // record slave domain moments
    //
    UTL::Vector<MPK::Tag, 4> const tags{tag + 0, tag + 1, tag + 2, tag + 3};
    for (int rank = delegate.comm()->size() - 1; rank >= 0; --rank) {
        if (delegate.comm().master() == rank) continue; // skip master
        //
        // first receive is offset
        //
        delegate.comm()->recv(offset, {MPK::Rank{rank}, std::get<0>(tags)});
        H5::Space::size_array_type start{offset.begin(), offset.end()};
        start.insert(start.begin(), 0); // first dimension is cold fluids
        start.push_back(0);
        //
        // enumerate cold fluids
        //
        for (long i = 0; i < Ns; ++i) {
            start[0] = static_cast<unsigned long>(i);
            //
            // second receive is moment<0>
            //
            delegate.comm()->recv(mom0, {MPK::Rank{rank}, std::get<1>(tags)});
            mom0_fspace.select(H5S_SELECT_SET, start, mom0_count);
            n.write(mom0_fspace, data(mom0), mom0_mspace);
            //
            // third receive is moment<1>
            //
            delegate.comm()->recv(mom1, {MPK::Rank{rank}, std::get<2>(tags)});
            mom1_fspace.select(H5S_SELECT_SET, start, mom1_count);
            V.write(mom1_fspace, data(mom1), mom1_mspace);
            //
            // fourth receive is moment<2>
            //
            delegate.comm()->recv(mom2, {MPK::Rank{rank}, std::get<3>(tags)});
            mom2_fspace.select(H5S_SELECT_SET, start, mom2_count);
            vv.write(mom2_fspace, data(mom2), mom2_mspace);
        }
    }
}
template <class Delegate>
void MPIC::MomentRecorder::record_cold_fluids(Delegate const &delegate, Identifier::Any) const
{
    using cold_fluid_type = typename Delegate::cold_fluid_type;
    using scalar_moment_type = typename cold_fluid_type::scalar_moment_type;
    using vector_moment_type = typename cold_fluid_type::vector_moment_type;
    using tensor_moment_type = typename cold_fluid_type::tensor_moment_type;
    auto const &domain = delegate.domain();
    long const Ns = domain.cold_fluids().size();
    UTL::Vector<MPK::Tag, 4> const tags{tag + 0, tag + 1, tag + 2, tag + 3};
    //
    // check if container is empty
    //
    if (Ns <= 0) {
        return;
    }
    //
    // first send is offset
    //
    auto offset = delegate.domain_offset();
    delegate.comm()->issend(offset, {delegate.comm().master(), std::get<0>(tags)}).wait();
    //
    // enumerate cold fluids
    //
    for (long i = 0; i < Ns; ++i) {
        cold_fluid_type const &cf = domain.cold_fluids().at(i);
        //
        // second send is moment<0>
        //
        scalar_moment_type const &mom0 = cf.template moment<0>();
        MPK::Request req0 = delegate.comm()->issend(mom0, {delegate.comm().master(), std::get<1>(tags)});
        //
        // third send is moment<1>
        //
        vector_moment_type const &mom1 = cf.template moment<1>();
        MPK::Request req1 = delegate.comm()->issend(mom1, {delegate.comm().master(), std::get<2>(tags)});
        //
        // fourth send is moment<2>
        //
        tensor_moment_type const &mom2 = cf.template moment<2>();
        MPK::Request req2 = delegate.comm()->issend(mom2, {delegate.comm().master(), std::get<3>(tags)});
        //
        // block until sent
        //
        req0.wait();
        req1.wait();
        req2.wait();
    }
}
