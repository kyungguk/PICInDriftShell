//
//  MPICTimeseriesRecorder.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICTimeseriesRecorder.h"

#include "MPICDelegate.h"
#include <stdexcept>
#include <fstream>
#include <string>

MPIC::TimeseriesRecorder::TimeseriesRecorder(char const *output_directory, Json::Value const json)
: Recorder{json.get("recording_frequency", unsigned{1}).asUInt()}
, _paths{}
, _should_record_moments{json.get("should_record_moments", false).asBool()} {
    Json::Value const pids = json.get("mpi_process_id", int{0});
    if (pids.isIntegral()) {
        int const pid = pids.asInt();
        _paths[pid] = std::string{output_directory} + "/" + json.get("filename_prefix", "timeseries").asString() + "-pid_" + std::to_string(pid) + ".csv";
    } else if (pids.isArray()) {
        for (Json::Value const &_pid : pids) {
            int const pid = _pid.asInt();
            _paths[pid] = std::string{output_directory} + "/" + json.get("filename_prefix", "timeseries").asString() + "-pid_" + std::to_string(pid) + ".csv";
        }
    } else {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - `mpi_process_id' json property is neither an array nor integral type");
    }
}
//
void MPIC::TimeseriesRecorder::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::TimeseriesRecorder::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::TimeseriesRecorder::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    record(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::TimeseriesRecorder::record(Delegate const &delegate, Identifier::Any) const
{
    int const pid = delegate.comm()->rank();
    if (!_paths.count(pid)) {
        return;
    }
    try {
        auto const &domain = delegate.domain();
        long const Ns = _should_record_moments ? domain.species().size() + domain.cold_fluids().size() : 0;
        bool const should_append = delegate.cur_step()/recording_frequency() > 1;
        //
        // open/create file
        //
        std::ofstream os; {
            os.open(_paths.at(pid), should_append ? os.app : os.trunc);
            os.setf(os.scientific);
            os.precision(14);
        }
        //
        // headers
        //
        if (!should_append) { // header
            printo(os,
                   "cur_step, time",
                   ", q1, q2, q3",
                   ", dBx, dBy, dBz",
                   ", dEx, dEy, dEz");
            for (long i = 0; i < Ns; ++i) {
                printo(os,
                       ", species(", i, ") n",
                       ", species(", i, ") nVx, species(", i, ") nVy, species(", i, ") nVz",
                       ", species(", i, ") nvxvx, species(", i, ") nvyvy, species(", i, ") nvzvz",
                       ", species(", i, ") nvxvy, species(", i, ") nvyvz, species(", i, ") nvzvx");
            }
            printo(os, "\n");
        }
        //
        // time stamp
        //
        printo(os, delegate.cur_step(), ", ", delegate.time());
        //
        // position
        //
        auto const curvi = delegate.curvi(domain.params().grid_q_min(), nullptr);
        printo(os, ", ", curvi.q1(), ", ", curvi.q2(), ", ", curvi.q3());
        //
        // B & E field : carts
        //
        {
            UTL::Vector<double, 3> const *dB = begin(domain.bfield()->carts);
            UTL::Vector<double, 3> const *dE = begin(domain.efield()->carts);
            printo(os,
                   ", ", dB->x, ", ", dB->y, ", ", dB->z,
                   ", ", dE->x, ", ", dE->y, ", ", dE->z);
        }
        if (Ns) {
            //
            // particle moments
            //
            for (typename Delegate::species_type const &sp : domain.species()) {
                UTL::Vector<double, 1> const *n = begin(sp.template moment<0>());
                UTL::Vector<double, 3> const *U = begin(sp.template moment<1>());
                UTL::Vector<double, 6> const *vv = begin(sp.template moment<2>());
                printo(os,
                       ", ", n->x,
                       ", ", U->x, ", ", U->y, ", ", U->z,
                       ", ", vv->xx(), ", ", vv->yy(), ", ", vv->zz(),
                       ", ", vv->xy(), ", ", vv->yz(), ", ", vv->zx());
            }
            //
            // cold fluid moments
            //
            for (typename Delegate::cold_fluid_type const &cf : domain.cold_fluids()) {
                UTL::Vector<double, 1> const *n = begin(cf.template moment<0>());
                UTL::Vector<double, 3> const *U = begin(cf.template moment<1>());
                UTL::Vector<double, 6> const *vv = begin(cf.template moment<2>());
                printo(os,
                       ", ", n->x,
                       ", ", U->x, ", ", U->y, ", ", U->z,
                       ", ", vv->xx(), ", ", vv->yy(), ", ", vv->zz(),
                       ", ", vv->xy(), ", ", vv->yz(), ", ", vv->zx());
            }
        }
        //
        // eol
        //
        printo(os, "\n");
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}
