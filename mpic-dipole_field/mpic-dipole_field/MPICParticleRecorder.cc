//
//  MPICParticleRecorder.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/20/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICParticleRecorder.h"

#include "MPICDelegate.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <random>
#include <algorithm>

namespace {
    constexpr unsigned UIntMax = std::numeric_limits<unsigned>::max();
    //
    UTL::DynamicArray<long> shuffled_indices(long n) {
        UTL::DynamicArray<long> indices(n);
        std::generate(indices.begin(), indices.end(), [n]() mutable->long { return --n; });
        //
        static std::mt19937 rng{std::random_device{}()};
        std::shuffle(indices.begin(), indices.end(), rng);
        return indices;
    }
}

MPIC::ParticleRecorder::ParticleRecorder(char const *output_directory, Json::Value const json)
: Recorder{json.get("recording_frequency", unsigned{1}).asUInt()}
, _output_directory{output_directory}
, _filename_prefix{json.get("filename_prefix", "particles").asString()}
, _species_id_bag{}
, _process_id_bag{}
, _max_particles_to_record{json.get("max_particles_to_record", UIntMax).asUInt()}
, _should_shuffle_particles{json.get("should_shuffle_particles", true).asBool()} {
    // species id
    //
    Json::Value const sids = json.get("species", Json::Value{Json::arrayValue});
    if (sids.isArray()) {
        for (Json::Value const &v : sids) {
            _species_id_bag.insert(v.asInt());
        }
    } else {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - `species' json property is not an array type");
    }
    // process id
    //
    Json::Value const pids = json.get("mpi_process_id", int{0});
    if (pids.isIntegral()) { // a single value of -1 indicate all processes
        _process_id_bag.insert(pids.asInt());
    } else if (pids.isArray()) {
        for (Json::Value const &_pid : pids) {
            int const pid = _pid.asInt();
            if (pid >= 0) _process_id_bag.insert(pid); // exclude negative values
        }
    } else {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - `mpi_process_id' json property is neither an array nor integral type");
    }
}
//
void MPIC::ParticleRecorder::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::ParticleRecorder::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::ParticleRecorder::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    record(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::ParticleRecorder::record(Delegate const &delegate, Identifier::Any) const
{
    try {
        int const pid = delegate.comm()->rank();
        if (!_process_id_bag.count(pid) && !_process_id_bag.count(-1)) return;
        auto const &domain = delegate.domain();
        long const Ns = domain.species().size();
        using particle_type = typename Delegate::species_type::particle_type;
        static_assert(0 == sizeof(particle_type)%sizeof(double), "sizeof(particle_type) is not divisible by sizeof(double)");

        // create hdf5 file
        //
        H5::Group root = [](std::string const path) {
            return H5::File{H5::File::Trunc{}, path.c_str()}.group("species", H5::PList{}, H5::PList{});
        }(_output_directory + "/" + _filename_prefix + "-pid_" + std::to_string(pid) + "-" + std::to_string(delegate.cur_step()) + ".h5");
        write_common_attr(root, delegate, delegate.identifier);
        write_species_attr(root, domain, delegate.identifier);

        // iterate over species
        //
        for (long spid = 0; spid < Ns; ++spid) {
            if (0 == _species_id_bag.count(spid)) continue;
            auto const &sp = domain.species()[spid];
            H5::Space fspace; {
                H5::Space::size_array_type dims(2);
                dims.at(0) = static_cast<unsigned long>(sp.bucket().size());
                if (dims[0] > _max_particles_to_record) {
                    dims[0] = _max_particles_to_record;
                }
                dims.at(1) = sizeof(particle_type)/sizeof(double);
                fspace = H5::Space{dims};
            }
            H5::Dataset dset; {
                std::string const name = std::to_string(spid);
                dset = root.dataset(name.c_str(), H5::Type::native<double>(), fspace);
            }
            if (_should_shuffle_particles) {
                particle_type const *data = sp.bucket().data();
                UTL::DynamicArray<long> const indices = shuffled_indices(sp.bucket().size());
                H5::Space::size_array_type const dims = fspace.simple_extent().first, count{1, dims.at(1)};
                H5::Space mspace{count}; {
                    mspace.select_all();
                }
                H5::Space::size_array_type start{0, 0};
                // make sure that indices.size() >= dims[0]
                for (auto const n = dims.at(0); start[0] < n; ++start[0]) {
                    fspace.select(H5S_SELECT_SET, start, count);
                    long const i = static_cast<long>(start[0]);
                    dset.write(fspace, reinterpret_cast<double const *>(data + indices[i]), mspace);
                }
            } else {
                fspace.select_all();
                dset.write(fspace, reinterpret_cast<double const *>(sp.bucket().data()), fspace);
            }
            write_common_attr(dset, delegate, delegate.identifier);
            write_species_attr(dset, sp, delegate.identifier);
        }

        // flush
        //
        root.flush();
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}
