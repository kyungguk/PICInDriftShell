//
//  MPICMomentRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICMomentRecorder_h
#define MPICMomentRecorder_h

#include "MPICRecorder.h"
#include <json/json.h>
#include <string>

MPIC_BEGIN_NAMESPACE
class MomentRecorder : public Recorder {
public:
    explicit MomentRecorder(char const *output_directory, Json::Value const json);
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate) override;
    void operator<<(__1_::PICDelegate<2L> const &delegate) override;
    void operator<<(__1_::PICDelegate<3L> const &delegate) override;

private:
    std::string _output_directory;
    std::string _filename_prefix;
    static constexpr int tag = 011;

    template <class Delegate>
    void record(Delegate const &delegate, Identifier::Any) const;

    template <class Delegate>
    void record_particles(H5::File &file, Delegate const &delegate, Identifier::Any) const;
    template <class Delegate>
    void record_particles(Delegate const &delegate, Identifier::Any) const;

    template <class Delegate>
    void record_cold_fluids(H5::File &file, Delegate const &delegate, Identifier::Any) const;
    template <class Delegate>
    void record_cold_fluids(Delegate const &delegate, Identifier::Any) const;
};
MPIC_END_NAMESPACE

#endif /* MPICMomentRecorder_h */
