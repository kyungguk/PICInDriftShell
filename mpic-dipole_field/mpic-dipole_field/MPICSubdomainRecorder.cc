//
//  MPICSubdomainRecorder.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/20/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICSubdomainRecorder.h"

#include "MPICDelegate.h"
#include <stdexcept>
#include <string>

MPIC::SubdomainRecorder::SubdomainRecorder(char const *output_directory, Json::Value const json)
: Recorder{json.get("recording_frequency", unsigned{1}).asUInt()}
, _output_directory{output_directory}
, _filename_prefix{json.get("filename_prefix", "subdomain").asString()}
, _pid_bag{}
, _should_record_moments{json.get("should_record_moments", false).asBool()} {
    Json::Value const pids = json.get("mpi_process_id", int{0});
    if (pids.isIntegral()) { // a single value of -1 indicate all processes
        _pid_bag.insert(pids.asInt());
    } else if (pids.isArray()) {
        for (Json::Value const &_pid : pids) {
            int const pid = _pid.asInt();
            if (pid >= 0) _pid_bag.insert(pid); // exclude negative values
        }
    } else {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - `mpi_process_id' json property is neither an array nor integral type");
    }
}
//
void MPIC::SubdomainRecorder::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::SubdomainRecorder::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::SubdomainRecorder::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    record(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::SubdomainRecorder::record(Delegate const &delegate, Identifier::Any) const
{
    int const pid = delegate.comm()->rank();
    if (!_pid_bag.count(pid) && !_pid_bag.count(-1)) {
        return;
    }
    try {
        // create hdf5 file
        //
        H5::File root = [](std::string const path) {
            return H5::File{H5::File::Trunc{}, path.c_str()};
        }(_output_directory + "/" + _filename_prefix + "-pid_" + std::to_string(pid) + "-" + std::to_string(delegate.cur_step()) + ".h5");

        // record fields & moments
        //
        record_fields(root, delegate, Delegate::identifier);
        if (_should_record_moments) {
            record_particles(root, delegate, Delegate::identifier);
            record_cold_fluids(root, delegate, Delegate::identifier);
        }

        // flush
        //
        root.flush();
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

template <class Delegate>
void MPIC::SubdomainRecorder::record_fields(H5::File &file, Delegate const &delegate, Identifier::Any) const
{
    using delegate_type = typename std::decay<decltype(delegate)>::type;
    using magnetic_field_type = typename delegate_type::bfield_type::magnetic_field_type;
    using electric_field_type = typename delegate_type::efield_type::electric_field_type;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    //
    // create group and populate attributes
    //
    H5::Group root = file.group("fields", H5::PList{}, H5::PList{});
    write_common_attr(root, delegate, delegate.identifier);

    // memory and file dataspace; assume that dims() and pad_size() of B and E are the same
    //
    static_assert(magnetic_field_type::pad_size() == electric_field_type::pad_size(), "B and E field pad sizes have to be the same");
    H5::Space mspace, fspace; {
        auto const N = params.N().template take<magnetic_field_type::rank()>();
        H5::Space::size_array_type dims{N.begin(), N.end()}, count = dims;
        for (auto &d : dims) {
            d += 2*magnetic_field_type::pad_size();
        }
        dims.push_back(magnetic_field_type::value_type::size());
        count.push_back(dims.back());
        //
        H5::Space::size_array_type start(N.size(), magnetic_field_type::pad_size());
        start.push_back(0);
        //
        (mspace = H5::Space{dims}).select(H5S_SELECT_SET, start, count);
        (fspace = H5::Space{count}).select_all();
    }

    // bfield
    //
    H5::Dataset B = root.dataset("B", H5::Type::native<double>(), fspace);
    write_common_attr(B, delegate, delegate.identifier);
    magnetic_field_type const &Bcarts = domain.bfield()->carts;
    B.write(fspace, data(Bcarts), mspace);

    // efield
    //
    H5::Dataset E = root.dataset("E", H5::Type::native<double>(), fspace);
    write_common_attr(E, delegate, delegate.identifier);
    electric_field_type const &Ecarts = domain.efield()->carts;
    E.write(fspace, data(Ecarts), mspace);
}
template <class Delegate>
void MPIC::SubdomainRecorder::record_particles(H5::File &file, Delegate const &delegate, Identifier::Any) const
{
    using species_type = typename Delegate::species_type;
    using scalar_moment_type = typename species_type::scalar_moment_type;
    using vector_moment_type = typename species_type::vector_moment_type;
    using tensor_moment_type = typename species_type::tensor_moment_type;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    long const Ns = domain.species().size();
    //
    // check if container is empty
    //
    if (Ns <= 0) {
        return;
    }
    //
    // create group and populate attributes
    //
    H5::Group root = file.group("particles", H5::PList{}, H5::PList{});
    write_attr(root, "Nspecies", Ns);
    write_common_attr(root, delegate, delegate.identifier);
    write_species_attr(root, domain, delegate.identifier);
    //
    // create file dataspace
    //
    H5::Space mom0_fspace, mom1_fspace, mom2_fspace; {
        auto const N = params.N().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type dims{N.begin(), N.end()};
        dims.insert(dims.begin(), static_cast<unsigned long>(Ns)); // first dimension is species
        mom0_fspace = H5::Space{dims};
        dims.push_back(vector_moment_type::value_type::size());
        mom1_fspace = H5::Space{dims};
        dims.back() = tensor_moment_type::value_type::size();
        mom2_fspace = H5::Space{dims};
    }
    //
    // create datasets and populate attributes
    //
    H5::Dataset n, V, vv; {
        n = root.dataset("n", H5::Type::native<double>(), mom0_fspace);
        write_common_attr(n, delegate, delegate.identifier);
        write_species_attr(n, domain, delegate.identifier);
        V = root.dataset("nV", H5::Type::native<double>(), mom1_fspace);
        write_common_attr(V, delegate, delegate.identifier);
        write_species_attr(V, domain, delegate.identifier);
        vv = root.dataset("nvv", H5::Type::native<double>(), mom2_fspace);
        write_common_attr(vv, delegate, delegate.identifier);
        write_species_attr(vv, domain, delegate.identifier);
    }
    //
    // memory dataspace
    //
    H5::Space::size_array_type mom0_count, mom1_count, mom2_count;
    H5::Space mom0_mspace, mom1_mspace, mom2_mspace; {
        auto const N = params.N().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type mom0_dims{N.begin(), N.end()}, mom1_dims = mom0_dims, mom2_dims = mom0_dims;
        for (long i = 0; i < mom0_dims.size(); ++i) {
            mom0_count.push_back(mom0_dims[i]);
            mom0_dims[i] += 2*scalar_moment_type::pad_size();
            mom1_dims[i] += 2*vector_moment_type::pad_size();
            mom2_dims[i] += 2*tensor_moment_type::pad_size();
        }
        mom2_count = mom1_count = mom0_count;
        mom1_dims.push_back(vector_moment_type::value_type::size());
        mom1_count.push_back(mom1_dims.back());
        mom2_dims.push_back(tensor_moment_type::value_type::size());
        mom2_count.push_back(mom2_dims.back());
        //
        H5::Space::size_array_type mom0_start(N.size(), scalar_moment_type::pad_size());
        mom0_start.push_back(0); // this will be ignored
        (mom0_mspace = H5::Space{mom0_dims}).select(H5S_SELECT_SET, mom0_start, mom0_count);
        H5::Space::size_array_type mom1_start(N.size(), vector_moment_type::pad_size());
        mom1_start.push_back(0);
        (mom1_mspace = H5::Space{mom1_dims}).select(H5S_SELECT_SET, mom1_start, mom1_count);
        H5::Space::size_array_type mom2_start(N.size(), tensor_moment_type::pad_size());
        mom2_start.push_back(0);
        (mom2_mspace = H5::Space{mom2_dims}).select(H5S_SELECT_SET, mom2_start, mom2_count);
    }
    mom0_count.insert(mom0_count.begin(), 1); // first dimension is species
    mom1_count.insert(mom1_count.begin(), 1);
    mom2_count.insert(mom2_count.begin(), 1);
    //
    // record subdomain moments
    //
    H5::Space::size_array_type start(1 + domain.ND + 1, 0);
    for (long i = 0; i < Ns; ++i) {
        species_type const &sp = domain.species().at(i);
        start[0] = static_cast<unsigned long>(i);
        // moment<0>
        //
        mom0_fspace.select(H5S_SELECT_SET, start, mom0_count);
        scalar_moment_type const &mom0 = sp.template moment<0>();
        n.write(mom0_fspace, data(mom0), mom0_mspace);
        // moment<1>
        //
        mom1_fspace.select(H5S_SELECT_SET, start, mom1_count);
        vector_moment_type const &mom1 = sp.template moment<1>();
        V.write(mom1_fspace, data(mom1), mom1_mspace);
        // moment<2>
        //
        mom2_fspace.select(H5S_SELECT_SET, start, mom2_count);
        tensor_moment_type const &mom2 = sp.template moment<2>();
        vv.write(mom2_fspace, data(mom2), mom2_mspace);
    }
}
template <class Delegate>
void MPIC::SubdomainRecorder::record_cold_fluids(H5::File &file, Delegate const &delegate, Identifier::Any) const
{
    using cold_fluid_type = typename Delegate::cold_fluid_type;
    using scalar_moment_type = typename cold_fluid_type::scalar_moment_type;
    using vector_moment_type = typename cold_fluid_type::vector_moment_type;
    using tensor_moment_type = typename cold_fluid_type::tensor_moment_type;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    long const Ns = domain.cold_fluids().size();
    //
    // check if container is empty
    //
    if (Ns <= 0) {
        return;
    }
    //
    // create group and populate attributes
    //
    H5::Group root = file.group("cold_fluids", H5::PList{}, H5::PList{});
    write_attr(root, "Nspecies", Ns);
    write_common_attr(root, delegate, delegate.identifier);
    write_cold_fluid_attr(root, domain, delegate.identifier);
    //
    // create file dataspace
    //
    H5::Space mom0_fspace, mom1_fspace, mom2_fspace; {
        auto const N = params.N().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type dims{N.begin(), N.end()};
        dims.insert(dims.begin(), static_cast<unsigned long>(Ns)); // first dimension is cold fluids
        mom0_fspace = H5::Space{dims};
        dims.push_back(vector_moment_type::value_type::size());
        mom1_fspace = H5::Space{dims};
        dims.back() = tensor_moment_type::value_type::size();
        mom2_fspace = H5::Space{dims};
    }
    //
    // create datasets and populate attributes
    //
    H5::Dataset n, V, vv; {
        n = root.dataset("n", H5::Type::native<double>(), mom0_fspace);
        write_common_attr(n, delegate, delegate.identifier);
        write_cold_fluid_attr(n, domain, delegate.identifier);
        V = root.dataset("nV", H5::Type::native<double>(), mom1_fspace);
        write_common_attr(V, delegate, delegate.identifier);
        write_cold_fluid_attr(V, domain, delegate.identifier);
        vv = root.dataset("nvv", H5::Type::native<double>(), mom2_fspace);
        write_common_attr(vv, delegate, delegate.identifier);
        write_cold_fluid_attr(vv, domain, delegate.identifier);
    }
    //
    // memory dataspace
    //
    H5::Space::size_array_type mom0_count, mom1_count, mom2_count;
    H5::Space mom0_mspace, mom1_mspace, mom2_mspace; {
        auto const N = params.N().template take<scalar_moment_type::rank()>();
        H5::Space::size_array_type mom0_dims{N.begin(), N.end()}, mom1_dims = mom0_dims, mom2_dims = mom0_dims;
        for (long i = 0; i < mom0_dims.size(); ++i) {
            mom0_count.push_back(mom0_dims[i]);
            mom0_dims[i] += 2*scalar_moment_type::pad_size();
            mom1_dims[i] += 2*vector_moment_type::pad_size();
            mom2_dims[i] += 2*tensor_moment_type::pad_size();
        }
        mom2_count = mom1_count = mom0_count;
        mom1_dims.push_back(vector_moment_type::value_type::size());
        mom1_count.push_back(mom1_dims.back());
        mom2_dims.push_back(tensor_moment_type::value_type::size());
        mom2_count.push_back(mom2_dims.back());
        //
        H5::Space::size_array_type mom0_start(N.size(), scalar_moment_type::pad_size());
        mom0_start.push_back(0); // this will be ignored
        (mom0_mspace = H5::Space{mom0_dims}).select(H5S_SELECT_SET, mom0_start, mom0_count);
        H5::Space::size_array_type mom1_start(N.size(), vector_moment_type::pad_size());
        mom1_start.push_back(0);
        (mom1_mspace = H5::Space{mom1_dims}).select(H5S_SELECT_SET, mom1_start, mom1_count);
        H5::Space::size_array_type mom2_start(N.size(), tensor_moment_type::pad_size());
        mom2_start.push_back(0);
        (mom2_mspace = H5::Space{mom2_dims}).select(H5S_SELECT_SET, mom2_start, mom2_count);
    }
    mom0_count.insert(mom0_count.begin(), 1); // first dimension is cold fluids
    mom1_count.insert(mom1_count.begin(), 1);
    mom2_count.insert(mom2_count.begin(), 1);
    //
    // record subdomain moments
    //
    H5::Space::size_array_type start(1 + domain.ND + 1, 0);
    for (long i = 0; i < Ns; ++i) {
        cold_fluid_type const &cf = domain.cold_fluids().at(i);
        start[0] = static_cast<unsigned long>(i);
        // moment<0>
        //
        mom0_fspace.select(H5S_SELECT_SET, start, mom0_count);
        scalar_moment_type const &mom0 = cf.template moment<0>();
        n.write(mom0_fspace, data(mom0), mom0_mspace);
        // moment<1>
        //
        mom1_fspace.select(H5S_SELECT_SET, start, mom1_count);
        vector_moment_type const &mom1 = cf.template moment<1>();
        V.write(mom1_fspace, data(mom1), mom1_mspace);
        // moment<2>
        //
        mom2_fspace.select(H5S_SELECT_SET, start, mom2_count);
        tensor_moment_type const &mom2 = cf.template moment<2>();
        vv.write(mom2_fspace, data(mom2), mom2_mspace);
    }
}
