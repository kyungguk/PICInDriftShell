//
//  MPICVelocityHistogramRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/25/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICVelocityHistogramRecorder_h
#define MPICVelocityHistogramRecorder_h

#include "MPICRecorder.h"
#include "MPICVelocityHistogram.h"
#include <MPIKit/MPIKit.h>
#include <json/json.h>
#include <string>
#include <map>

MPIC_BEGIN_NAMESPACE
template <long> struct VelocityHistogramRecorder;

template <>
struct VelocityHistogramRecorder<2L> : public Recorder {
public:
    using vhist_type = VelocityHistogram<2L>;
    using vhist_map_type = std::map<unsigned, vhist_type>;

    explicit VelocityHistogramRecorder(char const *output_directory, Json::Value const json);
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate) override;
    void operator<<(__1_::PICDelegate<2L> const &delegate) override;
    void operator<<(__1_::PICDelegate<3L> const &delegate) override;

private:
    std::string _output_directory;
    std::string _filename_prefix;
    vhist_map_type _vhists;

    template <class Delegate>
    void record(Delegate const &delegate, Identifier::Any);

    template <class Delegate>
    void write(H5::Group &root, Delegate const &delegate, UTL::DynamicArray<double> const &vhist_N, vhist_map_type::value_type &kv, Identifier::Any) const;

    template <class Delegate>
    UTL::DynamicArray<double> collect(Delegate const &delegate, vhist_map_type::value_type &kv, Identifier::Any) const;

    template <class T>
    static inline typename std::enable_if<std::is_default_constructible<MPK::TypeMap<T>>::value,
    void>::type reduce_plus(MPK::Comm const &comm, T &x);
    template <class T>
    static inline typename std::enable_if<std::is_default_constructible<MPK::TypeMap<T>>::value,
    void>::type reduce_plus(MPK::Comm const &comm, UTL::Array<T> &A);
};
MPIC_END_NAMESPACE

#endif /* MPICVelocityHistogramRecorder_h */
