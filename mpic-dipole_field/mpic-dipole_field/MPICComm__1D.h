//
//  MPICComm__1D.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 6/30/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef MPICComm__1D_h
#define MPICComm__1D_h

#include "MPICComm__ND.h"

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- MPIC::__1_::InterProcessComm<ND>
    //
    template <>
    class InterProcessComm<1> : public _InterProcessComm<1> {
    public:
        InterProcessComm(InterProcessComm &&) = default;
        InterProcessComm &operator=(InterProcessComm &&) = default;

        explicit InterProcessComm(MPK::Comm &&comm, size_vector_type const domain_decomposition);
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICComm__1D_h */
