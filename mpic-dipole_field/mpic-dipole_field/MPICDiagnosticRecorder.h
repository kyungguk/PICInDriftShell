//
//  MPICDiagnosticRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDiagnosticRecorder_h
#define MPICDiagnosticRecorder_h

#include "MPICRecorder.h"
#include <json/json.h>
#include <string>

MPIC_BEGIN_NAMESPACE
class DiagnosticRecorder : public Recorder {
public:
    explicit DiagnosticRecorder(char const *output_directory, Json::Value const json);
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate) override;
    void operator<<(__1_::PICDelegate<2L> const &delegate) override;
    void operator<<(__1_::PICDelegate<3L> const &delegate) override;

private:
    std::string _path;

    template <class Delegate>
    void record(Delegate const &delegate, Identifier::Any) const;
};
MPIC_END_NAMESPACE

#endif /* MPICDiagnosticRecorder_h */
