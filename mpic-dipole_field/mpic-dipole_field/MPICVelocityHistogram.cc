//
//  MPICVelocityHistogram.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/25/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICVelocityHistogram.h"
#include <stdexcept>
#include <string>
#include <cmath>

// MARK: VelocityHistogram<2D>
//
MPIC::VelocityHistogram<2L>::VelocityHistogram(UTL::DynamicArray<value_type> const &v1_bins, UTL::DynamicArray<value_type> const &v2_bins)
{
    // number of bins check
    //
    if (v1_bins.size() < 2 || v2_bins.size() < 2) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - the number of bins less than 2");
    }

    // v1 bin size check
    //
    value_type const dv1 = v1_bins[1] - v1_bins[0];
    if (dv1 < 0 || std::abs(dv1) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small or negative v1 bin size");
    }
    for (long i = 2, n = v1_bins.size(); i < n; ++i) {
        value_type const d = v1_bins[i] - v1_bins[i-1];
        if (std::abs(d - dv1)/dv1 > 1e-10) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - irregular v1 bin size");
        }
    }

    // v2 bin size check
    //
    value_type const dv2 = v2_bins[1] - v2_bins[0];
    if (dv2 < 0 || std::abs(dv2) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small or negative v2 bin size");
    }
    for (long i = 2, n = v2_bins.size(); i < n; ++i) {
        value_type const d = v2_bins[i] - v2_bins[i-1];
        if (std::abs(d - dv2)/dv2 > 1e-10) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - irregular v2 bin size");
        }
    }

    // populate member variables
    //
    _v1min = v1_bins.front(); _v2min = v2_bins.front();
    _dv1 = dv1; _dv2 = dv2;
    _nv1 = v1_bins.size() - 1; _nv2 = v2_bins.size() - 1;
    _hist = UTL::DynamicArray<size_type>(_nv1 * _nv2, 0);
    _N = 0; // total count
}
MPIC::VelocityHistogram<2L>::VelocityHistogram(value_vector_type const &vmin, value_vector_type const &vmax, size_vector_type const &nbins)
{
    // number of bins check
    //
    if (std::get<0>(nbins) < 1 || std::get<1>(nbins) < 1) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - elements of nbins should be non-zero");
    }

    // bin size check
    //
    value_vector_type const dv = {
        (std::get<0>(vmax) - std::get<0>(vmin)) / std::get<0>(nbins),
        (std::get<1>(vmax) - std::get<1>(vmin)) / std::get<1>(nbins)
    };
    if (std::get<0>(dv) < 0 || std::abs(std::get<0>(dv)) < 1e-10 ||
        std::get<1>(dv) < 0 || std::abs(std::get<1>(dv)) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small or negative bin size(s)");
    }

    // populate member variables
    //
    _v1min = std::get<0>(vmin);
    _v2min = std::get<1>(vmin);
    _dv1 = std::get<0>(dv);
    _dv2 = std::get<1>(dv);
    _nv1 = std::get<0>(nbins);
    _nv2 = std::get<1>(nbins);
    _hist = UTL::DynamicArray<size_type>(_nv1 * _nv2, 0);
    _N = 0; // total count
}

void MPIC::VelocityHistogram<2L>::reset()
{
    for (auto &x : _hist) {
        x = 0;
    }
    _N = 0;
}

auto MPIC::VelocityHistogram<2L>::operator<<(UTL::Vector<value_type, 3> const &vel)
-> VelocityHistogram &{
    // increment the total count first
    //
    ++_N;

    // v1 range check
    //
    value_type const v1 = vel.x;
    long const i1 = static_cast<long>(std::floor((v1 - _v1min)/_dv1));
    if (i1 < 0 || i1 >= _nv1) {
        return *this;
    }

    // v2 range check
    //
    value_type const v2 = std::sqrt(vel.y*vel.y + vel.z*vel.z);
    long const i2 = static_cast<long>(std::floor((v2 - _v2min)/_dv2));
    if (i2 < 0 || i2 >= _nv2) {
        return *this;
    }

    // increment histogram at the bin
    //
    ++_hist.at(i1*_nv2 + i2);

    return *this;
}

auto MPIC::VelocityHistogram<2L>::operator+=(VelocityHistogram const &hist)
-> VelocityHistogram &{
    if (_v1min != hist._v1min || _v2min != hist._v2min ||
        _dv1 != hist._dv1 || _dv2 != hist._dv2 ||
        _nv1 != hist._nv1 || _nv2 != hist._nv2 ||
        _hist.size() != hist._hist.size()) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }

    _N += hist._N;
    for (long i = 0, n = _hist.size(); i < n; ++i) {
        _hist[i] += hist._hist[i];
    }

    return *this;
}

// MARK: VelocityHistogram<3D>
//
MPIC::VelocityHistogram<3L>::VelocityHistogram(UTL::DynamicArray<value_type> const &vx_bins, UTL::DynamicArray<value_type> const &vy_bins, UTL::DynamicArray<value_type> const &vz_bins)
{
    // number of bins check
    //
    if (vx_bins.size() < 2 || vy_bins.size() < 2 || vz_bins.size() < 2) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - the number of bins less than 2");
    }

    // vx bin size check
    //
    value_type const dvx = vx_bins[1] - vx_bins[0];
    if (dvx < 0 || std::abs(dvx) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small or negative vx bin size");
    }
    for (long i = 2, n = vx_bins.size(); i < n; ++i) {
        value_type const d = vx_bins[i] - vx_bins[i-1];
        if (std::abs(d - dvx)/dvx > 1e-10) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - irregular vx bin size");
        }
    }

    // vy bin size check
    //
    value_type const dvy = vy_bins[1] - vy_bins[0];
    if (dvy < 0 || std::abs(dvy) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small or negative vy bin size");
    }
    for (long i = 2, n = vy_bins.size(); i < n; ++i) {
        value_type const d = vy_bins[i] - vy_bins[i-1];
        if (std::abs(d - dvy)/dvy > 1e-10) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - irregular vy bin size");
        }
    }

    // vz bin size check
    //
    value_type const dvz = vz_bins[1] - vz_bins[0];
    if (dvz < 0 || std::abs(dvz) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small or negative vz bin size");
    }
    for (long i = 2, n = vz_bins.size(); i < n; ++i) {
        value_type const d = vz_bins[i] - vz_bins[i-1];
        if (std::abs(d - dvz)/dvz > 1e-10) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - irregular vz bin size");
        }
    }

    // populate member variables
    //
    _vxmin = vx_bins.front(); _vymin = vy_bins.front(); _vzmin = vz_bins.front();
    _dvx = dvx; _dvy = dvy; _dvz = dvz;
    _nvx = vx_bins.size() - 1; _nvy = vy_bins.size() - 1; _nvz = vz_bins.size() - 1;
    _hist = UTL::DynamicArray<size_type>(_nvx * _nvy * _nvz, 0);
    _N = 0; // total count
}
MPIC::VelocityHistogram<3L>::VelocityHistogram(value_vector_type const &vmin, value_vector_type const &vmax, size_vector_type const &nbins)
{
    // number of bins check
    //
    if (std::get<0>(nbins) < 1 || std::get<1>(nbins) < 1 || std::get<2>(nbins) < 1) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - elements of nbins should be non-zero");
    }

    // bin size check
    //
    value_vector_type const dv = {
        (std::get<0>(vmax) - std::get<0>(vmin)) / std::get<0>(nbins),
        (std::get<1>(vmax) - std::get<1>(vmin)) / std::get<1>(nbins),
        (std::get<2>(vmax) - std::get<2>(vmin)) / std::get<2>(nbins)
    };
    if (std::get<0>(dv) < 0 || std::abs(std::get<0>(dv)) < 1e-10 ||
        std::get<1>(dv) < 0 || std::abs(std::get<1>(dv)) < 1e-10 ||
        std::get<2>(dv) < 0 || std::abs(std::get<2>(dv)) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - too small or negative bin size(s)");
    }

    // populate member variables
    //
    _vxmin = std::get<0>(vmin);
    _vymin = std::get<1>(vmin);
    _vzmin = std::get<2>(vmin);
    _dvx = std::get<0>(dv);
    _dvy = std::get<1>(dv);
    _dvz = std::get<2>(dv);
    _nvx = std::get<0>(nbins);
    _nvy = std::get<1>(nbins);
    _nvz = std::get<2>(nbins);
    _hist = UTL::DynamicArray<size_type>(_nvx * _nvy * _nvz, 0);
    _N = 0; // total count
}

void MPIC::VelocityHistogram<3L>::reset()
{
    for (auto &x : _hist) {
        x = 0;
    }
    _N = 0;
}

auto MPIC::VelocityHistogram<3L>::operator<<(const Utility::Vector<value_type, 3> &vel)
-> VelocityHistogram &{
    // increment the total count first
    //
    ++_N;

    // vx range check
    //
    long const ix = static_cast<long>(std::floor((vel.x - _vxmin)/_dvx));
    if (ix < 0 || ix >= _nvx) {
        return *this;
    }

    // vy range check
    //
    long const iy = static_cast<long>(std::floor((vel.y - _vymin)/_dvy));
    if (iy < 0 || iy >= _nvy) {
        return *this;
    }

    // vz range check
    //
    long const iz = static_cast<long>(std::floor((vel.z - _vzmin)/_dvz));
    if (iz < 0 || iz >= _nvz) {
        return *this;
    }

    // increment histogram at the bin
    //
    ++_hist.at(ix*_nvy*_nvz + iy*_nvz + iz);

    return *this;
}

auto MPIC::VelocityHistogram<3L>::operator+=(const VelocityHistogram &hist)
-> VelocityHistogram &{
    if (_vxmin != hist._vxmin || _vymin != hist._vymin || _vzmin != hist._vzmin ||
        _dvx != hist._dvx || _dvy != hist._dvy || _dvz != hist._dvz ||
        _nvx != hist._nvx || _nvy != hist._nvy || _nvz != hist._nvz ||
        _hist.size() != hist._hist.size()) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }

    _N += hist._N;
    for (long i = 0, n = _hist.size(); i < n; ++i) {
        _hist[i] += hist._hist[i];
    }

    return *this;
}
