//
//  MPICJsonProxy.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/21/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICJsonProxy.h"

#include <stdexcept>

MPIC::JsonProxy MPIC::JsonProxy::operator[](std::string const &key) const
{
    return _json.isMember(key) ? get(key, {}) :
    throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - no option for `" + key + "'");
}
MPIC::JsonProxy MPIC::JsonProxy::operator[](Json::ArrayIndex const &i) const
{
    return _json.isValidIndex(i) ? get(i, {}) :
    throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - out-of-range for index `" + std::to_string(i) + "'");
}
