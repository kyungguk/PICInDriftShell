//
//  MPICArguments.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 2/16/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef MPICArguments_h
#define MPICArguments_h

#include "mpic-config.h"

#include "MPICJsonProxy.h"
#include <json/json.h>

MPIC_BEGIN_NAMESPACE
class Arguments : public JsonProxy {
public:
    explicit Arguments() = default;
    explicit Arguments(const int argc, const char *const *argv);

private:
    static Json::Value parse(const int argc, const char *const *argv);
};
MPIC_END_NAMESPACE

#endif /* MPICArguments_h */
