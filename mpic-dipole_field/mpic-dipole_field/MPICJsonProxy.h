//
//  MPICJsonProxy.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/21/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICJsonProxy_h
#define MPICJsonProxy_h

#include "mpic-config.h"

#include <json/json.h>
#include <ostream>
#include <string>

MPIC_BEGIN_NAMESPACE
class JsonProxy {
    Json::Value _json;

public:
    // construct
    JsonProxy() = default;
    template <class T>
    JsonProxy(T const &obj) : _json(obj) {}

    // Json introspection
    bool operator!() const { return !_json; } // check for nullibility of Json itself
    bool isBool() const { return _json.isBool(); }
    bool isIntegral() const { return _json.isIntegral(); }
    bool isNumeric() const { return _json.isNumeric(); }
    bool isString() const { return _json.isString(); }
    bool isArray() const { return _json.isArray(); }
    bool isObject() const { return _json.isObject(); }
    bool empty() const { return _json.empty(); }
    bool isNull() const { return _json.isNull(); }
    Json::ArrayIndex size() const { return _json.size(); }
    bool isValidIndex(Json::ArrayIndex i) const { return _json.isValidIndex(i); }
    bool isMember(std::string const &key) const { return _json.isMember(key); }

    // native value access through implicit cast
    Json::Value &json() noexcept { return _json; }
    Json::Value const &json() const noexcept { return _json; }
    operator bool() const { return _json.asBool(); } // this is boolean cast of Json content, not the validity of Json
    operator int() const { return _json.asInt(); }
    operator unsigned int() const { return _json.asUInt(); }
    operator long() const { return _json.asInt64(); }
    operator unsigned long() const { return _json.asUInt64(); }
    operator float() const { return _json.asFloat(); }
    operator double() const { return _json.asDouble(); }
    operator std::string() const { return _json.asString(); }

    // object's or array's element access
    template <class Key> // Key should be convertible to ArrayIndex or std::string
    JsonProxy get(Key const &key, JsonProxy const &default_value) const { return JsonProxy(_json.get(key, default_value.json())); }

    JsonProxy operator[](Json::ArrayIndex const &index) const; // throw out_of_range exception if index is not found
    JsonProxy operator[](std::string const &key) const; // throw invalid_argument exception if key is not found
    JsonProxy operator[](char const *key) const { return (*this)[std::string(key)]; }

private:
    template <class CharT, class Traits>
    friend std::basic_ostream<CharT, Traits> &operator<<(std::basic_ostream<CharT, Traits> &os, JsonProxy const &proxy) {
        return os << proxy.json();
    }
};
MPIC_END_NAMESPACE

#endif /* MPICJsonProxy_h */
