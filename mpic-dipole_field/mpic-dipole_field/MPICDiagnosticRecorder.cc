//
//  MPICDiagnosticRecorder.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICDiagnosticRecorder.h"

#include "MPICDiagnostic.h"
#include "MPICDelegate.h"
#include <stdexcept>
#include <fstream>
#include <string>

MPIC::DiagnosticRecorder::DiagnosticRecorder(char const *output_directory, Json::Value const json)
: Recorder{json.get("recording_frequency", unsigned{1}).asUInt()}
, _path{output_directory} {
    _path += "/" + json.get("filename", "diagnostic.csv").asString();
}
//
void MPIC::DiagnosticRecorder::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::DiagnosticRecorder::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::DiagnosticRecorder::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    record(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::DiagnosticRecorder::record(Delegate const &delegate, Identifier::Any) const
{
    if (!delegate.comm().is_master()) {
        return;
    }
    try {
        auto const &diag = delegate.diagnostic();
        auto const &domain = delegate.domain();
        long const Ns = domain.species().size() + domain.cold_fluids().size();
        bool const should_append = delegate.cur_step()/recording_frequency() > 1;
        //
        // open/create file
        //
        std::ofstream os; {
            os.open(_path, should_append ? os.app : os.trunc);
            os.setf(os.scientific);
            os.precision(14);
        }
        //
        // headers
        //
        if (!should_append) { // header
            printo(os, "cur_step, time",
                   ", dBx^2/2, dBy^2/2, dBz^2/2",
                   ", dEx^2/2, dEy^2/2, dEz^2/2",
                   ", total mvx^2/2, total mvy^2/2, total mvz^2/2");
            for (long i = 0; i < Ns; ++i) {
                printo(os,
                       ", species(", i, ") mUx^2/2, species(", i, ") mUy^2/2, species(", i, ") mUz^2/2",
                       ", species(", i, ") mvx^2/2, species(", i, ") mvy^2/2, species(", i, ") mvz^2/2");
            }
            printo(os, "\n");
        }
        //
        // time stamp
        //
        printo(os, delegate.cur_step(), ", ", delegate.time());
        //
        // total energy
        //
        printo(os,
               ", ", diag.dB2Over2.x, ", ", diag.dB2Over2.y, ", ", diag.dB2Over2.z,
               ", ", diag.dE2Over2.x, ", ", diag.dE2Over2.y, ", ", diag.dE2Over2.z,
               ", ", diag.mv2Over2.x, ", ", diag.mv2Over2.y, ", ", diag.mv2Over2.z);
        //
        // species energy
        //
        for (long i = 0; i < Ns; ++i) {
            printo(os,
                   ", ", diag.msU2Over2.at(i).x, ", ", diag.msU2Over2.at(i).y, ", ", diag.msU2Over2.at(i).z,
                   ", ", diag.msv2Over2.at(i).x, ", ", diag.msv2Over2.at(i).y, ", ", diag.msv2Over2.at(i).z);
        }
        //
        // eol
        //
        printo(os, "\n");
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}
