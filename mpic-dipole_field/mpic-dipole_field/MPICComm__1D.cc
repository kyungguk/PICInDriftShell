//
//  MPICComm__1D.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 6/30/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#include "MPICComm__1D.h"
#include "MPICComm__ND.hh"
#include <stdexcept>
#include <string>

constexpr long ND = 1;

// MARK:- MPIC::__1_::InterProcessComm<1D>
//
MPIC::__1_::InterProcessComm<ND>::InterProcessComm(MPK::Comm &&tmp_comm, size_vector_type const domain_decomposition)
: _InterProcessComm{std::move(tmp_comm)}
{
    // domain topology
    //
    if (_comm.size() <= 0 || UTL::reduce_prod(domain_decomposition) != _comm.size()) {
        throw std::invalid_argument{std::string{__PRETTY_FUNCTION__} + " - # of mpi processes and domain topology inconsistency"};
    }
    _size = domain_decomposition;
    _rank = {_comm.rank()};

    // connect x-boundaries
    //
    {
        long const
        first= 0,
        last = _size.x - 1,
        prev = _rank.x - 1,
        next = _rank.x + 1;
        *_LB.x = int(prev < first ? last : prev);
        *_RB.x = int(next > last ? first : next);
    }

    // dimensional sub comms
    //
    (_dim_comms.x = _comm.split(int(0), int(_rank.x))).set_label("x-dim comm group"); // grouping of processors whose rank.x changes
}
