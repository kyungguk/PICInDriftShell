//
//  MPICDelegate__1D.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 4/1/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPICDelegate__1D_h
#define MPICDelegate__1D_h

#include "MPICDelegate__ND.h"
#include "MPICComm__1D.h"

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- MPIC::__1_::PICDelegate<ND>
    //
    template <>
    class PICDelegate<1> : public _PICDelegate<1> {
    public:
        // types
        //
        enum BC : long { periodic = 0, reflective = 1 };
        using boundary_type = UTL::Vector<BC, ND>;
        using size_vector_type = PIC::ParameterSet<ND>::size_vector_type;
        using position_type = PIC::ParameterSet<ND>::position_type;
        using vector_type = PIC::CoordSystem::vector_type;

        template <class... Args>
        static PIC::CurviCoord curvi(Args&&... args) noexcept {
            return PIC::CurviCoord{std::forward<Args>(args)...};
        }

    private:
        PIC::ParameterSet<ND> _params;
        PIC::WaveDamper<ND> _damper;
        boundary_type _bc;

    public:
        ~PICDelegate();

        /**
         @brief Construct delegate.
         */
        explicit PICDelegate(MPK::Comm &&comm, size_vector_type const domain_decomposition, PIC::ParameterSet<ND> const &params, PIC::WaveDamper<ND> const &damper, boundary_type const &bc);

        /**
         @brief Initialize domain for this mpi process.
         @discussion Given the parameters for the entire simulation domain, this call construct subdomain for this mpi process based on the process topology.
         @param dt Integration time step.
         @param use_metric_table Flag to use tabulated metrics to geometry calculations.
         */
        void init_domain(double const dt, bool const use_metric_table);

        /**
         @brief Access external source object.
         */
        PIC::Source<ND> const &source() const noexcept { return *_src; }
        PIC::Source<ND>       &source()       noexcept { return *_src; }

        /**
         @brief Parameter set in a global scope.
         */
        PIC::ParameterSet<ND> const &global_params() const noexcept { return _params; }
        /**
         @brief Wave damper in a global scope.
         */
        PIC::WaveDamper<ND> const &global_damper() const noexcept { return _damper; }

        /**
         @brief Boundary conditions.
         */
        template <Direction dir>
        BC const &bc() const noexcept { return std::get<dir>(_bc); }

        // update
        // called by multiple threads
        //
        void advance_by(PIC::Domain<ND> *domain, long const n_steps);

    private:
        // called by multiple threads
        //
        void once(PIC::Domain<ND> *domain) override;

        // communications
        // called by masther thread only
        //
        void pass(PIC::Domain<ND> const *, PIC::BField<ND> &bfield) override;
        void pass(PIC::Domain<ND> const *, PIC::EField<ND> &efield) override;
        void pass(PIC::Domain<ND> const *, PIC::Current<ND> &current) override;
        void gather(PIC::Domain<ND> const *, PIC::Current<ND> &current) override;
        void gather(PIC::Domain<ND> const *, PIC::Species<ND> &sp) override;
        void pass(PIC::Domain<ND> const *, PIC::Species<ND> &sp) override;
        void pass(PIC::Domain<ND> const*, Direction const, species_type::bucket_type &L_bucket, species_type::bucket_type &R_bucket) override;

        // called by multiple threads
        //
        void partition(PIC::Species<ND>&, Direction const, species_type::bucket_type &L_bucket, species_type::bucket_type &R_bucket) override;

    private:
        UTL::DynamicArray<PIC::Particle<ND>> spill_bucket{};
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICDelegate__1D_h */
