//
//  MPICDelegate__ND.hh
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/14/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDelegate__ND_hh
#define MPICDelegate__ND_hh

#include "MPICDelegate__ND.h"
#include "MPICComm__ND.hh"

#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>

// MARK:- MPIC::__1_::_PICDelegate<ND>
//
template <long ND>
MPIC::__1_::_PICDelegate<ND>::_PICDelegate(MPK::Comm &&comm, UTL::Vector<long, ND> const domain_decomposition)
: _comm{std::move(comm), domain_decomposition} {
}

template <long ND>
template <long i>
void MPIC::__1_::_PICDelegate<ND>::_partition(PIC::Species<ND>& sp, UTL::DynamicArray<PIC::Particle<ND>> &L_bucket, UTL::DynamicArray<PIC::Particle<ND>> &R_bucket)
{
    static_assert(i >= 0 && i < ND, "invalid index");
    auto &sp_bucket = sp.bucket();

    // group particles that have crossed left boundary
    //
    {
        auto const q_min = sp.params().ptl_q_min();
        auto it = std::partition(sp_bucket.begin(), sp_bucket.end(), [q_min](PIC::Particle<ND> const &ptl) noexcept->bool {
            return std::get<i>(ptl.pos) >= std::get<i>(q_min);
        });
        std::move(it, sp_bucket.end(), std::back_inserter(L_bucket));
        sp_bucket.erase(it, sp_bucket.end());
    }

    // group particles that have crossed right boundary
    //
    {
        auto const q_max = sp.params().ptl_q_max();
        auto it = std::partition(sp_bucket.begin(), sp_bucket.end(), [q_max](PIC::Particle<ND> const &ptl) noexcept->bool {
            return std::get<i>(ptl.pos) < std::get<i>(q_max);
        });
        std::move(it, sp_bucket.end(), std::back_inserter(R_bucket));
        sp_bucket.erase(it, sp_bucket.end());
    }
}

template <long ND>
template <long dir>
void MPIC::__1_::_PICDelegate<ND>::_particle_pass_periodic(PIC::ParameterSet<ND> const &params, UTL::DynamicArray<PIC::Particle<ND>> &L_bucket, UTL::DynamicArray<PIC::Particle<ND>> &R_bucket) const
{
    constexpr long i = dir;
    static_assert(i >= 0 && i < ND, "invalid index");
    auto const& cs = params.coord_system();
    auto const& q_len = simulation_size();
    //
    // note that velocity is in cartesian coordinates
    // pass mfa velocity components through the boundaries
    //
    if (comm().template is_lower_boundary<dir>()) {
        for (auto &ptl : L_bucket) { // lower boundary
            { // pre-pass:
                auto const &e = cs.template mfa_basis<0>(PIC::CurviCoord{ptl.pos, nullptr});
                ptl.vel = { UTL::reduce_plus(ptl.vel*e.x), UTL::reduce_plus(ptl.vel*e.y), UTL::reduce_plus(ptl.vel*e.z) };
            }
            // pass:
            std::get<i>(ptl.pos) += std::get<i>(q_len);
            { // post-pass:
                auto const &e = cs.template mfa_basis<0>(PIC::CurviCoord{ptl.pos, nullptr});
                ptl.vel = ptl.vel.x*e.x + ptl.vel.y*e.y + ptl.vel.z*e.z;
            }
        }
    }
    if (comm().template is_upper_boundary<dir>()) {
        for (auto &ptl : R_bucket) { // upper boundary
            { // pre-pass:
                auto const &e = cs.template mfa_basis<0>(PIC::CurviCoord{ptl.pos, nullptr});
                ptl.vel = { UTL::reduce_plus(ptl.vel*e.x), UTL::reduce_plus(ptl.vel*e.y), UTL::reduce_plus(ptl.vel*e.z) };
            }
            // pass:
            std::get<i>(ptl.pos) -= std::get<i>(q_len);
            { // post-pass:
                auto const &e = cs.template mfa_basis<0>(PIC::CurviCoord{ptl.pos, nullptr});
                ptl.vel = ptl.vel.x*e.x + ptl.vel.y*e.y + ptl.vel.z*e.z;
            }
        }
    }
}
template <long ND>
template <long dir>
void MPIC::__1_::_PICDelegate<ND>::_particle_pass_reflect(PIC::ParameterSet<ND> const &params, UTL::DynamicArray<PIC::Particle<ND>> &L_bucket, UTL::DynamicArray<PIC::Particle<ND>> &R_bucket) const
{
    constexpr long i = dir;
    static_assert(i >= 0 && i < ND, "invalid index");
    auto const& cs = params.coord_system();
    auto const &q_min = params.ptl_q_min();
    auto const &q_max = params.ptl_q_max();
    //
    // note that velocity is in cartesian coordinates
    // co/contra-variant basis vectors are obtained using mid-point of position before and after reflection
    //
    constexpr long e1 = PIC::Delegate<ND>::q2; // parallel coordinate identifier
    constexpr long sign = i == e1 ? 1 /*parallel reflect: v2 - v1*/ : -1 /*perpendicular reflect: v1 - v2*/;
    //
    if (comm().template is_lower_boundary<dir>()) {
        for (auto &ptl : L_bucket) { // lower boundary
            // position:
            PIC::CurviCoord curvi{ptl.pos, nullptr};
            std::get<i>(ptl.pos) += 2*(std::get<i>(q_min) - std::get<i>(ptl.pos));
            (std::get<i>(curvi) += std::get<i>(ptl.pos)) *= .5; // choose middle point
            // velocity:
            auto &vel = ptl.vel, v1 = cs.template mfa_basis<1>(curvi); // local unit vector parallel to the field line
            v1 *= UTL::reduce_plus(v1 * vel); // parallel component of velocity
            vel -= v1 *= 2; // v2 - v1
            vel *= sign;
        }
    }
    if (comm().template is_upper_boundary<dir>()) {
        for (auto &ptl : R_bucket) { // upper boundary
            // position:
            PIC::CurviCoord curvi{ptl.pos, nullptr};
            std::get<i>(ptl.pos) += 2*(std::get<i>(q_max) - std::get<i>(ptl.pos));
            (std::get<i>(curvi) += std::get<i>(ptl.pos)) *= .5; // choose middle point
            // velocity:
            auto &vel = ptl.vel, v1 = cs.template mfa_basis<1>(curvi); // local unit vector parallel to the field line
            v1 *= UTL::reduce_plus(v1 * vel); // parallel component of velocity
            vel -= v1 *= 2; // v2 - v1
            vel *= sign;
        }
    }
}

#endif /* MPICDelegate__ND_hh */
