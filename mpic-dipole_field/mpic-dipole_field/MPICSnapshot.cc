//
//  MPICSnapshot.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/14/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICSnapshot.h"

#include "MPICDelegate.h"
#include <MPIKit/MPIKit.h>
#include <type_traits>
#include <algorithm>
#include <stdexcept>
#include <string>

MPIC::Snapshot::Snapshot(char const *output_directory, Json::Value const json)
: _output_directory{output_directory}
, _filename_prefix{json.get("filename_prefix", "snapshot").asString()}
, _should_load{json.get("load", false).asBool()}
, _should_save{json.get("save", false).asBool()} {
}
//
template <class H5Object, class T>
void MPIC::Snapshot::write_attr(H5Object &obj, const char *name, T const &s)
{
    static_assert(std::is_arithmetic<T>::value, "type T is not a scalar type");
    obj.attribute(name, H5::Type::native<T>(), H5::Space{nullptr}).write(&s);
}
template <class H5Object, class T, long S>
void MPIC::Snapshot::write_attr(H5Object &obj, const char *name, UTL::Vector<T, S> const &v)
{
    static_assert(std::is_arithmetic<T>::value, "type T is not a scalar type");
    obj.attribute(name, H5::Type::native<T>(), H5::Space{{S}}).write(v.data());
}
template <class H5Object, class T, long S>
void MPIC::Snapshot::write_attr(H5Object &obj, const char *name, UTL::Vector<UTL::Vector<T, S>, S> const &m)
{
    static_assert(std::is_arithmetic<T>::value, "type T is not a scalar type");
    obj.attribute(name, H5::Type::native<T>(), H5::Space{{S, S}}).write(m.data()->data());
}
//
void MPIC::Snapshot::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    save(delegate, delegate.identifier);
}
void MPIC::Snapshot::operator>>(__1_::PICDelegate<1L> &delegate) const
{
    load(delegate, delegate.identifier);
}

void MPIC::Snapshot::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    save(delegate, delegate.identifier);
}
void MPIC::Snapshot::operator>>(__1_::PICDelegate<2L> &delegate) const
{
    load(delegate, delegate.identifier);
}

void MPIC::Snapshot::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    save(delegate, delegate.identifier);
}
void MPIC::Snapshot::operator>>(__1_::PICDelegate<3L> &delegate) const
{
    load(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::Snapshot::save(Delegate const &delegate, Identifier::Any)
{
    constexpr const char *gprefix = "DipoleB-";
    try {
        // create hdf5 file
        //
        std::string const fpath = _output_directory + "/" + _filename_prefix + "-pid_" + std::to_string(delegate.comm()->rank()) + ".h5";
        H5::File file{H5::File::Trunc{}, fpath.c_str()};

        // iterate over domains
        //
        for (long i = 0; i < delegate.domains().size(); ++i) {
            auto const &domain = delegate.domains()[i];

            // create thread-specific root group
            //
            std::string const gname = std::string{gprefix} + std::to_string(i);
            H5::Group root = file.group(gname.c_str(), H5::PList{}, H5::PList{});

            // attributes
            //
            save_attr(root, delegate, Delegate::identifier);

            // B field : contr
            //
            save_bfield(root, domain, Delegate::identifier);

            // E field : contr
            //
            save_efield(root, domain, Delegate::identifier);

            // particles
            //
            save_particles(root, domain, Delegate::identifier);

            // cold fluids
            //
            save_cold_fluids(root, domain, Delegate::identifier);

            // flush
            //
            root.flush();
        }
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}
template <class Delegate>
void MPIC::Snapshot::load(Delegate &delegate, Identifier::Any) const
{
    constexpr const char *gprefix = "DipoleB-";
    try {
        // open hdf5 file
        //
        std::string const fpath = _output_directory + "/" + _filename_prefix + "-pid_" + std::to_string(delegate.comm()->rank()) + ".h5";
        H5::File const file{H5::File::Rdonly{}, fpath.c_str()};

        // iterate over domains
        //
        for (long i = 0; i < delegate.domains().size(); ++i) {
            auto &domain = delegate.domains()[i];

            // open thread-specific root group
            //
            std::string const gname = std::string{gprefix} + std::to_string(i);
            H5::Group const root = file.group(gname.c_str());

            // step count
            //
            if (0 == i) {
                long cur_step;
                root.attribute("cur_step").read(&cur_step);
                delegate.set_cur_step(cur_step);
            }

            // B field : contr
            //
            load_bfield(domain, root, Delegate::identifier);

            // E field : contr
            //
            load_efield(domain, root, Delegate::identifier);

            // particles
            //
            load_particles(domain, root, Delegate::identifier);

            // cold fluid
            //
            load_cold_fluids(domain, root, Delegate::identifier);
        }
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

template <class Domain>
void MPIC::Snapshot::save_bfield(H5::Group &root, Domain const &domain, Identifier::Any)
{
    // B field : contr
    //
    using bfield_type = typename Domain::bfield_type;
    using magnetic_field_type = typename bfield_type::magnetic_field_type;
    constexpr long vector_size = magnetic_field_type::value_type::size();
    static_assert(vector_size == 3, "B field vector size inconsistency");
    //
    magnetic_field_type const &Bcontr = domain.bfield()->contr;
    auto const N = Bcontr.max_dims().append(vector_size);
    H5::Space::size_array_type const dims{N.begin(), N.end()};
    H5::Space space{dims};
    //
    space.select_all();
    root.dataset("B", H5::Type::native<double>(), space).write(space, data(Bcontr), space);
}
template <class Domain>
void MPIC::Snapshot::load_bfield(Domain &domain, H5::Group const &root, Identifier::Any) const
{
    // B field : contr
    //
    using bfield_type = typename Domain::bfield_type;
    using magnetic_field_type = typename bfield_type::magnetic_field_type;
    constexpr long vector_size = magnetic_field_type::value_type::size();
    static_assert(vector_size == 3, "B field vector size inconsistency");
    //
    magnetic_field_type &Bcontr = domain.bfield()->contr;
    auto const N = Bcontr.max_dims().append(vector_size);
    H5::Space::size_array_type const mdims{N.begin(), N.end()};
    //
    H5::Dataset const dset = root.dataset("B");
    H5::Space space = dset.space();
    H5::Space::size_array_type const fdims = space.simple_extent().first;
    if (fdims.size() != mdims.size() || !std::equal(fdims.begin(), fdims.end(), mdims.begin())) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - B field data space inconsistency");
    }
    //
    space.select_all();
    dset.read(space, data(Bcontr), space);
}

template <class Domain>
void MPIC::Snapshot::save_efield(H5::Group &root, Domain const &domain, Identifier::Any)
{
    // E field : contr
    //
    using efield_type = typename Domain::efield_type;
    using electric_field_type = typename efield_type::electric_field_type;
    constexpr long vector_size = electric_field_type::value_type::size();
    static_assert(vector_size == 3, "E field vector size inconsistency");
    //
    electric_field_type const &Econtr = domain.efield()->contr;
    auto const N = Econtr.max_dims().append(vector_size);
    H5::Space::size_array_type const dims{N.begin(), N.end()};
    H5::Space space{dims};
    //
    space.select_all();
    root.dataset("E", H5::Type::native<double>(), space).write(space, data(Econtr), space);
}
template <class Domain>
void MPIC::Snapshot::load_efield(Domain &domain, H5::Group const &root, Identifier::Any) const
{
    // E field : contr
    //
    using efield_type = typename Domain::efield_type;
    using electric_field_type = typename efield_type::electric_field_type;
    constexpr long vector_size = electric_field_type::value_type::size();
    static_assert(vector_size == 3, "E field vector size inconsistency");
    //
    electric_field_type &Econtr = domain.efield()->contr;
    auto const N = Econtr.max_dims().append(vector_size);
    H5::Space::size_array_type const mdims{N.begin(), N.end()};
    //
    H5::Dataset const dset = root.dataset("E");
    H5::Space space = dset.space();
    H5::Space::size_array_type const fdims = space.simple_extent().first;
    if (fdims.size() != mdims.size() || !std::equal(fdims.begin(), fdims.end(), mdims.begin())) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - E field data space inconsistency");
    }
    //
    space.select_all();
    dset.read(space, data(Econtr), space);
}

template <class Domain>
void MPIC::Snapshot::save_particles(H5::Group &_root, Domain const &domain, Identifier::Any)
{
    using species_type = typename Domain::species_type;
    //
    // create group and write Nspecies attribute
    //
    H5::Group root = _root.group("particles", H5::PList{}, H5::PList{});
    long const Ns = domain.species().size();
    write_attr(root, "Nspecies", Ns);
    //
    // enumerate species
    //
    for (long i = 0; i < Ns; ++i) {
        species_type const &sp = domain.species().at(i);
        // write particles
        //
        {
            std::string const name = std::string{"sp_"} + std::to_string(i);
            //
            using particle_type = typename species_type::particle_type;
            static_assert(0 == sizeof(particle_type)%sizeof(double), "sizeof(particle_type) is not divisible by sizeof(double)");
            constexpr long particle_size = sizeof(particle_type)/sizeof(double);
            //
            H5::Space::size_array_type const dims = {
                static_cast<unsigned long>(sp.bucket().size()),
                particle_size
            };
            H5::Space space{dims};
            //
            space.select_all();
            H5::Dataset dset = root.dataset(name.c_str(), H5::Type::native<double>(), space);
            dset.write(space, reinterpret_cast<double const *>(sp.bucket().data()), space);
            //
            write_attr(dset, "Oc0", sp.Oc0ref());
            write_attr(dset, "op0", sp.op0ref());
            write_attr(dset, "Nc0", sp.Nc0ref());
            write_attr(dset, "shape_order", sp.shape_order().v);
            write_attr(dset, "number_of_smoothing_passes", sp.number_of_smoothing_passes().v);
            write_attr(dset, "current_density_conversion_factor", sp.current_density_conversion_factor());
            write_attr(dset, "charge_density_conversion_factor", sp.charge_density_conversion_factor());
            write_attr(dset, "energy_density_conversion_factor", sp.energy_density_conversion_factor());
        }
        // write force balancing moment<1>
        //
        {
            std::string const name = std::string{"equilibrium_nV_"} + std::to_string(i);
            //
            using vector_moment_type = typename species_type::vector_moment_type;
            constexpr long vector_size = vector_moment_type::value_type::size();
            static_assert(vector_size == 3, "vector size inconsistency");
            //
            vector_moment_type const &V = sp.force_balancing_moment1();
            auto const N = V.max_dims().append(vector_size);
            H5::Space::size_array_type const dims{N.begin(), N.end()};
            H5::Space space{dims};
            //
            space.select_all();
            H5::Dataset dset = root.dataset(name.c_str(), H5::Type::native<double>(), space);
            dset.write(space, data(V), space);
            //
            write_attr(dset, "Oc0", sp.Oc0ref());
            write_attr(dset, "op0", sp.op0ref());
            write_attr(dset, "Nc0", sp.Nc0ref());
            write_attr(dset, "shape_order", sp.shape_order().v);
            write_attr(dset, "number_of_smoothing_passes", sp.number_of_smoothing_passes().v);
            write_attr(dset, "current_density_conversion_factor", sp.current_density_conversion_factor());
            write_attr(dset, "charge_density_conversion_factor", sp.charge_density_conversion_factor());
            write_attr(dset, "energy_density_conversion_factor", sp.energy_density_conversion_factor());
        }
    }
}
template <class Domain>
void MPIC::Snapshot::load_particles(Domain &domain, H5::Group const &_root, Identifier::Any) const
{
    using species_type = typename Domain::species_type;
    //
    // open group and read in Nspecies attribute
    //
    H5::Group const root = _root.group("particles");
    long const Ns = [&root](long Ns = 0) {
        return (void)root.attribute("Nspecies").read(&Ns), Ns;
    }();
    //
    // enumerate species
    //
    for (long i = 0; i < Ns; ++i) {
        species_type &sp = domain.add_species();
        // read particles
        //
        {
            std::string const name = std::string{"sp_"} + std::to_string(i);
            //
            using particle_type = typename species_type::particle_type;
            static_assert(0 == sizeof(particle_type)%sizeof(double), "sizeof(particle_type) is not divisible by sizeof(double)");
            constexpr long particle_size = sizeof(particle_type)/sizeof(double);
            //
            H5::Dataset const dset = root.dataset(name.c_str());
            H5::Space space = dset.space();
            //
            H5::Space::size_array_type const fdims = space.simple_extent().first;
            if (fdims.size() != 2 || fdims.at(1) != particle_size) {
                throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - particle data space inconsistency");
            }
            //
            space.select_all();
            sp.bucket().resize(static_cast<long>(fdims.at(0)));
            dset.read(space, reinterpret_cast<double *>(sp.bucket().data()), space);
            //
            double Oc0, op0, Nc0;
            dset.attribute("Oc0").read(&Oc0);
            dset.attribute("op0").read(&op0);
            dset.attribute("Nc0").read(&Nc0);
            auto shape_order = sp.shape_order();
            dset.attribute("shape_order").read(&shape_order.v);
            auto n_smooth = sp.number_of_smoothing_passes();
            dset.attribute("number_of_smoothing_passes").read(&n_smooth.v);
            sp.set_properties(Oc0, op0, Nc0, shape_order, n_smooth);
        }
        // read force balancing moment<1>
        //
        {
            std::string const name = std::string{"equilibrium_nV_"} + std::to_string(i);
            //
            using vector_moment_type = typename species_type::vector_moment_type;
            constexpr long vector_size = vector_moment_type::value_type::size();
            static_assert(vector_size == 3, "vector size inconsistency");
            //
            vector_moment_type &V = sp.force_balancing_moment1();
            auto const N = V.max_dims().append(vector_size);
            H5::Space::size_array_type const mdims{N.begin(), N.end()};
            //
            H5::Dataset const dset = root.dataset(name.c_str());
            H5::Space space = dset.space();
            //
            H5::Space::size_array_type const fdims = space.simple_extent().first;
            if (fdims.size() != mdims.size() || !std::equal(fdims.begin(), fdims.end(), mdims.begin())) {
                throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - moment<1> data space inconsistency");
            }
            //
            space.select_all();
            dset.read(space, data(V), space);
        }
    }
}

template <class Domain>
void MPIC::Snapshot::save_cold_fluids(H5::Group &_root, Domain const &domain, Identifier::Any)
{
    using cold_fluid_type = typename Domain::cold_fluid_type;
    //
    // create group and write Nspecies attribute
    //
    H5::Group root = _root.group("cold_fluids", H5::PList{}, H5::PList{});
    long const Ns = domain.cold_fluids().size();
    write_attr(root, "Nspecies", Ns);
    //
    // enumerate cold fluids
    //
    for (long i = 0; i < Ns; ++i) {
        cold_fluid_type const &cf = domain.cold_fluids().at(i);
        // write moment<0>
        //
        {
            using scalar_moment_type = typename cold_fluid_type::scalar_moment_type;
            constexpr long scalar_size = scalar_moment_type::value_type::size();
            static_assert(scalar_size == 1, "scalar size inconsistency");
            //
            std::string const name = std::string{"n_"} + std::to_string(i);
            scalar_moment_type const &n = cf.template moment<0>();
            auto const N = n.max_dims().append(scalar_size);
            H5::Space::size_array_type const dims{N.begin(), N.end()};
            H5::Space space{dims};
            //
            space.select_all();
            H5::Dataset dset = root.dataset(name.c_str(), H5::Type::native<double>(), space);
            dset.write(space, data(n), space);
            //
            write_attr(dset, "Oc0", cf.Oc0ref());
            write_attr(dset, "op0", cf.op0ref());
            write_attr(dset, "number_of_smoothing_passes", cf.number_of_smoothing_passes().v);
            write_attr(dset, "current_density_conversion_factor", cf.current_density_conversion_factor());
            write_attr(dset, "charge_density_conversion_factor", cf.charge_density_conversion_factor());
            write_attr(dset, "energy_density_conversion_factor", cf.energy_density_conversion_factor());
        }
        // write moment<1>
        //
        {
            using vector_moment_type = typename cold_fluid_type::vector_moment_type;
            constexpr long vector_size = vector_moment_type::value_type::size();
            static_assert(vector_size == 3, "vector size inconsistency");
            //
            std::string const name = std::string{"nV_"} + std::to_string(i);
            vector_moment_type const &V = cf.template moment<1>();
            auto const N = V.max_dims().append(vector_size);
            H5::Space::size_array_type const dims{N.begin(), N.end()};
            H5::Space space{dims};
            //
            space.select_all();
            H5::Dataset dset = root.dataset(name.c_str(), H5::Type::native<double>(), space);
            dset.write(space, data(V), space);
            //
            write_attr(dset, "Oc0", cf.Oc0ref());
            write_attr(dset, "op0", cf.op0ref());
            write_attr(dset, "number_of_smoothing_passes", cf.number_of_smoothing_passes().v);
            write_attr(dset, "current_density_conversion_factor", cf.current_density_conversion_factor());
            write_attr(dset, "charge_density_conversion_factor", cf.charge_density_conversion_factor());
            write_attr(dset, "energy_density_conversion_factor", cf.energy_density_conversion_factor());
        }
        // write moment<2>
        //
        {
            using tensor_moment_type = typename cold_fluid_type::tensor_moment_type;
            constexpr long tensor_size = tensor_moment_type::value_type::size();
            static_assert(tensor_size == 6, "tensor size inconsistency");
            //
            std::string const name = std::string{"nvv_"} + std::to_string(i);
            tensor_moment_type const &vv = cf.template moment<2>();
            auto const N = vv.max_dims().append(tensor_size);
            H5::Space::size_array_type const dims{N.begin(), N.end()};
            H5::Space space{dims};
            //
            space.select_all();
            H5::Dataset dset = root.dataset(name.c_str(), H5::Type::native<double>(), space);
            dset.write(space, data(vv), space);
            //
            write_attr(dset, "Oc0", cf.Oc0ref());
            write_attr(dset, "op0", cf.op0ref());
            write_attr(dset, "number_of_smoothing_passes", cf.number_of_smoothing_passes().v);
            write_attr(dset, "current_density_conversion_factor", cf.current_density_conversion_factor());
            write_attr(dset, "charge_density_conversion_factor", cf.charge_density_conversion_factor());
            write_attr(dset, "energy_density_conversion_factor", cf.energy_density_conversion_factor());
        }
    }
}
template <class Domain>
void MPIC::Snapshot::load_cold_fluids(Domain &domain, H5::Group const &_root, Identifier::Any) const
{
    using cold_fluid_type = typename Domain::cold_fluid_type;
    //
    // open group and read in Nspecies attribute
    //
    H5::Group const root = _root.group("cold_fluids");
    long const Ns = [&root](long Ns = 0) {
        return (void)root.attribute("Nspecies").read(&Ns), Ns;
    }();
    //
    // enumerate cold fluids
    //
    for (long i = 0; i < Ns; ++i) {
        cold_fluid_type &cf = domain.add_cold_fluid();
        // read moment<0>
        //
        {
            using scalar_moment_type = typename cold_fluid_type::scalar_moment_type;
            constexpr long scalar_size = scalar_moment_type::value_type::size();
            static_assert(scalar_size == 1, "scalar size inconsistency");
            //
            std::string const name = std::string{"n_"} + std::to_string(i);
            scalar_moment_type &n = cf.template moment<0>();
            auto const N = n.max_dims().append(scalar_size);
            H5::Space::size_array_type const mdims{N.begin(), N.end()};
            //
            H5::Dataset const dset = root.dataset(name.c_str());
            H5::Space space = dset.space();
            //
            H5::Space::size_array_type const fdims = space.simple_extent().first;
            if (fdims.size() != mdims.size() || !std::equal(fdims.begin(), fdims.end(), mdims.begin())) {
                throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - moment<0> data space inconsistency");
            }
            //
            space.select_all();
            dset.read(space, data(n), space);
            //
            double Oc0, op0;
            dset.attribute("Oc0").read(&Oc0);
            dset.attribute("op0").read(&op0);
            auto n_smooth = cf.number_of_smoothing_passes();
            dset.attribute("number_of_smoothing_passes").read(&n_smooth.v);
            cf.set_properties(Oc0, op0, n_smooth);
        }
        // read moment<1>
        //
        {
            using vector_moment_type = typename cold_fluid_type::vector_moment_type;
            constexpr long vector_size = vector_moment_type::value_type::size();
            static_assert(vector_size == 3, "vector size inconsistency");
            //
            std::string const name = std::string{"nV_"} + std::to_string(i);
            vector_moment_type &V = cf.template moment<1>();
            auto const N = V.max_dims().append(vector_size);
            H5::Space::size_array_type const mdims{N.begin(), N.end()};
            //
            H5::Dataset const dset = root.dataset(name.c_str());
            H5::Space space = dset.space();
            //
            H5::Space::size_array_type const fdims = space.simple_extent().first;
            if (fdims.size() != mdims.size() || !std::equal(fdims.begin(), fdims.end(), mdims.begin())) {
                throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - moment<1> data space inconsistency");
            }
            //
            space.select_all();
            dset.read(space, data(V), space);
            //
            double Oc0, op0;
            dset.attribute("Oc0").read(&Oc0);
            dset.attribute("op0").read(&op0);
            auto n_smooth = cf.number_of_smoothing_passes();
            dset.attribute("number_of_smoothing_passes").read(&n_smooth.v);
            cf.set_properties(Oc0, op0, n_smooth);
        }
        // read moment<2>
        //
        {
            using tensor_moment_type = typename cold_fluid_type::tensor_moment_type;
            constexpr long tensor_size = tensor_moment_type::value_type::size();
            static_assert(tensor_size == 6, "tensor size inconsistency");
            //
            std::string const name = std::string{"nvv_"} + std::to_string(i);
            tensor_moment_type &vv = cf.template moment<2>();
            auto const N = vv.max_dims().append(tensor_size);
            H5::Space::size_array_type const mdims{N.begin(), N.end()};
            //
            H5::Dataset const dset = root.dataset(name.c_str());
            H5::Space space = dset.space();
            //
            H5::Space::size_array_type const fdims = space.simple_extent().first;
            if (fdims.size() != mdims.size() || !std::equal(fdims.begin(), fdims.end(), mdims.begin())) {
                throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - moment<2> data space inconsistency");
            }
            //
            space.select_all();
            dset.read(space, data(vv), space);
            //
            double Oc0, op0;
            dset.attribute("Oc0").read(&Oc0);
            dset.attribute("op0").read(&op0);
            auto n_smooth = cf.number_of_smoothing_passes();
            dset.attribute("number_of_smoothing_passes").read(&n_smooth.v);
            cf.set_properties(Oc0, op0, n_smooth);
        }
    }
}

template <class Delegate>
void MPIC::Snapshot::save_attr(H5::Group &obj, Delegate const &delegate, Identifier::PIC<1>)
{
    constexpr long ND = std::decay<decltype(delegate)>::type::ND;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    auto const &damper = domain.damper();
    //
    write_attr(obj, "simulation_size", delegate.simulation_size());
    write_attr(obj, "process_rank", delegate.process_rank());
    write_attr(obj, "process_size", delegate.process_size());
    write_attr(obj, "subdomain_offset", delegate.domain_offset());
    write_attr(obj, "subdomain_size", params.N());
    write_attr(obj, "ND", ND);
    write_attr(obj, "c", params.c());
    write_attr(obj, "O0", params.O0ref());
    write_attr(obj, "cur_step", delegate.cur_step());
    write_attr(obj, "dt", domain.dt());
    write_attr(obj, "time", delegate.time());
    write_attr(obj, "r0ref", params.coord_system().r0ref());
    write_attr(obj, "q3ref", params.coord_system().q3ref());
    write_attr(obj, "D", params.coord_system().D());
    write_attr(obj, "grid_type", params.coord_system().n());
    write_attr(obj, "subdomain_grid_q_min", params.grid_q_min());
    write_attr(obj, "subdomain_grid_q_max", params.grid_q_max());
    write_attr(obj, "subdomain_ptl_q_min", params.ptl_q_min());
    write_attr(obj, "subdomain_ptl_q_max", params.ptl_q_max());
    write_attr(obj, "simulation_grid_q_min", delegate.global_params().grid_q_min());
    write_attr(obj, "simulation_grid_q_max", delegate.global_params().grid_q_max());
    write_attr(obj, "simulation_ptl_q_min", delegate.global_params().ptl_q_min());
    write_attr(obj, "simulation_ptl_q_max", delegate.global_params().ptl_q_max());
    write_attr(obj, "damper.masking_inset", damper.masking_inset());
    write_attr(obj, "damper.amplitude_damping", damper.amplitude_damping());
    write_attr(obj, "damper.phase_retardation", damper.phase_retardation());
    write_attr(obj, "grid_strategy", long{params.grid_strategy()});
    write_attr(obj, "use_metric_table", long{params.use_metric_table()});
    write_attr(obj, "enforce_force_balance", long{params.enforce_force_balance()});
    write_attr(obj, "number_of_worker_threads", long{params.number_of_worker_threads()});
    write_attr(obj, "Orot", params.Orot());
}
