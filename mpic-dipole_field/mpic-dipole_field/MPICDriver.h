//
//  MPICDriver.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 2/17/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef MPICDriver_h
#define MPICDriver_h

#include "MPIC_Driver.h"

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- MPIC::__1_::PICDriver
    //
    class PICDriver : public _Driver {
    public:
        explicit PICDriver(MPK::Comm &&world, JsonProxy &&opts) : _Driver{std::move(world), std::move(opts)} {}

        void operator()() const override;

    private:
        template <long ND>
        void run(std::unique_ptr<PICDelegate<ND>> = {}) const;

        template <long ND>
        void init_delegate(std::unique_ptr<PICDelegate<ND>> &delegate) const;
        template <long ND>
        void init_global(PIC::ParameterSet<ND> &params, PIC::WaveDamper<ND> &damper) const;

        template <long ND>
        void init_external_source(PICDelegate<ND> &delegate, JsonProxy const &js_src) const;
        template <long ND>
        void init_particles(PICDelegate<ND> &delegate, JsonProxy const &js_ptls) const;
        template <long ND>
        void init_cold_fluids(PICDelegate<ND> &delegate, JsonProxy const &js_fluids) const;
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICDriver_h */
