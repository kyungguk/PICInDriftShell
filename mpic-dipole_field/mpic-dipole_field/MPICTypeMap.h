//
//  MPICTypeMap.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 9/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPICTypeMap_h
#define MPICTypeMap_h

#include "mpic-config.h"

#include <PICKit/PICKit.h>
#include <GeometryKit/GeometryKit.h>
#include <MPIKit/MPIKit.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <tuple>

// MARK: GEO::CurviCoord
//
namespace MPK {
    template <>
    struct TypeMap<GEO::__1_::CurviCoord> {
        using native = GEO::__1_::CurviCoord;
        using overlay = UTL::Vector<native::value_type, native::size()>;
        static_assert(alignof(overlay) == alignof(native), "incompatible alignment of overlay");
        static_assert(sizeof(overlay) == sizeof(native), "incompatible size of overlay");
        static Type value() noexcept { return make_type<overlay>(); }
    };
}

// MARK: PIC::Particle
//
namespace MPK {
    template <long ND>
    struct TypeMap<PIC::__1_::Particle<ND>> {
        using native = PIC::__1_::Particle<ND>;
        //using overlay = UTL::Vector<double, sizeof(native)/sizeof(double)>;
        using overlay = std::tuple<typename native::_Velocity, typename native::_Position>; // this version is less performant when the Type instance has to be created repeatedly
        static_assert(alignof(overlay) == alignof(native), "incompatible alignment of particle overlay");
        static_assert(sizeof(overlay) == sizeof(native), "incompatible size of particle overlay");
        static Type value() noexcept { return make_type<overlay>(); }
    };
}

#endif /* MPICTypeMap_h */
