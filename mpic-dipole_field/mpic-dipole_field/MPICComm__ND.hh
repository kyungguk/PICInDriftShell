//
//  MPICComm__ND.hh
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 6/30/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef MPICComm__ND_hh
#define MPICComm__ND_hh

#include "MPICComm__ND.h"
#include <stdexcept>
#include <string>

// MARK: Helpers
//
namespace {
    template <class T, long ND, long Pad>
    inline UTL::ArraySliceND<T, ND, Pad> &&operator+=(UTL::ArraySliceND<T, ND, Pad>&& lhs, UTL::ArrayND<T, ND, Pad> const& rhs) {
        if (UTL::reduce_bit_or(lhs.dims() != rhs.dims())) {
            throw std::invalid_argument{__PRETTY_FUNCTION__};
        }
        auto l_first = lhs.leaf_pad_begin(), l_last = lhs.leaf_pad_end();
        auto r_first = rhs.flat_array().begin();
        while (l_first != l_last) {
            *l_first++ += *r_first++;
        }
        return std::move(lhs);
    }
    template <class T, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad> &operator+=(UTL::ArrayND<T, ND, Pad>& lhs, UTL::ArraySliceND<T, ND, Pad> const& rhs) {
        if (UTL::reduce_bit_or(lhs.dims() != rhs.dims())) {
            throw std::invalid_argument{__PRETTY_FUNCTION__};
        }
        auto l_first = lhs.flat_array().begin(), l_last = lhs.flat_array().end();
        auto r_first = rhs.leaf_pad_begin();
        while (l_first != l_last) {
            *l_first++ += *r_first++;
        }
        return lhs;
    }
    template <class T, long ND, long Pad>
    inline UTL::ArrayND<T, ND, Pad> &operator/=(UTL::ArrayND<T, ND, Pad>& lhs, T const& x) { // for spatial averaging
        auto l_first = lhs.flat_array().begin(), l_last = lhs.flat_array().end();
        while (l_first != l_last) {
            *l_first++ /= x;
        }
        return lhs;
    }
}

// MARK:- MPIC::__1_::_InterProcessComm<ND>
//
template <long _ND>
MPIC::__1_::_InterProcessComm<_ND>::_InterProcessComm(MPK::Comm &&comm)
: _comm{std::move(comm)} {
    _comm.set_label(__PRETTY_FUNCTION__);
}

template <long _ND>
template <long dim, class T>
void MPIC::__1_::_InterProcessComm<_ND>::pass(UTL::DynamicArray<T> &L_bucket, UTL::DynamicArray<T> &R_bucket, int const tag) const
{
    constexpr long i = dim;
    MPK::Tag const LR_tag{tag + 0}, RL_tag{tag + 1};
    _comm.send_recv(L_bucket, {std::get<i>(_LB), LR_tag}, {std::get<i>(_RB), LR_tag});
    _comm.send_recv(R_bucket, {std::get<i>(_RB), RL_tag}, {std::get<i>(_LB), RL_tag});
    L_bucket.swap(R_bucket);
}

template <long _ND>
template <long dim, class T, long ND, long Pad>
void MPIC::__1_::_InterProcessComm<_ND>::pass(UTL::ArrayND<T, ND, Pad> &A, int const tag, bool const is_symmetric) const
{
    constexpr long i = dim;
    static_assert(i < ND, "invalid array rank");
    constexpr long b = 0;
    long const e = A.template size<i>();
    UTL::Vector<long, ND> loc{0}, len{A.dims()};
    loc.template drop<i>() -= 1*Pad; // padding is only included >= dim
    len.template drop<i>() += 2*Pad; // padding is only included >= dim
    std::get<i>(len) = 1;
    UTL::ArrayND<T, ND, 0> LB{len}, RB{len};
    //
    for (int k = 0; k < Pad; ++k) { // inner ones first
        std::get<i>(loc) = b+0+k; // left inset
        LB = UTL::make_slice<0>(A, loc, len); // to left; note that this is MOVE assignment
        std::get<i>(loc) = e-1-k; // right inset
        RB = UTL::make_slice<0>(A, loc, len); // to right; note that this is MOVE assignment
        //
        MPK::Tag const tag_L{tag + k};
        _comm.send_recv(LB, {std::get<i>(_LB), tag_L}, {std::get<i>(_RB), tag_L});
        MPK::Tag const tag_R{tag + k + int(Pad)}; // explicit int conversion needed to suppress narrowing conversion warning
        _comm.send_recv(RB, {std::get<i>(_RB), tag_R}, {std::get<i>(_LB), tag_R});
        std::swap(LB, RB);
        //
        if (is_symmetric) {
            if (is_lower_boundary<dim>()) { // *this is left boundary
                std::get<i>(loc) = b+0+k; // left inset
                LB = UTL::make_slice<0>(A, loc, len); // note that this is MOVE assignment
            }
            if (is_upper_boundary<dim>()) { // *this is right boundary
                std::get<i>(loc) = e-1-k; // right inset
                RB = UTL::make_slice<0>(A, loc, len); // note that this is MOVE assignment
            }
        }
        //
        std::get<i>(loc) = b-1-k; // left ghost
        UTL::make_slice<0>(A, loc, len) = LB; // from left
        std::get<i>(loc) = e+0+k; // right ghost
        UTL::make_slice<0>(A, loc, len) = RB; // from right
    }
}

template <long _ND>
template <long dim, class T, long ND, long Pad>
void MPIC::__1_::_InterProcessComm<_ND>::gather(UTL::ArrayND<T, ND, Pad> &A, int const tag, bool const is_symmetric) const
{
    constexpr long i = dim;
    static_assert(i < ND, "invalid array rank");
    constexpr long b = 0;
    long const e = A.template size<i>();
    UTL::Vector<long, ND> loc{0}, len{A.dims()};
    loc.template drop<i>() -= 1*Pad; // padding is only included >= dim
    len.template drop<i>() += 2*Pad; // padding is only included >= dim
    std::get<i>(len) = 1;
    UTL::ArrayND<T, ND, 0> LB{len}, RB{len};
    //
    for (int k = Pad - 1; k >= 0; --k) { // outer ones first
        std::get<i>(loc) = b-1-k; // left ghost
        LB = UTL::make_slice<0>(A, loc, len); // to left; note that this is MOVE assignment
        std::get<i>(loc) = e+0+k; // right ghost
        RB = UTL::make_slice<0>(A, loc, len); // to right; note that this is MOVE assignment
        //
        if (is_symmetric) {
            if (is_lower_boundary<dim>()) { // *this is left boundary
                std::get<i>(loc) = b+0; // deposit to left inset
                UTL::make_slice<0>(A, loc, len) += LB;
                LB.fill(T{});
            }
            if (is_upper_boundary<dim>()) { // *this is right boundary
                std::get<i>(loc) = e-1; // deposit to right inset
                UTL::make_slice<0>(A, loc, len) += RB;
                RB.fill(T{});
            }
        }
        //
        MPK::Tag const tag_L{tag + k};
        _comm.send_recv(LB, {std::get<i>(_LB), tag_L}, {std::get<i>(_RB), tag_L});
        MPK::Tag const tag_R{tag + k + int(Pad)}; // explicit int conversion needed to suppress narrowing conversion warning
        _comm.send_recv(RB, {std::get<i>(_RB), tag_R}, {std::get<i>(_LB), tag_R});
        std::swap(LB, RB);
        //
        std::get<i>(loc) = b+0+k; // left inset
        UTL::make_slice<0>(A, loc, len) += LB; // from left
        std::get<i>(loc) = e-1-k; // right inset
        UTL::make_slice<0>(A, loc, len) += RB; // from right
    }
}

template <long _ND>
template <long dim, class T, long ND, long Pad>
void MPIC::__1_::_InterProcessComm<_ND>::dim_average(UTL::ArrayND<T, ND, Pad> &A) const
{
    constexpr long i = dim;
    static_assert(i < ND, "invalid array rank");
    long const N = A.template size<i>();
    MPK::Comm const &comm = std::get<i>(_dim_comms);
    UTL::Vector<long, ND> loc{0}, len{A.dims()};
    loc.template drop<i>() -= 1*Pad; // padding is only included >= dim
    len.template drop<i>() += 2*Pad; // padding is only included >= dim
    std::get<i>(len) = 1;
    UTL::ArrayND<T, ND, 0> B{len};
    //
    // local sum
    //
    B.fill(T{});
    for (long k = 0; k < N; ++k) {
        std::get<i>(loc) = k;
        B += UTL::make_slice<0>(A, loc, len);
    }
    //
    // global sum
    //
    reduce_plus(comm, B);
    //
    // average
    //
    B /= T(N*comm.size());
    //
    // local distribution
    //
    for (long k = 0; k < N; ++k) {
        std::get<i>(loc) = k;
        UTL::make_slice<0>(A, loc, len) = B;
    }
}

template <long _ND>
template <class T, long S, long Rank, long Pad>
void MPIC::__1_::_InterProcessComm<_ND>::reduce_plus(MPK::Comm const &comm, UTL::ArrayND<UTL::Vector<T, S>, Rank, Pad> &A)
{
    using Vector = UTL::Vector<T, S>;
    static_assert(std::is_same<Vector, typename std::decay<decltype(A)>::type::value_type>::value, "type mismatch");
    static MPK::ReduceOp const plus(true, [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *)->void {
        for (int i = 0; i < *vlen; ++i) {
            static_cast<Vector*>(inoutvec)[i] += static_cast<Vector const*>(invec)[i];
        }
    });
    comm.reduce(plus, A);
}

#endif /* MPICComm__ND_hh */
