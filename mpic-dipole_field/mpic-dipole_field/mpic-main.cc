//
//  mpic-main.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/11/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//


#include "mpic-config.h"

#include "MPICDriver.h"
#include "MPICArguments.h"
#include <MPIKit/MPIKit.h>
#include <UtilityKit/UtilityKit.h>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <string>

namespace {
    void dispatch([[maybe_unused]] MPK::Comm &&world, MPIC::Arguments &&args) {
        std::string const scheme = args["Scheme"];
        if (scheme == "PIC") {
            unsigned const version = args.get("Version", unsigned{1});
            if (version == 1) {
                return MPIC::__1_::PICDriver{MPK::Comm::world(), std::move(args)}();
            } // version
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid version number for " + scheme);
        } // scheme
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - `" + scheme + "' is an unknown scheme");
    }
}

int main(int argc, char * argv[])
{
    // MPI init
    //
    constexpr bool enable_mpi_thread = false;
    int required = enable_mpi_thread ? MPI_THREAD_MULTIPLE : MPI_THREAD_SINGLE, provided;
    if (MPK::Comm::init(&argc, &argv, required, provided) != MPI_SUCCESS) {
        println(std::cout, "%% ", __PRETTY_FUNCTION__, " - MPK::Comm::init(...) returned error");
        return 1;
    } else if (provided < required) {
        println(std::cout, "%% ", __PRETTY_FUNCTION__, " - provided < required");
        return 1;
    }

    // Driver::main
    //
    try {
        dispatch(MPK::Comm::world(), MPIC::Arguments{argc, argv});
    } catch (std::exception const &e) {
        println(std::cout, "%% ", e.what());
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    // MPI finalize
    //
    MPK::Comm::deinit();

    return 0;
}
