//
//  MPICArguments.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 2/16/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#include "MPICArguments.h"
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <string>

MPIC::Arguments::Arguments(const int argc, const char *const *argv)
: JsonProxy{parse(argc, argv)} {
}

Json::Value MPIC::Arguments::parse(const int argc, const char *const *argv)
{
    Json::Value root;
    int cur = 0;
    while (++cur < argc) {
        std::string const arg = argv[cur];
        if ('-' == arg.at(0)) { // parse option
            std::string const opt_name = arg.substr(1);
            if (opt_name.empty()) {
                throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + "`-' should be followed by an option name");
            } else if (++cur >= argc) {
                throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + "an option value is expected after `-" + opt_name + "'");
            }
            std::string const opt_value = argv[cur];
            if (opt_name == "i") { // parse json
                Json::Value json;
                try {
                    std::ifstream is(opt_value);
                    is >> json;
                } catch (std::exception &e) {
                    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - error in reading json file - " + e.what());
                }
                for (std::string const &key : json.getMemberNames()) { // merge json into option tree
                    root[key] = json[key];
                }
            } else {
                try { // identify numeric value if any
                    double const x = std::stod(opt_value); // throws an exception if parsing fails
                    int i;
                    if (x > std::numeric_limits<decltype(i)>::min() &&
                        x < std::numeric_limits<decltype(i)>::max()) { // an integer can fit
                        i = std::stoi(opt_value);
                        if (x == i) { // an integer
                            root[opt_name] = i;
                        } else { // not an integer
                            root[opt_name] = x;
                        }
                    } else {
                        root[opt_name] = x; // could be an integer that cannot fit in i
                    }
                } catch (std::exception &) { // no, just string
                    root[opt_name] = opt_value;
                }
            }
        } else { // skip non-optional argument for now
            print(std::cout, "%% ", __PRETTY_FUNCTION__, " - non-option argument, `", arg, "' is ignored\n");
        }
    }
    return root;
}
