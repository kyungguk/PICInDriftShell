//
//  MPICSnapshot.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/14/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICSnapshot_h
#define MPICSnapshot_h

#include "mpic-config.h"

#include <HDF5Kit/HDF5Kit.h>
#include <UtilityKit/UtilityKit.h>
#include <json/json.h>
#include <type_traits>
#include <string>

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    template <long> class PICDelegate;
}

class Snapshot {
public:
    explicit Snapshot() = default;
    explicit Snapshot(char const *output_directory, Json::Value const json);

    bool should_load() const noexcept { return _should_load; }
    bool should_save() const noexcept { return _should_save; }
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate); //!< Save.
    void operator>>(__1_::PICDelegate<1L> &delegate) const; //!< Load.

    void operator<<(__1_::PICDelegate<2L> const &delegate); //!< Save.
    void operator>>(__1_::PICDelegate<2L> &delegate) const; //!< Load.

    void operator<<(__1_::PICDelegate<3L> const &delegate); //!< Save.
    void operator>>(__1_::PICDelegate<3L> &delegate) const; //!< Load.

private: // member variables
    std::string _output_directory;
    std::string _filename_prefix;
    bool _should_load;
    bool _should_save;

private: // heavy liftings
    template <class Delegate>
    void save(Delegate const &delegate, Identifier::Any);
    template <class Delegate>
    void load(Delegate &delegate, Identifier::Any) const;

    template <class Domain>
    void save_bfield(H5::Group &root, Domain const &domain, Identifier::Any);
    template <class Domain>
    void load_bfield(Domain &domain, H5::Group const &root, Identifier::Any) const;

    template <class Domain>
    void save_efield(H5::Group &root, Domain const &domain, Identifier::Any);
    template <class Domain>
    void load_efield(Domain &domain, H5::Group const &root, Identifier::Any) const;

    template <class Domain>
    void save_particles(H5::Group &root, Domain const &domain, Identifier::Any);
    template <class Domain>
    void load_particles(Domain &domain, H5::Group const &root, Identifier::Any) const;

    template <class Domain>
    void save_cold_fluids(H5::Group &root, Domain const &domain, Identifier::Any);
    template <class Domain>
    void load_cold_fluids(Domain &domain, H5::Group const &root, Identifier::Any) const;

    template <class Delegate>
    void save_attr(H5::Group &root, Delegate const &delegate, Identifier::PIC<1>);

private: // helpers
    template <class H5Object, class T>
    inline static void write_attr(H5Object &obj, const char *name, T const &s);
    template <class H5Object, class T, long S>
    inline static void write_attr(H5Object &obj, const char *name, UTL::Vector<T, S> const &v);
    template <class H5Object, class T, long S>
    inline static void write_attr(H5Object &obj, const char *name, UTL::Vector<UTL::Vector<T, S>, S> const &m);

    template <long S>
    static double const *data(UTL::Vector<double, S> const &v) { return v.data(); }
    template <class T, long Rank, long Pad>
    static double const *data(UTL::ArrayND<T, Rank, Pad> const &a) { return data(*a.data()); }

    template <long S>
    static double *data(UTL::Vector<double, S> &v) { return v.data(); }
    template <class T, long Rank, long Pad>
    static double *data(UTL::ArrayND<T, Rank, Pad> &a) { return data(*a.data()); }
};
MPIC_END_NAMESPACE

#endif /* MPICSnapshot_h */
