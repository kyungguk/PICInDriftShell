//
//  MPICDiagnostic__ND.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/18/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDiagnostic__ND_h
#define MPICDiagnostic__ND_h

#include "mpic-config.h"

#include <MPIKit/MPIKit.h>
#include <PICKit/PICKit.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    template <long> struct Diagnostic;

    // MARK:- MPIC::__1_::_Diagnostic<ND>
    //
    template <long _ND>
    struct _Diagnostic {
        static constexpr long ND = _ND;
        using scalar_type = UTL::Vector<double, 1>;
        using vector_type = UTL::Vector<double, 3>;
        using position_type = UTL::Vector<double, ND>;
        using compact_tensor_type = UTL::Vector<double, 6>; //!< Compact representation of symmetric 3x3 matrix.

        vector_type dB2Over2{}; //!< Average magnetic field energy density.
        vector_type dE2Over2{}; //!< Average electric field energy density.
        vector_type mv2Over2{}; //!< Average particle kinetic energy density.
        UTL::DynamicArray<vector_type> msv2Over2{}; //!< Average particle species kinetic energy density.
        UTL::DynamicArray<vector_type> msU2Over2{}; //!< Average particle species bulk flow energy density.

    protected:
        _Diagnostic(_Diagnostic&&) = default;
        _Diagnostic &operator=(_Diagnostic&&) = default;
        explicit _Diagnostic(long const Nspecies);

        // plus op on vector_type
        //
        template <class T, long S>
        static inline UTL::Vector<T, S> reduce_plus(MPK::Comm const &comm, UTL::Vector<T, S> const &v);
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICDiagnostic__ND_h */
