//
//  MPIC_Driver.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/21/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPIC_Driver.h"

#include "MPICFieldRecorder.h"
#include "MPICMomentRecorder.h"
#include "MPICParticleRecorder.h"
#include "MPICSubdomainRecorder.h"
#include "MPICDiagnosticRecorder.h"
#include "MPICTimeseriesRecorder.h"
#include "MPICVelocityHistogramRecorder.h"
#include <utility>

MPIC::_Driver::~_Driver()
{
}

MPIC::_Driver::_Driver(MPK::Comm &&world, JsonProxy &&opts)
: comm{std::move(world)}, opts{std::move(opts)} {
    init_recorders();
}
void MPIC::_Driver::init_recorders()
{
    std::string const O = opts["O"]; // output directory

    // snapshot
    //
    snapshot.reset(new Snapshot{O.c_str(), opts.get("snapshot", Json::Value{Json::objectValue}).json()});

    // recorders
    //
    unsigned const inner_steps = opts["inner_steps"];
    if (opts.isMember("diagnostic_recorder")) {
        Json::Value json = opts["diagnostic_recorder"].json();
        json["recording_frequency"] = inner_steps*json.get("recording_frequency", unsigned{1}).asUInt();
        recorders["diagnostic_recorder"].reset(new DiagnosticRecorder{O.c_str(), json});
    }
    if (opts.isMember("timeseries_recorder")) {
        Json::Value json = opts["timeseries_recorder"].json();
        json["recording_frequency"] = inner_steps*json.get("recording_frequency", unsigned{1}).asUInt();
        recorders["timeseries_recorder"].reset(new TimeseriesRecorder{O.c_str(), json});
    }
    if (opts.isMember("field_recorder")) {
        Json::Value json = opts["field_recorder"].json();
        json["recording_frequency"] = inner_steps*json.get("recording_frequency", unsigned{1}).asUInt();
        recorders["field_recorder"].reset(new FieldRecorder{O.c_str(), json});
    }
    if (opts.isMember("moment_recorder")) {
        Json::Value json = opts["moment_recorder"].json();
        json["recording_frequency"] = inner_steps*json.get("recording_frequency", unsigned{1}).asUInt();
        recorders["moment_recorder"].reset(new MomentRecorder{O.c_str(), json});
    }
    if (opts.isMember("subdomain_recorder")) {
        Json::Value json = opts["subdomain_recorder"].json();
        json["recording_frequency"] = inner_steps*json.get("recording_frequency", unsigned{1}).asUInt();
        recorders["subdomain_recorder"].reset(new SubdomainRecorder{O.c_str(), json});
    }
    if (opts.isMember("particle_recorder")) {
        Json::Value json = opts["particle_recorder"].json();
        json["recording_frequency"] = inner_steps*json.get("recording_frequency", unsigned{1}).asUInt();
        recorders["particle_recorder"].reset(new ParticleRecorder{O.c_str(), json});
    }
    if (opts.isMember("2D_velocity_histogram_recorder")) {
        Json::Value json = opts["2D_velocity_histogram_recorder"].json();
        json["recording_frequency"] = inner_steps*json.get("recording_frequency", unsigned{1}).asUInt();
        recorders["2D_velocity_histogram_recorder"].reset(new VelocityHistogramRecorder<2L>{O.c_str(), json});
    }
}
