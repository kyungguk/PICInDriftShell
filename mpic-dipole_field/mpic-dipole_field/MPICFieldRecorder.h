//
//  MPICFieldRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICFieldRecorder_h
#define MPICFieldRecorder_h

#include "MPICRecorder.h"
#include <json/json.h>
#include <string>

MPIC_BEGIN_NAMESPACE
class FieldRecorder : public Recorder {
public:
    explicit FieldRecorder(char const *output_directory, Json::Value const json);
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate) override;
    void operator<<(__1_::PICDelegate<2L> const &delegate) override;
    void operator<<(__1_::PICDelegate<3L> const &delegate) override;

private:
    std::string _output_directory;
    std::string _filename_prefix;
    static constexpr int tag = 01;

    template <class Delegate>
    void record(Delegate const &delegate, Identifier::Any) const;

    template <class Delegate>
    void record_fields(H5::File &file, Delegate const &delegate, Identifier::Any) const;
    template <class Delegate>
    void record_fields(Delegate const &delegate, Identifier::Any) const;
};
MPIC_END_NAMESPACE

#endif /* MPICFieldRecorder_h */
