//
//  MPICDiagnostic__3D.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 4/3/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPICDiagnostic__3D_h
#define MPICDiagnostic__3D_h

#include "MPICDiagnostic__ND.h"

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- MPIC::__1_::Diagnostic<ND>
    //
    template <>
    struct Diagnostic<3L> : public _Diagnostic<3L> {
        explicit Diagnostic(long const Nspecies);

        void update(MPK::Comm const &comm, PIC::Domain<ND> const &domain);
    private:
        template <long Pad1>
        static vector_type _accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<vector_type, ND - 1, Pad1> const &A);
        template <long Pad0, long Pad1>
        static vector_type _accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<scalar_type, ND - 1, Pad0> const &n, UTL::ArrayND<vector_type, ND - 1, Pad1> const &nV);
        template <long Pad2>
        static vector_type _accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<compact_tensor_type, ND - 1, Pad2> const &nvv);
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICDiagnostic__3D_h */
