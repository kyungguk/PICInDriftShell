//
//  MPICSubdomainRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/20/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICSubdomainRecorder_h
#define MPICSubdomainRecorder_h

#include "MPICRecorder.h"
#include <json/json.h>
#include <string>
#include <set>

MPIC_BEGIN_NAMESPACE
class SubdomainRecorder : public Recorder {
public:
    explicit SubdomainRecorder(char const *output_directory, Json::Value const json);
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate) override;
    void operator<<(__1_::PICDelegate<2L> const &delegate) override;
    void operator<<(__1_::PICDelegate<3L> const &delegate) override;

private:
    std::string _output_directory;
    std::string _filename_prefix;
    std::set<int> _pid_bag; // a single value of -1 indicate all processes
    bool _should_record_moments;

    template <class Delegate>
    void record(Delegate const &delegate, Identifier::Any) const;

    template <class Delegate>
    void record_fields(H5::File &root, Delegate const &delegate, Identifier::Any) const;
    template <class Delegate>
    void record_particles(H5::File &file, Delegate const &delegate, Identifier::Any) const;
    template <class Delegate>
    void record_cold_fluids(H5::File &file, Delegate const &delegate, Identifier::Any) const;
};
MPIC_END_NAMESPACE

#endif /* MPICSubdomainRecorder_h */
