//
//  MPICParticleRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/20/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICParticleRecorder_h
#define MPICParticleRecorder_h

#include "MPICRecorder.h"
#include <json/json.h>
#include <string>
#include <set>

MPIC_BEGIN_NAMESPACE
class ParticleRecorder : public Recorder {
public:
    explicit ParticleRecorder(char const *output_directory, Json::Value const json);
    //
    void operator<<(__1_::PICDelegate<1L> const &delegate) override;
    void operator<<(__1_::PICDelegate<2L> const &delegate) override;
    void operator<<(__1_::PICDelegate<3L> const &delegate) override;

private:
    std::string _output_directory;
    std::string _filename_prefix;
    std::set<long> _species_id_bag;
    std::set<int> _process_id_bag; // a single value of -1 indicate all processes
    unsigned _max_particles_to_record;
    bool _should_shuffle_particles; // reorder particles in the bucket

    template <class Delegate>
    void record(Delegate const &delegate, Identifier::Any) const;
};
MPIC_END_NAMESPACE

#endif /* MPICParticleRecorder_h */
