//
//  MPICVelocityHistogram.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/25/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICVelocityHistogram_h
#define MPICVelocityHistogram_h

#include "mpic-config.h"

#include <UtilityKit/UtilityKit.h>

MPIC_BEGIN_NAMESPACE
template <long ND> class VelocityHistogram;

// MARK: VelocityHistogram<2D>
//
template <> class VelocityHistogram<2L> {
public:
    static constexpr long Nv = 2;
    using value_type = double;
    using size_type = long;
    using value_vector_type = UTL::Vector<value_type, Nv>;
    using size_vector_type = UTL::Vector<size_type, Nv>;

    explicit VelocityHistogram() = default;
    explicit VelocityHistogram(UTL::DynamicArray<value_type> const &v1_bins, UTL::DynamicArray<value_type> const &v2_bins);
    explicit VelocityHistogram(value_vector_type const &vmin, value_vector_type const &vmax, size_vector_type const &nbins);

    void reset();
    VelocityHistogram &operator<<(UTL::Vector<value_type, 3> const &vel);
    VelocityHistogram &operator+=(VelocityHistogram const &hist);

    value_type const &v1min() const { return _v1min; }
    value_type const &v2min() const { return _v2min; }
    value_type const &dv1() const { return _dv1; }
    value_type const &dv2() const { return _dv2; }
    size_type const &nv1bins() const { return _nv1; }
    size_type const &nv2bins() const { return _nv2; }
    UTL::DynamicArray<size_type> const &hist() const noexcept { return _hist; }
    size_type const &N() const noexcept { return _N; }

private:
    value_type _v1min, _v2min; // min {v1, v2}
    value_type _dv1, _dv2; // bin sizes
    size_type _nv1, _nv2; // the number of bins
    UTL::DynamicArray<size_type> _hist; // 2D vel. histogram; row-major memory layout
    size_type _N; // total counts of accumulation
};

// MARK: VelocityHistogram<3D>
//
template <> class VelocityHistogram<3L> {
public:
    static constexpr long Nv = 3;
    using value_type = double;
    using size_type = long;
    using value_vector_type = UTL::Vector<value_type, Nv>;
    using size_vector_type = UTL::Vector<size_type, Nv>;

    explicit VelocityHistogram() = default;
    explicit VelocityHistogram(UTL::DynamicArray<value_type> const &vx_bins, UTL::DynamicArray<value_type> const &vy_bins, UTL::DynamicArray<value_type> const &vz_bins);
    explicit VelocityHistogram(value_vector_type const &vmin, value_vector_type const &vmax, size_vector_type const &nbins);

    void reset();
    VelocityHistogram &operator<<(UTL::Vector<value_type, 3> const &vel);
    VelocityHistogram &operator+=(VelocityHistogram const &hist);

    value_type const &vxmin() const { return _vxmin; }
    value_type const &vymin() const { return _vymin; }
    value_type const &vzmin() const { return _vzmin; }
    value_type const &dvx() const { return _dvx; }
    value_type const &dvy() const { return _dvy; }
    value_type const &dvz() const { return _dvz; }
    size_type const &nvxbins() const { return _nvx; }
    size_type const &nvybins() const { return _nvy; }
    size_type const &nvzbins() const { return _nvz; }
    UTL::DynamicArray<size_type> const &hist() const noexcept { return _hist; }
    size_type const &N() const noexcept { return _N; }

private:
    value_type _vxmin, _vymin, _vzmin; // min {vx, vy, vz}
    value_type _dvx, _dvy, _dvz; // bin sizes
    size_type _nvx, _nvy, _nvz; // the number of bins
    UTL::DynamicArray<size_type> _hist; // 3D vel. histogram; row-major memory layout
    size_type _N; // total counts of accumulation
};
MPIC_END_NAMESPACE

#endif /* MPICVelocityHistogram_h */
