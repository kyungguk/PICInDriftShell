//
//  MPICFieldRecorder.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/19/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICFieldRecorder.h"

#include "MPICDelegate.h"
#include <stdexcept>
#include <string>

MPIC::FieldRecorder::FieldRecorder(char const *output_directory, Json::Value const json)
: Recorder{json.get("recording_frequency", unsigned{1}).asUInt()}
, _output_directory{output_directory}
, _filename_prefix{json.get("filename_prefix", "field").asString()} {
}
//
void MPIC::FieldRecorder::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::FieldRecorder::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::FieldRecorder::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    record(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::FieldRecorder::record(Delegate const &delegate, Identifier::Any) const
{
    try {
        if (delegate.comm().is_master()) { // in master process
            //
            // create hdf5 file
            //
            H5::File root = [](std::string const path) {
                return H5::File{H5::File::Trunc{}, path.c_str()};
            }(_output_directory + "/" + _filename_prefix + "_" + std::to_string(delegate.cur_step()) + ".h5");
            //
            // record fields
            //
            record_fields(root, delegate, Delegate::identifier);
            //
            // flush
            //
            root.flush();
        } else { // in non-master processes
            //
            // record fields
            //
            record_fields(delegate, Delegate::identifier);
        }
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

template <class Delegate>
void MPIC::FieldRecorder::record_fields(H5::File &file, Delegate const &delegate, Identifier::Any) const
{
    using delegate_type = typename std::decay<decltype(delegate)>::type;
    using magnetic_field_type = typename delegate_type::bfield_type::magnetic_field_type;
    using electric_field_type = typename delegate_type::efield_type::electric_field_type;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    //
    // create group and populate attributes
    //
    H5::Group root = file.group("fields", H5::PList{}, H5::PList{});
    write_common_attr(root, delegate, delegate.identifier);
    //
    // create file dataspace
    //
    H5::Space fspace; {
        auto const N = delegate.simulation_size().template take<magnetic_field_type::rank()>();
        H5::Space::size_array_type dims{N.begin(), N.end()};
        dims.push_back(magnetic_field_type::value_type::size());
        fspace = H5::Space{dims};
    }
    //
    // create datasets and populate attributes
    //
    H5::Dataset B, E; {
        write_common_attr(B = root.dataset("B", H5::Type::native<double>(), fspace), delegate, delegate.identifier);
        write_common_attr(E = root.dataset("E", H5::Type::native<double>(), fspace), delegate, delegate.identifier);
    }
    //
    // memory dataspace; assume that dims() and pad_size() of B and E are the same
    //
    static_assert(magnetic_field_type::pad_size() == electric_field_type::pad_size(), "B and E field pad sizes have to be the same");
    H5::Space::size_array_type count;
    H5::Space mspace; {
        auto const N = params.N().template take<magnetic_field_type::rank()>();
        H5::Space::size_array_type dims{N.begin(), N.end()};
        count = dims;
        for (auto &x : dims) {
            x += 2*magnetic_field_type::pad_size();
        }
        dims.push_back(magnetic_field_type::value_type::size());
        count.push_back(dims.back());
        //
        H5::Space::size_array_type start(N.size(), magnetic_field_type::pad_size());
        start.push_back(0);
        (mspace = H5::Space{dims}).select(H5S_SELECT_SET, start, count);
    }
    //
    // record master domain field
    //
    auto offset = delegate.domain_offset();
    magnetic_field_type Bcarts{domain.bfield()->carts}; // intentional copy
    electric_field_type Ecarts{domain.efield()->carts}; // intentional copy
    {
        H5::Space::size_array_type start{offset.begin(), offset.end()};
        start.push_back(0);
        fspace.select(H5S_SELECT_SET, start, count);
        B.write(fspace, data(Bcarts), mspace);
        E.write(fspace, data(Ecarts), mspace);
    }
    //
    // record slave domain field
    //
    UTL::Vector<MPK::Tag, 3> const tags{tag + 0, tag + 1, tag + 2};
    for (int rank = delegate.comm()->size() - 1; rank >= 0; --rank) {
        if (delegate.comm().master() == rank) continue; // skip master
        //
        // first receive is offset
        //
        delegate.comm()->recv(offset, {MPK::Rank{rank}, std::get<0>(tags)});
        H5::Space::size_array_type start{offset.begin(), offset.end()};
        start.push_back(0);
        fspace.select(H5S_SELECT_SET, start, count);
        //
        // second receive is bfield
        //
        delegate.comm()->recv(Bcarts, {MPK::Rank{rank}, std::get<1>(tags)});
        B.write(fspace, data(Bcarts), mspace);
        //
        // third receive is efield
        //
        delegate.comm()->recv(Ecarts, {MPK::Rank{rank}, std::get<2>(tags)});
        E.write(fspace, data(Ecarts), mspace);
    }
}
template <class Delegate>
void MPIC::FieldRecorder::record_fields(Delegate const &delegate, Identifier::Any) const
{
    using delegate_type = typename std::decay<decltype(delegate)>::type;
    using magnetic_field_type = typename delegate_type::bfield_type::magnetic_field_type;
    using electric_field_type = typename delegate_type::efield_type::electric_field_type;
    auto const &domain = delegate.domain();
    UTL::Vector<MPK::Tag, 3> const tags{tag + 0, tag + 1, tag + 2};
    //
    // first send is offset
    //
    auto offset = delegate.domain_offset();
    MPK::Request req0 = delegate.comm()->issend(offset, {delegate.comm().master(), std::get<0>(tags)});
    //
    // second send is bfield
    //
    magnetic_field_type const &Bcarts = domain.bfield()->carts;
    MPK::Request req1 = delegate.comm()->issend(Bcarts, {delegate.comm().master(), std::get<1>(tags)});
    //
    // third send is efield
    //
    electric_field_type const &Ecarts = domain.efield()->carts;
    MPK::Request req2 = delegate.comm()->issend(Ecarts, {delegate.comm().master(), std::get<2>(tags)});
    //
    // block until sent
    //
    req0.wait();
    req1.wait();
    req2.wait();
}
