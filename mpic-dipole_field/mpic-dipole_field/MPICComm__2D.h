//
//  MPICComm__2D.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 6/30/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef MPICComm__2D_h
#define MPICComm__2D_h

#include "MPICComm__ND.h"

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- MPIC::__1_::InterProcessComm<ND>
    //
    template <>
    class InterProcessComm<2> : public _InterProcessComm<2> {
    public:
        InterProcessComm(InterProcessComm &&) = default;
        InterProcessComm &operator=(InterProcessComm &&) = default;

        explicit InterProcessComm(MPK::Comm &&comm, size_vector_type const domain_decomposition);
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICComm__2D_h */
