//
//  MPICDiagnostic.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/18/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDiagnostic_h
#define MPICDiagnostic_h

#include "MPICDiagnostic__ND.h"
#include "MPICDiagnostic__1D.h"
#include "MPICDiagnostic__2D.h"
#include "MPICDiagnostic__3D.h"

#endif /* MPICDiagnostic_h */
