//
//  MPICComm__3D.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 6/30/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#include "MPICComm__3D.h"
#include "MPICComm__ND.hh"
#include <stdexcept>
#include <string>

constexpr long ND = 3;

// MARK:- MPIC::__1_::InterProcessComm<3D>
//
MPIC::__1_::InterProcessComm<ND>::InterProcessComm(MPK::Comm &&tmp_comm, size_vector_type const domain_decomposition)
: _InterProcessComm{std::move(tmp_comm)}
{
    // domain topology
    //
    if (_comm.size() <= 0 || UTL::reduce_prod(domain_decomposition) != _comm.size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - # of mpi processes and domain topology inconsistency");
    } else if (std::get<2>(domain_decomposition) != 1) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - no domain decomposition must be made in the 3rd direction");
    } else {
        _size = domain_decomposition;
        _rank.x = _comm.rank()/(_size.y*_size.z);
        _rank.y = _comm.rank()%(_size.y*_size.z);
        _rank.z = _rank.y % _size.z;
        _rank.y /= _size.z;
    }

    // connect x-boundaries
    //
    {
        long const
        first= 0,
        last = _size.x - 1,
        prev = _rank.x - 1,
        next = _rank.x + 1;
        *_LB.x = int((prev < first ? last : prev)*_size.y*_size.z + _rank.y*_size.z + _rank.z);
        *_RB.x = int((next > last ? first : next)*_size.y*_size.z + _rank.y*_size.z + _rank.z);
    }
    // connect y-boundaries
    //
    {
        long const
        first= 0,
        last = _size.y - 1,
        prev = _rank.y - 1,
        next = _rank.y + 1;
        *_LB.y = int(_rank.x*_size.y*_size.z + (prev < first ? last : prev)*_size.z + _rank.z);
        *_RB.y = int(_rank.x*_size.y*_size.z + (next > last ? first : next)*_size.z + _rank.z);
    }
    // connect z-boundaries
    //
    {
        long const
        first= 0,
        last = _size.z - 1,
        prev = _rank.z - 1,
        next = _rank.z + 1;
        *_LB.z = int(_rank.x*_size.y*_size.z + _rank.y*_size.z + (prev < first ? last : prev));
        *_RB.z = int(_rank.x*_size.y*_size.z + _rank.y*_size.z + (next > last ? first : next));
    }

    // dimensional sub comms
    //
    (_dim_comms.x = _comm.split(int(_rank.y*_size.z + _rank.z), int(_rank.x))).set_label("x-dim comm group"); // grouping of processors whose rank.x changes
    (_dim_comms.y = _comm.split(int(_rank.x*_size.z + _rank.z), int(_rank.y))).set_label("y-dim comm group"); // grouping of processors whose rank.y changes
    (_dim_comms.z = _comm.split(int(_rank.x*_size.y + _rank.y), int(_rank.z))).set_label("z-dim comm group"); // grouping of processors whose rank.z changes
}
