//
//  MPICDelegate.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/14/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDelegate_h
#define MPICDelegate_h

#include "MPICDelegate__ND.h"
#include "MPICDelegate__1D.h"
#include "MPICDelegate__2D.h"
#include "MPICDelegate__3D.h"

#endif /* MPICDelegate_h */
