//
//  MPICComm__ND.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 6/30/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#ifndef MPICComm__ND_h
#define MPICComm__ND_h

#include "mpic-config.h"

#include "MPICTypeMap.h"
#include <MPIKit/MPIKit.h>
#include <UtilityKit/UtilityKit.h>

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    template <long ND> class InterProcessComm;

    // MARK:- MPIC::__1_::_InterProcessComm<ND>
    //
    template <long ND>
    class _InterProcessComm {
    public:
        using size_vector_type = UTL::Vector<long, ND>;

    protected:
        MPK::Comm _comm;
        size_vector_type _size{1}, _rank{0};
        UTL::Vector<MPK::Rank, ND> _LB{}, _RB{}; // left/bottom and right/top domain ranks
        UTL::Vector<MPK::Comm, ND> _dim_comms{}; // dimensional group

    protected:
        _InterProcessComm(_InterProcessComm &&) = default;
        _InterProcessComm &operator=(_InterProcessComm &&) = default;

        explicit _InterProcessComm(MPK::Comm &&comm);

    public:
        MPK::Comm const &operator*() const noexcept { return _comm; }
        MPK::Comm const *operator->() const noexcept { return &_comm; }

        static constexpr MPK::Rank master() noexcept { return MPK::Rank{0}; }
        bool is_master() const { return _comm.rank() == master(); }

        size_vector_type const &process_rank() const noexcept { return _rank; }
        size_vector_type const &process_size() const noexcept { return _size; }

        template <long dim>
        bool is_lower_boundary() const noexcept {
            return std::get<dim>(process_rank()) == 0;
        }
        template <long dim>
        bool is_upper_boundary() const noexcept {
            return std::get<dim>(process_rank()) == std::get<dim>(process_size()) - 1;
        }

    public:
        template <long dim, class T>
        void pass(UTL::DynamicArray<T> &L_bucket, UTL::DynamicArray<T> &R_bucket, int const tag) const;

        template <long dim, class T, long Rank, long Pad>
        void pass(UTL::ArrayND<T, Rank, Pad> &A, int const tag, bool const is_symmetric) const;

        template <long dim, class T, long Rank, long Pad>
        void gather(UTL::ArrayND<T, Rank, Pad> &A, int const tag, bool const is_symmetric) const;

        template <long dim, class T, long Rank, long Pad>
        void dim_average(UTL::ArrayND<T, Rank, Pad> &A) const;

    private:
        template <class T, long S, long Rank, long Pad>
        static void reduce_plus(MPK::Comm const &comm, UTL::ArrayND<UTL::Vector<T, S>, Rank, Pad> &A);
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICComm__ND_h */
