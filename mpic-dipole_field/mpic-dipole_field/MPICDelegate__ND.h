//
//  MPICDelegate__ND.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/14/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDelegate__ND_h
#define MPICDelegate__ND_h

#include "mpic-config.h"

#include "MPICComm__ND.h"
#include <PICKit/PICKit.h>
#include <memory>
#include <future>

MPIC_BEGIN_NAMESPACE
struct DebuggingOptions {
    bool zero_out_electromagnetic_waves{false};
};

namespace __1_ {
    template <long> struct Diagnostic;
    template <long> class PICDelegate;

    // MARK:- MPIC::__1_::_PICDelegate<ND>
    //
    template <long ND>
    class _PICDelegate : public PIC::Delegate<ND> {
        InterProcessComm<ND> _comm;
        long cur_tick{0};
        long sub_tick{0};
    protected:
        std::unique_ptr<PIC::MasterDelegate<ND>> _master{};
        UTL::StaticArray<PIC::Domain<ND>, 256> _domains{};
        std::unique_ptr<PIC::Source<ND>> _src{};
        std::unique_ptr<Diagnostic<ND>> _diag{};
        DebuggingOptions _debug{};
        double prev_t{-1};

        explicit _PICDelegate(MPK::Comm &&comm, UTL::Vector<long, ND> const domain_decomposition);

    public:
        static constexpr Identifier::PIC<1> identifier{};

        InterProcessComm<ND> const &comm() const noexcept { return _comm; }

        UTL::Array<PIC::Domain<ND>> const &domains() const noexcept { return _domains; }
        UTL::Array<PIC::Domain<ND>>       &domains()       noexcept { return _domains; }
        //
        PIC::Domain<ND> const &domain() const { return _domains[0]; }

        Diagnostic<ND> const &diagnostic() const noexcept { return *_diag; }

        UTL::Vector<long, ND> const &process_rank() const noexcept { return _comm.process_rank(); }
        UTL::Vector<long, ND> const &process_size() const noexcept { return _comm.process_size(); }
        UTL::Vector<long, ND> domain_offset() const noexcept { return process_rank()*domain().params().N(); }
        UTL::Vector<long, ND> simulation_size() const noexcept { return process_size()*domain().params().N(); }

        void set_cur_step(long const step) noexcept { cur_tick = step; sub_tick = 0; } // should reset sub-tick
        long cur_step() const noexcept { return cur_tick; } // low-res step count
        double time() const { return domain().dt() * (cur_tick + sub_tick); } // high-res sim. time

        void set_debugging_options(DebuggingOptions const &debug) noexcept { _debug = debug; }
        DebuggingOptions const &debugging_options() const noexcept { return _debug; }

        // multi-threaded concurrent loop
        //
        template <class Loop/*void(Domain*, bool is_master_thread*/, class... Args>
        void parallel_loop(Loop loop, Args&&... args) {
            UTL::DynamicArray<std::future<void>> futures;
            //
            // spawn worker threads
            //
            for (auto it = _domains.begin() + 1, last = _domains.end(); it != last; ++it) {
                futures.push_back(std::async(std::launch::async, loop, it, false, args...));
            }
            //
            // master thread loop
            //
            loop(_domains.begin(), true, args...);
            //
            // wait for workers
            //
            for (std::future<void> &f : futures) {
                f.wait();
            }
        }

    protected:
        bool is_master_thread(PIC::Domain<ND> const* domain) const noexcept {
            return _domains.begin() == domain;
        }

        // called by multiple threads; hires time keeping
        //
        bool will_do_next_cycle(PIC::Domain<ND> const* domain, long const i_step) override {
            if (is_master_thread(domain)) {
                sub_tick = i_step;
            }
            return PIC::Delegate<ND>::will_do_next_cycle(domain, i_step);
        }

        template <long dir>
        static void _partition(PIC::Species<ND>& sp, UTL::DynamicArray<PIC::Particle<ND>> &L_bucket, UTL::DynamicArray<PIC::Particle<ND>> &R_bucket);

        template <long dir>
        void _particle_pass_periodic(PIC::ParameterSet<ND> const &params, UTL::DynamicArray<PIC::Particle<ND>> &L_bucket, UTL::DynamicArray<PIC::Particle<ND>> &R_bucket) const;
        template <long dir>
        void _particle_pass_reflect(PIC::ParameterSet<ND> const &params, UTL::DynamicArray<PIC::Particle<ND>> &L_bucket, UTL::DynamicArray<PIC::Particle<ND>> &R_bucket) const;
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICDelegate__ND_h */
