//
//  MPICDiagnostic__ND.hh
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/18/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDiagnostic__ND_hh
#define MPICDiagnostic__ND_hh

#include "MPICDiagnostic__ND.h"

#include <stdexcept>
#include <string>
#include <cmath>

// MARK:- MPIC::__1_::_Diagnostic<ND>
//
template <long _ND>
MPIC::__1_::_Diagnostic<_ND>::_Diagnostic(long const Nspecies)
{
    if (Nspecies < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative argument");
    }
    msv2Over2.resize(Nspecies);
    msU2Over2.resize(Nspecies);
}

template <long _ND>
template <class T, long S>
UTL::Vector<T, S> MPIC::__1_::_Diagnostic<_ND>::reduce_plus(MPK::Comm const &comm, UTL::Vector<T, S> const &v)
{
    using Vector = UTL::Vector<T, S>;
    static_assert(std::is_same<Vector, typename std::decay<decltype(v)>::type>::value, "type mismatch");
    static MPK::ReduceOp const plus(true, [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *)->void {
        for (int i = 0; i < *vlen; ++i) {
            static_cast<Vector*>(inoutvec)[i] += static_cast<Vector const*>(invec)[i];
        }
    });
    Vector w{v};
    return comm.reduce(plus, w), w;
}

#endif /* MPICDiagnostic__ND_hh */
