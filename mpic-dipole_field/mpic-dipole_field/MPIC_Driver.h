//
//  MPIC_Driver.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/21/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPIC_Driver_h
#define MPIC_Driver_h

#include "mpic-config.h"

#include "MPICDelegate.h"
#include "MPICSnapshot.h"
#include "MPICRecorder.h"
#include "MPICJsonProxy.h"
#include <PICKit/PICKit.h>
#include <MPIKit/MPIKit.h>
#include <UtilityKit/UtilityKit.h>
#include <string>
#include <memory>
#include <map>

MPIC_BEGIN_NAMESPACE
class _Driver {
    void init_recorders();
protected:
    explicit _Driver(MPK::Comm &&world, JsonProxy &&opts);
    virtual ~_Driver();
    virtual void operator()() const = 0;

    MPK::Comm comm;
    JsonProxy opts;
    std::unique_ptr<Snapshot> snapshot{};
    std::map<std::string, std::unique_ptr<Recorder>> recorders{};
};
MPIC_END_NAMESPACE

#endif /* MPIC_Driver_h */
