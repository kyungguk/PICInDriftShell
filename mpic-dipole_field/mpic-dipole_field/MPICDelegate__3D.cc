//
//  MPICDelegate__3D.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 4/3/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPICDelegate__3D.h"
#include "MPICDelegate__ND.hh"
#include "MPICDiagnostic__3D.h"

constexpr long ND = 3;

// MARK:- MPIC::__1_::PICDelegate<ND>
//
MPIC::__1_::PICDelegate<ND>::~PICDelegate<ND>()
{
}

MPIC::__1_::PICDelegate<ND>::PICDelegate(MPK::Comm &&comm, size_vector_type const domain_decomposition, PIC::ParameterSet<ND> const &params, PIC::WaveDamper<ND> const &damper, boundary_type const &bc)
: _PICDelegate{std::move(comm), domain_decomposition}, _params{params}, _damper{damper}, _bc{bc}
{
    _master.reset(new PIC::MasterDelegate<ND>{params.number_of_worker_threads(), this});
}

void MPIC::__1_::PICDelegate<ND>::init_domain(double const dt, bool const use_metric_table)
{
    PIC::ParameterSet<ND> const &global_params = this->global_params();
    PIC::WaveDamper<ND> local_damper = global_damper();
    //
    // 1. check if the simulation size is divisible by the mpi process size
    //
    if (UTL::reduce_bit_or(global_params.N()%process_size())) {
        throw std::invalid_argument{std::string{__PRETTY_FUNCTION__} + " - simulation size is not divisible by the mpi process size"};
    }
    //
    // 2. set damper boundary mask
    //
    {
        long boundary = 0;
        if (comm().is_lower_boundary<Direction::q1>()) boundary |= local_damper.q1_lo;
        if (comm().is_upper_boundary<Direction::q1>()) boundary |= local_damper.q1_hi;

        if (comm().is_lower_boundary<Direction::q2>()) boundary |= local_damper.q2_lo;
        if (comm().is_upper_boundary<Direction::q2>()) boundary |= local_damper.q2_hi;

        if (comm().is_lower_boundary<Direction::q3>()) boundary |= local_damper.q3_lo;
        if (comm().is_upper_boundary<Direction::q3>()) boundary |= local_damper.q3_hi;
        local_damper.set_boundary_mask(boundary);
    }
    //
    // 3. slice parameter set
    //
    size_vector_type const length = global_params.N()/process_size(); // subdomain size
    size_vector_type const location = length * process_rank(); // subdomain offset
    decltype(global_params) local_params = global_params.slice(location, length, use_metric_table);
    //
    // 4. construct domain
    //
    _domains.emplace_back(local_params, dt, local_damper).set_delegate(_master.get()); // master domain
    for (PIC::WorkerDelegate<ND> &worker : _master->workers()) {
        _domains.emplace_back(local_params, dt, local_damper).set_delegate(&worker); // worker domain
    }
    //
    // 5. construct source
    //
    _src.reset(new PIC::Source<ND>{local_params, local_damper});
}

void MPIC::__1_::PICDelegate<ND>::once(PIC::Domain<ND> *domain)
{
    if (is_master_thread(domain)) {
        // construct diagnostic
        //
        _diag.reset(new Diagnostic<ND>{domain->species().size() + domain->cold_fluids().size()});
    }
}
void MPIC::__1_::PICDelegate<ND>::advance_by(PIC::Domain<ND> *domain, long const n_steps)
{
    // advance by n steps
    //
    domain->advance_by(n_steps);

    if (is_master_thread(domain)) {
        //
        // update step count
        //
        set_cur_step(cur_step() + n_steps);
        //
        // update diagnostic
        //
        _diag->update(*comm(), *domain);
    }
}

void MPIC::__1_::PICDelegate<ND>::pass(PIC::Domain<ND> const *, PIC::BField<ND> &bfield)
{
    if (debugging_options().zero_out_electromagnetic_waves) {
        bfield->contr.fill({0, 0, 0});
    } else {
        constexpr int tag = 01;

        // pass q2-dir
        //
        {
            constexpr Direction dir = Direction::q2;
            comm().pass<dir>(bfield->contr, tag + 0, bc<dir>() != periodic);
        }
        // pass q1-dir
        //
        {
            constexpr Direction dir = Direction::q1;
            comm().pass<dir>(bfield->contr, tag + 4, bc<dir>() != periodic);
        }
    }
}
void MPIC::__1_::PICDelegate<ND>::pass(PIC::Domain<ND> const *, PIC::EField<ND> &efield)
{
    if (debugging_options().zero_out_electromagnetic_waves) {
        efield->contr.fill({0, 0, 0});
    } else {
        constexpr int tag = 011;

        // pass q2-dir
        //
        {
            constexpr Direction dir = Direction::q2;
            comm().pass<dir>(efield->contr, tag + 0, bc<dir>() != periodic);
        }
        // pass q1-dir
        //
        {
            constexpr Direction dir = Direction::q1;
            comm().pass<dir>(efield->contr, tag + 4, bc<dir>() != periodic);
        }
    }
}
void MPIC::__1_::PICDelegate<ND>::pass(PIC::Domain<ND> const *, PIC::Current<ND> &current)
{
    constexpr int tag = 0111;

    // pass q2-dir
    //
    {
        constexpr Direction dir = Direction::q2;
        comm().pass<dir>(current->mfa, tag + 0, bc<dir>() != periodic);
    }
    // pass q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        comm().pass<dir>(current->mfa, tag + 4, bc<dir>() != periodic);
    }
}

void MPIC::__1_::PICDelegate<ND>::gather(PIC::Domain<ND> const *domain, PIC::Current<ND> &current)
{
    if (_src && is_master_thread(domain)) { // external source
        if (this->time() != this->prev_t) {
            _src->update(this->prev_t = this->time());
            current += *_src;
        }
    }
    //
    constexpr int tag = 01111;

    // gather q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        comm().gather<dir>(current->mfa, tag + 0, bc<dir>() != periodic);
    }
    // gather q2-dir
    //
    {
        constexpr Direction dir = Direction::q2;
        comm().gather<dir>(current->mfa, tag + 4, bc<dir>() != periodic);
    }
}
void MPIC::__1_::PICDelegate<ND>::gather(PIC::Domain<ND> const *, PIC::Species<ND> &sp)
{
    constexpr int tag = 011111;

    // gather q1-dir
    //
    {
        constexpr Direction dir = Direction::q1;
        comm().gather<dir>(sp.moment<0>(), tag + 0, bc<dir>() != periodic);
        comm().gather<dir>(sp.moment<1>(), tag + 4, bc<dir>() != periodic);
        comm().gather<dir>(sp.moment<2>(), tag + 0, bc<dir>() != periodic);
    }
    // gather q2-dir
    //
    {
        constexpr Direction dir = Direction::q2;
        comm().gather<dir>(sp.moment<0>(), tag + 4, bc<dir>() != periodic);
        comm().gather<dir>(sp.moment<1>(), tag + 0, bc<dir>() != periodic);
        comm().gather<dir>(sp.moment<2>(), tag + 4, bc<dir>() != periodic);
    }
}

void MPIC::__1_::PICDelegate<ND>::pass(PIC::Domain<ND> const *, [[maybe_unused]] PIC::Species<ND> &sp)
{
    throw std::domain_error{std::string{__PRETTY_FUNCTION__} + " - use the partition/pass routine"};
}

void MPIC::__1_::PICDelegate<ND>::partition(PIC::Species<ND>& sp, Direction const dir, species_type::bucket_type &L_bucket, species_type::bucket_type &R_bucket)
{
    switch (dir) {
        case Direction::q1: {
            constexpr Direction dir = Direction::q1;
            _partition<dir>(sp, L_bucket, R_bucket);
            switch (bc<dir>()) {
                case periodic:
                    _particle_pass_periodic<dir>(sp.params(), L_bucket, R_bucket);
                    break;
                case reflective:
                    _particle_pass_reflect<dir>(sp.params(), L_bucket, R_bucket);
                    break;
            }
            break;
        }
        case Direction::q2: {
            constexpr Direction dir = Direction::q2;
            _partition<dir>(sp, L_bucket, R_bucket);
            switch (bc<dir>()) {
                case periodic:
                    _particle_pass_periodic<dir>(sp.params(), L_bucket, R_bucket);
                    break;
                case reflective:
                    _particle_pass_reflect<dir>(sp.params(), L_bucket, R_bucket);
                    break;
            }
            break;
        }
        case Direction::q3:
            throw std::invalid_argument{__PRETTY_FUNCTION__};
    }
}
void MPIC::__1_::PICDelegate<ND>::pass(PIC::Domain<ND> const*, Direction const dir, species_type::bucket_type &L_bucket, species_type::bucket_type &R_bucket)
{
    constexpr int tag = 0111111;

    spill_bucket.clear();
    //
    switch (dir) {
        case Direction::q1: {
            constexpr Direction dir = Direction::q1;
            //
            switch (bc<dir>()) {
                case periodic:
                    break;
                case reflective:
                    if (comm().is_lower_boundary<dir>()) {
                        std::move(L_bucket.begin(), L_bucket.end(), std::back_inserter(spill_bucket));
                        L_bucket.clear();
                    }
                    if (comm().is_upper_boundary<dir>()) {
                        std::move(R_bucket.begin(), R_bucket.end(), std::back_inserter(spill_bucket));
                        R_bucket.clear();
                    }
                    break;
            }
            //
            comm().pass<dir>(L_bucket, R_bucket, tag + 4);
            break;
        }
        case Direction::q2: {
            constexpr Direction dir = Direction::q2;
            //
            switch (bc<dir>()) {
                case periodic:
                    break;
                case reflective:
                    if (comm().is_lower_boundary<dir>()) {
                        std::move(L_bucket.begin(), L_bucket.end(), std::back_inserter(spill_bucket));
                        L_bucket.clear();
                    }
                    if (comm().is_upper_boundary<dir>()) {
                        std::move(R_bucket.begin(), R_bucket.end(), std::back_inserter(spill_bucket));
                        R_bucket.clear();
                    }
                    break;
            }
            //
            comm().pass<dir>(L_bucket, R_bucket, tag + 0);
            break;
        }
        case Direction::q3:
            throw std::invalid_argument{__PRETTY_FUNCTION__};
    }
    //
    auto mid = spill_bucket.begin() + spill_bucket.size()/2;
    std::move(spill_bucket.begin(), mid, std::back_inserter(L_bucket));
    std::move(mid, spill_bucket.end(), std::back_inserter(R_bucket));
}
