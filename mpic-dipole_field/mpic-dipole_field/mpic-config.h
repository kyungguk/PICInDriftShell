//
//  mpic-config.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/11/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef mpic_config_h
#define mpic_config_h


#ifndef MPIC_NAMESPACE
#define MPIC_NAMESPACE MPIC
#define MPIC_BEGIN_NAMESPACE namespace MPIC_NAMESPACE {
#define MPIC_END_NAMESPACE }
#endif


// simulation identifier
//
MPIC_BEGIN_NAMESPACE
namespace Identifier {
    struct Any {};
    template <long Ver> struct PIC : public Any { static constexpr long version = Ver; };
};
MPIC_END_NAMESPACE


// version selection
//
#include <PICKit/PICKit.h>

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    namespace PIC = ::PIC::__1_;
}
MPIC_END_NAMESPACE


#endif /* mpic_config_h */
