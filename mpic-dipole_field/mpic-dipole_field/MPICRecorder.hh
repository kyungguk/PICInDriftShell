//
//  MPICRecorder.hh
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 9/24/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPICRecorder_hh
#define MPICRecorder_hh

#include <type_traits>

// MARK:-
//
template <class H5Object, class T>
void MPIC::Recorder::write_attr(H5Object &obj, const char *name, T const &s)
{
    static_assert(std::is_arithmetic<T>::value, "type T is not a scalar type");
    obj.attribute(name, H5::Type::native<T>(), H5::Space{nullptr}).write(&s);
}
template <class H5Object, class T, long S>
void MPIC::Recorder::write_attr(H5Object &obj, const char *name, UTL::Vector<T, S> const &v)
{
    static_assert(std::is_arithmetic<T>::value, "type T is not a scalar type");
    obj.attribute(name, H5::Type::native<T>(), H5::Space{{S}}).write(v.data());
}
template <class H5Object, class T, long S>
void MPIC::Recorder::write_attr(H5Object &obj, const char *name, UTL::Vector<UTL::Vector<T, S>, S> const &m)
{
    static_assert(std::is_arithmetic<T>::value, "type T is not a scalar type");
    obj.attribute(name, H5::Type::native<T>(), H5::Space{{S, S}}).write(m.data()->data());
}
template <class H5Object, class T>
void MPIC::Recorder::write_attr(H5Object &obj, const char *name, UTL::DynamicArray<T> const &A)
{
    static_assert(std::is_arithmetic<T>::value, "type T is not a scalar type");
    obj.attribute(name, H5::Type::native<T>(), H5::Space{{static_cast<unsigned long>(A.size())}}).write(A.data());
}

// MARK:-
//
template <class H5Object, class Delegate>
void MPIC::Recorder::_write_common_attr(H5Object &obj, Delegate const &delegate, Identifier::PIC<1>)
{
    constexpr long ND = std::decay<decltype(delegate)>::type::ND;
    auto const &domain = delegate.domain();
    auto const &params = domain.params();
    auto const &damper = domain.damper();
    //
    write_attr(obj, "simulation_size", delegate.simulation_size());
    write_attr(obj, "process_rank", delegate.process_rank());
    write_attr(obj, "process_size", delegate.process_size());
    write_attr(obj, "subdomain_offset", delegate.domain_offset());
    write_attr(obj, "subdomain_size", params.N());
    write_attr(obj, "ND", ND);
    write_attr(obj, "c", params.c());
    write_attr(obj, "O0", params.O0ref());
    write_attr(obj, "cur_step", delegate.cur_step());
    write_attr(obj, "dt", domain.dt());
    write_attr(obj, "time", delegate.time());
    write_attr(obj, "r0ref", params.coord_system().r0ref());
    write_attr(obj, "q3ref", params.coord_system().q3ref());
    write_attr(obj, "D", params.coord_system().D());
    write_attr(obj, "grid_type", params.coord_system().n());
    write_attr(obj, "subdomain_grid_q_min", params.grid_q_min());
    write_attr(obj, "subdomain_grid_q_max", params.grid_q_max());
    write_attr(obj, "subdomain_ptl_q_min", params.ptl_q_min());
    write_attr(obj, "subdomain_ptl_q_max", params.ptl_q_max());
    write_attr(obj, "simulation_grid_q_min", delegate.global_params().grid_q_min());
    write_attr(obj, "simulation_grid_q_max", delegate.global_params().grid_q_max());
    write_attr(obj, "simulation_ptl_q_min", delegate.global_params().ptl_q_min());
    write_attr(obj, "simulation_ptl_q_max", delegate.global_params().ptl_q_max());
    write_attr(obj, "damper.masking_inset", damper.masking_inset());
    write_attr(obj, "damper.amplitude_damping", damper.amplitude_damping());
    write_attr(obj, "damper.phase_retardation", damper.phase_retardation());
    write_attr(obj, "grid_strategy", long{params.grid_strategy()});
    write_attr(obj, "use_metric_table", long{params.use_metric_table()});
    write_attr(obj, "enforce_force_balance", long{params.enforce_force_balance()});
    write_attr(obj, "number_of_worker_threads", long{params.number_of_worker_threads()});
    write_attr(obj, "Orot", params.Orot());
}
template <class H5Object, class Domain>
void MPIC::Recorder::_write_all_species_attr(H5Object &obj, Domain const &domain, Identifier::Any)
{
    UTL::DynamicArray<double> Ocs, ops, Ncs, current_density_conversion_factors, charge_density_conversion_factors, energy_density_conversion_factors;
    UTL::DynamicArray<long> shape_orders, n_smooths;
    for (auto const &sp : domain.species()) {
        Ocs.push_back(sp.Oc0ref());
        ops.push_back(sp.op0ref());
        Ncs.push_back(sp.Nc0ref());
        shape_orders.push_back(sp.shape_order());
        n_smooths.push_back(sp.number_of_smoothing_passes());
        current_density_conversion_factors.push_back(sp.current_density_conversion_factor());
        charge_density_conversion_factors.push_back(sp.charge_density_conversion_factor());
        energy_density_conversion_factors.push_back(sp.energy_density_conversion_factor());
    }
    write_attr(obj, "Ocs", Ocs);
    write_attr(obj, "ops", ops);
    write_attr(obj, "Ncs", Ncs);
    write_attr(obj, "shape_orders", shape_orders);
    write_attr(obj, "number_of_smoothing_passes", n_smooths);
    write_attr(obj, "current_density_conversion_factors", current_density_conversion_factors);
    write_attr(obj, "charge_density_conversion_factors", charge_density_conversion_factors);
    write_attr(obj, "energy_density_conversion_factors", energy_density_conversion_factors);
}
template <class H5Object, class Species>
void MPIC::Recorder::_write_one_species_attr(H5Object &obj, Species const &sp, Identifier::Any)
{
    write_attr(obj, "Oc0", sp.Oc0ref());
    write_attr(obj, "op0", sp.op0ref());
    write_attr(obj, "Nc0", sp.Nc0ref());
    write_attr(obj, "shape_order", sp.shape_order().v);
    write_attr(obj, "number_of_smoothing_passes", sp.number_of_smoothing_passes().v);
    write_attr(obj, "current_density_conversion_factor", sp.current_density_conversion_factor());
    write_attr(obj, "charge_density_conversion_factor", sp.charge_density_conversion_factor());
    write_attr(obj, "energy_density_conversion_factor", sp.energy_density_conversion_factor());
}
template <class H5Object, class Domain>
void MPIC::Recorder::_write_cold_fluid_attr(H5Object &obj, Domain const &domain, Identifier::Any)
{
    UTL::DynamicArray<double> Ocs, ops, current_density_conversion_factors, charge_density_conversion_factors, energy_density_conversion_factors;
    UTL::DynamicArray<long> n_smooths;
    for (auto const &sp : domain.cold_fluids()) {
        Ocs.push_back(sp.Oc0ref());
        ops.push_back(sp.op0ref());
        n_smooths.push_back(sp.number_of_smoothing_passes());
        current_density_conversion_factors.push_back(sp.current_density_conversion_factor());
        charge_density_conversion_factors.push_back(sp.charge_density_conversion_factor());
        energy_density_conversion_factors.push_back(sp.energy_density_conversion_factor());
    }
    write_attr(obj, "Ocs", Ocs);
    write_attr(obj, "ops", ops);
    write_attr(obj, "number_of_smoothing_passes", n_smooths);
    write_attr(obj, "current_density_conversion_factors", current_density_conversion_factors);
    write_attr(obj, "charge_density_conversion_factors", charge_density_conversion_factors);
    write_attr(obj, "energy_density_conversion_factors", energy_density_conversion_factors);
}

#endif /* MPICRecorder_hh */
