//
//  MPICRecorder.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/14/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICRecorder_h
#define MPICRecorder_h

#include "mpic-config.h"

#include <PICKit/PICKit.h>
#include <HDF5Kit/HDF5Kit.h>
#include <UtilityKit/UtilityKit.h>

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    template <long> class PICDelegate;
}

class Recorder {
    unsigned _recording_frequency;

public:
    virtual ~Recorder() = default;

    unsigned const &recording_frequency() const noexcept { return _recording_frequency; }
    bool should_record(long const cur_step) const noexcept { return 0 == cur_step % _recording_frequency; }
    //
    virtual void operator<<(__1_::PICDelegate<1L> const &delegate) = 0;
    virtual void operator<<(__1_::PICDelegate<2L> const &delegate) = 0;
    virtual void operator<<(__1_::PICDelegate<3L> const &delegate) = 0;

protected:
    explicit Recorder(unsigned const recording_frequency) noexcept
    : _recording_frequency{recording_frequency ? recording_frequency : unsigned{1}} {
    }

    template <long S>
    static double const *data(UTL::Vector<double, S> const &v) { return v.data(); }
    template <class T, long Rank, long Pad>
    static double const *data(UTL::ArrayND<T, Rank, Pad> const &a) { return data(*a.data()); }
    //
    template <class H5Object, long ND, long Ver>
    static void write_common_attr(H5Object &obj, __1_::PICDelegate<ND> const &delegate, Identifier::PIC<Ver> id) {
        _write_common_attr(obj, delegate, id);
    }
    template <class H5Object, long ND, long Ver>
    static void write_species_attr(H5Object &obj, PIC::__1_::Domain<ND> const &domain, Identifier::PIC<Ver> id) {
        _write_all_species_attr(obj, domain, id);
    }
    template <class H5Object, long ND, long Ver>
    static void write_species_attr(H5Object &obj, PIC::__1_::Species<ND> const &species, Identifier::PIC<Ver> id) {
        _write_one_species_attr(obj, species, id);
    }
    template <class H5Object, long ND, long Ver>
    static void write_cold_fluid_attr(H5Object &obj, PIC::__1_::Domain<ND> const &domain, Identifier::PIC<Ver> id) {
        _write_cold_fluid_attr(obj, domain, id);
    }
    //
    template <class H5Object, class T>
    inline static void write_attr(H5Object &obj, const char *name, T const &s);
    template <class H5Object, class T, long S>
    inline static void write_attr(H5Object &obj, const char *name, UTL::Vector<T, S> const &v);
    template <class H5Object, class T, long S>
    inline static void write_attr(H5Object &obj, const char *name, UTL::Vector<UTL::Vector<T, S>, S> const &m);
    template <class H5Object, class T>
    inline static void write_attr(H5Object &obj, const char *name, UTL::DynamicArray<T> const &A);

private:
    template <class H5Object, class Delegate>
    inline static void _write_common_attr(H5Object &obj, Delegate const &delegate, Identifier::PIC<1>);
    template <class H5Object, class Domain>
    inline static void _write_all_species_attr(H5Object &obj, Domain const &domain, Identifier::Any);
    template <class H5Object, class Species>
    inline static void _write_one_species_attr(H5Object &obj, Species const &species, Identifier::Any);
    template <class H5Object, class Domain>
    inline static void _write_cold_fluid_attr(H5Object &obj, Domain const &domain, Identifier::Any);
};
MPIC_END_NAMESPACE


// MARK:- Implementation Header
//
#include "MPICRecorder.hh"

#endif /* MPICRecorder_h */
