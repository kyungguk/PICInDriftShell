//
//  MPICVelocityHistogramRecorder.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/25/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICVelocityHistogramRecorder.h"

#include "MPICDelegate.h"
#include <MPIKit/MPIKit.h>
#include <stdexcept>
#include <string>

MPIC::VelocityHistogramRecorder<2L>::VelocityHistogramRecorder(char const *output_directory, Json::Value const json)
: Recorder{json.get("recording_frequency", unsigned{1}).asUInt()}
, _output_directory{output_directory}
, _filename_prefix{json.get("filename_prefix", "vhists").asString()}
, _vhists{} {
    for (Json::Value const &binning : json["binning"]) {
        _vhists[binning["id"].asUInt()]
        = vhist_type({binning["v_min"][0].asDouble(), binning["v_min"][1].asDouble()},
                     {binning["v_max"][0].asDouble(), binning["v_max"][1].asDouble()},
                     {binning["n_bins"][0].asInt(), binning["n_bins"][1].asInt()});
    }
}
//
void MPIC::VelocityHistogramRecorder<2L>::operator<<(__1_::PICDelegate<1L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::VelocityHistogramRecorder<2L>::operator<<(__1_::PICDelegate<2L> const &delegate)
{
    record(delegate, delegate.identifier);
}
void MPIC::VelocityHistogramRecorder<2L>::operator<<(__1_::PICDelegate<3L> const &delegate)
{
    record(delegate, delegate.identifier);
}

template <class Delegate>
void MPIC::VelocityHistogramRecorder<2L>::record(Delegate const &delegate, Identifier::Any)
{
    try {
        long const Ns = delegate.domain().species().size();
        if (delegate.comm().is_master()) { // in master process
            //
            // create hdf5 file
            //
            H5::File root = [](std::string const path) {
                return H5::File{H5::File::Trunc{}, path.c_str()};
            }(_output_directory + "/" + _filename_prefix + "_" + std::to_string(delegate.cur_step()) + ".h5");
            //
            // collect and write histograms
            //
            for (vhist_map_type::value_type &kv : _vhists) {
                if (kv.first >= Ns) continue;
                auto const &vhist_N = collect(delegate, kv, Delegate::identifier);
                write(root, delegate, vhist_N, kv, Delegate::identifier);
            }
            //
            // flush
            //
            root.flush();
        } else { // in non-master processes
            //
            // collect histograms
            //
            for (vhist_map_type::value_type &kv : _vhists) {
                if (kv.first >= Ns) continue;
                collect(delegate, kv, Delegate::identifier);
            }
        }
    } catch (std::exception &e) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

template <class Delegate>
void MPIC::VelocityHistogramRecorder<2L>::write(H5::Group &root, Delegate const &delegate, UTL::DynamicArray<double> const &vhist_N, vhist_map_type::value_type &kv, Identifier::Any) const
{
    H5::Space space; {
        H5::Space::size_array_type const dims = {
            static_cast<unsigned long>(kv.second.nv1bins()),
            static_cast<unsigned long>(kv.second.nv2bins())
        };
        (space = H5::Space{dims}).select_all();
    }

    H5::Dataset vhist; {
        std::string const name = std::string{"vhist_"} + std::to_string(kv.first);
        vhist = root.dataset(name.c_str(), H5::Type::native<double>(), space);
    }
    write_common_attr(vhist, delegate, delegate.identifier);
    write_species_attr(vhist, delegate.domain().species().at(kv.first), delegate.identifier);
    UTL::Vector<double, 2> const vmin = {kv.second.v1min(), kv.second.v2min()};
    write_attr(vhist, "vmin", vmin);
    UTL::Vector<double, 2> const dv = {kv.second.dv1(), kv.second.dv2()};
    write_attr(vhist, "dv", dv);
    vhist.write(space, vhist_N.data(), space);
}

template <class Delegate>
UTL::DynamicArray<double> MPIC::VelocityHistogramRecorder<2L>::collect(Delegate const &delegate, vhist_map_type::value_type &kv, Identifier::Any) const
{
    auto const &domain = delegate.domain();
    auto const &params = domain.params();

    // accumulate local vhist
    //
    kv.second.reset();
    for (auto const &ptl : domain.species()[kv.first].bucket()) {
        auto e = params.coord_system().template mfa_basis<0>(delegate.curvi(ptl.pos, nullptr));
        UTL::Vector<double, 3> const vel{
            UTL::reduce_plus(e.x *= ptl.vel),
            UTL::reduce_plus(e.y *= ptl.vel),
            UTL::reduce_plus(e.z *= ptl.vel)
        };
        kv.second << vel;
    }
    UTL::DynamicArray<double> const vhist_0{kv.second.hist().begin(), kv.second.hist().end()}; // type conversion
    double const N_0 = kv.second.N();

    // reduction to master
    //
    double N{N_0};
    reduce_plus(*delegate.comm(), N);
    if (N < 1) {
        N = 1;
    }
    UTL::DynamicArray<double> vhist_N{vhist_0};
    reduce_plus(*delegate.comm(), vhist_N);
    for (double &x : vhist_N) {
        x /= N;
    }
    return vhist_N;
}

template <class T> typename std::enable_if<std::is_default_constructible<MPK::TypeMap<T>>::value,
void>::type MPIC::VelocityHistogramRecorder<2L>::reduce_plus(MPK::Comm const &comm, T &x)
{
    static MPK::ReduceOp const plus(true, [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *)->void {
        for (int i = 0; i < *vlen; ++i) {
            static_cast<T*>(inoutvec)[i] += static_cast<T const*>(invec)[i];
        }
    });
    comm.reduce(plus, x);
}
template <class T> typename std::enable_if<std::is_default_constructible<MPK::TypeMap<T>>::value,
void>::type MPIC::VelocityHistogramRecorder<2L>::reduce_plus(MPK::Comm const &comm, UTL::Array<T> &A)
{
    static MPK::ReduceOp const plus(true, [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *)->void {
        for (int i = 0; i < *vlen; ++i) {
            static_cast<T*>(inoutvec)[i] += static_cast<T const*>(invec)[i];
        }
    });
    comm.reduce(plus, A);
}
