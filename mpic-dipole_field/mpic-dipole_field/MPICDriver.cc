//
//  MPICDriver.cc
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 2/16/19.
//  Copyright © 2019 kyungguk.com. All rights reserved.
//

#include "MPICDriver.h"

#include <type_traits>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <chrono>
#include <string>
#include <cmath>

// MARK:- MPIC::__1_::PICDriver
//
void MPIC::__1_::PICDriver::operator()() const
{
    long const ND = this->opts["ND"];
    if        (ND == 1) {
        return this->run<1>();
    } else if (ND == 2) {
        return this->run<2>();
    } else if (ND == 3) {
        return this->run<3>();
    }
    return throw std::invalid_argument{std::string{__PRETTY_FUNCTION__} + " - invalid ND: " + std::to_string(ND)};
}

template <long ND>
void MPIC::__1_::PICDriver::run(std::unique_ptr<PICDelegate<ND>> delegate) const
{
    using Delegate = typename std::decay<decltype(delegate)>::type::element_type;
    using Domain = typename Delegate::domain_type;

    if (!this->comm.rank()) {
        println(std::cout, "In ", __PRETTY_FUNCTION__);
        println(std::cout, '\t', __PRETTY_FUNCTION__, " - initialization begin");
    }

    // init
    //
    this->init_delegate(delegate);
    this->comm.barrier();
    if (delegate->comm().is_master()) {
        println(std::cout, '\t', __PRETTY_FUNCTION__, " - initialization done");
    }

    // main loop
    //
    auto const time_stamp = std::chrono::steady_clock::now();
    delegate->parallel_loop([this](Domain *domain, bool const is_master_thread, Delegate* delegate, char const* pretty_function) {
        unsigned const inner_steps = this->opts["inner_steps"];
        unsigned const outer_steps = this->opts["outer_steps"];
        for (unsigned i_step = 1; i_step <= outer_steps; ++i_step) {
            if (is_master_thread && delegate->comm().is_master()) {
                println(std::cout,
                        '\t', pretty_function, '\n',
                        "\t\t", " - steps(x", inner_steps, ") = ", i_step, '/', outer_steps,
                        "; time = ", delegate->time());
            }
            // advance
            //
            delegate->advance_by(domain, inner_steps);

            // recording
            //
            if (is_master_thread) {
                for (auto const &pair : this->recorders) {
                    if (pair.second->should_record(delegate->cur_step())) {
                        *pair.second << *delegate;
                    }
                }
            }
        }
    }, delegate.get(), __PRETTY_FUNCTION__);
    std::chrono::duration<double> const elapsed = decltype(time_stamp)::clock::now() - time_stamp;

    // snapshot
    //
    if (this->snapshot->should_save()) {
        *this->snapshot << *delegate;
    }

    if (delegate->comm().is_master()) {
        println(std::cout, "Out ", __PRETTY_FUNCTION__, " - elapsed = ", elapsed.count(), 's');
    }
}

template <long ND>
void MPIC::__1_::PICDriver::init_delegate(std::unique_ptr<PICDelegate<ND>> &delegate) const
{
    using Delegate = typename std::decay<decltype(delegate)>::type::element_type;

    // init delegate
    //
    {
        PIC::ParameterSet<Delegate::ND> params;
        PIC::WaveDamper<Delegate::ND> damper;
        init_global(params, damper);
        //
        typename Delegate::size_vector_type domain_decomposition;
        for (unsigned i = 0; i < domain_decomposition.size(); ++i) {
            domain_decomposition.at(i) = this->opts["domain_decomposition"][i];
        }
        typename Delegate::boundary_type bc;
        if (this->opts["boundary_condition"].isArray()) {
            for (unsigned i = 0; i < bc.size(); ++i) {
                std::string const name = this->opts["boundary_condition"][i];
                if (name == "periodic") {
                    bc.at(i) = Delegate::periodic;
                } else if (name == "reflective") {
                    bc.at(i) = Delegate::reflective;
                } else {
                    throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - unknown boundary identifier: \"" + name + "\"");
                }
            }
        } else {
            std::string const name = this->opts["boundary_condition"];
            if (name == "periodic") {
                bc.fill(Delegate::periodic);
            } else if (name == "reflective") {
                bc.fill(Delegate::reflective);
            } else {
                throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - unknown boundary identifier: \"" + name + "\"");
            }
        }
        delegate.reset(new Delegate{this->comm.duplicated(), domain_decomposition, params, damper, bc});
        //
        if (this->opts.isMember("debug")) {
            JsonProxy const json_proxy = this->opts["debug"];
            DebuggingOptions debug; {
                debug.zero_out_electromagnetic_waves = json_proxy["zero_out_electromagnetic_waves"];
            }
            delegate->set_debugging_options(debug);
        }
    }

    // init domain
    //
    {
        double const dt = this->opts["dt"];
        bool const use_metric_table = this->opts["use_metric_table"];
        delegate->init_domain(dt, use_metric_table);
    }

    // init source
    //
    if (this->opts.isMember("sources")) {
        JsonProxy const js_srcs = this->opts["sources"];
        if (js_srcs.isArray()) {
            for (JsonProxy const js_src : js_srcs.json()) {
                init_external_source(*delegate, js_src);
            }
        } else {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - \"sources\" option should be an array of objects");
        }
    }

    // load snapshot and return
    //
    if (this->snapshot->should_load()) {
        *this->snapshot >> *delegate;
        if (delegate->comm().is_master()) {
            println(std::cout, "\t\t", __PRETTY_FUNCTION__, " - snapshot loaded");
        }
        return;
    }

    // otherwise, init species
    //
    if (this->opts.isMember("particles")) {
        init_particles(*delegate, this->opts["particles"]);
    }

    // and init cold fluid
    //
    if (this->opts.isMember("cold_fluids")) {
        init_cold_fluids(*delegate, this->opts["cold_fluids"]);
    }
}
template <long ND>
void MPIC::__1_::PICDriver::init_global(PIC::ParameterSet<ND> &params, PIC::WaveDamper<ND> &damper) const
{
    using size_vector_type = typename PIC::ParameterSet<ND>::size_vector_type;
    using position_type = typename PIC::ParameterSet<ND>::position_type;
    //
    size_vector_type N, damping_inset;
    position_type D, amplitude_damping, phase_retardation;
    for (unsigned i = 0; i < N.size(); ++i) {
        D.at(i) = this->opts["D"][i];
        N.at(i) = this->opts["N"][i];
        damping_inset.at(i) = this->opts["damping_inset"][i];
        amplitude_damping.at(i) = this->opts["amplitude_damping"][i];
        phase_retardation.at(i) = this->opts["phase_retardation"][i];
    }
    double const r0ref = this->opts["r0ref"];
    double const grid_type = this->opts["grid_type"];
    if (std::abs(grid_type - 3) < 1e-10) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - grid_type cannot be 3: \"" + std::to_string(grid_type) + "\"");
    }
    PIC::CoordSystem const cs{r0ref, D, grid_type};
    double const c = this->opts["c"];
    double const O0ref = this->opts["O0"];
    typename PIC::ParameterSet<ND>::OptionSet options; {
        options.number_of_worker_threads = this->opts.get("number_of_worker_threads", unsigned{0});
        options.enforce_force_balance = this->opts.get("enforce_force_balance", false);
        options.Orot = this->opts.get("Orot", double{0});
    }
    position_type const q_min = -0.5*position_type{N - 1L};
    params = PIC::ParameterSet<ND>{cs, q_min, N, c, O0ref, options};
    damper = PIC::WaveDamper<ND>{damping_inset, amplitude_damping, phase_retardation};
}

template <long ND>
void MPIC::__1_::PICDriver::init_external_source(PICDelegate<ND> &delegate, JsonProxy const &js_src) const
{
    using Delegate [[maybe_unused]] = typename std::decay<decltype(delegate)>::type;
    auto const &params = delegate.domain().params();
    //
    typename PIC::Source<ND>::Descriptor desc; {
        desc.omega = js_src["omega"];
        desc.start = js_src["start"];
        desc.duration = js_src["duration"];
        desc.ease_in = js_src["ease_in"];
    }
    //
    typename PIC::CoordSystem::vector_type Jreal{}, Jimag{}; {
        Jreal.x = js_src["J1"][0U];
        Jimag.x = js_src["J1"][1U];
        //
        Jreal.y = js_src["J2"][0U];
        Jimag.y = js_src["J2"][1U];
        //
        Jreal.z = js_src["J3"][0U];
        Jimag.z = js_src["J3"][1U];
    };
    //
    if (js_src.isMember("locations")) { // same source at several locations
        JsonProxy const js_locs = js_src["locations"];
        if (js_locs.isArray()) {
            for (JsonProxy const loc : js_locs.json()) {
                if (loc.isArray()) {
                    for (unsigned i = 0; i < desc.location.size(); ++i) {
                        desc.location.at(i) = loc[i];
                    }
                } else {
                    throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - elements of \"sources.locations\" option should be an array of length at least ND");
                }
                auto const curvi = delegate.curvi(desc.location, nullptr);
                params.coord_system().is_valid(curvi);
                typename PIC::CoordSystem::tensor_type const e = params.coord_system().template mfa_basis<0>(curvi);
                typename PIC::CoordSystem::vector_type const Jr = Jreal.x*e.x + Jreal.y*e.y + Jreal.z*e.z;
                typename PIC::CoordSystem::vector_type const Ji = Jimag.x*e.x + Jimag.y*e.y + Jimag.z*e.z;
                for (int i = 0; i < desc.J0carts.size(); ++i) {
                    desc.J0carts.at(i).real(Jr.at(i));
                    desc.J0carts.at(i).imag(Ji.at(i));
                }
                //
                delegate.source().add(desc);
            }
        } else {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - \"sources.locations\" option should be an array of arrays");
        }
    } else { // source at a single location
        JsonProxy const loc = js_src["location"];
        for (unsigned i = 0; i < desc.location.size(); ++i) {
            desc.location.at(i) = loc[i];
        }
        auto const curvi = delegate.curvi(desc.location, nullptr);
        params.coord_system().is_valid(curvi);
        typename PIC::CoordSystem::tensor_type const e = params.coord_system().template mfa_basis<0>(curvi);
        typename PIC::CoordSystem::vector_type const Jr = Jreal.x*e.x + Jreal.y*e.y + Jreal.z*e.z;
        typename PIC::CoordSystem::vector_type const Ji = Jimag.x*e.x + Jimag.y*e.y + Jimag.z*e.z;
        for (int i = 0; i < desc.J0carts.size(); ++i) {
            desc.J0carts.at(i).real(Jr.at(i));
            desc.J0carts.at(i).imag(Ji.at(i));
        }
        //
        delegate.source().add(desc);
    }
}
template <long ND>
void MPIC::__1_::PICDriver::init_particles(PICDelegate<ND> &delegate, JsonProxy const &js_ptls) const
{
    using Delegate = typename std::decay<decltype(delegate)>::type;
    using species_type [[maybe_unused]] = typename Delegate::species_type;
    using GEO::__1_::BiMaxwellianLoader;
    using GEO::__1_::PartialShellLoader;
    //
    if (js_ptls.isArray()) {
        long const Ns = js_ptls.size();
        for (unsigned i = 0; i < Ns; ++i) {
            JsonProxy const js_sp = js_ptls[i];
            if (js_sp.isNull()) {
                continue;
            }
            if (delegate.comm().is_master()) {
                println(std::cout, "\t\t", __PRETTY_FUNCTION__, " - initializing ", i, "th species");
            }
            //
            // retrieve parameters
            //
            double const Oc = js_sp["Oc"];
            double const op = js_sp["op"];
            unsigned const Nc = js_sp["Nc"];
            unsigned const shape_order = js_sp.get("shape_order", this->opts.get("shape_order", unsigned{1}));
            unsigned const n_smooth = js_sp.get("number_of_smoothing_passes", this->opts.get("number_of_smoothing_passes", unsigned{2}));
            //
            // velocity distribution
            //
            std::unique_ptr<PIC::ParticleLoader<ND>> vdist;
            {
                auto const &params = delegate.global_params();
                JsonProxy const &js_vdist = js_sp["vdist"];
                std::string const vd_type = js_vdist["type"];
                if (vd_type == "bi_maxwellian") {
                    double const beta1 = js_vdist["beta"];
                    double const _T2OT1 = js_vdist.get("T2/T1", 1.0), A0 = _T2OT1 - 1;
                    typename BiMaxwellianLoader<ND>::param_type bimax; {
                        bimax.vdf = typename BiMaxwellianLoader<ND>::vdf_type{std::sqrt(beta1)*params.c() * std::abs(Oc)/op, A0, Delegate::ND == 1};
                    }
                    vdist.reset(new BiMaxwellianLoader<ND>{params.coord_system(), params.grid_q_min(), params.N(), std::move(bimax)});
                } else if (vd_type == "partial_shell") {
                    double const beta = js_vdist["beta"];
                    double const vs = js_vdist.get("shell_speed", 0.0);
                    double const zeta = js_vdist.get("sine_index", 0.0);
                    typename PartialShellLoader<ND>::param_type shell; {
                        shell.vdf = typename PartialShellLoader<ND>::vdf_type{std::sqrt(beta)*params.c() * std::abs(Oc)/op, vs, zeta, Delegate::ND == 1};
                    }
                    vdist.reset(new PartialShellLoader<ND>{params.coord_system(), params.grid_q_min(), params.N(), std::move(shell)});
                } else {
                    throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - not supported velocity distribution: " + vd_type);
                }
            }
            //
            // load particles
            //
            for (auto &domain : delegate.domains()) {
                domain.add_species().load(Oc, op, Nc, PIC::ShapeOrder{shape_order}, PIC::NSmooth{n_smooth}, *vdist);
            }
        }
    } else {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - \"particles\" option should be an array of objects");
    }
}
template <long ND>
void MPIC::__1_::PICDriver::init_cold_fluids(PICDelegate<ND> &delegate, JsonProxy const &js_fluids) const
{
    using Delegate = typename std::decay<decltype(delegate)>::type;
    using cold_fluid_type [[maybe_unused]] = typename Delegate::cold_fluid_type;
    //
    if (js_fluids.isArray()) {
        long const Ns = js_fluids.size();
        for (unsigned i = 0; i < Ns; ++i) {
            JsonProxy const js_cf = js_fluids[i];
            if (js_cf.isNull()) {
                continue;
            }
            if (delegate.comm().is_master()) {
                println(std::cout, "\t\t", __PRETTY_FUNCTION__, " - initializing ", i, "th cold fluid");
            }
            //
            // retrieve parameters
            //
            double const Oc = js_cf["Oc"];
            double const op = js_cf["op"];
            unsigned const n_smooth = js_cf.get("number_of_smoothing_passes", this->opts.get("number_of_smoothing_passes", unsigned{2}));
            //
            // load cold fluid
            //
            for (auto &domain : delegate.domains()) {
                domain.add_cold_fluid().load(Oc, op, PIC::NSmooth{n_smooth});
            }
        }
    } else {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - \"cold_fluids\" option should be an array of objects");
    }
}
