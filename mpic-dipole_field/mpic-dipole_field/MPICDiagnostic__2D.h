//
//  MPICDiagnostic__2D.h
//  mpic-dipole_field
//
//  Created by KYUNGGUK MIN on 11/18/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICDiagnostic__2D_h
#define MPICDiagnostic__2D_h

#include "MPICDiagnostic__ND.h"

MPIC_BEGIN_NAMESPACE
namespace __1_ {
    // MARK:- MPIC::__1_::Diagnostic<ND>
    //
    template <>
    struct Diagnostic<2L> : public _Diagnostic<2L> {
        explicit Diagnostic(long const Nspecies);

        void update(MPK::Comm const &comm, PIC::Domain<ND> const &domain);
    private:
        template <long Pad1>
        static vector_type _accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<vector_type, ND, Pad1> const &A);
        template <long Pad0, long Pad1>
        static vector_type _accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<scalar_type, ND, Pad0> const &n, UTL::ArrayND<vector_type, ND, Pad1> const &nV);
        template <long Pad2>
        static vector_type _accumulate(PIC::CoordSystem const &cs, position_type const &q_min, UTL::ArrayND<compact_tensor_type, ND, Pad2> const &nvv);
    };
} // namespace __1_
MPIC_END_NAMESPACE

#endif /* MPICDiagnostic__2D_h */
